var timeoutID;
var email_flag = true; // for email unique validation 

function ActiveMenu(className){
    if(className == undefined || className == ""){
        console.log('Enter Class Name to active this menu');
        return false;
    }
    
    if($(".page-sidebar.navbar-collapse ."+className).length > 0){

        var mainMenu = $(".page-sidebar.navbar-collapse ."+className);
        var subMenu= $(".page-sidebar.navbar-collapse ."+className).parent('li').parent('ul.sub-menu');
        mainMenu.addClass('active open').siblings().removeClass('active open');
        if(subMenu.length > 0){
            subMenu.parent('li').addClass('active open').siblings().removeClass('active');
            mainMenu.parent('li').addClass('active open').siblings().removeClass('active');
            subMenu.parent('li').find('span.arrow').addClass('open').siblings().removeClass('open');
            
        }else{
            mainMenu.parent('li').addClass('active open').siblings().removeClass('active open');   
        }
    }else{
        console.log('Class Name is not valid');
    }
    
}
var tour = new Tour({
    storage: false,
    onShown: function (tour) {
        $('.popover').css('top',parseInt($('.popover').css('top')) - 27 + 'px');
        var step = tour._options.steps[tour.getCurrentStep()];
        var element=$(step.element);
        element.parent().find('.form-control').focus();
    },
});
tour.addStep({
    element: "#shortName_error",
    title: "ShortName",
    content: "Add content of shortName",
    container: "body",


});

tour.addStep({
    element: "#eng_error",
    title: "english",
    content: "Add content of English",
    container: "body",
});

tour.addStep({
    element: "#swe_error",
    title: "Swedish",
    content: "Add content of swedish",
    container: "body",

});
tour.addStep({
    element: "#fra_error",
    title: "Franch",
    content: "Add content of French",
    container: "body",

});
tour.addStep({
    element: "#ger_error",
    title: "German",
    content: "Add content of German",
    container: "body",

});
tour.addStep({
    element: "#esp_error",
    title: "Spanish",
    content: "Add content of Spanish",
    container: "body",
});
tour.addStep({
    element: "#type_error",
    title: "Spanish",
    content: "Add content of Type",
    container: "body",
});
tour.addStep({
    element: "#tags_error",
    title: "Spanish",
    content: "Add content of Tag",
    container: "body",

});
tour.addStep({
    element: ".button-next",
    title: "Press entre for Tranlsation",
    content: "Press Save button",
    container: "body",
    backdrop: true,
});
Tour.prototype._focus = function($tip, $element, end) {
    var $next, role;
    role = end ? 'end' : 'next';
    $next = $tip.find("[data-role='" + role + "']");
    return $element.on('shown.bs.popover', function() {
        return $next;
    });
};
var invalidStep = -1;
function validateStepInput(tour, inputSelector) {
    var step = tour._options.steps[tour.getCurrentStep()];

    var $attachedEl = typeof inputSelector == 'undefined' ? $(step.element) : $(inputSelector);

    if ($attachedEl.length > 0 && $attachedEl.is('input')) {

        if ($attachedEl.attr('type') == 'text') {
            var val = $attachedEl.val();
            if (val.length == 0) {
                invalidStep = tour.getCurrentStep();
            }
        } else if ($attachedEl.attr('type') == 'radio') {
            var anyChecked = $attachedEl.is(':checked');
            if (!anyChecked) {
                invalidStep = tour.getCurrentStep();
            }
        } else if ($attachedEl.attr('type') == 'checkbox') {
            var anyChecked = $attachedEl.is(':checked');
            if (!anyChecked) {
                invalidStep = tour.getCurrentStep();
            }
        }
    }

    return invalidStep == -1;
}

function checkPreviousStepValid(tour) {
    // .goTo only seems to work in the onShown step event so I had to put this check
    // on the next step's onShown event in order to redisplay the previous step with
    // the error
    if (invalidStep > -1) {
        var tempStep = invalidStep;
        invalidStep = -1;
        tour.goTo(tempStep);
    }
}
$(document).ready(function() {

    $("body").on('click','#categoryEditt',function(){
        var form = $("#categoryEdit");
        form.find('.duplicate-error').hide();
        form.find(".form-group").removeClass('has-error');
        if(form_valid("#categoryEdit")) {
            $('.required_captcha').html("");
            var curObj = $(this);
            curObj.find('button[type=submit]').attr('disabled', 'disabled');
            var send_data = $("#categoryEdit").serialize();            
            $.post($("#categoryEdit").attr("action"), send_data, function(data) {
                curObj.find('button[type=submit]').removeAttr('disabled');
                if($.trim(data) == 'TRUE') 
                {
                    $('#v_logo_val').val('');
                    $('#basic').hide();
                    $('#error_name').hide();
                    $('#datatable_ajax').DataTable().ajax.reload();
                    $('.alert-success').fadeOut(5000);
                    $('#error_v_email').hide();
                    $('.form-group').removeClass('has-error');
                    form.find('.alert-success').show();
                    form.find('.alert-danger').hide();
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    jQuery('#g-recaptcha-response').val('');
                    
                    //loadData();
                    
                } else if($.trim(data) == 'category-exist'){
                        $('#error_name').show();
                    }
                    else if($.trim(data) == 'exist')
                {
                    $('.company-add-error').show();
                    $('#error_v_email').show();
                    $('.duplicate-error').show();
                    return false;
                } else if($.trim(data) == 'topicexist')
                {
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    $('.alert-danger').show();
                    $('.alert-danger .message').html('This topic have quiz.');
                    setTimeout(function(){ $('.alert-danger').fadeOut(4000); },3000);
                    return false;
                }
                else if($.trim(data) == 'TRUE_SETUP')
                {
                    $('.company-add-error').hide();
                    $('.company-add-success').show();
                    $('#v_logo_val').val('');
                    $('#frmAdd')[0].reset();
                    $('#basic').hide();
                    loadData();
                    return false;
                }
                 else if($.trim(data) == 'FALSE')
                {
                    $('#d_error').show();
                    return false;
                }
                if($.trim(data) == '') {
                    $('#d_error').hide();
                    $('#error_name').hide();
                    form.find('.alert-success').fadeOut(5000);
                    form.find('.alert-danger').hide();
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    if(save_and_continue_flag) {
                        save_and_continue_flag = false;
                        window.location.href = window.location.href; 
                    } else {
                        save_and_continue_flag = false;
                        window.location.href = (window.location.href).replace('/add','');
                    }
                } else {                    
                    if($.trim(data) == 'DOMAIN_ERROR') {
                        var same_domain = false;
                        bootbox.confirm('Email id with same domain name is already exists.', 
                        function (confirmed) {
                            if(confirmed) {
                                same_domain = true;    
                                send_data = send_data+'&same_domain='+same_domain;
                                $.post($("#frmAdd").attr("action"), send_data, function(data) {
                                    if($.trim(data) == '') {
                                        form.find('.alert-danger').hide();
                                        form.find('.duplicate-error').hide();
                                        $("html, body").animate({ scrollTop: 0 }, 600);
                                        if(save_and_continue_flag) {
                                            save_and_continue_flag = false;
                                            window.location.href = window.location.href; 
                                        } else {
                                            save_and_continue_flag = false;
                                            window.location.href = (window.location.href).replace('/add',''); 
                                        }
                                    }               
                                });
                            }
                        });
                        return false;
                    }
                    
                    form.find('.alert-success').hide();
                    form.find('.alert-danger').show();
                    $(data).each(function(i,val){
                        $.each(val,function(key,v){
                              $('#'+key).closest('.form-group').addClass('has-error');
                              $('#error_'+key).show();
                        });
                    });
                    
                    if($('.has-error .form-control').length > 0) {
                        $('html, body').animate({
                            scrollTop:$('.has-error .form-control').first().offset().top - 200
                        }, 1000);
                        $('.has-error .form-control').first().focus()
                    }
                }
            });            
        }
        else
        {
            //tour.init();
            //tour.start();
            save_and_continue_flag = false;
        }
        return false;
    });

    $(document).ajaxError(function(event, request, settings) {
        if (request.responseText === 'Unauthorized.') {
           window.location = SITE_URL;
        }
    });
   // setTimeout(function(){ $(".alert-success").fadeOut(3000); },4000);
    // $(".alert-success").hide();
    if($( window ).height() > 900) {  
        $("#main-content").attr('style',"min-height: "+(eval($( window ).height())-95)+"px"); 
    }
    if($("#sidebar").height() >  $("#main-content").height()) {
        $("#main-content").attr('style',"min-height: "+($("#sidebar").height()+50)+"px");
    }
    
    $(document).on('click',"input[type=checkbox]",function() {
        if($(this).is(':checked'))  {
            $(this).parent().addClass('checked');
            $(this).attr('checked', 'checked');  
        } else {    
           $(this).removeAttr('checked');
           $(this).parent().removeClass('checked');
        }
    });
    handleUniform();
    $(document).on('change','#import-excel-file',function(){
        $('#import-excel').trigger('submit');
    });
    $("#uniform-drop-remove-all").click(function() {
        var flag = false;
        if($(this).children().children().attr('class') == "checked") {
            flag = true;
        } else {
            flag = false;
        }        
        $(".all-checkbox").each(function() {
            var id = this.id;
            if(flag) {
                $("#"+id).children().children().addClass('checked');
            } else {
                $("#"+id).children().children().removeClass('checked');
            }
        });
    });
    
    $("#btn_showall").click(function(){
        if($(this).hasClass('faq-list'))
        {
            window.location = ADMIN_URL+"faqs";
        }
        else
        {
            window.location = window.location.href;
        }
		
	});
    
    $('#widget-body-inner-content').on('click','.drag_raw',function(){
        rtn=confirm("Are you sure you want to change the order?");
        if(rtn==false)
        {   
                return false;
        }
        else
        {
            var qry_str = $('#ajax_div').text();
            var edit_url = $(this).attr('rel');
            $.ajax({
                type: 'post',
                url: edit_url,
                data: qry_str,
                success: function(data)
                {
                    if($("#last-loaded-page").length > 0) {                        
                        loadPiece($("#last-loaded-page").val());                        
                    }
                    $('#drag_raw').hide();
                    return false;
                },
            });
        }
   });
   
    $(".widget").on("click",".btn_view",function () 
    {
        curId = $(this).attr('rel');
        if(curId != '') { 
            $.get($("#view_url").val()+curId, function (data) {
                $("#widget-view-inner-content").html(data);
                var current_div = "#image_table";
                var loader_div = "#load_image";
                var current_div_gallery = ".image_table_gallery";
                var loader_div_gallery = ".load_image_gallery";
                var poster_current_div = "#poster_table";
                var poster_loader_div = "#load_poster";
                $(current_div).hide();
                $(loader_div).show();
                $(current_div_gallery).hide();
                $(loader_div_gallery).show();
                $(poster_current_div).hide();
                $(poster_loader_div).show();
                
                $('#widget-view-inner-content').addClass('view-record');
                $('#widget-view-inner-content').removeClass('edit-record');
                
                $('#widget-body-inner-content').fadeOut(500,function() 
                {
                    $('#widget-view-inner-content').fadeIn(500); 
                    if($(".view-image").length > 0 && $(".view-image").html() != "" && $(".view-image").html() != undefined) 
                    {
                        setTimeout(function() 
                        {
                            $(loader_div).hide();
                            $(current_div).show();
                            $(loader_div_gallery).hide();
                            $(current_div_gallery).show();
                            $(poster_loader_div).hide();
                            $(poster_current_div).show();
                            $(".parent-div-img-container").each(function() { set_size(".div-img-container"); }); 
                        },1500);
                    }
                    
                    prevLink = nextLink = '';
                    curRow = "#ApplicantRow-"+curId; 
                    if($(curRow).prev('tr').attr('id') !== undefined) {
                        prevId = ($(curRow).prev('tr').attr('id')).replace('ApplicantRow-',''); 
                        prevLink = '<a href="javascript:void(0);" rel="'+prevId+'" class="view_prev btn_view"><i class="icon-arrow-left"></i>Previous</a>';
                    } else {
                        prevId = ($(curRow).parent('tbody').find('tr:last').attr('id')).replace('ApplicantRow-',''); 
                        //prevLink = '<a href="javascript:void(0);" rel="'+prevId+'" class="view_prev btn_view">Previous</a>';
                    }
                    if($(curRow).next('tr').attr('id') !== undefined) {
                        nextId = ($(curRow).next('tr').attr('id')).replace('ApplicantRow-',''); 
                        nextLink = '<a href="javascript:void(0);" rel="'+nextId+'" class="view_next btn_view">Next<i class="icon-arrow-right"></i></a>';
                    } else {
                        nextId = ($(curRow).parent('tbody').find('tr:first').attr('id')).replace('ApplicantRow-',''); 
                        //nextLink = '<a href="javascript:void(0);" rel="'+nextId+'" class="view_next btn_view">Next</a>';
                    }
                    addPrevNextContent='<table class="prev-next-link-table"><tr><td>'+prevLink+'</td><td>'+nextLink+'</td></tr></table>';
                    $("#widget-view-inner-content").find('.table-borderless').before(addPrevNextContent); 
                });            
            });            
        }
    });
  
   /* $(document).on("click",".btn_edit",function () {
        arrId = $(this).attr('rel');
        if(arrId != '') {
            
            var tmpurl = rtrim(window.location.href,'/').replace("index/driver","");
            tmpurl = tmpurl.replace("index/user","");
            edit_url = rtrim(tmpurl,'/') + "/edit/";
            $.get(edit_url+arrId+"/"+(new Date()).getTime(), function (data) {
                $('#widget-body-inner-content').fadeOut(500,function() {
                    $('#widget-view-inner-content').fadeIn(500);
                });
                $("#widget-view-inner-content").html(data);
                $('#widget-view-inner-content').addClass('edit-record');
                $('#widget-view-inner-content').removeClass('view-record');
                if($("#single_upload_img").length > 0 ||  $("#single_upload_img1").length > 0 || $("#multi_upload_img").length > 0 || $("#multi_upload_img1").length > 0 ||$("#multi_upload_file").length > 0) { 
                    $.getScript( ASSET_URL+'js/admin/upload_script.js', function() { });                    
                }
                if($("#CkEditor").length > 0) { CKEDITOR.replace('CkEditor'); }
                if($("#CkEditor2").length > 0) { CKEDITOR.replace('CkEditor2'); }
                if($("#vCkEditor").length > 0) 
                { 
                    CKEDITOR.replace( 'vCkEditor', {
                    	toolbar: [
                    		{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline' ] }
                    	]
                   }); 
                }
            });
        }        
    });*/
    
    $(document).on("click","#delete_record",function () {
        var url = $(this).attr('delete-url');
        var arrId = $(this).attr('rel');
        var el = $(this);
        if(arrId != '') {
            bootbox.confirm('Are you sure you want to delete this record?', function (confirmed) {
                if(confirmed){                                  
                    $.get(url, function (data){
                        if($.trim(data) == "TRUE") {
                            $('#success-msg').show();
                            $('#success-msg .message').html('Record deleted successfully.');
                            
                            el.closest('tr').fadeOut(1500, function() {
        						$(this).closest('tr').remove();
                                $('#datatable_ajax').DataTable().ajax.reload();
                                setTimeout(function(){
                                    $('.d_back_ordered_date').datepicker({
                                        format: 'mm/dd/yyyy',
                                        todayHighlight:true,
                                        autoclose: true
                                    });   
                                    $('.d_vendor_expected_delivery_date').datepicker({
                                        format: 'mm/dd/yyyy',
                                        todayHighlight:true,
                                        autoclose: true
                                    });   
                                },1000);
                              /*  if($("#datatable_ajax tbody > tr").length <= 1) {
                                    $(".filter-submit").trigger( "click" );
                                    $('#datatable_ajax').DataTable().ajax.reload();
                                }*/
        					});    
                                                
                            setTimeout(function(){ $('#success-msg').fadeOut(1000); },3000);                        
                        }
                    });
                }
            }); 
        }      
    });
    $(document).on("click","#change_status",function () {
        var url = $(this).attr('change-url');
        var id = $(this).attr('data-id');
        var value = $(this).attr('rel');
        var el = $(this);
        if(id != '') {
            if(value == 'Active'){
               var message = 'inactive'; 
               var change_val = 0; 
            } else if(value == 'Inactive'){
               var message = 'active'; 
               var change_val = 1; 
            }
            bootbox.confirm("Are you sure that you want to "+message+" this record?", function (confirmed) {
                if(confirmed){   
                    $.post(url, { id :id, status:change_val},function(result){
                        if(result == 'TRUE'){
                            $('#datatable_ajax').DataTable().ajax.reload();
                        }
                    });
                }
            }); 
        }      
    });
    
    $(document).on("click","#cancelled_record",function () {
        var url = $(this).attr('cancelled-url');
        var arrId = $(this).attr('rel');
        var el = $(this);
        if(arrId != '') {
            bootbox.confirm('Are you sure you want to Cancelled this record?', function (confirmed) {
                if(confirmed){                                  
                    $.get(url, function (data){
                        if(data.status == 'TRUE') {
                            $('.alert-success').show();
                            $('.alert-success .message').html('Record Cancelled successfully.');
                            
                            el.closest('tr').fadeOut(1500, function() {
        						$(this).closest('tr').remove();
                                $('#datatable_ajax').DataTable().ajax.reload();
        					});    
                                                
                            setTimeout(function(){ $('.alert-success').fadeOut(4000); },3000);                        
                        }
                    });
                }
            }); 
        }      
    });
    $(document).on('click','.down_order', function(){
        var topicId = $(this).attr('rel');
        var order_no = $(this).attr('rel1');
        $.ajax({
            url:  ADMIN_URL+'topic/down-order',
            data: {'topic_id':topicId, 'order_no':order_no},
            type: 'post',
            success: function(result){
                result = $.trim(result);
                if(result == 'TRUE')
                {
                  $('#datatable_ajax').DataTable().ajax.reload();
                } 
                return false;
            }
        });
    }); 
    $(document).on('click','.up_order', function(){
        var topicId = $(this).attr('rel');
        var order_no = $(this).attr('rel1');
        $.ajax({
            url:  ADMIN_URL+'topic/up-order',
            data: {'topic_id':topicId, 'order_no':order_no},
            type: 'post',
            success: function(result){
                result = $.trim(result);
                if(result == 'TRUE')
                {
                  $('#datatable_ajax').DataTable().ajax.reload();
                } 
                return false;
            }
        });
    });
    
    $(document).on('click','#save_status', function(){
        var curr_obj = $(this);
        var id_data = $('#save_chng').val();
        var credit_status = $('#e_credit_status_modal').val();
        var credit_date = $('#d_credited_date_modal').val();
          $.post(ADMIN_URL+"bank_deposit/update-status", { id :id_data, credit_status:credit_status, credit_date:credit_date },function(result){
                if(result == 'TRUE'){
                     $('.status_'+id_data).html(credit_status);
                     $('.statusbtn_'+id_data).remove();
                     $('#update_status_modal').modal('toggle');
                    $('#datatable_ajax').DataTable().ajax.reload();
                }
          });
    }); 
    
    $(document).on('click','#prepaid_save_status', function(){
        var curr_obj = $(this);
        var id_data = $('#save_chng').val();
        var credit_status = $('#e_credit_status_modal').val();
        var credit_date = $('#d_credited_date_modal').val();
          $.post(ADMIN_URL+"prepaid_service_deposit/update-status", { id :id_data, credit_status:credit_status, credit_date:credit_date },function(result){
                if(result == 'TRUE'){
                     $('.status_'+id_data).html(credit_status);
                     $('.statusbtn_'+id_data).remove();
                     $('#update_status_modal').modal('toggle');
                    $('#datatable_ajax').DataTable().ajax.reload();
                }
          });
    });
    var import_timout;
    $(document).on('click','#import_button', function(event){
        if(form_valid("#import_data_form")){
            $("#import_button").attr("disabled", true);
            setTimeout(import_timout);
            import_timout = setTimeout(function() {
                var id_data = $('#client_id').val();
                $.post(ADMIN_URL+"order/import-to-excel-order", { id :id_data},function(result){
                    if(result == 'TRUE'){
                        $('#client_id').val('');
                        $('#form_modal11').modal('toggle');
                        location.reload();
                        $("#import_button").attr("disabled", false);
                    } else {
                        $("#import_button").attr("disabled", false);
                    }
                });
            }, 300);
        }
    });
    $(document).on('click','#import_client', function(){
        $("#import_client").attr("disabled", true);
        setTimeout(import_timout);
        import_timout = setTimeout(function() {
            $.post(ADMIN_URL+"order/import-to-excel-order",function(result){
                if(result == 'TRUE'){
                    location.reload();
                    $("#import_client").attr("disabled", false);
                    //window.location.href = window.location.href;
                } else {
                    $("#import_client").attr("disabled", false);
                }
            });
        }, 300);
       
    });
    $(document).on('click','#import_client_purchase', function(){
        $("#import_client_purchase").attr("disabled", true);
        setTimeout(import_timout);
        import_timout = setTimeout(function() {
            $.post(ADMIN_URL+"purchase_order/import-to-excel-purchase-order",function(result){
                if(result == 'TRUE'){
                    location.reload();
                    $("#import_client_purchase").attr("disabled", false);
                    //window.location.href = window.location.href;
                } else {
                    $("#import_client_purchase").attr("disabled", false);
                }
            });
        }, 300);
       
    });
    $(document).on('click','#import_purchase_order', function(event){        
        // bootbox.confirm('Are you sure want to import purchase order data feed?', function (confirmed) {
        //     if(confirmed) {
        //         $("#import_purchase_order").attr("disabled", true);
        //         setTimeout(import_timout);
        //         import_timout = setTimeout(function() {                    
        //             $.post(ADMIN_URL+"purchase_order/import-to-excel-purchase-order",function(result){
        //                 if(result == 'TRUE'){
        //                     $('#client_id').val('');
        //                     // $('#form_purchase_order_modal').modal('toggle');
        //                     location.reload();
        //                     $("#import_purchase_order").attr("disabled", false);
        //                 } else {
        //                     $("#import_purchase_order").attr("disabled", false);
        //                 }
        //             });
        //         }, 300);
        //     }
        // });
        if(form_valid("#import_purchase_order_form")){
            $("#import_purchase_order").attr("disabled", true);
            setTimeout(import_timout);
            import_timout = setTimeout(function() {
                var id_data = $('#client_id').val();
                $.post(ADMIN_URL+"purchase_order/import-to-excel-purchase-order", { id :id_data},function(result){
                    if(result == 'TRUE'){
                        $('#client_id').val('');
                        $('#form_purchase_order_modal').modal('toggle');
                        location.reload();
                        $("#import_purchase_order").attr("disabled", false);
                    } else {
                        $("#import_purchase_order").attr("disabled", false);
                    }
                });
            }, 300);
        }
    });
    $('#update_credit_amount').on('hidden.bs.modal', function () {
            $('#f_credit_amount').val('');
            $('#f_credit_amount_error').hide();
            var date = new Date();
            var time = date.getDate() + "-" + (date.getMonth()+1) +"-" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() ;
            $('#d_credit_date').val(time);
    // do something�
    });
    
    
    /* $("#change_status_form").submit(function() {
        var form = $("#change_status_form");
        if(form_valid("#change_status_form")) 
        {
            var send_data = $("#change_status_form").serialize();
            $.post($("#change_status_form").attr("action"), send_data, function(data)
            {
                if($.trim(data) == 'TRUE') 
                {
                  
                     $('.alert-success').show();
                    $('#datatable_ajax').DataTable().ajax.reload();
                   
                } 
              
            });
        } 
        else
        {
            save_and_continue_flag = false;
        }
        return false;
    });*/
    
    
    
    
    $(document).on("click",".back_table",function () {
        if($(".edit-record").length > 0) {
            if(!confirm('Updated information will discarded. Are you sure you want to continue?')) {
                return false;
            }
        } 
        $('#widget-view-inner-content').fadeOut(500,function(){
  			$('#widget-body-inner-content').fadeIn(500); 
            $('#widget-view-inner-content').removeClass('edit-record');            
            $('#widget-view-inner-content').removeClass('view-record');
            $('#widget-view-inner-content').html("");
        });
        return true;
    }); 
    
    $("#frmEdit").submit(function() {
        var form = $("#frmEdit");
        var error_flag = 0;
        if($(this).hasClass('edit_purchase_order')){
            $("#frmEdit .e_item_status").each(function () {
                var str = $(this).attr('data-id');                
                if($(this).val() == 'Partial Inventory'){
                    if($('#i_qty_on_hand-'+str).val() == '0'){                        
                        $('#i_qty_on_hand-'+str+'number_error').show();
                        error_flag = 1;
                    }
                }
            });

        }             
        form.find('.duplicate-error').hide();
        form.find(".form-group").removeClass('has-error');
        form.find(".form-group").removeClass('error-inner');
        if($('#v_cms_content').length > 0){
            CKEDITOR.instances.v_cms_content.updateElement();
            $('#v_cms_content').val(CKEDITOR.instances['v_cms_content'].getData());    
        }

        if($('#t_email_content').length > 0){
            CKEDITOR.instances.t_email_content.updateElement();
            $('#t_email_content').val(CKEDITOR.instances['t_email_content'].getData());    
        }
        if(form_valid("#frmEdit") && error_flag == 0) 
        {            
            if($('#image_val1 img').prop('src') != undefined && $('#image_val1 img').prop('src') != '' && $('#image_val1 img').prop('src').indexOf("base64") > 0){
                $('#v_logo_val1').val($('#image_val1 img').prop('src')); 
            }
            if($('#image_val img').prop('src') != '')
            {
             $('#v_logo_val1').val($('#image_val1 img').prop('src')); 
            }        
            var send_data = $("#frmEdit").serialize();
            $.post($("#frmEdit").attr("action"), send_data, function(data)
            {
                if($.trim(data) == 'TRUE') 
                {
                    $('.edit-success').show();
                    $('.alert-success').fadeOut(5000);
                    $('.edit-success').show();
                    $("#error_order").hide();
                    $('#edit-project').hide();
                    $('#alert-danger').hide();
                    $('#edit-timelog').hide();
                    $('#error_v_email').hide();
                    $('#error_i_store_number').hide();
                    $('#error_d_date_hidden').hide();
                    $('.form-group').removeClass('has-error');
                    $('#name').text($('#fname').val() + ' ' + $('#lname').val());
                    $('#storename').text($('#v_store_name').val());
                    $('#datatable_ajax').DataTable().ajax.reload();

                } else if($.trim(data) == 'FALSE'){
                    $("#error_order").hide();
                    $('#d_error').show();
                    return false;
                } else if($.trim(data) == 'min_order_error' || $.trim(data) == 'max_order_error'){
                    $("#error_order").show();
                    $("#error_order").html('Enter valid order number.');
                    return false;
                } else if($.trim(data) == 'topicexist') {
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    $('.alert-danger').show();
                    $('.alert-danger .message').html('This topic have quiz.');
                    setTimeout(function(){ $('.alert-danger').fadeOut(4000); },3000);
                    return false;
                } else if($.trim(data) == 'exist') {
                    $('.duplicate_name_error').show();
                    $('.duplicate-error').show();
                    $('#error_v_email').show();
                    $('.company-edit-error').show();
                    return false;
                } else if($.trim(data) == 'TRUE_SETUP') {
                    $('.duplicate_name_error').hide();
                    $('#frmEdit')[0].reset();
                    loadData();
                    return false;
                }
                if($.trim(data) == '') {
                    form.find('.alert-success').show();
                    form.find('.alert-danger').hide();
                    form.find('.duplicate-error').hide();
                    form.find('.duplicate_name_error').hide();
                    form.find('#d_error').hide();
                    if($("#redirect_url").length == 1) {
                        window.location.href = $("#redirect_url").val(); 
                    } else {
                        window.location = strstr($("#frmEdit").attr("action"),'/edit/',true);
                    }
                } else if($.trim(data) == 'update_cms'){
                    form.find('.alert-success').show();
                    window.location.href = window.location.href; 
                }else {
                    // Same email domain validate
                    if($.trim(data) == 'DOMAIN_ERROR') {
                        var same_domain = false;
                        bootbox.confirm('Email id with same domain name is already exists.', function (confirmed) {
                            if(confirmed) {
                                same_domain = true;    
                                send_data = send_data+'&same_domain='+same_domain;
                                $.post($("#frmEdit").attr("action"), send_data, function(data) {
                                    if($.trim(data) == '') {
                                        form.find('.alert-danger').hide();
                                        //form.find('.alert-success').hide();
                                        form.find('.duplicate-error').hide();
                                        form.find('.duplicate_name_error').hide();
                                        if($("#redirect_url").length == 1) {
                                            window.location.href = $("#redirect_url").val(); 
                                        } else {
                                            window.location = strstr($("#frmEdit").attr("action"),'/edit/',true);
                                        }
                                    }               
                                });
                            }
                        });
                        return false;
                    }
                    //form.find('.alert-success').hide();
                    //form.find('.alert-danger').show();
                    //form.find('.duplicate-error').hide();
                    form.find('.duplicate_name_error').show();
                    $(data).each(function(i,val){
                        
                        $.each(val,function(key,v){
                              $('#'+key).closest('.form-group').addClass('has-error');
                              $('#error_'+key).show();
                              $('#error_d_date_hidden').show();
                        });
                    });
                    if($('.has-error .form-control').length > 0) {
                        $('html, body').animate({
                            scrollTop:$('.has-error .form-control').first().offset().top - 200
                        }, 1000);
                        $('.has-error .form-control').first().focus()
                    }
                }
            });
        } 
        else
        {
            save_and_continue_flag = false;
        }
        return false;
    });
    
    $("#frmPreviewSubmit").on('click',function(){
        if(form_valid("#frmAdd")) {
            formAction = $("#frmAdd").attr("action");
            formAction = formAction.replace('/add',"/preview");            
            $.post(formAction, $("#frmAdd").serialize(), function(data) {
                if($.trim(data) == '') { 
                    var win = window.open( ASSET_URL+'page-preview/'+$("#v_url").val(), '_blank');
                    if(win){
                        win.focus(); //Browser has allowed it to be opened
                    } else {
                        alert('Please allow popups for this site'); //Broswer has blocked it
                    }
                } else {
                    var errorArray = data.split(':::');
                    if(typeof errorArray[0] != "undefined" && errorArray[0] != "") {
                        $('.duplicate_name_error').html('<label class="error-inner">'+$.trim(errorArray[0])+'.</label>');
                    } else if(typeof errorArray[1] != "undefined" &&  errorArray[1] != "") {
                        $('.duplicate_url_error').html('<label class="error-inner">'+$.trim(errorArray[1])+'.</label>');
                    }
                } 
            });    
        }
    })
    var save_and_continue_flag = false;
    $("#frmAddNewSubmit").click(function() {
       save_and_continue_flag = true
    });
  
  
    
    function clearForm(form)
    {
        $(":input", form).each(function()
        {
        var type = this.type;
        var tag = this.tagName.toLowerCase();
        	if (type == 'text')
        	{
        	this.value = "";
        	}
        });
    };
 
    
  
    
    $(document).on('click',".remove", function()
    {
        del_id = $(this).attr('id1');
        $("#"+del_id).remove();
    });
    $(document).on('click',"#select_all_sales",function() {
        if($(this).is(':checked'))  {
            flag = true;
        } else {    
          flag = false;
        }
        
        $(".sales-list input[type=checkbox]").each(function() {
            if(flag) {
                $(this).parent().addClass('checked');
                $(this).attr('checked', true);
            } else {
                $(this).attr('checked', false);
                $(this).parent().removeClass('checked');
            }
        });
        
    });
    
    $("#contact_us").submit(function(){
        var reCaptcha = true;
        var googleResponse = jQuery('#g-recaptcha-response').val();
        $('.required_captcha').html("");
        if(googleResponse == '') {
            $('.required_captcha').html("Please select captcha.");
            reCaptcha = false;
        }
        else{
            $('.required_captcha').html("");
            reCaptcha = true;
        }
        if(form_valid("#contact_us")){
            $.ajax({
                type: 'post',
                url: SITE_URL+'contact-us/captcha-check',
                data: {'googleResponse':googleResponse},
                datatype:'json',
                success: function(data)
                {
                    if(data == 'FALSE'){
                        grecaptcha.reset();
                        //jQuery('#g-recaptcha-response').val('');
                        //$('.required_captcha').html("Please select captcha again.");
                        //jQuery('#g-recaptcha-response').val('');
                        reCaptcha = false;
                        return false;
                    }else{
                         $('.required_captcha').html("");
                         if(googleResponse == '') {
                            $('.required_captcha').html("Please select captcha.");
                            reCaptcha = false;
                        }else{
                            $('.required_captcha').html("");
                            reCaptcha = true;
                             googleResponse = '';
                             var curObj = $(this);
                             curObj.find('button[type=submit]').attr('disabled', 'disabled'); 
                             data = $("#contact_us").serialize();
                             $.ajax({
                                type: 'POST',        
                                url: "form_submit",
                                data: data,
                                success: function(response)
                                {
                                  if(response == "SUCCESS")
                                  {
                                    
                                    $(".success_msg").html("<div class='Metronic-alerts alert alert-success fade in'><span aria-hidden='true' data-dismiss='alert' class='close submit_success'><b>Your Contact Information has been submitted successfully. Thank you.</b></span></div>");
                                    setTimeout(function(){ $(".success_msg").html(""); }, 4000);
                                    $('#contact_us')[0].reset();
                                    grecaptcha.reset();
                                  }
                                  else{
                                    grecaptcha.reset();
                                  }
                                  
                                }
                        });
                            
                        }
                    }
                }
            });
        }
        else
        {
            grecaptcha.reset();
            save_and_continue_flag = false;
        }
        return false;
    });
    
     $("#frmReg").submit(function () 
    {
        var reCaptcha = true;
        var googleResponse = jQuery('#g-recaptcha-response').val();
         var form = $("#frmReg");
        form.find('.duplicate-error').hide();
        form.find(".form-group").removeClass('has-error');
        $('.required_captcha').html("");
        if(googleResponse == '') {
            $('.required_captcha').html("Please select captcha.");
            reCaptcha = false;
        }
        else{
             $('.required_captcha').html("");
             reCaptcha = true;
        }
        if(form_valid("#frmReg")) 
        {
            $('.loading').fadeIn();
             var curObj = $(this);
             curObj.find('button[type=submit]').attr('disabled', 'disabled');
             if(googleResponse == '') {
                $('.loading').fadeOut();
                $('.required_captcha').html("Please select captcha.");
                reCaptcha = false;
            }
            else{
                $('.required_captcha').html("");
             reCaptcha = true;
            $.ajax({
                type: 'post',
                url: SITE_URL+'signup/captcha-check',
                data: {'googleResponse':googleResponse},
                datatype:'json',
                success: function(data)
                {
                    
                    if(data == 'FALSE'){
                        $('.loading').fadeOut();
                        var curObj = $(this);
                        curObj.find('button[type=submit]').removeAttr('disabled'); 
                        grecaptcha.reset();
                        setTimeout(function(){ $('.required_captcha').html("Please select captcha again.")},3000);
                        //$('.required_captcha').html("Please select captcha again.");
                        //jQuery('#g-recaptcha-response').val('');
                        reCaptcha = false;
                        return false;
                    }else{
                        var curObj = $(this);
                        curObj.find('button[type=submit]').attr('disabled', 'disabled'); 
                        var send_data = $("#frmReg").serialize();
                        $.post($("#frmReg").attr("action"), send_data, function(data) {
                            $('.loading').fadeOut();
                            curObj.find('button[type=submit]').removeAttr('disabled');
                            if($.trim(data) == 'TRUE') 
                            {
                                 $('.required_captcha').html("");
                                $('#v_logo_val').val('');
                               // $('#frmAdd')[0].reset();
                                $('#basic').hide();
                                //$('.add-success').show();
                                $('#datatable_ajax').DataTable().ajax.reload();
                                $('.alert-success').fadeOut(5000);
                                $('#error_v_email').hide();
                                $('#error_i_store_number').hide();
                                $('#error_d_date_hidden').hide();
                                $('.form-group').removeClass('has-error');
                                form.find('.alert-success').show();
                                form.find('.alert-danger').hide();
                                $("html, body").animate({ scrollTop: 0 }, 600);
                                //jQuery('#g-recaptcha-response').val('');
                                
                                //loadData();
                            }else if($.trim(data) == 'exist')
                            {
                                $('.company-add-error').show();
                                $('#error_v_email').show();
                                $('.duplicate-error').show();
                                return false;
                            }
                            else if($.trim(data) == 'TRUE_SETUP')
                            {
                                $('.company-add-error').hide();
                                $('.company-add-success').show();
                                $('#v_logo_val').val('');
                                $('#frmReg')[0].reset();
                                $('#basic').hide();
                                loadData();
                                return false;
                            }
                             else if($.trim(data) == 'FALSE')
                            {
                                $('#d_error').show();
                                return false;
                            }
                            if($.trim(data) == '') {
                                 $('.required_captcha').html("");
                                 //$('.required_captcha').html("");
                                //jQuery('#g-recaptcha-response').val('');
                                //form.find('.alert-success').show();
                                grecaptcha.reset();
                                $('#d_error').hide();
                                form.find('.alert-success').fadeOut(5000);
                                form.find('.alert-danger').hide();
                                //form.find('.duplicate-error').hide();
                                $("html, body").animate({ scrollTop: 0 }, 600);
                                if(save_and_continue_flag) {
                                    save_and_continue_flag = false;
                                    window.location.href = window.location.href; 
                                } else {
                                    save_and_continue_flag = false;
                                    window.location.href = SITE_URL+'transaction';
                                }
                            } else {   
                                grecaptcha.reset();
                               //Recaptcha.reload();
                                form.find('.alert-success').hide();
                                form.find('.alert-danger').show();
                                $(data).each(function(i,val){
                                    $.each(val,function(key,v){
                                        //setTimeout(function(){ grecaptcha.reset()},2000);
                                        //setTimeout(function(){ $('.required_captcha').html("Please select captcha again.")},3000);
                                          $('#'+key).closest('.form-group').addClass('has-error');
                                          $('#error_'+key).show();
                                    });
                                });
                                
                                if($('.has-error .form-control').length > 0) {
                                    $('html, body').animate({
                                        scrollTop:$('.has-error .form-control').first().offset().top - 200
                                    }, 1000);
                                    $('.has-error .form-control').first().focus()
                                }
                            }
                        });
                                }
                            }
            });
           }            
        }
        else
        {
            save_and_continue_flag = false;
        }
        return false;
    });
    
    
    
      $("#frmAdd").submit(function () {        
        var form = $("#frmAdd");
        form.find('.duplicate-error').hide();
        form.find(".form-group").removeClass('has-error');
        form.find(".form-group").removeClass('error-inner');
        if($('#v_desc').length > 0) {
            CKEDITOR.instances.v_desc.updateElement();
            $('#v_desc').val(CKEDITOR.instances['v_desc'].getData());
        }
        /*  var reCaptcha = true;
        var googleResponse = jQuery('#g-recaptcha-response').val();
        if(googleResponse == '') {
            $('.required_captcha').html("Please select captcha");
            reCaptcha = false;
        }
        else */
         if(form_valid("#frmAdd")) 
        {

            $('.required_captcha').html("");
            jQuery('#g-recaptcha-response').val('');
            var curObj = $(this);
            curObj.find('button[type=submit]').attr('disabled', 'disabled'); 
            if($('#image_val img').prop('src') != undefined && $('#image_val img').prop('src') != ''){
                $('#v_logo_val').val($('#image_val img').prop('src')); 
            }
            var send_data = $("#frmAdd").serialize();            
            $.post($("#frmAdd").attr("action"), send_data, function(data) {
                curObj.find('button[type=submit]').removeAttr('disabled');
                
                if($.trim(data) == 'TRUE') 
                {
                    $('#v_logo_val').val('');
                    $('#basic').hide();
                    $('#datatable_ajax').DataTable().ajax.reload();
                    $('.alert-success').fadeOut(5000);
                    $('#error_v_email').hide();
                    $('.form-group').removeClass('has-error');
                    form.find('.alert-success').show();
                    form.find('.alert-danger').hide();
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    jQuery('#g-recaptcha-response').val('');
                    
                    //loadData();
                }else if($.trim(data) == 'exist')
                {
                    $('.company-add-error').show();
                    $('#error_v_email').show();
                    $('.duplicate-error').show();
                    return false;
                }else if($.trim(data) == 'topicexist')
                {
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    $('.alert-danger').show();
                    $('.alert-danger .message').html('This topic have quiz.');
                    setTimeout(function(){ $('.alert-danger').fadeOut(4000); },3000);
                    return false;
                }
                else if($.trim(data) == 'TRUE_SETUP')
                {
                    $('.company-add-error').hide();
                    $('.company-add-success').show();
                    $('#v_logo_val').val('');
                    $('#frmAdd')[0].reset();
                    $('#basic').hide();
                    loadData();
                    return false;
                }
                 else if($.trim(data) == 'FALSE')
                {
                    $('#d_error').show();
                    return false;
                }
                if($.trim(data) == '') {
                    $('#d_error').hide();
                    form.find('.alert-success').fadeOut(5000);
                    form.find('.alert-danger').hide();
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    if(save_and_continue_flag) {
                        save_and_continue_flag = false;
                        window.location.href = window.location.href; 
                    } else {
                        save_and_continue_flag = false;
                        window.location.href = (window.location.href).replace('/add','');
                    }
                } else {                    
                    if($.trim(data) == 'DOMAIN_ERROR') {
                        var same_domain = false;
                        bootbox.confirm('Email id with same domain name is already exists.', 
                        function (confirmed) {
                            if(confirmed) {
                                same_domain = true;    
                                send_data = send_data+'&same_domain='+same_domain;
                                $.post($("#frmAdd").attr("action"), send_data, function(data) {
                                    if($.trim(data) == '') {
                                        form.find('.alert-danger').hide();
                                        form.find('.duplicate-error').hide();
                                        $("html, body").animate({ scrollTop: 0 }, 600);
                                        if(save_and_continue_flag) {
                                            save_and_continue_flag = false;
                                            window.location.href = window.location.href; 
                                        } else {
                                            save_and_continue_flag = false;
                                            window.location.href = (window.location.href).replace('/add',''); 
                                        }
                                    }               
                                });
                            }
                        });
                        return false;
                    }
                    
                    form.find('.alert-success').hide();
                    form.find('.alert-danger').show();
                    $(data).each(function(i,val){
                        $.each(val,function(key,v){
                              $('#'+key).closest('.form-group').addClass('has-error');
                              $('#error_'+key).show();
                        });
                    });
                    
                    if($('.has-error .form-control').length > 0) {
                        $('html, body').animate({
                            scrollTop:$('.has-error .form-control').first().offset().top - 200
                        }, 1000);
                        $('.has-error .form-control').first().focus()
                    }
                }
            });            
        }
        else
        {
            //tour.init();
            //tour.start();
            save_and_continue_flag = false;
        }
        return false;
    });
    
    $("#frmCampaigns").submit(function () {
        var form = $("#frmCampaigns");
        var curObj = $(this);
        form.find(".form-group").removeClass('has-error');
        if(form_valid("#frmCampaigns")) {
            $('#condition_error').hide();
            $('#messge_error').hide();
            curObj.find('button[type=submit]').attr('disabled', 'disabled'); 
            var send_data = $("#frmCampaigns").serialize();
            $.post($("#frmCampaigns").attr("action"), send_data, function(data) {
                curObj.find('button[type=submit]').removeAttr('disabled');
                if($.trim(data) == 'TRUE'){
                    $('#messge_error').hide();
                    $('#condition_error').hide();
                    $('#v_vendor_number').val(''); 
                    $('#i_cell_number').val(''); 
                    $('#chk').removeAttr('checked');
                    $('#v_vendor_number').removeAttr('disabled'); 
                    $('#uniform-chk').removeClass('disabled'); 
                    $('#uniform-chk').find("span").removeClass("checked");
                    $('#i_cell_number').removeAttr('disabled'); 
                    $('#charNum').hide();
                    $('#condition_vendor_error').hide();
                    $('.form-group').removeClass('has-error');
                    form.find('.alert-success').show();
                     form.find('.alert-success').fadeOut(5000);
                     $('#frmCampaigns')[0].reset();
                    form.find('.alert-danger').hide();
                    $("html, body").animate({ scrollTop: 0 }, 600);
                } else if($.trim(data) == 'FALSE') {
                    $('#messge_error').hide();
                    $('#chk').removeAttr('checked');
                    $('#v_vendor_number').val(''); 
                    $('#i_cell_number').val(''); 
                    $('#uniform-chk').find("span").removeClass("checked");
                    $('#condition_error').hide();
                    $('#v_vendor_number').removeAttr('disabled'); 
                    $('#uniform-chk').removeClass('disabled'); 
                    $('#i_cell_number').removeAttr('disabled'); 
                    $('#charNum').hide();
                    //$('#message_true').hide();
                    //$('#message_false').show();
                     form.find('.alert-danger').show();
                    form.find('.alert-danger').fadeOut(5000);
                    form.find('.alert-success').hide();
                    $('#condition_vendor_error').hide();
                    $('#frmCampaigns')[0].reset();
                    return false;
                } else if($.trim(data) == 'FALSE_ERROR') {
                    $('#messge_error').hide();
                    $('#chk').removeAttr('checked');
                    $('#v_vendor_number').val(''); 
                    $('#i_cell_number').val(''); 
                    $('#uniform-chk').find("span").removeClass("checked");
                    $('#v_vendor_number').removeAttr('disabled'); 
                    $('#uniform-chk').removeClass('disabled'); 
                    $('#i_cell_number').removeAttr('disabled');
                    $('#condition_error').show();
                    $('#condition_vendor_error').hide();
                    return false;
                } else if($.trim(data) == 'FALSE_VENDOR') {
                    $('#messge_error').hide();
                    $('#i_cell_number').attr('disabled', 'disabled'); 
                    $('#uniform-chk').addClass('disabled'); 
                    $('#condition_error').hide();
                    $('#condition_vendor_error').show();
                    return false;
                }  else {                    
                    if($.trim(data) == 'DOMAIN_ERROR') {
                        var same_domain = false;
                        bootbox.confirm('Email id with same domain name is already exists.', 
                        function (confirmed) {
                            if(confirmed) {
                                same_domain = true;    
                                send_data = send_data+'&same_domain='+same_domain;
                                $.post($("#frmCampaigns").attr("action"), send_data, function(data) {
                                    if($.trim(data) == '') {
                                        form.find('.alert-danger').hide();
                                        form.find('.duplicate-error').hide();
                                        $("html, body").animate({ scrollTop: 0 }, 600);
                                        if(save_and_continue_flag) {
                                            save_and_continue_flag = false;
                                            window.location.href = window.location.href; 
                                        } else {
                                            save_and_continue_flag = false;
                                            window.location.href = (window.location.href).replace('/add',''); 
                                        }
                                    }               
                                });
                            }
                        });
                        return false;
                    }
                    
                    form.find('.alert-success').hide();
                    form.find('.alert-danger').show();
                    $(data).each(function(i,val){
                        $.each(val,function(key,v){
                              $('#'+key).closest('.form-group').addClass('has-error');
                              $('#error_'+key).show();
                        });
                    });
                    
                    if($('.has-error .form-control').length > 0) {
                        $('html, body').animate({
                            scrollTop:$('.has-error .form-control').first().offset().top - 200
                        }, 1000);
                        $('.has-error .form-control').first().focus()
                    }
                }
            });            
        }
        else
        {
            $('#messge_error').hide();
            save_and_continue_flag = false;
        }
        return false;
    });
    
});

function handleGoTop() 
{
    /* set variables locally for increased performance */
    $('#footer .go-top').click(function () {
        $('html,body').animate({
                scrollTop: 0
            }, 'slow');
    });
}


function handleUniform() 
{
    if (!$().uniform) {
        return;
    }
    if (test = $("input[type=checkbox]:not(.toggle), input[type=radio]:not(.toggle)")) {
        test.uniform();
    }
}





function handleLoginForm() 
{  
    console.log("here");
    $("#frmEmpLogin").submit(function() {
        if(form_valid("#frmEmpLogin")) {
            alert($('#remember').val());
            return true;
        } else {
            return false;
        }
    });    
}


function handleProfileForm() {  
    $('#star_password').dblclick(function(){
        $('#star_password').hide();
        $('#original_password').show();
    });
    $('#star_password_tw').dblclick(function(){
        $('#star_password_tw').hide();
        $('#original_password_tw').show();
    });
    
    $('#file_trriger').click(function(){
        $('#image_change').trigger('click');
    });
    
    $('#back_first_tab').click(function(){
       $('#tab_1_1').addClass('active');
       $('#tab_1').parent('li').addClass('active');
       $('#tab_1_2'). removeClass('active');
       $('#tab_2').parent('li').removeClass('active');
       $('html, body').animate({
            scrollTop:$('.page-header').first().offset().top - 200
        }, 1000);
    });
    
    $("#update_profile").submit(function() {
        var form = $("#update_profile");
        var img_default_val=0;
        form.find('.duplicate-error').hide();
        form.find(".form-group").removeClass('has-error');
        form.find(".form-group").removeClass('error-inner');
        if($('#password_new').val() != '') {
            if($('#password_old').val() == ''){
                    $('#password_old').addClass('required');
            }
        } else {
            if($('#password_old').val() == ''){
                $('#password_old').removeClass('required');
            }
        }
        // var status_flag = true;
        // if($('#password_new').val() != '' && !$('#password_new').hasClass('passMeter-strong')) {
        //     status_flag = false;
        // }

        if(form_valid("#update_profile") /* && status_flag */ ){
            
            var curObj = $(this);
            curObj.find('button[type=submit]').attr('disabled', 'disabled');
            if($('#image_val1 img').prop('src') != undefined && $('#image_val1 img').prop('src') != '' && $('#image_val1 img').prop('src').indexOf("base64") > 0){
                $('#v_logo_val1').val($('#image_val1 img').prop('src')); 
            }else
            {
                if($('#image_val1 img').prop('src')!='')
                {                    
                    $('#v_logo_val1').val($('#image_val1 img').prop('src')); 
                }
                    img_default_val=1;    
            }
            var send_data = $("#update_profile").serialize();
            $.post($("#update_profile").attr("action"), send_data, function(data){
                curObj.find('button[type=submit]').removeAttr('disabled');
                if(data == 'TRUE') {
                    form.find('.alert-success').show();
                        var email = form.find('#v_email').val();
                        $('.duplicate-error').hide();
                        // if(img_default_val==1)
                        // {                            
                        //     if($('#image_val1 img').prop('src')!=''){                            
                        //         if($('#image_val1 img').prop('src')==undefined)
                        //         {
                        //             console.log("else");
                        //             $('.img-circle').attr('src',$('#default_img').val());     
                        //         }else
                        //         {
                        //             console.log("if");
                        //             $('.img-circle').attr('src',$('#image_val1 img').prop('src'));         
                        //         }
                        //     }
                        // }else
                        // {
                        //     $('.img-circle').attr('src',$('#image_val1 img').prop('src'));     
                        // }
                         $('#password_old').val('');
                        $('#password_new').val('');
                        $('#password_retype').val('');
                       window.location.href = window.location.href; 
                } else if(data == 'false_password') {
                    $('#error_old_password').show();

               } else {
                    form.find('.alert-success').hide();
                    form.find('.alert-danger').show();
                    $(data).each(function(i,val){
                        $.each(val,function(key,v){
                              $('#'+key).closest('.form-group').addClass('has-error');
                              $('#error_'+key).show();
                        });
                    });
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    $('.has-error .form-control').first().focus();
                }
            });
        } else {
            save_and_continue_flag = false;
        }
        return false;
    });
    
    $("#update_setting").submit(function() {
        var form = $("#update_setting");
        form.find('.duplicate-error').hide();
        form.find(".form-group").removeClass('has-error');
        if(form_valid("#update_setting")){
             var curObj = $(this);
            curObj.find('button[type=submit]').attr('disabled', 'disabled'); 
            var send_data = $("#update_setting").serialize();
            $.post($("#update_setting").attr("action"), send_data, function(data){
                curObj.find('button[type=submit]').removeAttr('disabled');
                if(data == 'TRUE') {
                        $('.duplicate-error').hide();
                        form.find('.alert-success').show();
                        form.find('.alert-danger').hide();
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        setTimeout(function(){ $(".alert-success").fadeOut(2000); },4000);
                } else {
                    form.find('.alert-success').hide();
                    form.find('.alert-danger').show();
                    $(data).each(function(i,val){
                        $.each(val,function(key,v){
                              $('#'+key).closest('.form-group').addClass('has-error');
                              $('#error_'+key).show();
                        });
                    });
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    $('.has-error .form-control').first().focus()
                }
            });
        } else {
            save_and_continue_flag = false;
        }
        return false;
    });
    
         
    $("#profile_image").submit(function() {
        var form = $("#profile_image");
         $("#profile_image_submit").val('Please wait..');
         $("#profile_image_submit").attr('disabled',true);
         $.post(form.attr('action'), form.serialize(), function (data) {
            if(data != 'FALSE') {
                $("#profile_image_submit").attr('disabled',false);
                $("#profile_image_submit").val('Save Changes');
                if($('.page-content').hasClass('my-profile')){
                    $('.page-header').find('img.img-circle').attr('src', SITE_URL + USER_PROFILE_THUMB_PATH + data);
                    //$('#sideprofileimage').attr('src', SITE_URL + USER_PROFILE_THUMB_PATH + data);
                    $('#tab_1_2').addClass('tab_2_visible');
                }
                $('.profile_pic_success').fadeIn(1000);
               $("html, body").animate({ scrollTop: 0 }, 600);
               setTimeout(function(){ $(".profile_pic_success").fadeOut(2000); },4000); 
            }
         });
    });
}

function handleSettingForm() { 
    
}
function loadData()
{
    var url = SITE_URL+'setup/list-ajax';
    $('#setup_company tbody').html('');
    $('#setup_technologies tbody').html('');
    $('#setup_designation tbody').html('');
    $.ajax({
        type: 'post',
        url: url,
        //data: qry_str,
        success: function(data)
        {
            company_data = data.company; //img-responsive
            $.each(data.company,function(key,value){
                var comp_html = '<tr><td><div  class="profile-userpic"><img height="75px" width="75px" src="'+value.v_logo+'"  class="" alt=""></div></td><td>'+value.v_company_name+'</td><td><a href="'+value.v_website_url+'" target="_blank">'+value.v_website_url+'</a></td><td><a href="tel:'+value.v_phone+'">'+value.v_phone+'</a> </td><td class="actions"><a class="edit edit_company"  data-toggle="modal" href="#edit_company" rel="'+value.id+'"><i class="fa fa-edit"></i></a><a class="delete delete_company" href="javascript:;" rel="'+value.id+'"><i class="fa fa-trash-o"></i></a></td></tr>';
             $('#setup_company tbody').append(comp_html);
            });
            $.each(data.technology,function(key,value){
                var tech_html = '<tr><td>'+value.v_technology_title+'</td><td class="actions"><a class="edit edit_btn"  rel="'+value.id+'" href="javascript:;"><i class="fa fa-edit"></i></a><a class="delete" rel="'+value.id+'" href="javascript:;"><i class="fa fa-trash-o"></i></a></td></tr>'; 
                $('#setup_technologies tbody').append(tech_html);
            });
            $.each(data.designation,function(key,value){
                var des_html = '<tr><td>'+value.v_designation_title+'</td><td class="actions"><a class="edit edit_btn"  rel="'+value.id+'" href="javascript:;"><i class="fa fa-edit"></i></a><a class="delete" rel="'+value.id+'" href="javascript:;"><i class="fa fa-trash-o"></i></a></td></tr>'; 
                $('#setup_designation tbody').append(des_html);     
            });
            return false;
        },
    })
    
}

function handleListRecord() {
    $('#frmSearchForm input[type="text"]').on("keyup",function(event) {        
		var unicode = event.keyCode;
        if(unicode == 13) {        
            loadPiece($("#frmSearchForm").attr('action'));         
		}       
        return false;  
	});	 	
    
    $("#btn_submit").click(function() {
        loadPiece($("#frmSearchForm").attr('action'));
        return false;         
	});
    
    $(document).on('change','.record_page',function() {
        $("#rec_per_page").val($(this).val());
        loadPiece($("#frmSearchForm").attr('action'));
        return false;         
	});
    
}

// this is optional to use if you want animated show/hide. But plot charts can make the animation slow.
function handleSidebarTogglerAnimated(){
    $('.sidebar-toggler').click(function () {
        if ($('#sidebar > ul').is(":visible") === true) 
        {
            $('#main-content').animate({
                'margin-left': '25px'
            });

            $('#sidebar').animate({
                'margin-left': '-190px'
            }, {
                complete: function () {
                    $('#sidebar > ul').hide();
                    $("#container").addClass("sidebar-closed");
                }
            });
        } 
        else 
        {
            $('#main-content').animate({
                'margin-left': '215px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').animate({
                'margin-left': '0'
            }, {
                complete: function () {
                    $("#container").removeClass("sidebar-closed");
                }
            });
        }        
    })
}

// by default used simple show/hide without animation due to the issue with handleSidebarTogglerAnimated.
function handleSidebarToggler() {
    $('.sidebar-toggler').click(function () {
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '25px'
            });
            $('#sidebar').css({
                'margin-left': '-190px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
           $('#main-content').css({
                'margin-left': '215px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }       
    })
}

function handleWidgetTools() 
{
    $('.widget .tools .icon-remove').click(function () {
        $(this).parents(".widget").parent().remove();
    });

    $('.widget .tools .icon-refresh').click(function () {
        var el = $(this).parents(".widget");
        App.blockUI(el);
        window.setTimeout(function () {
            App.unblockUI(el);
        }, 1000);
    });

    $('.widget .tools .icon-chevron-down, .widget .tools .icon-chevron-up').click(function () {
        var el = $(this).parents(".widget").children(".widget-body");
        if ($(this).hasClass("icon-chevron-down")) {
            $(this).removeClass("icon-chevron-down").addClass("icon-chevron-up");
            el.slideUp(200);
        } else {
            $(this).removeClass("icon-chevron-up").addClass("icon-chevron-down");
            el.slideDown(200);
        }
    });
    
    
}


function nl2br (str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function resetForm(frmID){
    $('#summernote_send_a_message').code('');
    if($("#message_id").length > 0){
        $("#message_id").val('');
    }
    if($("#follow_id").length > 0){
        $("#follow_id").val('');
        $("#i_followed_up_type_id, #i_next_followed_up_type_id, #i_next_followed_up_by").select2('val', '')
    }
    $("#"+frmID)[0].reset();    
}  

function rtrim(str,lastChar) {
    if (str.substring(str.length-1) == lastChar) {
        str = str.substring(0, str.length-1);
    }    
    return str;
} 

function scroll_up_to(selector){
    $('html, body').animate({
        scrollTop: (parseInt($(selector).offset().top,10) - 200)
    }, 500);
}

function export_to_excel(ModelName) {
    exportAction = ($("#frmSearchForm").attr('action')).replace("list-ajax","export");
    window.location.href = exportAction+'?'+$("#frmSearchForm").serialize();
    return false;
}

function strstr(haystack, needle, bool) {
   
    var pos = 0;

    haystack += "";
    pos = haystack.indexOf(needle); if (pos == -1) {
        return false;
    } else {
        if (bool) {
            return haystack.substr(0, pos);
        } else {
            return haystack.slice(pos);
        }
    }
}
$(document).on("click",".back_table",function () {
        if($(".edit-record").length > 0) {
            if(!confirm('Updated information will discarded. Are you sure you want to continue?')) {
                return false;
            }
        } 
        $('#widget-view-inner-content').fadeOut(500,function(){
  			$('#widget-body-inner-content').fadeIn(500); 
            $('#widget-view-inner-content').removeClass('edit-record');            
            $('#widget-view-inner-content').removeClass('view-record');
            $('#widget-view-inner-content').html("");
        });
        return true;
}); 
    
$(".widget").on("click",".btn_view",function () 
    {
        curId = $(this).attr('rel');
        if(curId != '') { 
            $.get($("#view_url").val()+curId, function (data) {
                $("#widget-view-inner-content").html(data);
                var current_div = "#image_table";
                var loader_div = "#load_image";
                var current_div_gallery = ".image_table_gallery";
                var loader_div_gallery = ".load_image_gallery";
                var poster_current_div = "#poster_table";
                var poster_loader_div = "#load_poster";
                $(current_div).hide();
                $(loader_div).show();
                $(current_div_gallery).hide();
                $(loader_div_gallery).show();
                $(poster_current_div).hide();
                $(poster_loader_div).show();
                
                $('#widget-view-inner-content').addClass('view-record');
                $('#widget-view-inner-content').removeClass('edit-record');
                
                $('#widget-body-inner-content').fadeOut(500,function() 
                {
                    $('#widget-view-inner-content').fadeIn(500); 
                    if($(".view-image").length > 0 && $(".view-image").html() != "" && $(".view-image").html() != undefined) 
                    {
                        setTimeout(function() 
                        {
                            $(loader_div).hide();
                            $(current_div).show();
                            $(loader_div_gallery).hide();
                            $(current_div_gallery).show();
                            $(poster_loader_div).hide();
                            $(poster_current_div).show();
                            $(".parent-div-img-container").each(function() { set_size(".div-img-container"); }); 
                        },1500);
                    }
                    prevLink = nextLink = '';
                    curRow = "#ApplicantRow-"+curId; 
                    if($(curRow).prev('tr').attr('id') !== undefined) {
                        prevId = ($(curRow).prev('tr').attr('id')).replace('ApplicantRow-',''); 
                        prevLink = '<a href="javascript:void(0);" rel="'+prevId+'" class="view_prev btn_view"><i class="icon-arrow-left"></i>Previous</a>';
                    } else {
                        prevId = ($(curRow).parent('tbody').find('tr:last').attr('id')).replace('ApplicantRow-',''); 
                        //prevLink = '<a href="javascript:void(0);" rel="'+prevId+'" class="view_prev btn_view">Previous</a>';
                    }
                    if($(curRow).next('tr').attr('id') !== undefined) {
                        nextId = ($(curRow).next('tr').attr('id')).replace('ApplicantRow-',''); 
                        nextLink = '<a href="javascript:void(0);" rel="'+nextId+'" class="view_next btn_view">Next<i class="icon-arrow-right"></i></a>';
                    } else {
                        nextId = ($(curRow).parent('tbody').find('tr:first').attr('id')).replace('ApplicantRow-',''); 
                        //nextLink = '<a href="javascript:void(0);" rel="'+nextId+'" class="view_next btn_view">Next</a>';
                    }
                    addPrevNextContent='<table class="prev-next-link-table"><tr><td>'+prevLink+'</td><td>'+nextLink+'</td></tr></table>';
                    $("#widget-view-inner-content").find('.table-borderless').before(addPrevNextContent); 
                });            
            });            
        }
    });

    $(document).on('click','.inactive.record', function(){
        var recordID = parseInt($(this).attr('data-id'));
        var activeValue = 1;
        if(recordID > 0){
            bootbox.confirm('Are you sure you want to change the status?', function (confirmed) {
                if(confirmed){
                    $.ajax({
                        url:  ADMIN_URL+'user/action',
                        data: {'recordID':recordID, 'activeValue':activeValue},
                        type: 'post',
                        success: function(result){
                            result = $.trim(result);
                            if(result == 'TRUE')
                            {
                                $('.alert-success').show();
                                $('.alert-success .message').html('User status changed successfully.');
                                setTimeout(function(){ $('.alert-success').fadeOut(4000); },3000);
                                $('#datatable_ajax').DataTable().ajax.reload();
                            } 
                            return false;
                        }
                    });
                }
            })
        }
    });

  $(document).on('click','.active.record', function(){
        var recordID = parseInt($(this).attr('data-id'));
        var activeValue = 0;
        if(recordID > 0){
            bootbox.confirm('Are you sure you want to change the status?', function (confirmed) {
                if(confirmed){
                    $.ajax({
                        url:  ADMIN_URL+'user/action',
                        data: {'recordID':recordID, 'activeValue':activeValue},
                        type: 'post',
                        success: function(result){
                            result = $.trim(result);
                            if(result == 'TRUE')
                            {
                                $('.alert-success').show();
                                $('.alert-success .message').html('User status changed successfully.');
                                setTimeout(function(){ $('.alert-success').fadeOut(4000); },3000);
                                $('#datatable_ajax').DataTable().ajax.reload();
                            } 
                            return false;
                        }
                    });
                }
            });
        }
    });

    $(document).on('click','.inactive.videorecord', function(){
        var recordID = parseInt($(this).attr('data-id'));
        var activeValue = 1;
        if(recordID > 0){
            bootbox.confirm('Are you sure you want to change the status?', function (confirmed) {
                if(confirmed){
                    $.ajax({
                        url:  ADMIN_URL+'videoTopic/action',
                        data: {'recordID':recordID, 'activeValue':activeValue},
                        type: 'post',
                        success: function(result){
                            result = $.trim(result);
                            if(result == 'TRUE')
                            {
                                $('.alert-success').show();
                                $('.alert-success .message').html('Video status changed successfully.');
                                setTimeout(function(){ $('.alert-success').fadeOut(4000); },3000);
                                $('#datatable_ajax').DataTable().ajax.reload();
                            } 
                            return false;
                        }
                    });
                }
            });
        }
    });

    $(document).on('click','.active.videorecord', function(){
        var recordID = parseInt($(this).attr('data-id'));
        var activeValue = 0;
        if(recordID > 0){
            bootbox.confirm('Are you sure you want to change the status?', function (confirmed) {
                if(confirmed){
                    $.ajax({
                        url:  ADMIN_URL+'videoTopic/action',
                        data: {'recordID':recordID, 'activeValue':activeValue},
                        type: 'post',
                        success: function(result){
                            result = $.trim(result);
                            if(result == 'TRUE')
                            {
                                $('.alert-success').show();
                                $('.alert-success .message').html('Video status changed successfully.');
                                setTimeout(function(){ $('.alert-success').fadeOut(4000); },3000);
                                $('#datatable_ajax').DataTable().ajax.reload();
                            } 
                            return false;
                        }
                    });
                }
            });
        }
    });

    $(document).on('click','.inactive.quiz', function(){
        var recordID = parseInt($(this).attr('data-id'));
        var activeValue = 1;
        if(recordID > 0){
            bootbox.confirm('Are you sure you want to change the status?', function (confirmed) {
                if(confirmed){
                    $.ajax({
                        url:  ADMIN_URL+'quiz/action',
                        data: {'recordID':recordID, 'activeValue':activeValue},
                        type: 'post',
                        success: function(result){
                            result = $.trim(result);
                            if(result == 'TRUE')
                            {
                                $('.alert-success').show();
                                $('.alert-success .message').html('Quiz status changed successfully.');
                                setTimeout(function(){ $('.alert-success').fadeOut(4000); },3000);
                                $('#datatable_ajax').DataTable().ajax.reload();
                            } 
                            return false;
                        }
                    });
                }
            });
        }
    });

    $(document).on('click','.active.quiz', function(){
        var recordID = parseInt($(this).attr('data-id'));
        var activeValue = 0;
        if(recordID > 0){
            bootbox.confirm('Are you sure you want to change the status?', function (confirmed) {
                if(confirmed){
                    $.ajax({
                        url:  ADMIN_URL+'quiz/action',
                        data: {'recordID':recordID, 'activeValue':activeValue},
                        type: 'post',
                        success: function(result){
                            result = $.trim(result);
                            if(result == 'TRUE')
                            {
                                $('.alert-success').show();
                                $('.alert-success .message').html('Quiz status changed successfully.');
                                setTimeout(function(){ $('.alert-success').fadeOut(4000); },3000);
                                $('#datatable_ajax').DataTable().ajax.reload();
                            } 
                            return false;
                        }
                    });
                }
            });
        }
    });

    $(document).on('click','.inactive.quiz_question', function(){
        var recordID = parseInt($(this).attr('data-id'));
        var activeValue = 1;
        if(recordID > 0){
            bootbox.confirm('Are you sure you want to change the status?', function (confirmed) {
                if(confirmed){
                    $.ajax({
                        url:  ADMIN_URL+'quiz_question/action',
                        data: {'recordID':recordID, 'activeValue':activeValue},
                        type: 'post',
                        success: function(result){
                            result = $.trim(result);
                            if(result == 'TRUE')
                            {
                                $('.alert-success').show();
                                $('.alert-success .message').html('Quiz status changed successfully.');
                                setTimeout(function(){ $('.alert-success').fadeOut(4000); },3000);
                                $('#datatable_ajax').DataTable().ajax.reload();
                            } 
                            return false;
                        }
                    });
                }
            });
        }
    });

    $(document).on('click','.active.quiz_question', function(){
        var recordID = parseInt($(this).attr('data-id'));
        var activeValue = 0;
        if(recordID > 0){
            bootbox.confirm('Are you sure you want to change the status?', function (confirmed) {
                if(confirmed){
                    $.ajax({
                        url:  ADMIN_URL+'quiz_question/action',
                        data: {'recordID':recordID, 'activeValue':activeValue},
                        type: 'post',
                        success: function(result){
                            result = $.trim(result);
                            if(result == 'TRUE')
                            {
                                $('.alert-success').show();
                                $('.alert-success .message').html('Quiz status changed successfully.');
                                setTimeout(function(){ $('.alert-success').fadeOut(4000); },3000);
                                $('#datatable_ajax').DataTable().ajax.reload();
                            } 
                            return false;
                        }
                    });
                }
            });
        }
    });
$(document).on('click','#edit_order_status', function(){
    var recordURL = $(this).attr('action-url');
    var send_data = $("#frmStatus").serialize();
    var form=$("body");
    $("#frmStatus .item_status").each(function () {
        var str=$(this).attr('id');
        var res = str.split("-");
        if($('#'+str).val()=="Canceled")
        {
            $('#d_back_ordered_date-'+res[1]).attr("readonly", true).val('');
            // $('#v_canceled_reason-'+res[1]).attr("readonly", false);
            $('#d_back_ordered_date-'+res[1]).removeClass('required');    
            // $('#v_canceled_reason-'+res[1]).addClass('required');    
        }else if($('#'+str).val()=="Back Ordered"){
            $('#d_back_ordered_date-'+res[1]).attr("readonly", false);
            // $('#v_canceled_reason-'+res[1]).attr("readonly", true).val('');
            $('#d_back_ordered_date-'+res[1]).addClass('required');    
            // $('#v_canceled_reason-'+res[1]).removeClass('required');    
        }else{
            $('#d_back_ordered_date-'+res[1]).attr("readonly", true).val('');
            // $('#v_canceled_reason-'+res[1]).attr("readonly", true).val('');
            // $('#v_canceled_reason-'+res[1]).removeClass('required');    
            $('#d_back_ordered_date-'+res[1]).removeClass('required');
        }
    });
    if(form_valid("#frmStatus")) 
    {
        $.post(recordURL, send_data, function(data)
        {
            if($.trim(data) == 'TRUE') 
            {
                $('#datatable_ajax').DataTable().ajax.reload();
                $("html, body").animate({ scrollTop: 0 }, 600);
                form.find('.alert-success').show();
                form.find('.alert-success').fadeOut(5000);
                form.find('.alert-danger').hide();
                form.find('.duplicate-error').hide();
                form.find('.duplicate_name_error').hide();
                form.find('#d_error').hide();
                
            }else
            {
                form.find('.alert-danger').fadeOut(5000);
            }
        });
    }
});
$("body").on('focas','.d_back_ordered_date',function(){
        $(this).datepicker({
        format: 'mm/dd/yyyy',
        todayHighlight:true,
        autoclose: true
        });  
});
$("body").on('focas','.d_vendor_expected_delivery_date',function(){
    $(this).datepicker({
    format: 'mm/dd/yyyy',
    todayHighlight:true,
    autoclose: true
    });  
});
$(document).on('change','.item_status', function(){
    var str=$(this).attr('id');
    var res = str.split("-");
    if($('#'+str).val()=="Canceled")
    {
        $('#d_back_ordered_date-'+res[1]).datepicker("remove");
        $('#d_back_ordered_date-'+res[1]).removeClass("d_back_ordered_date");
        $('#d_back_ordered_date-'+res[1]).unbind();
        $('#d_back_ordered_date-'+res[1]).attr("readonly", true).val('');
        // $('#v_canceled_reason-'+res[1]).attr("readonly", false);
        $('#d_back_ordered_date-'+res[1]).removeClass('required');    
        // $('#v_canceled_reason-'+res[1]).addClass('required');    
    }else if($('#'+str).val()=="Back Ordered"){
        $('#d_back_ordered_date-'+res[1]).addClass("d_back_ordered_date");
         $('#d_back_ordered_date-'+res[1]).datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight:true,
            autoclose: true
        }); 
        $('#d_back_ordered_date-'+res[1]).attr("readonly", false);
        // $('#v_canceled_reason-'+res[1]).attr("readonly", true).val('');
        $('#d_back_ordered_date-'+res[1]).addClass('required');    
        // $('#v_canceled_reason-'+res[1]).removeClass('required');
        // $('#v_canceled_reason-'+res[1]).val('');
    }else{
        $('#d_back_ordered_date-'+res[1]).datepicker("remove");
        $('#d_back_ordered_date-'+res[1]).removeClass("d_back_ordered_date");
        $('#d_back_ordered_date-'+res[1]).unbind();
        $('#d_back_ordered_date-'+res[1]).attr("readonly", true).val('');
        // $('#v_canceled_reason-'+res[1]).attr("readonly", true).val('');
        // $('#v_canceled_reason-'+res[1]).removeClass('required');    
        $('#d_back_ordered_date-'+res[1]).removeClass('required');
        $('#d_back_ordered_date-'+res[1]).val('');
        // $('#v_canceled_reason-'+res[1]).val('');
        
        
    }
});
$(document).on('change','.purchase_item_status', function(){
    var str=$(this).attr('id');
    var res = str.split("-");
    
    if ($('#'+str).val() == "Back Ordered") {
        $('#d_back_ordered_date-'+res[1]).addClass("d_back_ordered_date");
        $('#d_back_ordered_date-'+res[1]).datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight:true,
            autoclose: true
        }); 
        $('#d_back_ordered_date-'+res[1]).attr("readonly", false);
        $('#d_back_ordered_date-'+res[1]).addClass('required');
        $('#d_vendor_expected_delivery_date-'+res[1]).datepicker("remove");
        $('#d_vendor_expected_delivery_date-'+res[1]).removeClass("d_back_ordered_date");
        $('#d_vendor_expected_delivery_date-'+res[1]).unbind();
        $('#d_vendor_expected_delivery_date-'+res[1]).attr("readonly", true).val('');
        $('#d_vendor_expected_delivery_date-'+res[1]).removeClass('required');     
       
    } else if ($('#'+str).val() == "Confirm and Approved") {
        $('#d_vendor_expected_delivery_date-'+res[1]).addClass("d_vendor_expected_delivery_date");
        $('#d_vendor_expected_delivery_date-'+res[1]).datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight:true,
            autoclose: true
        }); 
        $('#d_vendor_expected_delivery_date-'+res[1]).attr("readonly", false);
        $('#d_vendor_expected_delivery_date-'+res[1]).addClass('required');    
        $('#d_back_ordered_date-'+res[1]).datepicker("remove");
        $('#d_back_ordered_date-'+res[1]).removeClass("d_back_ordered_date");
        $('#d_back_ordered_date-'+res[1]).unbind();
        $('#d_back_ordered_date-'+res[1]).attr("readonly", true).val('');
        $('#d_back_ordered_date-'+res[1]).removeClass('required');
        $('#d_back_ordered_date-'+res[1]).val('');
    } else if($('#'+str).val() == "Partial Inventory") {
        $('#d_vendor_expected_delivery_date-'+res[1]).addClass("d_vendor_expected_delivery_date");
        $('#d_vendor_expected_delivery_date-'+res[1]).datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight:true,
            autoclose: true
        }); 
        $('#d_vendor_expected_delivery_date-'+res[1]).attr("readonly", false);
        $('#d_vendor_expected_delivery_date-'+res[1]).addClass('required');

        $('#d_back_ordered_date-'+res[1]).addClass("d_back_ordered_date");
        $('#d_back_ordered_date-'+res[1]).datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight:true,
            autoclose: true
        }); 
        $('#d_back_ordered_date-'+res[1]).attr("readonly", false);
        $('#d_back_ordered_date-'+res[1]).addClass('required');

        $('#i_qty_on_hand-'+res[1]).attr("readonly", false).val('');
        $('#i_qty_on_hand-'+res[1]).addClass('required');            

    } else {
        $('#d_back_ordered_date-'+res[1]).datepicker("remove");
        $('#d_back_ordered_date-'+res[1]).removeClass("d_back_ordered_date");
        $('#d_back_ordered_date-'+res[1]).unbind();
        $('#d_back_ordered_date-'+res[1]).attr("readonly", true).val('');
        $('#d_back_ordered_date-'+res[1]).removeClass('required');
        $('#d_back_ordered_date-'+res[1]).val('');
        $('#d_vendor_expected_delivery_date-'+res[1]).datepicker("remove");
        $('#d_vendor_expected_delivery_date-'+res[1]).removeClass("d_back_ordered_date");
        $('#d_vendor_expected_delivery_date-'+res[1]).unbind();
        $('#d_vendor_expected_delivery_date-'+res[1]).attr("readonly", true).val('');
        $('#d_vendor_expected_delivery_date-'+res[1]).removeClass('required');    
        $('#i_qty_on_hand-'+res[1]).attr("readonly", true).val('');
        $('#i_qty_on_hand-'+res[1]).removeClass('required');    
    }
});

$(document).on('click','#edit_purchase_order_status', function(){
    var recordURL = $(this).attr('action-url');
    var send_data = $("#frmStatus").serialize();
    var form=$("body");
    var err_flg = 0;
    $("#frmStatus .purchase_item_status").each(function () {
        var str=$(this).attr('id');
        var res = str.split("-");
        if($('#'+str).val() == "Back Ordered") {
            $('#d_back_ordered_date-'+res[1]).attr("readonly", false);
            $('#d_back_ordered_date-'+res[1]).addClass('required'); 
            $('#d_vendor_expected_delivery_date-'+res[1]).attr("readonly", true).val('');
            $('#d_vendor_expected_delivery_date-'+res[1]).removeClass('required');   
        } else if ($('#'+str).val() == "Confirm and Approved") {
            $('#d_vendor_expected_delivery_date-'+res[1]).attr("readonly", false);
            $('#d_vendor_expected_delivery_date-'+res[1]).addClass('required'); 
            $('#d_back_ordered_date-'+res[1]).attr("readonly", true).val('');
            $('#d_back_ordered_date-'+res[1]).removeClass('required');
        } else if ($('#'+str).val() == "Partial Inventory") {
            $('#d_vendor_expected_delivery_date-'+res[1]).addClass("d_vendor_expected_delivery_date");
            $('#d_vendor_expected_delivery_date-'+res[1]).datepicker({
                format: 'mm/dd/yyyy',
                todayHighlight:true,
                autoclose: true
            }); 
            $('#d_vendor_expected_delivery_date-'+res[1]).attr("readonly", false);
            $('#d_vendor_expected_delivery_date-'+res[1]).addClass('required');

            $('#d_back_ordered_date-'+res[1]).addClass("d_back_ordered_date");
            $('#d_back_ordered_date-'+res[1]).datepicker({
                format: 'mm/dd/yyyy',
                todayHighlight:true,
                autoclose: true
            }); 
            $('#d_back_ordered_date-'+res[1]).attr("readonly", false);
            $('#d_back_ordered_date-'+res[1]).addClass('required');

            $('#i_qty_on_hand-'+res[1]).attr("readonly", false);
            $('#i_qty_on_hand-'+res[1]).addClass('required');             
            if($('#i_qty_on_hand-'+res[1]).val() <= 0 && $('#i_qty_on_hand-'+res[1]).val() != ''){                
                $('#i_qty_on_hand-'+res[1]+'number_error').show();
                 err_flg = 1;
            }else{
                $('#i_qty_on_hand-'+res[1]+'number_error').hide();
            }
        } else {
            $('#d_back_ordered_date-'+res[1]).attr("readonly", true).val('');
            $('#d_back_ordered_date-'+res[1]).removeClass('required');
            $('#d_vendor_expected_delivery_date-'+res[1]).attr("readonly", true).val('');
            $('#d_vendor_expected_delivery_date-'+res[1]).removeClass('required');
            $('#i_qty_on_hand-'+res[1]).removeClass('required');
            $('#i_qty_on_hand-'+res[1]).attr("readonly", true).val('');
            $('#i_qty_on_hand-'+res[1]+'number_error').hide();
        }
    });    
    if(form_valid("#frmStatus") && err_flg==0) 
    {
        $.post(recordURL, send_data, function(data)
        {
            if($.trim(data) == 'TRUE') 
            {
                $('#datatable_ajax').DataTable().ajax.reload();
                setTimeout(function(){
                    $('.d_back_ordered_date').datepicker({
                        format: 'mm/dd/yyyy',
                        todayHighlight:true,
                        autoclose: true
                    });   
                    $('.d_vendor_expected_delivery_date').datepicker({
                        format: 'mm/dd/yyyy',
                        todayHighlight:true,
                        autoclose: true
                    });   
                },1000);
                $("html, body").animate({ scrollTop: 0 }, 600);
                form.find('.alert-success').show();
                form.find('.alert-success').fadeOut(5000);
                form.find('.alert-danger').hide();
                form.find('.duplicate-error').hide();
                form.find('.duplicate_name_error').hide();
                form.find('#d_error').hide();
                
            }else
            {
                form.find('.alert-danger').fadeOut(5000);
            }
      });
    }
});