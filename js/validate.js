function handleLoginForm(){

    if (window.localStorage) {
        localStorage.clear();
    }
    
    $('#frmEmpLogin').validate({
        errorElement: 'span', 
        errorClass: 'help-block', 
        focusInvalid: false, 
        rules: {
           uname: {
                required: true,
                //email: true
            },
            password: {
                required: true,
            },
            remember: {
                required: false
            }
        },    
        messages: {
            uname: {
                required: "Please enter email or username.",
                //email: "Please enter valid email id."
            },
            password: {
                required: "Please enter Password."
            }
        },
    
        invalidHandler: function(event, validator) {  
            $('.alert-danger', $('#frmEmpLogin')).show();
        },
    
        highlight: function(element) { 
            $(element).closest('.form-group').addClass('has-error'); 
        },
    
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
    
        errorPlacement: function(error, element) {
            error.insertAfter(element.closest('.input-icon'));
        },
    
        submitHandler: function(form) {
            form.submit(); 
        }
    });
    $('#forget-password').click(function()
    {
        $(".alert").remove();
        $('.help-block').remove();
        $('.form-group').removeClass('has-error');
        $('#loginform').hide();
        $('#forget_password').show();
    });

    $('#back-login').click(function()
    {
        $(".alert").remove();
        $('.help-block').remove();
        $('.form-group').removeClass('has-error');
        $('#loginform').show();
        $('#forget_password').hide();
    });
    $('#forgot_pass_form').validate({
        errorElement: 'span', 
        errorClass: 'help-block', 
        focusInvalid: false,         
        invalidHandler: function(event, validator) {  
            $('.alert-danger', $('#forgot_pass_form')).show();
        },

        highlight: function(element) { 
            $(element).closest('.form-group').addClass('has-error'); 
        },

        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },

        errorPlacement: function(error, element) {
            error.insertAfter(element.closest('.input-icon'));
        },

        submitHandler: function(form) {
            form.submit(); 
            $("#forgot_btn").attr('disabled',true);
        }
    });
    
    $('#forgot_pass_client_form').validate({
        errorElement: 'span', 
        errorClass: 'help-block', 
        focusInvalid: false, 
        rules: {
            email: {
                required: true,
                email: true
            }
        },
         messages: { 
         email: {
                required: "Email address is required."
            }
        },
        invalidHandler: function(event, validator) {  
            $('.alert-danger', $('#forgot_pass_client_form')).show();
        },

        highlight: function(element) { 
            $(element).closest('.form-group').addClass('has-error'); 
        },

        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },

        errorPlacement: function(error, element) {
            error.insertAfter(element.closest('.input-icon'));
        },

        submitHandler: function(form) {
            form.submit(); 
            $("#forgot_btn").attr('disabled',true);
        }
    });

    $.validator.addMethod("passwordStrenth", function(value, element) {
        if($('#password').hasClass('passMeter-strong')){
            return true;
        } else {
            return false;
        }
        return this.optional(element) || value != 'default' ;
    }, " Please enter valid password.");
    //Password strenth is not strong enough. (Use combination of characters, special characters and numbers)
    $('#reset_pass_form').validate({
        errorElement: 'span', 
        errorClass: 'help-block', 
        focusInvalid: false, 
        rules: {
            v_email:{
                required: true
            },
            password: {
                required: true,
                passwordStrenth : true,
                minlength: 6
            },
            rpassword: {
                required: true,
                equalTo: "#password"
            }
        },
         messages: {
         v_email: {
                required: "Please enter email id."
            },
         password: {
                required: "Password is required.",
                minlength: "Minimum 6 characters required."
            },
         rpassword: {
                required: "Confirm password is required."
            }
        },
        invalidHandler: function(event, validator) {  
            $('.alert-danger', $('#forgot_pass_form')).show();
        },

        highlight: function(element) { 
            $(element).closest('.form-group').addClass('has-error'); 
        },

        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },

        errorPlacement: function(error, element) {
            error.insertAfter(element.closest('.input-icon'));
        },

        submitHandler: function(form) {
            form.submit(); 
        }
    });
}