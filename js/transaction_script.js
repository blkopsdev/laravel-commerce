var f_amount;
var f_amount_bank;
$(document).ready(function() {
    $('body').on('click','#f_amount',function(){
        f_amount = $('#f_amount').val();
        if($('#f_amount').val() <= 0){
            $('#f_amount').val('');
        }
    });
    $('body').on('click','#f_amount_share',function(){
        f_amount = $('#f_amount_share').val();
        if($('#f_amount_share').val() <= 0){
            $('#f_amount_share').val('');
        }
    });
    $('body').on('blur','#f_amount',function(){
        if($('#f_amount').val() > 0){
            f_amount = $('#f_amount').val();
            $('#f_amount').val(f_amount);
        }else{
            f_amount = '';
            $('#f_amount').val('0.00');
        }
    });
    $('body').on('blur','#f_amount_share',function(){
        if($('#f_amount_share').val() > 0){
            f_amount = $('#f_amount_share').val();
            $('#f_amount_share').val(f_amount);
        }else{
            f_amount = '';
            $('#f_amount_share').val('0.00');
        }
    });
    $('body').on('click','#f_amount_voucher_meter',function(){
        f_amount_voucher_meter = $('#f_amount_voucher_meter').val();
        if($('#f_amount_voucher_meter').val() <= 0){
            $('#f_amount_voucher_meter').val('');
        }
    });
    $('body').on('blur','#f_amount_voucher_meter',function(){
        if($('#f_amount_voucher_meter').val() > 0){
            f_amount_voucher_meter = $('#f_amount_voucher_meter').val();
            $('#f_amount_voucher_meter').val(f_amount_voucher_meter);
        }else{
            f_amount_voucher_meter = '';
            $('#f_amount_voucher_meter').val('0.00');
        }
    });
    $('body').on('click','#f_amount_bank',function(){
        f_amount_bank = $('#f_amount_bank').val();
        if($('#f_amount_bank').val() <= 0){
            $('#f_amount_bank').val('');
        }
    });
    $('body').on('blur','#f_amount_bank',function(){
        if($('#f_amount_bank').val() > 0){
            f_amount_bank = $('#f_amount_bank').val();
            $('#f_amount_bank').val(f_amount_bank);
        }else{
            f_amount_bank = '';
            $('#f_amount_bank').val('0.00');
        }
    });
    $('body').on('blur','#f_amount_withdraw',function(){
        if($('#f_amount_withdraw').val() > 0){
            f_amount = $('#f_amount_withdraw').val();
            $('#f_amount_withdraw').val(f_amount);
        }else{
            f_amount = '';
            $('#f_amount_withdraw').val('0.00');
        }
    });
    $('#f_amount').keydown(function(event){
        if ((event.Keycode != 46 || $(this).val().indexOf('.') != -1) && ((event.Keycode < 48 || event.Keycode > 57) && event.Keycode != 8 && event.Keycode != 0)){
            event.preventDefault();
        }else{
            setTimeout(function(){
                if($('#f_amount').val() == '0.00'){
                    var t = $("#frmTrans").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    $("#frmTrans").find('.your_total').html('<strong> p'+t_r+'</strong>');
                } else{
                    var f_amount = parseFloat($('#f_amount').val());
                    var t = $("#frmTrans").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    var your_total = (parseFloat(f_amount) + parseFloat(t_r)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                    $("#frmTrans").find('.your_total').html('<strong>p '+your_total+'</strong>');
                    var y_total = parseFloat(f_amount) + parseFloat(t_r);
                    $("#frmTrans").find('.f_total_amount').val(y_total);
                }
                if($('#f_amount').val() == ''){
                    var t = $("#frmTrans").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    $("#frmTrans").find('.your_total').html('<strong>p '+t_r+'</strong>');
                }
           },100);
        }
    });
    $('#f_amount_share').keydown(function(event){
        if ((event.Keycode != 46 || $(this).val().indexOf('.') != -1) && ((event.Keycode < 48 || event.Keycode > 57) && event.Keycode != 8 && event.Keycode != 0)){
            event.preventDefault();
        }else{
            setTimeout(function(){
                if($('#f_amount_share').val() == '0.00'){
                    var t = $("#frmTrans").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    $("#frmTrans").find('.your_total').html('<strong> p'+t_r+'</strong>');
                } else{
                    var f_amount = parseFloat($('#f_amount_share').val());
                    var t = $("#frmTransShare").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    var your_total = (parseFloat(f_amount) + parseFloat(t_r)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                    $("#frmTransShare").find('.your_total').html('<strong>p '+your_total+'</strong>');
                    var y_total = parseFloat(f_amount) + parseFloat(t_r);
                    $("#frmTransShare").find('.f_total_amount').val(y_total);
                }
                if($('#f_amount_share').val() == ''){
                    var t = $("#frmTransShare").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    $("#frmTransShare").find('.your_total').html('<strong>p '+t_r+'</strong>');
                }
           },100);
           
        }
    });
    $('#f_amount_withdraw').keydown(function(event){
        if ((event.Keycode != 46 || $(this).val().indexOf('.') != -1) && ((event.Keycode < 48 || event.Keycode > 57) && event.Keycode != 8 && event.Keycode != 0)){
            event.preventDefault();
        }else{
            setTimeout(function(){
                if($('#f_amount_withdraw').val() == '0.00'){
                    var t = $("#frmTrans").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    $("#frmTrans").find('.your_total').html('<strong> p'+t_r+'</strong>');
                } else{
                    var f_amount = parseFloat($('#f_amount_withdraw').val());
                    var t = $("#frmTransWithdraw").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    var your_total = (parseFloat(f_amount) + parseFloat(t_r)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                    $("#frmTransWithdraw").find('.your_total').html('<strong>p '+your_total+'</strong>');
                    var y_total = parseFloat(f_amount) + parseFloat(t_r);
                    $("#frmTransWithdraw").find('.f_total_amount').val(y_total);
                }
                if($('#f_amount_withdraw').val() == ''){
                    var t = $("#frmTransWithdraw").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    $("#frmTransWithdraw").find('.your_total').html('<strong>p '+t_r+'</strong>');
                }
           },100);
           
        }
    });
    $('#f_amount_voucher_meter').keydown(function(event){
        if ((event.Keycode != 46 || $(this).val().indexOf('.') != -1) && ((event.Keycode < 48 || event.Keycode > 57) && event.Keycode != 8 && event.Keycode != 0)){
            event.preventDefault();
        } else {
            setTimeout(function(){
                if($('#f_amount_voucher_meter').val() == '0.00'){
                    var t = $("#frmTransVoucherMeter").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    $("#frmTransVoucherMeter").find('.your_total').html('<strong>p '+t_r+'</strong>');
                } else {
                    var f_amount = parseFloat($('#f_amount_voucher_meter').val());
                    var t = $("#frmTransVoucherMeter").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    var your_total = (parseFloat(f_amount) + parseFloat(t_r)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                    $("#frmTransVoucherMeter").find('.your_total').html('<strong>p '+your_total+'</strong>');
                    var y_total = parseFloat(f_amount) + parseFloat(t_r);
                    $("#frmTransVoucherMeter").find('.f_total_amount').val(y_total);  
                }
                if($('#f_amount_voucher_meter').val() == ''){
                    var t = $("#frmTransVoucherMeter").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    $("#frmTransVoucherMeter").find('.your_total').html('<strong>p '+t_r+'</strong>');
                }
                
            },100);
        }
    });
    $('#f_amount_bank').keydown(function(event){
        if ((event.Keycode != 46 || $(this).val().indexOf('.') != -1) && ((event.Keycode < 48 || event.Keycode > 57) && event.Keycode != 8 && event.Keycode != 0)){
            event.preventDefault();
        } else {
            setTimeout(function(){
                if($('#f_amount_bank').val() == '0.00'){
                    var t = $("#frmBankTrans").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    $("#frmBankTrans").find('.your_total').html('<strong>p '+t_r+'</strong>');
                } else {
                    var f_amount = parseFloat($('#f_amount_bank').val());
                    var t = $("#frmBankTrans").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    var your_total = (parseFloat(f_amount) + parseFloat(t_r)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                    $("#frmBankTrans").find('.your_total').html('<strong>p '+your_total+'</strong>');
                    var y_total = parseFloat(f_amount) + parseFloat(t_r);
                    $("#frmBankTrans").find('.f_total_amount').val(y_total);  
                }
                if($('#f_amount_bank').val() == ''){
                    var t = $("#frmBankTrans").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    $("#frmBankTrans").find('.your_total').html('<strong>p '+t_r+'</strong>');
                }
                
            },100);
        }
    });
    // Change Form
    
    $(document).on("click" , ".prepaid_service_div" , function(){
            if($('#f_amount_voucher').val() == 'null' || $('#f_amount_voucher').val() == '' ){
                var t = $("#frmTransVoucher").find('.transcation_fee').text();
                var t_r = t.replace('p','');
                $("#frmTransVoucher").find('.your_total').html('<strong>p '+t_r+'</strong>');
            } 
            $("#frmTransVoucher").css("display", "block");
            $("#frmTrans").css("display", "none");
            $("#frmBankTrans").css("display",'none');
            $('#bankservice').hide();
            $('#frmBankTrans')[0].reset();
            $('#frmTrans')[0].reset();
            $('#frmTransVoucher')[0].reset();
            $('#prepaidservice').hide();
            $('#prepaidservice_voucher').show();
            $("#frmTransShare").css("display", "none");
            $('#prepaidservice_voucher_meter').hide();
            $('#prepaidservice_share').hide();
            $('#error_bal').hide();
            $('#error_bal_service').hide();
            $('#error_bal_voucher').hide();
            $('#error_bal_share').hide();
            $('#error_voucher').hide();
            $('.error-inner').hide();
            $('#prepaidservice_withdraw').hide();
            var i = $(this).attr('src');
            $('.chnge_prepaid_service').attr('src',i);
            var id = $(this).attr('rel');
            var service_type = prepaid_service_image[id].e_service_type;
            var meter_type = prepaid_service_image[id].e_is_meter;
            var calculation_type = prepaid_service_image[id].e_calculation_type;
            if(service_type == 'Voucher' && meter_type == 'No'){
                $('.bank-logo').removeClass('active');
                $.ajax({
                    type: 'post',
                    url: SITE_URL+'transaction/service-type',
                    data: {'service_id':id},
                    datatype:'json',
                    success: function(data){
                        
                        
                        $('#f_amount_voucher').html('');
                        if(data == ''){
                            $('#f_amount_voucher').html('');
                           $('#f_amount_voucher').append('<option value="">' + "No Voucher Available" +'</option>');
                            var t = $("#frmTransVoucher").find('.transcation_fee').text();
                            var t_r = t.replace('p','');
                            $("#frmTransVoucher").find('.your_total').html('<strong>p '+t_r+'</strong>');
                        }else if(data == 'False'){
                            $('#f_amount_voucher').html('');
                            $('#f_amount_voucher').append('<option value="">' + "No Voucher Available" +'</option>');
                            var t = $("#frmTransVoucher").find('.transcation_fee').text();
                            var t_r = t.replace('p','');
                            $("#frmTransVoucher").find('.your_total').html('<strong>p '+t_r+'</strong>');
                        }else{
                            data = $.parseJSON(data);
                            $.each(data , function(k, m){
                                $.each(m, function(i, v){  
                                var value = v.v_voucher_type.substr(1);
                                if(v.v_voucher_type_data == ''){
                                    $('#f_amount_voucher').append('<option value="'+value+'">' + v.v_voucher_type + '</option>');
                                } else{
                                    $('#f_amount_voucher').append('<option value="'+value+'">' + v.v_voucher_type + " - " + v.v_voucher_type_data + '</option>');
                                }
                                
                            });
                         }); 
                         if($('#f_amount_voucher').val() == ''){
                            var t = $("#frmTransVoucher").find('.transcation_fee').text();
                            var t_r = t.replace('p','');
                            $("#frmTransVoucher").find('.your_total').html('<strong>p '+t_r+'</strong>');
                         } 
                          var f_amount = $('#f_amount_voucher').val();
                          //$(this).(div:first-child).addClass('active');
                            $(this).closest('.bank-logo').addClass('active');
                            var a = parseFloat(prepaid_service_image[id].f_paypro_transaction_share);
                            var b = parseFloat(prepaid_service_image[id].f_vendor_transaction_share);
                            if(calculation_type == 'ByPercent'){
                                var c = parseFloat(prepaid_service_image[id].f_transaction_share);
                                if(c == "0.00"){
                                    var total = "0.00";
                                } else {
                                    /* var total = parseFloat((f_amount*c)/100).toFixed(2);
                                    var payproshare =  parseFloat((total*a)/100).toFixed(2);
                                    var vendorshare =  parseFloat((total*b)/100).toFixed(2);
                                    $("#frmTransVoucher").find('.f_paypro_transaction_share').val(payproshare);
                                    $("#frmTransVoucher").find('.f_vendor_transaction_share').val(vendorshare); */ 
                                    var total_share = parseFloat((f_amount*c)/100).toFixed(2);
                                    var payproshare =  parseFloat((total_share*a)/100).toFixed(2);
                                    var vendorshare =  parseFloat((total_share*b)/100).toFixed(2);
                                    var total = parseFloat(vendorshare).toFixed(2);
                                    $("#frmTransVoucher").find('.f_paypro_transaction_share').val(payproshare);
                                    $("#frmTransVoucher").find('.f_vendor_transaction_share').val(vendorshare);
                                }
                            } else{
                                var total = parseFloat(b).toFixed(2);
                                $("#frmTransVoucher").find('.f_paypro_transaction_share').val(a);
                                $("#frmTransVoucher").find('.f_vendor_transaction_share').val(b); 
                            }
                            /*$("#frmTransVoucher").find('.transcation_fee').html('<strong>p '+total+'</strong>');
                            var your_total = (parseFloat(f_amount) + parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                            $("#frmTransVoucher").find('.your_total').html('<strong>p '+your_total+'</strong>');
                            y_total = parseFloat(f_amount) + parseFloat(total);
                            $("#frmTransVoucher").find('.f_total_amount').val(y_total);*/
                            $("#frmTransVoucher").find('.transcation_fee').html('<strong>p '+vendorshare+'</strong>');
                            var your_total = (parseFloat(f_amount) - parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                            $("#frmTransVoucher").find('.your_total').html('<strong>p '+(parseFloat(f_amount)).toFixed(2)+'</strong>');
                            y_total = parseFloat(f_amount) - parseFloat(total);
                            $("#frmTransVoucher").find('.f_total_amount').val(y_total);
                            var s_id = prepaid_service_image[id].id;
                            $("#frmTransVoucher").find('.i_service_id').val(s_id);
                            $(document).on("change" , "#f_amount_voucher" , function(){
                                if(calculation_type == 'ByPercent'){
                                    var f_amount = $('#f_amount_voucher').val();
                                    var c = parseFloat(prepaid_service_image[id].f_transaction_share);
                                    var total_share = parseFloat((f_amount*c)/100).toFixed(2);
                                    var payproshare =  parseFloat((total_share*a)/100).toFixed(2);
                                    var vendorshare =  parseFloat((total_share*b)/100).toFixed(2);
                                    var total = parseFloat(vendorshare).toFixed(2);
                                    $("#frmTransVoucher").find('.f_paypro_transaction_share').val(payproshare);
                                    $("#frmTransVoucher").find('.f_vendor_transaction_share').val(vendorshare); 
                                } else{
                                    var total = parseFloat(b).toFixed(2);
                                    $("#frmTransVoucher").find('.f_paypro_transaction_share').val(a);
                                    $("#frmTransVoucher").find('.f_vendor_transaction_share').val(b); 
                                }
                               /* $("#frmTransVoucher").find('.transcation_fee').html('<strong>p '+total+'</strong>');
                                var your_total = (parseFloat(f_amount) + parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                                $("#frmTransVoucher").find('.your_total').html('<strong>p '+your_total+'</strong>');
                                y_total = parseFloat(f_amount) + parseFloat(total);
                                $("#frmTransVoucher").find('.f_total_amount').val(y_total);
                                var s_id = prepaid_service_image[id].id;
                                $("#frmTransVoucher").find('.i_service_id').val(s_id);*/
                                $("#frmTransVoucher").find('.transcation_fee').html('<strong>p '+vendorshare+'</strong>');
                                var your_total = (parseFloat(f_amount) - parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                                $("#frmTransVoucher").find('.your_total').html('<strong>p '+(parseFloat(f_amount)).toFixed(2)+'</strong>');
                                y_total = parseFloat(f_amount) - parseFloat(total);
                                $("#frmTransVoucher").find('.f_total_amount').val(y_total);
                                var s_id = prepaid_service_image[id].id;
                                $("#frmTransVoucher").find('.i_service_id').val(s_id);
                            });
                        }
                    }
            });
            if($('#f_amount_voucher').val() == ''){
                    var t = $("#frmTransVoucher").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    $("#frmTransVoucher").find('.your_total').html('<strong>p '+t_r+'</strong>');
            }
            $(this).closest('.bank-logo').addClass('active');
            var a = parseFloat(prepaid_service_image[id].f_paypro_transaction_share);
            var b = parseFloat(prepaid_service_image[id].f_vendor_transaction_share);
                var total = parseFloat(b).toFixed(2);
                $("#frmTransVoucher").find('.f_paypro_transaction_share').val(a);
                $("#frmTransVoucher").find('.f_vendor_transaction_share').val(b); 
            
            $("#frmTransVoucher").find('.transcation_fee').html('<strong>p '+total+'</strong>');
            var your_total = (parseFloat(f_amount) - parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            $("#frmTransVoucher").find('.your_total').html('<strong>p '+(parseFloat(f_amount)).toFixed(2)+'</strong>');
            y_total = parseFloat(f_amount) - parseFloat(total);
            $("#frmTransVoucher").find('.f_total_amount').val(y_total);
            var s_id = prepaid_service_image[id].id;
            $("#frmTransVoucher").find('.i_service_id').val(s_id);
            //$("#frmTransVoucher").find('.f_paypro_transaction_share').val(a);
            //$("#frmTransVoucher").find('.f_vendor_transaction_share').val(b); 
           
        } else if(service_type == 'Voucher' && meter_type == 'Yes'){
                $("#frmTransVoucherMeter").css("display", "block");
                $("#frmTransVoucher").css("display", "none");
                $("#frmTrans").css("display", "none");
                $("#frmBankTrans").css("display",'none');
                $('#bankservice').hide();
                $('#frmBankTrans')[0].reset();
                $('#frmTrans')[0].reset();
                $('#frmTransVoucher')[0].reset();
                $('#prepaidservice').hide();
                $('#prepaidservice_voucher_meter').show();
                $('#prepaidservice_voucher').hide();
                $("#frmTransShare").css("display", "none");
                $('#prepaidservice_share').hide();
                $('#error_bal').hide();
                $('#error_bal_service').hide();
                $('#error_bal_voucher').hide();
                $('#error_bal_share').hide();
                $('#error_voucher').hide();
                $('.error-inner').hide();
                $('#prepaidservice_withdraw').hide();
                $('.bank-logo').removeClass('active');
           
            if($('#f_amount_voucher_meter').val() == ''){
                    var t = $("#frmTransVoucherMeter").find('.transcation_fee').text();
                    var t_r = t.replace('p','');
                    $("#frmTransVoucherMeter").find('.your_total').html('<strong>p '+t_r+'</strong>');
            }
            $(this).closest('.bank-logo').addClass('active');
            
            var f_amount = parseFloat($('#f_amount_voucher_meter').val());
            var a = parseFloat(prepaid_service_image[id].f_paypro_transaction_share);
            var b = parseFloat(prepaid_service_image[id].f_vendor_transaction_share);
            if(calculation_type == 'ByPercent'){
                var c = parseFloat(prepaid_service_image[id].f_transaction_share);
                var total_share = parseFloat((f_amount*c)/100).toFixed(2);
                var payproshare =  parseFloat((total_share*a)/100).toFixed(2);
                var vendorshare =  parseFloat((total_share*b)/100).toFixed(2);
                /*var total = parseFloat((f_amount*c)/100).toFixed(2);*/
                var total = parseFloat(vendorshare).toFixed(2);
                $("#frmTransVoucherMeter").find('.f_paypro_transaction_share').val(payproshare);
                $("#frmTransVoucherMeter").find('.f_vendor_transaction_share').val(vendorshare); 
            } else{
                /*var total = parseFloat(a+b).toFixed(2);*/
                var total = parseFloat(b).toFixed(2);
                $("#frmTransVoucherMeter").find('.f_paypro_transaction_share').val(a);
                $("#frmTransVoucherMeter").find('.f_vendor_transaction_share').val(b); 
            }
            $("#frmTransVoucherMeter").find('.transcation_fee').html('<strong>p '+total+'</strong>');
            var f_amount = parseFloat($('#f_amount_voucher_meter').val());
            /*var your_total = (parseFloat(f_amount) + parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");*/
            var your_total = (parseFloat(f_amount) - parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            $("#frmTransVoucherMeter").find('.your_total').html('<strong>p '+(parseFloat(f_amount)).toFixed(2)+'</strong>');
            /*y_total = parseFloat(f_amount) + parseFloat(total);*/
            y_total = parseFloat(f_amount) - parseFloat(total);
            $("#frmTransVoucherMeter").find('.f_total_amount').val(y_total);
            var s_id = prepaid_service_image[id].id;
            $("#frmTransVoucherMeter").find('.i_service_id').val(s_id);
            //$("#frmTransWithdraw").find('.f_paypro_transaction_share').val(a);
            //$("#frmTransWithdraw").find('.f_vendor_transaction_share').val(b);
            
            $('#f_amount_voucher_meter').keydown(function(event){
            if ((event.Keycode != 46 || $(this).val().indexOf('.') != -1) && ((event.Keycode < 48 || event.Keycode > 57) && event.Keycode != 8 && event.Keycode != 0)){
                event.preventDefault();
            } else {
                setTimeout(function(){
                    if($('#f_amount_voucher_meter').val() == '0.00'){
                        var t = $("#frmTransVoucherMeter").find('.transcation_fee').text();
                        var t_r = t.replace('p','');
                        $("#frmTransVoucherMeter").find('.your_total').html('<strong>p '+t_r+'</strong>');
                    } else {
                        if(calculation_type == 'ByPercent'){
                            var f_amount = $('#f_amount_voucher_meter').val();
                            var c = parseFloat(prepaid_service_image[id].f_transaction_share);
                            var total_share = parseFloat((f_amount*c)/100).toFixed(2);
                            var payproshare =  parseFloat((total_share*a)/100).toFixed(2);
                            var vendorshare =  parseFloat((total_share*b)/100).toFixed(2);
                            var total = parseFloat(vendorshare).toFixed(2);
                            $("#frmTransVoucherMeter").find('.f_paypro_transaction_share').val(payproshare);
                            $("#frmTransVoucherMeter").find('.f_vendor_transaction_share').val(vendorshare); 
                        } else{
                            var total = parseFloat(b).toFixed(2);
                            $("#frmTransVoucherMeter").find('.f_paypro_transaction_share').val(a);
                            $("#frmTransVoucherMeter").find('.f_vendor_transaction_share').val(b); 
                        }
                        $("#frmTransVoucherMeter").find('.transcation_fee').html('<strong>p '+total+'</strong>');
                        var your_total = (parseFloat(f_amount) - parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                        $("#frmTransVoucherMeter").find('.your_total').html('<strong>p '+(parseFloat(f_amount)).toFixed(2)+'</strong>');
                        y_total = parseFloat(f_amount) - parseFloat(total);
                        $("#frmTransVoucherMeter").find('.f_total_amount').val(y_total);
                        var s_id = prepaid_service_image[id].id;
                        $("#frmTransVoucherMeter").find('.i_service_id').val(s_id);
                        var f_amount = parseFloat($('#f_amount_voucher_meter').val());
                        var t = $("#frmTransVoucherMeter").find('.transcation_fee').text();
                        var t_r = t.replace('p','');
                        var your_total = (parseFloat(f_amount) - parseFloat(t_r)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                        $("#frmTransVoucherMeter").find('.your_total').html('<strong>p '+(parseFloat(f_amount)).toFixed(2)+'</strong>');
                        var y_total = parseFloat(f_amount) - parseFloat(t_r);
                        $("#frmTransVoucherMeter").find('.f_total_amount').val(y_total);  
                    }
                    if($('#f_amount_voucher_meter').val() == ''){
                        var t = $("#frmTransVoucherMeter").find('.transcation_fee').text();
                        var t_r = t.replace('p','');
                        $("#frmTransVoucherMeter").find('.your_total').html('<strong>p '+t_r+'</strong>');
                    }
                    
                },100);
            }
        });
            
           /* $(document).on("change" , "#f_amount_voucher_meter" , function(){
                if(calculation_type == 'ByPercent'){
                    var f_amount = $('#f_amount_voucher_meter').val();
                    var c = parseFloat(prepaid_service_image[id].f_transaction_share);
                    var total = parseFloat((f_amount*c)/100).toFixed(2);
                    var payproshare =  parseFloat((total*a)/100).toFixed(2);
                    var vendorshare =  parseFloat((total*b)/100).toFixed(2);
                    $("#frmTransVoucherMeter").find('.f_paypro_transaction_share').val(payproshare);
                    $("#frmTransVoucherMeter").find('.f_vendor_transaction_share').val(vendorshare); 
                } else{
                    var total = parseFloat(a+b).toFixed(2);
                    $("#frmTransVoucherMeter").find('.f_paypro_transaction_share').val(a);
                    $("#frmTransVoucherMeter").find('.f_vendor_transaction_share').val(b); 
                }
                $("#frmTransVoucherMeter").find('.transcation_fee').html('<strong>p '+total+'</strong>');
                var your_total = (parseFloat(f_amount) + parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                $("#frmTransVoucherMeter").find('.your_total').html('<strong>p '+your_total+'</strong>');
                y_total = parseFloat(f_amount) + parseFloat(total);
                $("#frmTransVoucherMeter").find('.f_total_amount').val(y_total);
                var s_id = prepaid_service_image[id].id;
                $("#frmTransVoucherMeter").find('.i_service_id').val(s_id);
            }); */
            
           /* var a = parseFloat(prepaid_service_image[id].f_paypro_transaction_share);
            var b = parseFloat(prepaid_service_image[id].f_vendor_transaction_share);
                var total = parseFloat(a+b).toFixed(2);
                $("#frmTransVoucherMeter").find('.f_paypro_transaction_share').val(a);
                $("#frmTransVoucherMeter").find('.f_vendor_transaction_share').val(b); 
            
            $("#frmTransVoucherMeter").find('.transcation_fee').html('<strong>p '+total+'</strong>');
            var your_total = (parseFloat(f_amount) + parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            $("#frmTransVoucherMeter").find('.your_total').html('<strong>p '+your_total+'</strong>');
            y_total = parseFloat(f_amount) + parseFloat(total);
            $("#frmTransVoucherMeter").find('.f_total_amount').val(y_total);
            var s_id = prepaid_service_image[id].id;
            $("#frmTransVoucherMeter").find('.i_service_id').val(s_id);*/
            //$("#frmTransVoucher").find('.f_paypro_transaction_share').val(a);
            //$("#frmTransVoucher").find('.f_vendor_transaction_share').val(b); 
           
        } else if(service_type == 'Share' && meter_type == 'No'){
            $('#prepaidservice_withdraw').hide();
            $("#frmTransShare").css("display", "block");
            $("#frmTrans").css("display", "none");
            $("#frmTransVoucher").css("display", "none");
            $("#frmBankTrans").css("display",'none');
            $('#bankservice').hide();
            $('#frmBankTrans')[0].reset();
            $('#frmTrans')[0].reset();
            $('#frmTransVoucher')[0].reset();
            $('#frmTransShare')[0].reset();
            $('#prepaidservice_voucher').hide();
            $('#prepaidservice').hide();
            $('#prepaidservice_share').show();
            var i = $(this).attr('src');
            $('#chnge_prepaid_service').attr('src',i);
            var id = $(this).attr('rel');
            $('.bank-logo').removeClass('active');
            $(this).closest('.bank-logo').addClass('active');
            var a = parseFloat(prepaid_service_image[id].f_paypro_transaction_share);
            var b = parseFloat(prepaid_service_image[id].f_vendor_transaction_share);
            var f_amount = parseFloat($('#f_amount_share').val());
            if(calculation_type == 'ByPercent'){
                var c = parseFloat(prepaid_service_image[id].f_transaction_share);
                var total = parseFloat((f_amount*c)/100).toFixed(2);
                var payproshare =  parseFloat((total*a)/100).toFixed(2);
                var vendorshare =  parseFloat((total*b)/100).toFixed(2);
                $("#frmTransShare").find('.f_paypro_transaction_share').val(payproshare);
                $("#frmTransShare").find('.f_vendor_transaction_share').val(vendorshare); 
            } else{
                var total = parseFloat(a+b).toFixed(2);
                $("#frmTransShare").find('.f_paypro_transaction_share').val(a);
                $("#frmTransShare").find('.f_vendor_transaction_share').val(b); 
            }
            
            if(vendor_details.v_vendor_number == '96636060'){
                var total = parseFloat(a+b+1).toFixed(2);
                $("#frmTransShare").find('.transcation_fee').html('<strong>p '+total+'</strong>');
              //  $('#tran_super_ven').val((parseFloat(total) + parseFloat(1)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
                var your_total = (parseFloat(f_amount) + parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                $("#frmTransShare").find('.your_total').html('<strong>p '+your_total+'</strong>');
            }else{
                $("#frmTransShare").find('.transcation_fee').html('<strong>p '+total+'</strong>');
                var your_total = (parseFloat(f_amount) + parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                $("#frmTransShare").find('.your_total').html('<strong>p '+your_total+'</strong>');
            }
            
            
            
            y_total = parseFloat(f_amount) + parseFloat(total);
            $("#frmTransShare").find('.f_total_amount').val(y_total);
            var s_id = prepaid_service_image[id].id;
            $("#frmTransShare").find('.i_service_id').val(s_id);
            //$("#frmTransShare").find('.f_paypro_transaction_share').val(a);
            //$("#frmTransShare").find('.f_vendor_transaction_share').val(b);
            $(document).on("change" , "#f_amount_voucher" , function(){
                if(calculation_type == 'ByPercent'){
                    var f_amount = $('#f_amount_voucher').val();
                    var c = parseFloat(prepaid_service_image[id].f_transaction_share);
                    var total = parseFloat((f_amount*c)/100).toFixed(2);
                    var payproshare =  parseFloat((total*a)/100).toFixed(2);
                    var vendorshare =  parseFloat((total*b)/100).toFixed(2);
                    $("#frmTransVoucher").find('.f_paypro_transaction_share').val(payproshare);
                    $("#frmTransVoucher").find('.f_vendor_transaction_share').val(vendorshare); 
            } else{
                var total = parseFloat(a+b).toFixed(2);
                $("#frmTransVoucher").find('.f_paypro_transaction_share').val(a);
                $("#frmTransVoucher").find('.f_vendor_transaction_share').val(b); 
            }
            $("#frmTransVoucher").find('.transcation_fee').html('<strong>p '+total+'</strong>');
            var your_total = (parseFloat(f_amount) + parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            $("#frmTransVoucher").find('.your_total').html('<strong>p '+your_total+'</strong>');
            y_total = parseFloat(f_amount) + parseFloat(total);
            $("#frmTransVoucher").find('.f_total_amount').val(y_total);
            var s_id = prepaid_service_image[id].id;
            $("#frmTransVoucher").find('.i_service_id').val(s_id);
        });
            
        } else if(service_type == 'Withdraw' && meter_type == 'No'){
            $("#frmTransWithdraw").css("display", "block");
            $("#frmTransShare").css("display", "none");
            $("#frmTrans").css("display", "none");
            $("#frmTransVoucher").css("display", "none");
            $("#frmBankTrans").css("display",'none');
            $('#bankservice').hide();
            $('#frmBankTrans')[0].reset();
            $('#frmTrans')[0].reset();
            $('#frmTransVoucher')[0].reset();
            $('#frmTransShare')[0].reset();
            $('#frmTransWithdraw')[0].reset();
            $('#prepaidservice_voucher').hide();
            $('#prepaidservice').hide();
            $('#prepaidservice_share').hide();
            $('#prepaidservice_withdraw').show();
            var i = $(this).attr('src');
            $('#chnge_prepaid_service').attr('src',i);
            var id = $(this).attr('rel');
            $('.bank-logo').removeClass('active');
            $(this).closest('.bank-logo').addClass('active');
            var a = parseFloat(prepaid_service_image[id].f_paypro_transaction_share);
            var b = parseFloat(prepaid_service_image[id].f_vendor_transaction_share);
            if(calculation_type == 'ByPercent'){
                var c = parseFloat(prepaid_service_image[id].f_transaction_share);
                var total = parseFloat((f_amount*c)/100).toFixed(2);
                var payproshare =  parseFloat((total*a)/100).toFixed(2);
                var vendorshare =  parseFloat((total*b)/100).toFixed(2);
                $("#frmTransWithdraw").find('.f_paypro_transaction_share').val(payproshare);
                $("#frmTransWithdraw").find('.f_vendor_transaction_share').val(vendorshare); 
            } else{
                var total = parseFloat(a+b).toFixed(2);
                $("#frmTransWithdraw").find('.f_paypro_transaction_share').val(a);
                $("#frmTransWithdraw").find('.f_vendor_transaction_share').val(b); 
            }
            $("#frmTransWithdraw").find('.transcation_fee').html('<strong>p '+total+'</strong>');
            var f_amount = parseFloat($('#f_amount_withdraw').val());
            var your_total = (parseFloat(f_amount) + parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            $("#frmTransWithdraw").find('.your_total').html('<strong>p '+your_total+'</strong>');
            y_total = parseFloat(f_amount) + parseFloat(total);
            $("#frmTransWithdraw").find('.f_total_amount').val(y_total);
            var s_id = prepaid_service_image[id].id;
            $("#frmTransWithdraw").find('.i_service_id').val(s_id);
            //$("#frmTransWithdraw").find('.f_paypro_transaction_share').val(a);
            //$("#frmTransWithdraw").find('.f_vendor_transaction_share').val(b);
            $(document).on("change" , "#f_amount_voucher" , function(){
                if(calculation_type == 'ByPercent'){
                    var f_amount = $('#f_amount_voucher').val();
                    var c = parseFloat(prepaid_service_image[id].f_transaction_share);
                    var total = parseFloat((f_amount*c)/100).toFixed(2);
                    var payproshare =  parseFloat((total*a)/100).toFixed(2);
                    var vendorshare =  parseFloat((total*b)/100).toFixed(2);
                    $("#frmTransVoucher").find('.f_paypro_transaction_share').val(payproshare);
                    $("#frmTransVoucher").find('.f_vendor_transaction_share').val(vendorshare); 
            } else{
                var total = parseFloat(a+b).toFixed(2);
                $("#frmTransVoucher").find('.f_paypro_transaction_share').val(a);
                $("#frmTransVoucher").find('.f_vendor_transaction_share').val(b); 
            }
            $("#frmTransVoucher").find('.transcation_fee').html('<strong>p '+total+'</strong>');
            var your_total = (parseFloat(f_amount) + parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            $("#frmTransVoucher").find('.your_total').html('<strong>p '+your_total+'</strong>');
            y_total = parseFloat(f_amount) + parseFloat(total);
            $("#frmTransVoucher").find('.f_total_amount').val(y_total);
            var s_id = prepaid_service_image[id].id;
            $("#frmTransVoucher").find('.i_service_id').val(s_id);
        });
            
        } else{
            $("#frmTrans").css("display", "block");
            $("#frmTransVoucher").css("display", "none");
            $("#frmBankTrans").css("display",'none');
            $('#bankservice').hide();
            $('#frmBankTrans')[0].reset();
            $('#frmTrans')[0].reset();
            $('#frmTransVoucher')[0].reset();
            $('#prepaidservice_voucher').hide();
            $("#frmTransShare").css("display", "none");
            $('#prepaidservice_share').hide();
            $('#prepaidservice').show();
            var i = $(this).attr('src');
            $('#chnge_prepaid_service').attr('src',i);
            var id = $(this).attr('rel');
            $('.bank-logo').removeClass('active');
            $(this).closest('.bank-logo').addClass('active');
            var a = parseFloat(prepaid_service_image[id].f_paypro_transaction_share);
            var b = parseFloat(prepaid_service_image[id].f_vendor_transaction_share);
            if(calculation_type == 'ByPercent'){
                var c = parseFloat(prepaid_service_image[id].f_transaction_share);
                var total = parseFloat((f_amount*c)/100).toFixed(2);
                var payproshare =  parseFloat((total*a)/100).toFixed(2);
                var vendorshare =  parseFloat((total*b)/100).toFixed(2);
                $("#frmTrans").find('.f_paypro_transaction_share').val(payproshare);
                $("#frmTrans").find('.f_vendor_transaction_share').val(vendorshare); 
            } else{
                var total = parseFloat(a+b).toFixed(2);
                $("#frmTrans").find('.f_paypro_transaction_share').val(a);
                $("#frmTrans").find('.f_vendor_transaction_share').val(b); 
            }
            $("#frmTrans").find('.transcation_fee').html('<strong>p '+total+'</strong>');
            var f_amount = parseFloat($('#f_amount').val());
            var your_total = (parseFloat(f_amount) + parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            $("#frmTrans").find('.your_total').html('<strong>p '+your_total+'</strong>');
            y_total = parseFloat(f_amount) + parseFloat(total);
            $("#frmTrans").find('.f_total_amount').val(y_total);
            var s_id = prepaid_service_image[id].id;
            $("#frmTrans").find('.i_service_id').val(s_id);
            //$("#frmTrans").find('.f_paypro_transaction_share').val(a);
            //$("#frmTrans").find('.f_vendor_transaction_share').val(b);
            $(document).on("change" , "#f_amount_voucher" , function(){
                if(calculation_type == 'ByPercent'){
                    var f_amount = $('#f_amount_voucher').val();
                    var c = parseFloat(prepaid_service_image[id].f_transaction_share);
                    var total = parseFloat((f_amount*c)/100).toFixed(2);
                    var payproshare =  parseFloat((total*a)/100).toFixed(2);
                    var vendorshare =  parseFloat((total*b)/100).toFixed(2);
                    $("#frmTransVoucher").find('.f_paypro_transaction_share').val(payproshare);
                    $("#frmTransVoucher").find('.f_vendor_transaction_share').val(vendorshare); 
                } else{
                    var total = parseFloat(a+b).toFixed(2);
                    $("#frmTransVoucher").find('.f_paypro_transaction_share').val(a);
                    $("#frmTransVoucher").find('.f_vendor_transaction_share').val(b); 
                }
                $("#frmTransVoucher").find('.transcation_fee').html('<strong>p '+total+'</strong>');
                var your_total = (parseFloat(f_amount) + parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                $("#frmTransVoucher").find('.your_total').html('<strong>p '+your_total+'</strong>');
                y_total = parseFloat(f_amount) + parseFloat(total);
                $("#frmTransVoucher").find('.f_total_amount').val(y_total);
                var s_id = prepaid_service_image[id].id;
                $("#frmTransVoucher").find('.i_service_id').val(s_id);
            });
          }    
    });
    $(document).on("click" , ".bank_botswana_div" , function(){
        $("#frmTrans").css("display", "none");
        $("#frmTransVoucher").css("display", "none");
        $("#frmTransShare").css("display", "none");
        $("#frmBankTrans").css("display",'block');
        $('#prepaidservice').hide();
        $('#prepaidservice_voucher').hide();
        $('#prepaidservice_share').hide();
        $('#prepaidservice_withdraw').hide();
        $('#frmTransWithdraw').css("display",'none');
        $('#prepaidservice_voucher_meter').hide();
        $('#frmTransVoucherMeter').css('display','none');
        $('#bankservice').show();
        $('#frmTransVoucher')[0].reset();
        $('#frmTransShare')[0].reset();
        $('#frmBankTrans')[0].reset();
        $('#frmTrans')[0].reset();
        $('.error-inner').hide();
        var i = $(this).attr('src');
        $('#chnge_bank_img').attr('src',i);
        var id = $(this).attr('rel');
        $('.bank-logo').removeClass('active');
        $(this).closest('.bank-logo').addClass('active');
        var a = parseFloat(bank_botswana_image[id].f_paypro_transaction_share);
        var b = parseFloat(bank_botswana_image[id].f_vendor_transaction_share);
        var total = parseFloat(a+b).toFixed(2);
        $("#frmBankTrans").find('.transcation_fee').html('<strong>p '+total+'</strong>');
        var f_amount = parseFloat($('#f_amount_bank').val());
        var your_total = (parseFloat(f_amount) + parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        $("#frmBankTrans").find('.your_total').html('<strong>p '+your_total+'</strong>');
        y_total = parseFloat(f_amount) + parseFloat(total);
        $("#frmBankTrans").find('.f_total_amount').val(y_total);
        var s_id = bank_botswana_image[id].id;
        $("#frmBankTrans").find('.i_bank_id').val(s_id);
        $("#frmBankTrans").find('.f_paypro_transaction_share').val(a);
        $("#frmBankTrans").find('.f_vendor_transaction_share').val(b);    
    });
    $(document).on("click" , ".bank_african_div" , function(){
        $("#frmTrans").css("display", "none");
        $("#frmTransVoucher").css("display", "none");
        $("#frmTransShare").css("display", "none");
        $("#frmBankTrans").css("display",'block');
        $('#prepaidservice').hide();
        $('#prepaidservice_voucher').hide();
        $("#frmTransShare").css("display", "none");
        $('#prepaidservice_share').hide();
        $('#prepaidservice_withdraw').hide();
        $('#frmTransWithdraw').css("display",'none');
        $('#prepaidservice_voucher_meter').hide();
        $('#frmTransVoucherMeter').css('display','none');
        $('#bankservice').show();
        $('#frmBankTrans')[0].reset();
        $('#frmTrans')[0].reset();
        $('#frmTransVoucher')[0].reset();
        $('#frmTransShare')[0].reset();
        $('.error-inner').hide();
        var i = $(this).attr('src');
        $('#chnge_bank_img').attr('src',i);
        var id = $(this).attr('rel');
        $('.bank-logo').removeClass('active');
        $(this).closest('.bank-logo').addClass('active');
        var a = parseFloat(bank_african_image[id].f_paypro_transaction_share);
        var b = parseFloat(bank_african_image[id].f_vendor_transaction_share);
        var total = parseFloat(a+b).toFixed(2);
        $("#frmBankTrans").find('.transcation_fee').html('<strong>p '+total+'</strong>');
        var f_amount = parseFloat($('#f_amount_bank').val());
        var your_total = (parseFloat(f_amount) + parseFloat(total)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        $("#frmBankTrans").find('.your_total').html('<strong>p '+your_total+'</strong>');
        y_total = parseFloat(f_amount) + parseFloat(total);
        $("#frmBankTrans").find('.f_total_amount').val(y_total);
        var s_id = bank_african_image[id].id;
        $("#frmBankTrans").find('.i_bank_id').val(s_id);
        $("#frmBankTrans").find('.f_paypro_transaction_share').val(a);
        $("#frmBankTrans").find('.f_vendor_transaction_share').val(b);    
    });
    
   $(document).on('change','#f_amount_voucher', function(){
        var f_amount = parseFloat($('#f_amount_voucher').val());
        var t = $("#frmTransVoucher").find('.transcation_fee').text();
        var t_r = t.replace('p','');
        var your_total = (parseFloat(f_amount) + parseFloat(t_r)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        $("#frmTransVoucher").find('.your_total').html('<strong>p '+your_total+'</strong>');
        y_total = parseFloat(f_amount) + parseFloat(t_r);
        $("#frmTransVoucher").find('.f_total_amount').val(y_total);
    });
    /*$(document).on('change','#f_amount_bank', function(){
        var f_amount = parseFloat($('#f_amount_bank').val());
        var t = $("#frmBankTrans").find('.transcation_fee').text();
        var t_r = t.replace('p','');
        var your_total = (parseFloat(f_amount) + parseFloat(t_r)).toFixed(2);
        $("#frmBankTrans").find('.your_total').html('<strong>p '+your_total+'</strong>');
        $("#frmBankTrans").find('.f_total_amount').val(your_total);
    }); */
    
    $('#frmTrans')[0].reset();
    $("#frmTrans").submit(function() {
        var form = $("#frmTrans");
        form.find('.duplicate-error').hide();
        form.find(".form-group").removeClass('has-error');
        if(form_valid("#frmTrans")){
            var curObj = $(this);
            curObj.find('button[type=submit]').attr('disabled', 'disabled'); 
            var send_data = $("#frmTrans").serialize();
            bootbox.confirm("Are you sure you want to proceed?", function(result) {
                if(result) {
                    $('.loading').fadeIn();
                    $.post($("#frmTrans").attr("action"), send_data, function(data){
                        $('.loading').fadeOut();
                        curObj.find('button[type=submit]').removeAttr('disabled');
                        if(data.status == 'TRUE') {
                             if(data.updated_credit_value != ''){
                            //var credit_amount = parseFloat(data.updated_credit_value).toFixed(2);
                                $('#vendor-money').text("P " + data.updated_credit_value);
                            }                                
                            $('#error_bal').hide();
                            $('.duplicate-error').hide();
                            form.find('.alert-success').show();
                            form.find('.alert-danger').hide();
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            setTimeout(function(){ $(".alert-success").fadeOut(2000); },4000);
                            $('#frmTrans')[0].reset();
                          /*  $('#v_smartcard_number').val('');
                            $('#i_cell_number').val('');
                            $('#f_amount').val('0.00'); */
                            var t = $("#frmTrans").find('.transcation_fee').text();
                            var t_r = t.replace('p','');
                            $("#frmTrans").find('.your_total').html('<strong>p '+t_r+'</strong>');
                        } else if(data.status == 'FALSE'){
                                $('#error_bal').show();
                                return false;                            
                        }else {
                            form.find('.alert-success').hide();
                            form.find('.alert-danger').show();
                            $(data).each(function(i,val){
                                $.each(val,function(key,v){
                                      $('#'+key).closest('.form-group').addClass('has-error');
                                      $('#error_'+key).show();
                                });
                            });
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            $('.has-error .form-control').first().focus()
                        }
                    }); 
            }
        });
    } else {
            return false;
        }
        return false;
     
    });
    $('#frmTransShare')[0].reset();
    $("#frmTransShare").submit(function() {
        var form = $("#frmTransShare");
        form.find('.duplicate-error').hide();
        form.find(".form-group").removeClass('has-error');
        if(form_valid("#frmTransShare")){
            var curObj = $(this);
            curObj.find('button[type=submit]').attr('disabled', 'disabled'); 
            var send_data = $("#frmTransShare").serialize();
            //$('#i_cell_number').val('');
            bootbox.confirm("Are you sure you want to proceed?", function(result) {
                if(result) {
                    $('.loading').fadeIn();
                    $.post($("#frmTransShare").attr("action"), send_data, function(data){
                        $('.loading').fadeOut();
                        curObj.find('button[type=submit]').removeAttr('disabled');
                        if(data.status == 'TRUE') {
                             if(data.updated_credit_value != ''){
                            //var credit_amount = parseFloat(data.updated_credit_value).toFixed(2);
                                $('#vendor-money').text("P " + data.updated_credit_value);
                            }                                
                            $('#error_bal_share').hide();
                            $('#error_vendor_number').hide();
                            $('#error_vendor_share_number').hide();
                            $('.duplicate-error').hide();
                            form.find('.alert-success').show();
                            form.find('.alert-danger').hide();
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            setTimeout(function(){ $(".alert-success").fadeOut(2000); },4000);
                            $('#frmTransShare')[0].reset();
                          /*  $('#v_smartcard_number').val('');
                            $('#i_cell_number').val('');
                            $('#f_amount').val('0.00'); */
                            var t = $("#frmTransShare").find('.transcation_fee').text();
                            var t_r = t.replace('p','');
                            $("#frmTransShare").find('.your_total').html('<strong>p '+t_r+'</strong>');
                        } else if(data.status == 'FALSE'){
                                $('#error_bal_share').show();
                                $('#error_vendor_number').hide();
                                $('#error_vendor_share_number').hide();
                                return false;                            
                        } else if(data.status == 'FALSE_NUMBER'){
                                $('#error_vendor_number').show();
                                $('#error_vendor_share_number').hide();
                                $('#error_bal_share').hide();
                                return false;                            
                        } else if(data.status == 'OWN_VENDOR_NUMBER'){
                                $('#error_vendor_number').hide();
                                $('#error_vendor_share_number').show();
                                $('#error_bal_share').hide();
                                return false;                            
                        }else {
                            form.find('.alert-success').hide();
                            form.find('.alert-danger').show();
                            $(data).each(function(i,val){
                                $.each(val,function(key,v){
                                      $('#'+key).closest('.form-group').addClass('has-error');
                                      $('#error_'+key).show();
                                });
                            });
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            $('.has-error .form-control').first().focus()
                        }
                    }); 
            }
        });
    } else {
            return false;
        }
        return false;
     
    });
    $('#frmTransWithdraw')[0].reset();
    $("#frmTransWithdraw").submit(function() {
        var form = $("#frmTransWithdraw");
        form.find('.duplicate-error').hide();
        form.find(".form-group").removeClass('has-error');
        if(form_valid("#frmTransWithdraw")){
            var curObj = $(this);
            curObj.find('button[type=submit]').attr('disabled', 'disabled'); 
            var send_data = $("#frmTransWithdraw").serialize();
            //$('#i_cell_number').val('');
          /*  bootbox.confirm("Are you sure you want to proceed?", function(result) {
                if(result) { */
                   // $('#i_omang_number_withdraw').val('');
                    $.post($("#frmTransWithdraw").attr("action"), send_data, function(data){
                        curObj.find('button[type=submit]').removeAttr('disabled');
                        if(data.status == 'TRUE') {
                             if(data.updated_credit_value != ''){
                            //var credit_amount = parseFloat(data.updated_credit_value).toFixed(2);
                                $('#vendor-money').text("P " + data.updated_credit_value);
                            }
                            $('#error_bal_withdraw').hide();
                            $('#error_vendor_number').hide();
                            $('#error_vendor_withdraw_number').hide();
                            $('#error_omang_number').hide();
                            $('.duplicate-error').hide();
                            form.find('.alert-success').show();
                            form.find('.alert-danger').hide();
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            setTimeout(function(){ $(".alert-success").fadeOut(2000); },4000);
                            $('#frmTransWithdraw')[0].reset();
                          /*  $('#v_smartcard_number').val('');
                            $('#i_cell_number').val('');
                            $('#f_amount').val('0.00'); */
                            var t = $("#frmTransWithdraw").find('.transcation_fee').text();
                            var t_r = t.replace('p','');
                            $("#frmTransWithdraw").find('.your_total').html('<strong>p '+t_r+'</strong>');
                        } else if(data.status == 'VERIFY_DETAILS'){
                            $('#error_bal_withdraw').hide();
                            $('#error_vendor_number').hide();
                            $('#error_vendor_withdraw_number').hide();
                            $('#error_omang_number').hide();
                            $('.duplicate-error').hide();
                            $('#vend_number').html(data.withdraw_details.vend_number);
                            $('#omang_number').html(data.withdraw_details.omang_number);
                            $('#name').html(data.withdraw_details.name);
                            $('#surname').html(data.withdraw_details.surname);
                            $('#amount').html(data.withdraw_details.amount);
                            $('#fee').html(data.withdraw_details.fee);
                            $('#hid_vend_number').val(data.withdraw_details.vend_number);
                            $('#hid_omang_number').val(data.withdraw_details.omang_number);
                            $('#hid_name').val(data.withdraw_details.name);
                            $('#hid_surname').val(data.withdraw_details.surname);
                            $('#hid_amount').val(data.withdraw_details.amount);
                            $('#hid_fee').val(data.withdraw_details.fee);
                            $('#hid_paypro_transaction').val(data.withdraw_details.paypro_transaction);
                            $('#hid_vendor_transaction').val(data.withdraw_details.vendor_transaction);
                            $('#hid_service_id').val(data.withdraw_details.service_id);
                            $('#hid_vendor_id').val(data.withdraw_details.vendor_id);
                            $(".close").click(function(){
                                $('#i_withdraw_pin').val('');
                                $('#wrong_withdraw_pin').hide();
                                $('#verify_withdraw_details').modal('hide');
                            });
                            $("#close").click(function(){
                                $('#i_withdraw_pin').val('');
                                $('#wrong_withdraw_pin').hide();
                            });
                            $('#verify_withdraw_details').modal('show');
                            $('#verify_withdraw_pin_form').submit(function(){
                                
                                if(form_valid("#verify_withdraw_pin_form")){
                                    $('.loading').fadeIn();
                                    var curObj = $(this);
                                    curObj.find('button[type=submit]').attr('disabled', 'disabled'); 
                                    var send_data = $("#verify_withdraw_pin_form").serialize();
                                    $.post($("#verify_withdraw_pin_form").attr("action"), send_data, function(data){
                                        $('.loading').fadeOut();
                                        curObj.find('button[type=submit]').removeAttr('disabled');
                                        if(data.status == 'TRUE') {
                                             $('#wrong_withdraw_pin').hide();
                                             $('#verify_withdraw_details').modal('hide');
                                             if(data.updated_credit_value != ''){
                                            //var credit_amount = parseFloat(data.updated_credit_value).toFixed(2);
                                                $('#vendor-money').text("P " + data.updated_credit_value);
                                            }                                
                                            $('#error_bal_withdraw').hide();
                                            $('#error_vendor_number').hide();
                                            $('#error_vendor_withdraw_number').hide();
                                            $('#error_omang_number').hide();
                                            $('.duplicate-error').hide();
                                            form.find('.alert-success').show();
                                            form.find('.alert-danger').hide();
                                            $("html, body").animate({ scrollTop: 0 }, 600);
                                            setTimeout(function(){ $(".alert-success").fadeOut(2000); },4000);
                                            $('#frmTransWithdraw')[0].reset();
                                          /*  $('#v_smartcard_number').val('');
                                            $('#i_cell_number').val('');
                                            $('#f_amount').val('0.00'); */
                                            var t = $("#frmTransWithdraw").find('.transcation_fee').text();
                                            var t_r = t.replace('p','');
                                            $("#frmTransWithdraw").find('.your_total').html('<strong>p '+t_r+'</strong>');
                                        } else if(data.status == 'WRONG_PIN_NUMBER'){
                                            $('#wrong_withdraw_pin').show();
                                            return false;  
                                        }
                                    });
                                }
                                return false;
                            });
                            return false;
                            
                        }else if(data.status == 'FALSE'){
                                $('#error_bal_withdraw').show();
                                $('#error_vendor_withdraw_number').hide();
                                $('#error_vendor_number').hide();
                                $('#error_omang_number').hide();
                                return false;                            
                        } else if(data.status == 'FALSE_NUMBER'){
                                $('#error_vendor_number').show();
                                $('#error_vendor_withdraw_number').hide();
                                $('#error_bal_withdraw').hide();
                                $('#error_omang_number').hide();
                                return false;                            
                        } else if(data.status == 'OWN_VENDOR_NUMBER'){
                                $('#error_vendor_number').hide();
                                $('#error_vendor_withdraw_number').show();
                                $('#error_bal_withdraw').hide();
                                $('#error_omang_number').hide();
                                return false;                            
                        } else if(data.status == 'VALID_OMANG_NUMBER'){
                                $('#error_vendor_number').hide();
                                $('#error_omang_number').show();
                                $('#error_bal_withdraw').hide();
                                $('#error_vendor_withdraw_number').hide();
                                return false;                            
                        }else {
                            form.find('.alert-success').hide();
                            form.find('.alert-danger').show();
                            $(data).each(function(i,val){
                                $.each(val,function(key,v){
                                      $('#'+key).closest('.form-group').addClass('has-error');
                                      $('#error_'+key).show();
                                });
                            });
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            $('.has-error .form-control').first().focus()
                        }
                    }); 
           /* }
        }); */
    } else {
            return false;
        }
        return false;
     
    });
    $("#frmTransVoucher").submit(function() {
        var form = $("#frmTransVoucher");
        form.find('.duplicate-error').hide();
        form.find(".form-group").removeClass('has-error');
        if(form_valid("#frmTransVoucher")){           
            var curObj = $(this);
            curObj.find('button[type=submit]').attr('disabled', 'disabled'); 
            var send_data = $("#frmTransVoucher").serialize();
            bootbox.confirm("Are you sure you want to proceed?", function(result) {
             if(result) {
                $('.loading').fadeIn();
                $.post($("#frmTransVoucher").attr("action"), send_data, function(data){
                    $('.loading').fadeOut();
                    curObj.find('button[type=submit]').removeAttr('disabled');
                    if(data.status == 'TRUE') {
                        $('#i_cell_number_voucher').val('');
                        if(data.updated_credit_value != ''){
                            //var credit_amount = parseFloat(data.updated_credit_value).toFixed(2);
                            $('#vendor-money').text("P " + data.updated_credit_value);
                        }                                
                        $('#error_bal_voucher').hide();
                        $('#error_voucher').hide();
                        $('.duplicate-error').hide();
                        form.find('.alert-success').show();
                        form.find('.alert-danger').hide();
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        setTimeout(function(){ $(".alert-success").fadeOut(2000); },4000);
                        $('#frmTransVoucher')[0].reset();
                        $('#f_amount_voucher').val('10');
                        var t = $("#frmTransVoucher").find('.transcation_fee').text();
                        var t_r = t.replace('p','');
                        $("#frmTransVoucher").find('.your_total').html('<strong>p '+t_r+'</strong>');
                    } else if(data.status == 'FALSE'){
                            $('#error_bal_voucher').show();
                            return false;                            
                    }
                    else if(data.status == 'FALSE_Voucher'){
                            $('#error_voucher').show();
                            $('#error_bal_voucher').hide();
                            return false;                            
                    }else if(data.status == 'FALSE_api_response'){
                            $('#error_voucher').hide();
                            $('#error_bal_voucher').hide();
                            form.find('.alert-danger').show();
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            $('.has-error .form-control').first().focus()
                            return false;                            
                    }else {
                        form.find('.alert-success').hide();
                        form.find('.alert-danger').show();
                        $(data).each(function(i,val){
                            $.each(val,function(key,v){
                                  $('#'+key).closest('.form-group').addClass('has-error');
                                  $('#error_'+key).show();
                            });
                        });
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        $('.has-error .form-control').first().focus()
                    }
                });
            }
        });
    } else {
            return false;
        }
        return false;
    });
    $("#frmTransVoucherMeter").submit(function() {
        var form = $("#frmTransVoucherMeter");
        form.find('.duplicate-error').hide();
        form.find(".form-group").removeClass('has-error');
        if(form_valid("#frmTransVoucherMeter")){           
            var curObj = $(this);
            curObj.find('button[type=submit]').attr('disabled', 'disabled'); 
            var send_data = $("#frmTransVoucherMeter").serialize();
            bootbox.confirm("Are you sure you want to proceed?", function(result) {
             if(result) {
                $('.loading').fadeIn();
                $.post($("#frmTransVoucherMeter").attr("action"), send_data, function(data){
                    $('.loading').fadeOut();
                    curObj.find('button[type=submit]').removeAttr('disabled');
                    if(data.status == 'TRUE') {
                        $('#i_cell_number_voucher').val('');
                        if(data.updated_credit_value != ''){
                            //var credit_amount = parseFloat(data.updated_credit_value).toFixed(2);
                            $('#vendor-money').text("P " + data.updated_credit_value);
                        }                                
                        $('#error_bal_voucher_meter').hide();
                        $('#error_voucher').hide();
                        $('.duplicate-error').hide();
                        form.find('.alert-success').show();
                        form.find('.alert-danger').hide();
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        setTimeout(function(){ $(".alert-success").fadeOut(2000); },4000);
                        $('#frmTransVoucherMeter')[0].reset();
                        $('#f_amount_voucher').val('10');
                        var t = $("#frmTransVoucherMeter").find('.transcation_fee').text();
                        var t_r = t.replace('p','');
                        $("#frmTransVoucherMeter").find('.your_total').html('<strong>p '+t_r+'</strong>');
                    } else if(data.status == 'FALSE'){
                            $('#error_bal_voucher_meter').show();
                            return false;                            
                    }
                    else if(data.status == 'FALSE_Voucher'){
                            $('#error_voucher').show();
                            $('#error_bal_voucher_meter').hide();
                            return false;                            
                    }else if(data.status == 'FALSE_api_response'){
                            $('#error_voucher').hide();
                            $('#error_bal_voucher_meter').hide();
                            form.find('.alert-danger').show();
                            form.find('#error_alert_danger').html('Error occurs during process. Please try again later.');
                            
                           // form.find('.alert-danger').show();
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            $('.has-error .form-control').first().focus()
                            return false;                            
                    }else {
                        form.find('.alert-success').hide();
                        form.find('.alert-danger').show();
                        form.find('#error_alert_danger').html('You have some form errors. Please check below.');
                        $(data).each(function(i,val){
                            $.each(val,function(key,v){
                                  $('#'+key).closest('.form-group').addClass('has-error');
                                  $('#error_'+key).show();
                            });
                        });
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        $('.has-error .form-control').first().focus()
                    }
                });
            }
        });
    } else {
            return false;
        }
        return false;
    });
    $('#frmBankTrans')[0].reset();
    $("#frmBankTrans").submit(function() {
        var form = $("#frmBankTrans");
        form.find('.duplicate-error').hide();
        form.find(".form-group").removeClass('has-error');
        if(form_valid("#frmBankTrans")){
            
            var curObj = $(this);
            curObj.find('button[type=submit]').attr('disabled', 'disabled'); 
            var send_data = $("#frmBankTrans").serialize();
            bootbox.confirm("Are you sure you want to proceed?", function(result) {
            if(result){
                $('.loading').fadeIn();
                $('#i_sender_cell_number').val('');      
                $.post($("#frmBankTrans").attr("action"), send_data, function(data){
                    $('.loading').fadeOut();
                    curObj.find('button[type=submit]').removeAttr('disabled');
                    if(data.status == 'TRUE') {
                        if(data.updated_credit_value != ''){
                            $('#vendor-money').text("P " + data.updated_credit_value);
                            //var credit_amount = parseFloat(data.updated_credit_value).toFixed(2);
                            //$('#vendor-money').text("P " + credit_amount);
                        }
                        $('#error_bal_service').hide();
                        $('.duplicate-error').hide();
                        form.find('.alert-success').show();
                        form.find('.alert-danger').hide();
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        setTimeout(function(){ $(".alert-success").fadeOut(2000); },4000);
                        $('#frmBankTrans')[0].reset();
                        var t = $("#frmBankTrans").find('.transcation_fee').text();
                        var t_r = t.replace('p','');
                        $("#frmBankTrans").find('.your_total').html('<strong>p '+t_r+'</strong>');
                    } else if(data.status == 'FALSE'){
                            $('#error_bal_service').show();
                            return false;                            
                    } else {
                        form.find('.alert-success').hide();
                        form.find('.alert-danger').show();
                        $(data).each(function(i,val){
                            $.each(val,function(key,v){
                                  $('#'+key).closest('.form-group').addClass('has-error');
                                  $('#error_'+key).show();
                            });
                        });
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        $('.has-error .form-control').first().focus()
                    }
                });
            }
      });
    } else {
            return false;
        }
        return false;
    });
});