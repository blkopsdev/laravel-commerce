function form_valid(e, t) {
    console.log("test123");
    var n = true;
    t = typeof t !== "undefined" ? t : "";
    if (t != "") {
        $(t).html("")
    }
    $("." + err_class).remove();
    $(e).find("input,select,textarea").each(function() {
        var e = $.trim($(this).val());
        var r = "";
        if (t != "") {
            $(this).css("border", "1px solid #838383")
        }
        if ($(this).attr("placeholder") !== undefined) {
            r = $(this).attr("placeholder")
        }
        var i = "<" + err_element + ' class="' + err_class + '" id="' + this.id + '_error">';
        var s = ".</" + err_element + ">";
        var o = "";
        if ($(this).hasClass("email") && e != "" && e !== undefined && e != r) {
            var u = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm);
            if (!u.test(e)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).hasClass("url") && e != "" && e !== undefined && e != r) {
            var u = new RegExp(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/);
            if (!u.test(e)) {
                o = "Please enter valid URL";
                n = false
            }
        }
        if ($(this).hasClass("digits") && e != "" && e !== undefined && e != r) {
            if (!e.match(/^\d+$/)) {
                o = "Please enter valid digits";
                n = false
            }
        }
        if ($(this).hasClass("grezero") && e != "" && e !== undefined && e != r) {
            if (!e.match(/^(?!0(\.0*)?$)\d+(\.?\d{0,2})?$/)) {
                o = "Please enter number greater than zero";
                n = false
            }
        }
         if ($(this).hasClass("alpha") && e != "" && e !== undefined && e != r) {
            if (!e.match(/^[A-Za-z ]+$/)) {
                o = "Please enter valid "+ r.toLowerCase();
                n = false
            }
        }
        if ($(this).hasClass("length") && e != "" && e !== undefined && e != r) {
             var u = new RegExp(/^7\d{7}$/);
            if (!u.test(e)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).hasClass("length_omang_number") && e != "" && e !== undefined && e != r) {
           /* var u = new RegExp(/^(\d{9}?)?$/); */
            var u = new RegExp(/^[a-zA-Z0-9]{9}$/);
            if (!u.test(e)) {
                o = "Please enter valid " + r.toLowerCase();
                    n = false
            }
        }
        if ($(this).hasClass("length_withdraw_pin") && e != "" && e !== undefined && e != r) {
             var u = new RegExp(/^[A-Za-z0-9]{4}$/);
            if (!u.test(e)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).hasClass("number") && e != "" && e !== undefined && e != r) {
            if (!e.match(/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/)) {
                o = "Please enter valid number";
                n = false
            }
        }
        if ($(this).hasClass("validate_zip") && e != "" && e !== undefined && e != r) {
            var u = new RegExp(/^[0-9]{5,}/);
            if (!u.test(e)) {
                o = "Minimum 5 character required";
                n = false
            }
        }
        if ($(this).hasClass("validate_creditcard") && e != "" && e !== undefined && e != r) {
            var u = new RegExp(/^\d{15,16}$/);
            if (!u.test(e)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).hasClass("validate_month") && e != "" && e !== undefined && e != r) {
            var a = /^[0-9]{1,2}$/;
            if (!a.test(e)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).hasClass("validate_year") && e != "" && e !== undefined && e != r) {
            var a = /^[0-9]{4}$/;
            var f = (new Date).getFullYear();
            if (!a.test(e) || parseInt(e) < parseInt(f)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).hasClass("validate_cvccode") && e != "" && e !== undefined && e != r) {
            var a = /^[0-9]{3}$/;
            if (!a.test(e)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).attr("equalTo") !== undefined) {
            if (e != "Confirm Password" && $.trim($("#"+$(this).attr("equalTo")).val()) != e) {
                o = "Password does not match";
                n = false
            }
        }
        if ($(this).hasClass("validate_password") && e != "" && e !== undefined && e != r) {
            var a = /[a-zA-Z0-9\!\@\#\$\.\%\^\&\*\(\)\_\\ \+]{8,}/;
            if (!a.test(e)) {
                o = "Minimum 8 characters required";
                n = false
            }
        }
        if ($(this).hasClass("validate_social_secutiry") && e != "" && e !== undefined && e != r) {
            var a = /[a-zA-Z0-9\!\@\#\$\.\%\^\&\*\(\)\_\+]{9,}/;
            if (!a.test(e)) {
                o = "Invalid social security number";
                n = false
            }
        }
        if (this.id == "v_email" && $("#v_email").attr("class").indexOf("duplicate-email-error") >= 0) {
            n = false;
            o = "Email address already exist";
        }
        if ($(this).hasClass("check-url-char") && e != "" && e !== undefined && e != r) {
            var u = new RegExp(/[a-zA-Z0-9-]/g);
            if (!u.test(e)) {
                o = "Please enter valid text";
                n = false
            }
        }
        if ($(this).hasClass("phone") && e != "" && e !== undefined && e != r) {
            var u = /^[0-9()\s]+$/;
            if (!u.test(e)) {
                o = "Please enter valid phone";
                n = false
            }
        }

        
        
        if ($(this).hasClass("ckeditor")) {
            if (this.id == "CkEditor") {
                $(this).val(CKEDITOR.instances.CkEditor.getData());
                e = $(this).val()
            }
        }
        if ($(this).hasClass("required") && (e == "" || e == undefined || e == r)) {
            if ($(this).attr("type") !== undefined && $(this).attr("type") == "file") {
                o = "Please upload file"
            } else if ($(this).attr("type") !== undefined && $(this).attr("type") == "hidden") {
                if ($(this).hasClass("dropdown_date_validation")) {
                    o = "Please select hour, minute and AM/PM"
                } else if ($(this).hasClass("dropdown_date_msg")) {
                    o = "Start time should be less than end time"
                } else {
                    o = "Please select " + r.toLowerCase()
                }
            } else if ($(this).is("select")) {
                o = "Please select " + r.toLowerCase()
            } else if ($(this).hasClass("select_msg")) {
                o = "Please select " + r.toLowerCase()
            } else {
                o = "Please enter " + r.toLowerCase()
            }
            n = false
        }
        if ($(this).hasClass("required-least-one") && $(this).attr("groupid") != "" && $(this).attr("groupid") != undefined) {
            if ($('input[groupid="' + $(this).attr("groupid") + '"]:checked').length < 1) {
                o = "Please select any option.";
                n = false
            }
        }

        if($(this).hasClass('main_password') && $(this).val() != "" && !$(this).hasClass('passMeter-strong')){
            o = 'Please use 8-12 characters, include an uppercase letter, include a lower case letter and include a number. Do not use commonly used phrases';
            n = false;
        }
        if($(this).hasClass('main_password1') && $(this).val() != "" && !$(this).hasClass('passMeter-strong')){
            o = 'Please enter valid password';
            n = false;
        }

        
        
        if (!n && o != "") {
            o = i + o + s;
            if (t != "") {
                $(t).append(o);
                $(this).css("border", "1px solid red")
            } else {
                console.log("test");
                if ($(this).hasClass("ckeditor")) {
                    $(this).next("div").after(o)
                }
                else if ($(this).hasClass("file")) {
                    $(this).next("div").after(o)
                } 
                else {
                    if (this.id == "month") {
                        $(this).next().after(o)
                    } else if (this.id == "date") {
                        $(this).next().after(o)
                    } else if (this.id == "year") {
                        $(this).next().after(o)
                    } else if (this.id == "v_social_secutiry_number") {
                        $(this).next().after(o)
                    } else {
                        if($(this).hasClass('grezero')){
                            $(this).parent('.input-grp').after(o);
                        } else {
                            console.log(o + "--" + n);
                            $(this).after(o)
                        }
                    }
                }
            }
        }
    });
    return n
}

/* Begin custom code from developer */

function form_valid_custom(e, t) {
    var n = true;
    t = typeof t !== "undefined" ? t : "";
    if (t != "") {
        $(t).html("")
    }
    $("." + err_class).remove();
    $(e).find("input,select,textarea").each(function() {
        var e = $.trim($(this).val());
        var r = "";
        if (t != "") {
            $(this).css("border", "1px solid #ebccd1")
        }
        /*if ($(this).attr("placeholder") !== undefined) {
            r = $(this).attr("placeholder")
        }*/
        
        if($(this).attr("error-message") !== undefined){
            r = $(this).attr("error-message");
        } else {
            r = "This field is required";
        }
        
        var im = '<'+err_element_custom+' id="emp_rule-error" class="'+err_class_custom+'">';
        var sm = ".</" + err_element_custom + ">";
        
        var i = "<" + err_element + ' class="' + err_class + '" id="' + this.id + '_error">';
        var s = ".</" + err_element + ">";
        var o = "";
        if ($(this).hasClass("email") && e != "" && e !== undefined && e != r) {
            var u = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm);
            if (!u.test(e)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).hasClass("url") && e != "" && e !== undefined && e != r) {
            var u = new RegExp(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/);
            if (!u.test(e)) {
                o = "Please enter valid URL";
                n = false
            }
        }
        if ($(this).hasClass("digits") && e != "" && e !== undefined && e != r) {
            if (!e.match(/^\d+$/)) {
                o = "Please enter valid digits";
                n = false
            }
        }
        if ($(this).hasClass("grezero") && e != "" && e !== undefined && e != r) {
            if (!e.match(/^(?!0(\.0*)?$)\d+(\.?\d{0,2})?$/)) {
                o = "Please enter number greater than zero";
                n = false
            }
        }
         if ($(this).hasClass("alpha") && e != "" && e !== undefined && e != r) {
            if (!e.match(/^[A-Za-z ]+$/)) {
                o = "Please enter valid name" + r.toLowerCase();
                n = false
            }
        }
          if ($(this).hasClass("length") && e != "" && e !== undefined && e != r) {
            var u = new RegExp(/^7\d{7}$/);
            if (!u.test(e)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).hasClass("length_omang_number") && e != "" && e !== undefined && e != r) {
             /*var u = new RegExp(/^(\d{9}?)?$/);*/
             var u = new RegExp(/^[a-zA-Z0-9]{9}$/);
            if (!u.test(e)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).hasClass("length_withdraw_pin") && e != "" && e !== undefined && e != r) {
             var u = new RegExp(/^[A-Za-z0-9]{4}$/);
            if (!u.test(e)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).hasClass("number") && e != "" && e !== undefined && e != r) {
            if (!e.match(/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/)) {
                o = "Please enter valid number";
                n = false
            }
        }
        if ($(this).hasClass("validate_zip") && e != "" && e !== undefined && e != r) {
            
            var u = new RegExp(/^[0-9]{5,}/);
            if (!u.test(e)) {
                o = "Minimum 5 character required";
                n = false
            }
        }
        if ($(this).hasClass("validate_creditcard") && e != "" && e !== undefined && e != r) {
            var u = new RegExp(/^\d{15,16}$/);
            if (!u.test(e)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).hasClass("validate_month") && e != "" && e !== undefined && e != r) {
            var a = /^[0-9]{1,2}$/;
            if (!a.test(e)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).hasClass("validate_year") && e != "" && e !== undefined && e != r) {
            var a = /^[0-9]{4}$/;
            var f = (new Date).getFullYear();
            if (!a.test(e) || parseInt(e) < parseInt(f)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).hasClass("validate_cvccode") && e != "" && e !== undefined && e != r) {
            var a = /^[0-9]{3}$/;
            if (!a.test(e)) {
                o = "Please enter valid " + r.toLowerCase();
                n = false
            }
        }
        if ($(this).attr("equalTo") !== undefined) {
            if (e != "Confirm Password" && $.trim($("#" + $(this).attr("equalTo")).val()) != e) {
                o = "Password does not match";
                n = false
            }
        }
        if ($(this).hasClass("validate_password") && e != "" && e !== undefined && e != r) {
            var a = /[a-zA-Z0-9\!\@\#\$\.\%\^\&\*\(\)\_\\ \+]{8,}/;
            if (!a.test(e)) {
                o = "Minimum 8 characters required";
                n = false
            }
        }
        if ($(this).hasClass("validate_social_secutiry") && e != "" && e !== undefined && e != r) {
            var a = /[a-zA-Z0-9\!\@\#\$\.\%\^\&\*\(\)\_\+]{9,}/;
            if (!a.test(e)) {
                o = "Invalid social security number";
                n = false
            }
        }
        if (this.id == "v_email" && $("#v_email").attr("class").indexOf("duplicate-email-error") >= 0) {
            n = false;
            o = "Email address already exist"
        }
        if ($(this).hasClass("check-url-char") && e != "" && e !== undefined && e != r) {
            var u = new RegExp(/[a-zA-Z0-9-]/g);
            if (!u.test(e)) {
                o = "Please enter valid text";
                n = false
            }
        }
        if ($(this).hasClass("phone") && e != "" && e !== undefined && e != r) {
            var u = /^[0-9()\s]+$/;
            if (!u.test(e)) {
                o = "Please enter valid phone";
                n = false
            }
        }
        if ($(this).hasClass("ckeditor")) {
            if (this.id == "CkEditor") {
                $(this).val(CKEDITOR.instances.CkEditor.getData());
                e = $(this).val()
            }
        }
        if ($(this).hasClass("required") && (e == "" || e == undefined || e == r)) {
            if ($(this).attr("type") !== undefined && $(this).attr("type") == "file") {
                o = "Please upload file"
            } else if ($(this).attr("type") !== undefined && $(this).attr("type") == "hidden") {
                if ($(this).hasClass("dropdown_date_validation")) {
                    o = "Please select hour, minute and AM/PM"
                } else if ($(this).hasClass("dropdown_date_msg")) {
                    o = "Start time should be less than end time"
                } else {
                    o = "Please select  any one"
                }
            } else if ($(this).is("select")) {
                o = "Please select " + r.toLowerCase()
            } else if ($(this).hasClass("select_msg")) {
                o = "Please select " + r.toLowerCase()
            } else {
                o = "Please enter " + r.toLowerCase()
            }
            n = false
        }
        if ($(this).hasClass("required-least-one") && $(this).attr("groupid") != "" && $(this).attr("groupid") != undefined) {
            if ($('input[groupid="' + $(this).attr("groupid") + '"]:checked').length < 1) {
                o = "Please select any option.";
                n = false
            }
        }
        if (!n && o != "") {
            o = im + o + sm;
            
            if (t != "") {
                $(t).append(o);
                $(this).parent().addClass('has-error');
                $(this).css("border", "1px solid #ebccd1");
            } else {
                if ($(this).hasClass("ckeditor")) {
                    $(this).next("div").after(o);
                    $(this).css("border", "1px solid #ebccd1");
                } else {
                    if (this.id == "month") {
                        $(this).next().after(o)
                    } else if (this.id == "date") {
                        $(this).next().after(o)
                    } else if (this.id == "year") {
                        $(this).next().after(o)
                    } else if (this.id == "v_social_secutiry_number") {
                        $(this).next().after(o)
                    } else {
                        $(this).after(o)
                    }
                    $(this).parent().addClass('has-error');
                    $(this).css("border", "1px solid #ebccd1");
                }
            }
        }
    });
    return n
}

/* End custom code for developer */

var err_element_custom = "span";
var err_class_custom = "help-block help-block-error";

var err_element = "div";
var err_class = "error-inner";
$(document).ready(function() {
    if ($("#CkEditor").length > 0) {
        var e = CKEDITOR.replace("CkEditor");
        e.on("blur", function(t) {
            $("#CkEditor").val(e.getData());
            if ($("#CkEditor").val() != "") {
                $("#CkEditor_error").remove()
            }
        });
        e.on("focus", function(t) {
            $("#CkEditor").val(e.getData());
            if ($("#CkEditor").val() != "") {
                $("#CkEditor_error").remove()
            }
        })
    }
    if ($("#CkEditor2").length > 0) {
        var e = CKEDITOR.replace("CkEditor2");
        e.on("blur", function(t) {
            $("#CkEditor2").val(e.getData());
            if ($("#CkEditor2").val() != "") {
                $("#CkEditor2_error").remove()
            }
        });
        e.on("focus", function(t) {
            $("#CkEditor2").val(e.getData());
            if ($("#CkEditor2").val() != "") {
                $("#CkEditor2_error").remove()
            }
        })
    }
    if ($("#vCkEditor").length > 0) {
        var e = CKEDITOR.replace("vCkEditor", {
            toolbar: [{
                name: "basicstyles",
                items: ["Bold", "Italic", "Underline", "Link"]
            }]
        });
        e.on("blur", function(t) {
            $("#vCkEditor").val(e.getData());
            if ($("#vCkEditor").val() != "") {
                $("#vCkEditor_error").remove()
            }
        });
        e.on("focus", function(t) {
            $("#vCkEditor").val(e.getData());
            if ($("#vCkEditor").val() != "") {
                $("#vCkEditor_error").remove()
            }
        })
    }
    $("input,select,textarea").on("blur", function(e) {
        var t = this.id;
        var n = $.trim($(this).val());
        var r = "";
        if ($(this).attr("placeholder") !== undefined) {
            r = $(this).attr("placeholder")
        }
        var i = "<" + err_element + ' class="' + err_class + '" id="' + this.id + '_error">';
        var s = ".</" + err_element + ">";
        var o = "";
        flag = true;
        if ($(this).hasClass("email") && n != "" && n !== undefined && n != r) {
            var u = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm);
            if (!u.test(n)) {
                o = "Please enter valid " + r.toLowerCase();
                flag = false
            }
        }
        if ($(this).hasClass("url") && n != "" && n !== undefined && n != r) {
            var u = new RegExp(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/);
            if (!u.test(n)) {
                o = "Please enter valid URL";
                flag = false
            }
        }
        if ($(this).hasClass("digits") && n != "" && n !== undefined && n != r) {
            if (!n.match(/^[0-9]$/)) {
                o = "Please enter valid digits";
                flag = false
            }
        }
        if ($(this).hasClass("grezero") && n != "" && n !== undefined && n != r) {
            if (!n.match(/^(?!0(\.0*)?$)\d+(\.?\d{0,2})?$/)) {
                o = "Please enter number greater than zero";
                flag = false
            }
        }
        if ($(this).hasClass("alpha") && n != "" && n !== undefined && n != r) {
            if (!n.match(/^[A-Za-z ]+$/)) {
                o = "Please enter valid name" + r.toLowerCase();
                flag = false
            }
        }
        if ($(this).hasClass("length") && n != "" && n !== undefined && n != r) {
            var u = new RegExp(/^7\d{7}$/);
            if (!u.test(n)) {
                o = "Please enter valid " + r.toLowerCase();
                flag = false
            }  
        }
        if ($(this).hasClass("length_omang_number") && n != "" && n !== undefined && n != r) {
            /*var u = new RegExp(/^(\d{9}?)?$/);*/
            var u = new RegExp(/^[a-zA-Z0-9]{9}$/);
            if (!u.test(n)) {
                o = "Please enter valid " + r.toLowerCase();
                flag = false
            }  
        }
        if ($(this).hasClass("length_withdraw_pin") && n != "" && n !== undefined && n != r) {
            var u = new RegExp(/^[A-Za-z0-9]{4}$/);
            if (!u.test(n)) {
                o = "Please enter valid " + r.toLowerCase();
                flag = false
            }  
        }
        if ($(this).hasClass("number") && n != "" && n !== undefined && n != r) {
            if (!n.match(/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/)) {
                o = "Please enter valid number";
                flag = false
            }
        }
        if ($(this).hasClass("validate_zip") && n != "" && n !== undefined && n != r) {
            var u = new RegExp(/^[0-9]{5,}/);
            if (!u.test(n)) {
                o = "Minimum 5 character required";
                flag = false
            }
        }
        if ($(this).hasClass("validate_creditcard") && n != "" && n !== undefined && n != r) {
            var u = new RegExp(/^\d{15,16}$/);
            if (!u.test(n)) {
                o = "Please enter valid " + r.toLowerCase();
                flag = false
            }
        }
        if ($(this).hasClass("validate_month") && n != "" && n !== undefined && n != r) {
            var a = /^[0-9]{1,2}$/;
            if (!a.test(n)) {
                o = "Please enter valid " + r.toLowerCase();
                flag = false
            }
        }
        if ($(this).hasClass("validate_year") && n != "" && n !== undefined && n != r) {
            var a = /^[0-9]{4}$/;
            var f = (new Date).getFullYear();
            if (!a.test(n) || parseInt(n) < parseInt(f)) {
                o = "Please enter valid " + r.toLowerCase();
                flag = false
            }
        }
        if ($(this).hasClass("validate_cvccode") && n != "" && n !== undefined && n != r) {
            var a = /^[0-9]{3}$/;
            if (!a.test(n)) {
                o = "Please enter valid " + r.toLowerCase();
                flag = false
            }
        }
        if ($(this).attr("equalTo") !== undefined) {
            if (n != "Confirm Password" && $.trim($("#" + $(this).attr("equalTo")).val()) != n) {
                o = "Password does not match";
                flag = false
            }
        }
        if ($(this).hasClass("validate_password") && n != "" && n !== undefined && n != r) {
            var a = /[a-zA-Z0-9\!\@\#\$\.\%\^\&\*\(\)\_\\ \+]{5,}/;
            if (!a.test(n)) {
                o = "Minimum 5 characters required";
                flag = false
            }
        }
        if ($(this).hasClass("validate_social_secutiry") && n != "" && n !== undefined && n != r) {
            var a = /[a-zA-Z0-9\!\@\#\$\.\%\^\&\*\(\)\_\+]{9,}/;
            if (!a.test(n)) {
                o = "Invalid social security number";
                flag = false
            }
        }
        if (this.id == "v_email" && $("#v_email").attr("class").indexOf("duplicate-email-error") >= 0) {
            flag = false;
            o = "Email address already exist";
        }
        if ($(this).hasClass("phone") && n != "" && n !== undefined && n != r) {
            var u = /^[0-9()\s]+$/;
            if (!u.test(n)) {
                o = "Please enter valid phone";
                flag = false
            }
        }
        if ($(this).hasClass("length") && n != "" && n !== undefined && n != r) {
            var u = new RegExp(/^7\d{7}$/);
            if (!u.test(n)) {
                o = "Please enter valid " + r.toLowerCase();
                flag = false
            }  
        }
        if ($(this).hasClass("check-url-char") && n != "" && n !== undefined && n != r) {
            var u = new RegExp(/[a-zA-Z0-9-]/g);
            if (!u.test(n)) {
                o = "Please enter valid text";
                flag = false
            }
        }
        
        if($(this).hasClass('main_password') && !$(this).hasClass('passMeter-strong') && $(this).val() != ""){
            o = 'Please use 8-12 characters, include an uppercase letter, include a lower case letter and include a number. Do not use commonly used phrases';
            flag = false;
        }
        if($(this).hasClass('main_password1') && !$(this).hasClass('passMeter-strong') && $(this).val() != ""){
            o = 'Please enter valid password';
            flag = false;
        }
        

        if ($(this).hasClass("required") && (n == "" || n == undefined || n == r)) {
            if ($(this).attr("type") !== undefined && $(this).attr("type") == "file") {
                o = "Please upload file"
            } else if ($(this).attr("type") !== undefined && $(this).attr("type") == "hidden") {
                o = "Please select any one."
            } else if ($(this).is("select")) {
                o = "Please select " + r.toLowerCase()
            } else {
                o = "Please enter " + r.toLowerCase()
            }
            flag = false
        }
        if ($(this).hasClass("required-least-one") && $(this).attr("groupid") != "" && $(this).attr("groupid") != undefined) {
            if ($('input[groupid="' + $(this).attr("groupid") + '"]:checked').length < 1) {
                o = "Please select any option";
                flag = false
            }
        }
        
        if (!flag && o != "") {} else {
            $("#" + t + "_error").remove();
            $("#_error").remove();
            $(this).css("border", "");
            if ($("#div_validation_msg").is(":empty")) {
                $("#div_validation_msg").hide()
            }
        }
        return flag
    })
})