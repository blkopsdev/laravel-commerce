
/* Script on scroll
------------------------------------------------------------------------------*/
$(window).scroll(function() {
	//---- main banner script ----- //
	
	//----- news banner script----- //
});

/* Script on resiz
------------------------------------------------------------------------------*/
$(window).resize(function() {
    
});

/* Script on load
------------------------------------------------------------------------------*/
$(window).load(function() {
	
});

/* Script on ready
------------------------------------------------------------------------------*/	
$(document).ready(function(){
	
	//------- mobile navigation menu script ---------//
	$(".mobile-nav-icon ").click(function(){
		if($(this).hasClass("active")){
			$(this).removeClass("active");
			$(this).removeClass("icon-cancel");
			$(this).addClass("icon-mobile-menu");
			$("#wrapper, footer").animate({"left":"0"},500);
		}else{
			$(this).addClass("active");
			$(this).addClass("icon-cancel");
			$(this).removeClass("icon-mobile-menu");
			$("#wrapper, footer").animate({"left":"-70%"},500); 	
		}	
	});
	
	//-----styles select----- //
	if($('.custom-selectbox').length>0){
	var config = {
		'.chosen-select'           : {},
		'.chosen-select-deselect'  : {allow_single_deselect:true},
		'.chosen-select-no-single' : {disable_search_threshold:10},
		'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		'.chosen-select-width'     : {width:"95%"}
	  }
	  for (var selector in config) {
		$(selector).chosen(config[selector]);
	  }
	}
	
	//------- accordion script ---------//
	//$('.accordion-content').slideUp();
	$('.accordion-title').click(function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$(this).next('.accordion-content').slideUp(500).removeClass('active');
					
		}else{
			$(this).addClass('active');
			$(this).next('.accordion-content').slideDown(500).addClass('active');
		}
	});
	
	//------- fancybox script ---------//
	
		$('.fancybox').fancybox( function(){
		
	});
	
	/* $('#submit-card-no').click(function(){
		$.fancybox.open('#account-content');
	});	*/	
	
});

/* Script all functions
------------------------------------------------------------------------------*/	
	//---- sticky footer script ----- //
	function StickyFooter(){
		var Stickyfooter =    $('footer').outerHeight()
		$('#wrapper').css('margin-bottom',-Stickyfooter)
		$('#wrapper .footer-push').css('height',Stickyfooter)
	}
	$(window).on("load resize scroll ready",function(){
		var width = $(window).width();
		if( width <= 641){
			$('html,body').css("height","auto");
		}
		else{
			StickyFooter()
			$('html,body').css("height","100%");
		}
	});
