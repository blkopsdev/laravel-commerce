var project_array;
var durl='';
var TableAjax = function (url) {

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            autoclose: true
        });
    }

    var handleRecords = function (url, order_array,pagination,info,page_menu) {
        durl = url;
       
        if(order_array == ''){
            order_array = [0, "asc"];
        }else if (order_array == 'desc'){

            order_array = [0, "desc"];
        }if(pagination == ''){
            pagination = true;
        }
        if(pagination == 'false'){
            pagination = false;
        }
        if(info == 'false'){
            info =false;
        }
        if(page_menu == '1'){
            page_menu = [
                    [100, 200, 300, 400, 500],
                    [100, 200, 300, 400, 500] // change per page values here
                ];
            pageLength = 100;
        } else {
            page_menu = [
                    [10, 20, 50, 100, 150, 500],
                    [10, 20, 50, 100, 150, 500] // change per page values here
                ];
            pageLength = 10;
        }
        var grid = new Datatable();
        var table = $('#datatable_ajax'); // actual table jquery object
        grid.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid) {
                  //Store filter data on cookie 
                  if (url.indexOf("list-ajax") >= 0){
                if ($.cookie(url) === null || $.cookie(url) === "") {} else {
                    $.removeCookie(url);
                }

                var filterData = grid.getAjaxParams();
                
                var cookieData = {};
                var order_sort = grid.getOrder();

                $.each(filterData, function(index, value) {
                    if (index == 'page' || index == 'length') {
                        cookieData[index] = value;
                    }
                    if ($('[name=' + index + ']').length > 0) {
                        $('[name=' + index + ']').val(value);
                        if ($('[name=' + index + ']').data('select2') != undefined) {
                            $('[name=' + index + ']').select2('val', value);
                        }
                        cookieData[index] = value;
                    }
                });
                
                if(order_sort.length > 0){
                    cookieData['order_sort'] = {};
                    cookieData['order_sort']['column'] = order_sort[0][0];
                    cookieData['order_sort']['sort'] = order_sort[0][1];
                }

                if (url == SITE_URL + 'contacts/list-ajax') {
                    setAddedDate();
                }
                if (url.indexOf("list-ajax") >= 0){
                    $.cookie(url, JSON.stringify(cookieData));
                }
            }
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
               // "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                
                "lengthMenu": page_menu,
                "pageLength": pageLength, // default record count per page
                "ajax": {
                    "url": url, // ajax source
                },
                "bPaginate" : pagination,
                "bInfo" : info,
                "columnDefs":[{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                'displayStart': 0,
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                  // Bold the grade for all 'A' grade browsers
                    setTimeout(function(){
                        $('span.total_cost').each(function() {
                            var total_cost = $(this).attr('data-rel-total');                            
                            switch (total_cost) {
                                case 'total_cost':
                                    $(this).closest('tr').find("td:gt(0)").remove();
                                    $(this).closest('tr').find("td:first").attr('colspan','12');
                                    break;
                            }
                        });
                        $(nRow).find('.make-switch').bootstrapSwitch();
                        $('span.data-hidden-val').each(function() {
                            var val = $(this).attr('data-hidden-val');                            
                            switch (val) {
                                case '1':
                                    $(this).closest('tr').addClass('hidden-light-red');
                                    break;                                
                            }
                        });
                },1);
                },
                "order":  order_array ,// set first column as a default sort by asc
                "bStateSave" : true
              
            }
        });

        // handle group actionsubmit button click
        /*grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                Metronic.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                Metronic.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });*/
        // handle group actionsubmit button click
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            var alert_message = '';
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            var url = $(".table-group-action-url", grid.getTableWrapper()).val();
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                 var send_data = {};
                 send_data.action = action.val();
                 send_data['ids'] = grid.getSelectedRows();
                 if(send_data.action=='Delete'){
                    bootbox.confirm('Are you sure you want to delete this record?', function (confirmed) {
                    if(confirmed){ 
                            $.post(url,send_data, function(data) {
                                if(data == 'TRUE') {
                                    send_data_action = send_data.action.toLowerCase();
                                        if(send_data_action == 'active') {
                                            send_data_action = 'actived';
                                        } else if(send_data_action == 'inactive') { 
                                            send_data_action = 'inactived';
                                        } else if(send_data_action == 'delete') {
                                            send_data_action = 'deleted';
                                        }
                                    Metronic.alert({
                                        type: 'success',
                                        icon: 'check',
                                        message: 'Record has been '+send_data_action +' successfully.',
                                        container: grid.getTableWrapper(),
                                        place: 'prepend'
                                    });
                                    $.each(send_data['ids'], function(i,id) {
                                        if(send_data.action == 'Active'){
                                            $('tbody > tr > td > .status_' + id, table).addClass('label-success');
                                            $('tbody > tr > td > .status_' + id, table).removeClass('label-danger');
                                            $('tbody > tr > td > .status_' + id, table).text(send_data.action);
                                           // $('#datatable_ajax').DataTable().ajax.reload();
                                        } else if(send_data.action == 'Inactive'){
                                            $('tbody > tr > td > .status_' + id, table).addClass('label-danger');
                                            $('tbody > tr > td > .status_' + id, table).removeClass('label-success');
                                            $('tbody > tr > td > .status_' + id, table).text(send_data.action);
                                            //$('#datatable_ajax').DataTable().ajax.reload();
                                        } else {
                                           $('tbody > tr > td  .cheched').closest('tr').fadeOut(1500, function() {
                                                $(this).closest('tr').remove();
                                                //$('#datatable_ajax').DataTable().ajax.reload();
                                                if($("#datatable_ajax tbody > tr").length <= 1) {
                                                    $(".filter-submit").trigger( "click" );
                                                }
                                           });  
                                        }
                                    });
                                    $('#datatable_ajax').DataTable().ajax.reload();
                                    setTimeout(function(){ $('.alert-success').fadeOut(4000); },3000);      
                                    var set = $('tbody > tr > td:nth-child(1) input[type="checkbox"]', table);
                                    var checked = $(this).is(":checked");
                                    $(set).each(function() {
                                        $(this).attr("checked", false);
                                    });
                                    $('.table-group-action-input').val('');
                                    $('.group-checkable').attr("checked", false);
                                    $.uniform.update(set, table);
                                    $.uniform.update($('.group-checkable', table));
                                }
                            });
                        }
                    }); 
                }else{
                    $.post(url,send_data, function(data) {
                                if(data == 'TRUE') {
                                    send_data_action = send_data.action.toLowerCase();
                                        if(send_data_action == 'active') {
                                            send_data_action = 'actived';
                                        } else if(send_data_action == 'inactive') { 
                                            send_data_action = 'inactived';
                                        } else if(send_data_action == 'delete') {
                                            send_data_action = 'deleted';
                                        }
                                    Metronic.alert({
                                        type: 'success',
                                        icon: 'check',
                                        message: 'Record has been '+send_data_action +' successfully.',
                                        container: grid.getTableWrapper(),
                                        place: 'prepend'
                                    });
                                    $.each(send_data['ids'], function(i,id) {
                                        if(send_data.action == 'Active'){
                                            $('tbody > tr > td > .status_' + id, table).addClass('label-success');
                                            $('tbody > tr > td > .status_' + id, table).removeClass('label-danger');
                                            $('tbody > tr > td > .status_' + id, table).text(send_data.action);
                                           // $('#datatable_ajax').DataTable().ajax.reload();
                                        } else if(send_data.action == 'Inactive'){
                                            $('tbody > tr > td > .status_' + id, table).addClass('label-danger');
                                            $('tbody > tr > td > .status_' + id, table).removeClass('label-success');
                                            $('tbody > tr > td > .status_' + id, table).text(send_data.action);
                                            //$('#datatable_ajax').DataTable().ajax.reload();
                                        } else {
                                           $('tbody > tr > td  .cheched').closest('tr').fadeOut(1500, function() {
                                                $(this).closest('tr').remove();
                                                //$('#datatable_ajax').DataTable().ajax.reload();
                                                if($("#datatable_ajax tbody > tr").length <= 1) {
                                                    $(".filter-submit").trigger( "click" );
                                                }
                                           });  
                                        }
                                    });
                                    $('#datatable_ajax').DataTable().ajax.reload();
                                    setTimeout(function(){ $('.alert-success').fadeOut(4000); },3000);      
                                    var set = $('tbody > tr > td:nth-child(1) input[type="checkbox"]', table);
                                    var checked = $(this).is(":checked");
                                    $(set).each(function() {
                                        $(this).attr("checked", false);
                                    });
                                    $('.table-group-action-input').val('');
                                    $('.group-checkable').attr("checked", false);
                                    $.uniform.update(set, table);
                                    $.uniform.update($('.group-checkable', table));
                                }
                            });
                }
                    
            } else if (action.val() == "") {
                /*Metronic.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });*/
                bootbox.alert('Please select an action.');
            } else if (grid.getSelectedRowsCount() === 0) {
               /* Metronic.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });*/
                bootbox.alert('Please select checkbox(s).');
                $('.table-group-action-input').val('');
            }
        });
        
        $(".form-filter").val('');
        // handle group actionsubmit button click
        $(document).on('click', '#export_to_excel', function (e) {
             var send_data = $('#datatable_ajax').DataTable().ajax.params();
             var send_data = $.param(send_data);
             var url = $(this).attr('action-url');
             window.location.href = url+ '?' + send_data;
        });
    }

    return {

        //main function to initiate the module
        init: function (url, order_array,pagination,info,page_menu) {
            initPickers();
            
            if(order_array == undefined){
                order_array = '';
            }
            if(pagination == undefined){
                pagination = true;
            }
            if(info == undefined){
                info = true;
            }
            handleRecords(url, order_array,pagination,info,page_menu);
        }

    };

}();