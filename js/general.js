function equal(){
	$(".payment").each(function(){
		var max = 1; 
		$(this).find(".link").css("height","auto"); 
		$(this).find(".link").each(function() {
			var height1 = $(this).height(); 
			max = (height1 > max) ? height1 : max;
		});
		//jQuery(this).find(".equal-height").css("height",max); 
		$(this).find(".link").css("height",max+30); 
	}) 
}
$(window).load(equal);
$(window).resize(equal);

$(document).ready(function(e) {
    var tId;
	/* Home Page Slider
	-----------------------------------------*/
	$('#slides').slidesjs({
		width: 1000,
		height: 410,
		navigation: true,
		play: {
			active: false,
			effect: "fade",
			interval: 5000,
			auto: true,
			swap: true,
			pauseOnHover: false,
			restartDelay: 2500
		},
		navigation: {
			active: true,
			effect: "fade"
		},
		effect: {
			fade: {
			speed: 300,
			crossfade: true
			}
		},
		pagination: {
			active: false,
			effect: "fade"
		}
	});
	
	/* Mobile Menu
	-----------------------------------------*/
	var nav_clone = $("header nav").clone().addClass("mobile_menu cf");
	var support = $("header .support").clone();
	$("body").append(nav_clone);
	$(".mobile_menu").wrap("<nav class='mobile_menu_outer'>");
	$(".mobile_menu_outer").append(support);
	//$("<span class='menu_title'></span>").insertBefore(".mobile_menu");
	$("<span class='menu_title'>Navigation</span>").insertBefore(".mobile_menu");
	
	$(".mobile_menu_icon").click(function() {
        if($(this).hasClass("active")){
			$(this).removeClass("active");
			$("#wrapper").animate({"left":"0"});
		}else{
			$(this).addClass("active");
			$("#wrapper").animate({"left":"-60%"});	
		}
    });
	
	/* Fancybox
	-----------------------------------------*/
    
	if($(".fancybox").length > 0){
		$(".fancybox").fancybox({
		  arrows : false,
		  afterClose:function(){
                $(".smartcard").val("");
                $(".input_error").html("");
            }
		});
        $(".fancybox2").fancybox({
            'width': 530 // set the width
        });
        $(".fancybox3").fancybox({
            'frameWidth': 530, // set the width
        });
        $(".fancybox4").fancybox({
            'frameWidth': 530, // set the width
        });
	}
    
    //for bank account
    
    $("body").on("click" , "#smart_submit" , function(){
       data = $("#check_number_form").serialize();
       $.ajax({
            type: "POST",
            url: "check_number",
            data: data,
            success:function(result){
                if(result == "False")
                {
                    $(".smartcard").css("margin-bottom","1px")
                    $(".input_error").css("display","block");
                    $(".input_error").html('Invalid smartcard number.').css("color" , "red");
                }
                else{
                    $("#bank_account").html(result);
                    $(".smartcard").val('');
                    $(".fancybox2").click();
                }
            }
       }); 
    });
    
    $("body").on("click" , ".smartcard" , function(){
       $(".input_error").html(''); 
    });
    
    //for ewallet
    
    $("body").on("click" , "#smart_submit_2" , function(){
       data = $("#check_number_form_2").serialize();
       $.ajax({
            type: "POST",
            url: "check_number",
            data: data,
            success:function(result){
                if(result == "False")
                {
                    $(".smartcard").css("margin-bottom","1px")
                    $(".input_error").css("display","block");
                    $(".input_error").html('Invalid smartcard number.').css("color" , "red");
                }
                else{
                    $("#wallet").html(result);
                    $(".smartcard").val('');
                    $(".fancybox3").click();
                }
            }
       }); 
    });
    
    $("body").on("click" , "#smart_submit_3" , function(){
       data = $("#check_number_form_3").serialize();
       $.ajax({
            type: "POST",
            url: "check_number",
            data: data,
            success:function(result){
                if(result == "False")
                {
                    $(".smartcard").css("margin-bottom","1px")
                    $(".input_error").css("display","block");
                    $(".input_error").html('Invalid smartcard number.').css("color" , "red");
                }
                else{
                    $("#orange_money").html(result);
                    $(".smartcard").val('');
                    $(".fancybox4").click();
                }
            }
       }); 
    });
	
	/* FAQ
	-----------------------------------------*/
	$(".faq_block .faq_list .title").click(function() {
        if($(this).hasClass("active")){
			$(this).removeClass("active");
			$(".faq_block .faq_list .faq_desc").slideUp();
		}else{
			$(".faq_block .faq_list .title").removeClass("active");
			$(this).addClass("active");
			$(".faq_block .faq_list .faq_desc").slideUp();
			$(this).next(".faq_desc").slideDown();
		}
    });
    
    //Packages
    
    $('.row.packages').each(function(){  
            
            var highestBox = 0;
            $('.special', this).each(function(){
            
                if($(this).height() > highestBox) 
                   highestBox = $(this).height(); 
            });  
            
            $('.special',this).height(highestBox);
            
        
    });
    
    //contact us form submission
    
    $("body").on("click" , "#contact_submit" , function(){
        if(form_valid("#contact_us"))
        {
            data = $("#contact_us").serialize();
            $.ajax({
                type: 'POST',        
        		url: "captcha-test",
                data: data,
        		success: function(response)
        		{
        		  if(response == "Incorrect")
                  {
                    $(".invalid_captcha").html("Please try again");
                    $("#captcha_img").attr("src" , "captcha?"+Math.floor(+new Date() / 1000));
                  }
                  else
                  {
                    $(".invalid_captcha").html('');
                    $("#contact_us")[0].reset();
                    $("#captcha_img").attr("src" , "captcha?"+Math.floor(+new Date() / 1000));
                    $.ajax({
                        type: 'POST',        
                		url: "form_submit",
                        data: data,
                		success: function(response)
                		{
                		  if(response == "SUCCESS")
                          {
                            $(".success_msg").html("<span class='submit_success'><b>Your form has been submitted successfully. Thank you.</b></span>");
                            clearTimeout(tId);
                                tId=setTimeout(function(){
                                  $(".success_msg").html("");        
                            }, 4000);
                          }
                		  
                        }
                    })
                  }
                }
            })
        }
        else
        {
            return false;
        }
    });
    
    $("body").on("click" , ".form-control" , function(){
        $(".invalid_captcha").html('');
    })    
	
});

/* News
-----------------------------------------*/

    // on load generate all news using ajax
    $(document).ready(function(){
    
    // applying isotope
		var $container = $('#news');
        
		$container.isotope({
		  itemSelector : '.news_box',
		  masonry: {
			columnWidth: 1
		  }
		});
     
    var records = 6;
    var skip_rec = 6;
    var months = '';
    var month_name = "";
    $("body").on("click" , ".news .view_all" , function(){
    var url = "view_all" ; 
     	$.ajax(
        {
    		type: 'POST',        
    		url: url,
            data: "data="+records+"&"+"skip_rec="+skip_rec,
    		success: function(response)
    		{
                data = response['news'];
                last = response['last'];
                if(last == "Yes")
                {
                    $(".view_all").attr('id' , "view_hide");
                }
                for(i=0 ; i<data.length;i++)
                {
                    var month = data[i]['d_date'].split('-');
                    day = month[2];
                        months = (month[1]);
                        
                        if(months == 01)
                        {
                            month_name = "JAN";
                        }
                        if(months == 02)
                        {
                            month_name = "FEB";
                        }
                        if(months == 03)
                        {
                            month_name = "MAR";
                        }
                        if(months == 04)
                        {
                            month_name = "APR";
                        }
                        if(months == 05)
                        {
                            month_name = "MAY";
                        }
                        if(months == 06)
                        {
                            month_name = "JUN";
                        }
                        if(months == 07)
                        {
                            month_name = "JUL";
                        }
                        if(months == 08)
                        {
                            month_name = "AUG";
                        }
                        if(months == 09)
                        {
                            month_name = "SEP";
                        }
                        if(months == 10)
                        {
                            month_name = "OCT";
                        }
                        if(months == 11)
                        {
                            month_name = "NOV";
                        }
                        if(months == 12)
                        {
                            month_name = "DEC";
                        }
                     var $elems = ('<ul class="row" id="news"><li class="col-md-4 news_box"><div class="news_inner cf"><div class="date"><span>'+month_name+'<strong>'+day+'</strong></span></div><a href="news-detail/'+((data[i]['id']*12345)+12345)+'">'+data[i]['v_news_title']+'</a><p>'+data[i]['v_short_desc']+'</p><a href="news-detail/'+((data[i]['id']*12345)+12345)+'" class="read_more">Read More</a></div></li></ul>');
                    
                    $container.append( $elems).isotope( 'appended', $elems );
                    
                    $container.isotope('destroy'); 

                            $container.isotope({
                        		  itemSelector : '.news_box',
                        		  masonry: {
                        			columnWidth: 1
                        		  }
                    		});
                }
                records = records + 6;
    		}
        });
        });
      });