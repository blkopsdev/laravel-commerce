@extends('admin.layouts.default')
@section('content')
<style>
    .error-inner{color: red;}
    .duplicate-error{color: red;}
    .profile-userpic img{ border-radius: 0 !important; }
    .error-inner {color:red;}
    .duplicate-error{color:red;}
</style>
<script>
        $(document).ready(function(){
            $("#t_email_content").ckeditor();
        });
    </script>
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Site Setting <small></small></h1>
        </div>        
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="portlet light" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green-sharp bold uppercase">
                                    <i class="fa fa-users">
                                    </i>
                                    Site Setting
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body form">                            
                            <form role="form" method="post" class="form-horizontal" id="update_setting" name="update_setting" onsubmit="return false;" action="{{ADMIN_URL}}my_settings" enctype="multipart/form-data">
                            {!! csrf_field() !!}     
                                            <div class="form-wizard">
                                    <div class="form-body">
                                        <div class="tab-content">
                                            <div class="alert alert-danger display-none">
                                                <button class="close">
                                                </button>
                                                You have some form errors. Please check below.
                                            </div>
                                            <?php $msg = Session::get('msg'); if($msg){ ?>
                                            <div class="alert alert-danger">
                                                <button class="close"></button>
                                                {{ $msg }}
                                            </div>
                                            <?php } ?>
                                            <div class="alert alert-success display-hide">
                                                <span class="close" data-close="alert"></span>
                                                Your personal information has been successfully updated.
                                            </div>
                                            <div class="alert alert-danger display-hide">
                                                <span class="close" data-close="alert"></span>
                                                You have some form errors. Please check below.
                                            </div>
                                            <div class="tab-pane active" id="tab1">  
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Template Email ID
                                                    <span class="redLabel">
                                                        *
                                                    </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                            <input type="text" id="v_email" name="v_email" class="form-control required email" placeholder="Email Id" value="{{$admin_data['v_email']}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Template Site Url
                                                    <span class="redLabel">
                                                        *
                                                    </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                            <input type="text" name="v_site_url" id="v_site_url" class="form-control" placeholder="Site URL" value="{{$setting_data['v_site_url']}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-2 col-md-8">
                                                <button type="submit" class="btn blue-madison button-next">Save Changes  <i class="fa fa-check-square-o "></i></button>      
                                                <a href="{{ ADMIN_URL }}dashboard" class=" btn default button-previous">Cancel </a>                        
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    </div>
</div>
<script>
$(document).ready(function(){
    handleProfileForm();
    $('#tab_2_visible').removeClass('tab_2_visible'); 

});
</script>
@stop