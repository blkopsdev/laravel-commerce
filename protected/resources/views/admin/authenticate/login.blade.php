@extends('admin.layouts.master')
@section('content')	

<style>
    .error-inner {color:red;}
    .duplicate-error{color:red;}
</style>
<div class="logo">
    <a href="{{ ADMIN_URL }}">
        <img src="{{ SITE_URL }}img/logo.png" alt=""/>
    </a>
</div>
<div class="content" id="loginform" @if($forgotpass_flag == "1") style="display:none" @endif>
    <form action="{{ URL::route('form-admin-login') }}" id="frmEmpLogin" name="frmLogin" class="login-form" method="POST">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
            <h3 class="form-title">Login to your account</h3>
        <?php $msg = Session::get('message'); if($msg) { ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-close="alert"></button>
                <span>{{ $msg }}</span>
            </div>
  
        <?php } ?>
        <?php
            $success_msg = Session::get('msg');
            if($success_msg) { 
        ?>
            <div class="alert alert-success">
    			<button type="button" class="close" data-close="alert"></button>
    			{{ $success_msg }}
    		</div>
            <?php } ?>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Email Id</label>
                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username or Email" id="uname" name="uname" value="{{ Cookie::get('user_name') }}" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input class="form-control placeholder-no-fix  required" type="password" autocomplete="off" placeholder="Password" name="password" id="password" value="{{ Cookie::get('password') }}" />
                </div>
            </div>
            <label class="checkbox"><input type="checkbox" name="remember" value="1" id="remember"/> Remember me </label>
            <div class="form-actions clearfix">
                <button type="submit" class="btn blue-madison pull-right" id="login-btn" >
                    Login <i class="m-icon-swapright m-icon-white"></i>
                </button>
            </div>
            <div class="forget-password"> 
                <h4>Forgot your password ?</h4>
                <p>
                    No worries, click <a href="javascript:void(0);" id="forget-password">
                    here </a>
                    to reset your password.
                </p>
            </div>
      </form>
</div>
<div id="forget_password" class="content" @if($forgotpass_flag == "1") style="display:block" @endif>
   <form action="{{ URL::route('form-admin-forgot') }}" id="forgot_pass_form" name="frmLogin" class="form-horizontal" method="POST">
     {!! csrf_field() !!}
        <h3>Forgot Password ?</h3>         
		<p>
			 Enter your e-mail address below to reset your password.<br>
            <strong>Vendor:</strong>  Enter your username below to reset your password.
        </p>          
        <?php $msg = Session::get('forgot_message'); if($msg) { ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-close="alert"></button>
                <span>{{ $msg }}</span>
            </div>
  
        <?php } ?>
        <?php
            $success_msg = Session::get('forgot_msg');
            if($success_msg) { 
        ?>
            <div class="alert alert-success">
    			<button type="button" class="close" data-close="alert"></button>
    			{{ $success_msg }}
    		</div>
        <?php } ?>
		<div class="form-group" style="padding-left: 18px; border-left: 0 none;">
                <input type="text" class="form-control placeholder-no-fix required" name="email" placeholder="Email or Username" autocomplete="off" />
			
		</div>
		<div class="form-actions">
			<button class="btn default" id="back-login" type="button">
			<i class="m-icon-swapleft m-icon-white"></i> Back </button>
			<button class="btn blue-madison pull-right" type="submit" id="forgot_btn">
			Submit <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
	</form> 
</div>
<div class="copyright">
   {{ date('Y') }} &copy; {{ SITE_NAME }}. All Rights Reserved.
</div>
<script>handleLoginForm();</script>
@stop