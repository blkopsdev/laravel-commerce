@extends('admin.layouts.default')
@section('content')	
<?php
$userArr = Auth::guard('admin')->user();
if(!empty($userArr))
{
$date1=date('F d Y', strtotime($userArr['d_last_login_time']));
$time=date('h:i A', strtotime($userArr['d_last_login_time']));
$userType=($userArr['e_type']=="Super")?"Admin":$userArr['e_type'];
$user_info = '';
if(Session::has('USER_DATA')){
	$user_info=Session::get('USER_DATA');	
}
if($userArr['e_type']=="Super")
{
	$DashboardMsg="Welcome ".$userArr['v_username']." to Drop Ship Administrator";
}else if($userArr['e_type']=="Client" && $user_info != '')
{
	if(isset($user_info->Client)){ 
		$DashboardMsg="Welcome ".$user_info->Client->v_firstname." ".$user_info->Client->v_lastname." to Drop Ship Administrator";
	}else{
		$DashboardMsg="Welcome";
	}
}else if($userArr['e_type']=="Vendor" && $user_info != '')
{
	if(isset($user_info->Vendor)) { 
		$DashboardMsg="Welcome ".$user_info->Vendor->v_vendor_name." to Drop Ship Administrator";
	}else{
		$DashboardMsg="Welcome";
	}
}else if($userArr['e_type']=="Employee" && $user_info != '')
{
	if($userArr['i_emp_id']!=0) {
		$DashboardMsg="Welcome ".$user_info->Employee->v_vname." to Drop Ship Administrator";
	}else{
		$DashboardMsg="Welcome ".$user_info->DropshipEmployee->v_vname." to Drop Ship Administrator";
	}
	
}else{
	$DashboardMsg="Welcome";
}
?>
	<div class="page-head">
		<div class="container-fluid">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Dashboard <small>statistics & reports</small></h1>
			</div>
		</div>
	</div>
	        <div class="page-content">
            <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
               	    <div class="portlet light">
					   <div class="portlet-title">
                          <div class="caption"><i class="fa fa-cogs font-green-sharp"></i>
							<span class="caption-subject font-green-sharp bold uppercase">{{$DashboardMsg}}</span></div>
					</div>                    
                <div class="portlet-body">
                <center>
                    <div class="table-container ">
                            <table class="table table-striped table-hover table-bordered dashboard-table"> 
                                <thead>                
                                    <tr role="row" class="heading">
                                      	 <th>
			                                Resource
			                            </th>
			                            <th>
			                                Used
			                            </th>
                                    </tr>
                                </thead>
                                <tbody>
	                        		<tr>
									<td>Last Login Date
									</td>
									<td >{{$date1}} ( {{$time}})</td>
									</tr>
									<tr>
										<td >From IP
										</td>
										<td >{{$userArr['v_ip_address']}}</td>
									</tr>
                                </tbody>
                            </table>
                        </div>
                </center>
                    </div> 
                    </div>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-12">
            			@if($userArr['role']==1 || (isset($UserPermission["2"]["e_view"]) && $UserPermission["2"]["e_view"]  == 1))
		            	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="dashboard-stat blue-madison">
								<div class="visual">
									<i class="fa fa-users"></i>
								</div>
								<div class="details">
									<div class="number">
										 {{ $responseData['Client']}}
									</div>
									<div class="desc">
										 Client
									</div>
								</div>
								<a class="more" href="{{ ADMIN_URL }}client">
								View more <i class="m-icon-swapright m-icon-white"></i>
								</a>
							</div>
						</div>
						@endif
						@if($userArr['role']==1 || (isset($UserPermission["3"]["e_view"]) && $UserPermission["3"]["e_view"]  == 1))
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="dashboard-stat red-intense">
								<div class="visual">
									<i class="fa fa-users"></i>
								</div>
								<div class="details">
									<div class="number">
										 {{ $responseData['Vendor']}}
									</div>
									<div class="desc">
										 Vendor
									</div>
								</div>
								<a class="more" href="{{ ADMIN_URL }}vendor">
								View more <i class="m-icon-swapright m-icon-white"></i>
								</a>
							</div>
						</div>
						@endif
						@if($userArr['role']==1 || (isset($UserPermission["4"]["e_view"]) && $UserPermission["4"]["e_view"]  == 1))
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="dashboard-stat green-haze">
								<div class="visual">
									<i class="fa fa-users"></i>
								</div>
								<div class="details">
									<div class="number">
										 {{ $responseData['Employee']}}
									</div>
									<div class="desc">
										 Employee
									</div>
								</div>
								<a class="more" href="{{ ADMIN_URL }}employee">
								View more <i class="m-icon-swapright m-icon-white"></i>
								</a>
							</div>
						</div>
						@endif
						@if($userArr['role']==1 || (isset($UserPermission["1"]["e_view"]) && $UserPermission["1"]["e_view"]  == 1))
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="dashboard-stat purple">
								<div class="visual">
									<i class="fa fa-tag"></i>
								</div>
								<div class="details">
									<div class="number">
										 {{ $responseData['Order']}}
									</div>
									<div class="desc">
										 Order
									</div>
								</div>
								<a class="more" href="{{ ADMIN_URL }}order">
								View more <i class="m-icon-swapright m-icon-white"></i>
								</a>
							</div>
						</div>
						@endif
						@if($userArr['role']==1 || (isset($UserPermission["6"]["e_view"]) && $UserPermission["6"]["e_view"]  == 1))
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="dashboard-stat yellow-gold">
								<div class="visual">
									<i class="fa fa-tag"></i>
								</div>
								<div class="details">
									<div class="number">
										 {{ $responseData['PurchaseOrder']}}
									</div>
									<div class="desc">
										Purchase Order
									</div>
								</div>
								<a class="more" href="{{ ADMIN_URL }}purchase_order">
								View more <i class="m-icon-swapright m-icon-white"></i>
								</a>
							</div>
						</div>
						@endif
				</div>
            </div>
        </div>
        </div>
    <?php } ?>
@stop

