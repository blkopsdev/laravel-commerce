@extends('admin.layouts.master')
@section('content')	
<script src="{{ SITE_URL }}js/admin/jquery.passMeter.js"></script>
<style>
    .error-inner {color:red;}
    .duplicate-error{color:red;}
    .passMeter-bad{background-color:#fc6c6c;}
    .passMeter-low{background-color:#ff9a68;}
    .passMeter-good{background-color:#70beff;}
    .passMeter-strong{background-color:#9ef78c;}
</style>
<div class="logo">
    <a href="{{ ADMIN_URL }}">
        <img src="{{ SITE_URL }}img/logo.png" alt=""/>
    </a>
</div>
<div id="reset_password">
    <div class="container">
        <div class="portlet light" id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
                        <i class="fa fa-lock"></i> Reset password
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                    <?php if($status == 'force-reset') { ?>
                        <form class="form-horizontal" action="<?php echo ADMIN_URL.'reset-password/'.$record->remember_token.'/force-reset';?>" id="reset_pass_form" name="reset_pass_form" method="POST">
                    <?php } else { ?>
                        <form class="form-horizontal" action="<?php echo ADMIN_URL.'reset-password/'.$record->remember_token;?>" id="reset_pass_form" name="reset_pass_form" method="POST">
                    <?php } ?>
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                        <div class="form-wizard">
                            <div class="form-body">
                                <div class="tab-content">
                                     <?php $msg = Session::get('message'); if($msg) { ?>
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-close="alert"></button>
                                                <span>{{ $msg }}</span>
                                        </div>
                                    <?php } ?>
                                    <div class="alert alert-success display-none">
                                        <button class="close" ></button>
                                        Your form validation is successful!
                                    </div>
                                    <div class="tab-pane active" id="tab1">
                                        <div class="form-group">
                                            <label class="control-label col-md-5">                                     
                                                    Username: 
                                            </label>
                                            <div class="col-md-6">
                                                 <label class="control-label col-md-5" style="font-weight: bold;">
                                                     {{ $record->v_username }}
                                                 </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-5">                                     
                                                    Email: 
                                            </label>
                                            <div class="col-md-6">
                                                 <label class="control-label col-md-5" style="font-weight: bold;">
                                                    {{ $record->v_email }}
                                                 </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-5">New Password <span class="required">* </span>
                                            </label>
                                            <div class="col-md-6">
                                                <input type="password" class="form-control input-icon" name="password" id="password" placeholder="password"/>
                                                <!--div id="password-result"></div-->
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-5">Confirm Password <span class="required">* </span></label>
                                            <div class="col-md-6">
                                                <input type="password" class="form-control input-icon" name="rpassword" id="rpassword" placeholder="confirm password"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-10" style="color: #a94442; font-size: 12px !important; margin-bottom: 10px;"><?php echo "* Please use 8-12 characters, include an uppercase letter, include a lower case letter and include a number. Do not use commonly used phrases."; ?></div>
                                        <div class="col-md-2 text-center">
                                            <button type="submit" class="btn blue-madison pull-right" id="reset-btn" >
                                                Reset <i class="m-icon-swapright m-icon-white"></i>
                                            </button>   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>
<div class="copyright">
   {{ date('Y') }} &copy; {{ SITE_NAME }}. All Rights Reserved.
</div> 
<script>
handleLoginForm();
$(function(){
    $.passMeter({
        // configure the divs
        'id_password'   :   '#password',

        // customize the message levels
        'bad'   :   'Bad',
        'low'   :   'Low',
        'good'  :   'Good',
        'strong'    :   'Strong'
    });
});
</script>
@stop