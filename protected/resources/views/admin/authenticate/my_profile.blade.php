@extends('admin.layouts.default')
@section('content')
<style>
    .error-inner{color: red;}
    .duplicate-error{color: red;}
    .profile-userpic img{ border-radius: 0 !important; }
    .error-inner {color:red;}
    .duplicate-error{color:red;}
     .jcrop-holder div {
      -webkit-border-radius: 50% !important;
      -moz-border-radius: 50% !important;
      border-radius: 50% !important;
      margin: -1px;
    }
</style>
<script>
	$(document).ready(function(){
    	$("#t_email_content").ckeditor();
    });
</script>
<div class="page-container">
    <div class="page-head">
    	<div class="container-fluid">
        	<div class="page-title">
            	<h1>My Profile <small></small></h1>
        	</div>
    	</div>
	</div>
	<div class="page-content">
    	<div class="container-fluid">
        	<div class="row">
                <div class="col-md-12">
                    <div class="portlet light" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green-sharp bold uppercase">
                                    <i class="fa fa-users">
                                    </i>
                                    My Profile
                                </span>
                            </div>
                        </div>
                    	<div class="portlet-body form">
                			<form role="form" method="post" class="<?php echo ((isset($records->Vendor))||($records->Client=='' && $records->Vendor=='' && $records->Employee=='' && $records->DropshipEmployee=='' && $records->Admin==''))?'form-horizontal':'horizontal-form';?>" id="update_profile" name="update_profile" onsubmit="return false;" action="{{ADMIN_URL}}my_profile" enctype="multipart/form-data">
                            @if(Session::has('success-message'))                    
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <i class="fa-lg fa fa-check "></i> 
                                <span class="message">Your personal information has been successfully updated.</span>
                            </div>
                            @endif
                            <div class="alert alert-danger display-hide">
                                    <span class="close" data-close="alert"></span>
                                    You have some form errors. Please check below.
                            </div>
                    		{!! csrf_field() !!} 
                    		@if(isset($records->Employee))
                                <div class="form-body">
                                    <h3 class="form-section">Personal Details</h3>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Username
                                                    <span class="redLabel">
                                                        *
                                                    </span>
                                                </label>
                                                <input type="text" id="v_username" name="v_username" class="form-control required" placeholder="Username" value="{{$records->v_username}}" />
                                                <div id="error_v_username" class="duplicate-error " style="display: none;">Username already exits.</div>
                                                <input type="hidden" name="user_type" value="employee">
                                                <input type="hidden" name="id" value="{{$records->Employee->id}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Old Password
                                                </label>
                                                <input type="password" name="password_old" id="password_old" class="form-control password" placeholder="Old Password">
                                                <div id="error_old_password" class="duplicate-error " style="display: none;">Please enter correct password.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">New Password <span class="redLabel">(Note: Password will be changed only if entered.)</span> </label>
                                                <input placeholder=" New Password" type="password" name="password_new" id="password_new" class="form-control password validate_password"/>
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Confirm Password</label>
                                                <input placeholder=" Confirm Password" type="password" name="password_retype" class="form-control input-icon password" id="password_retype" equalTo="password_new"/>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="row">
                                    	<div class="col-md-6">
                                        	<div class="form-group">
                                                <label class="control-label"> Name
                                                <span class="redLabel">
                                                    *
                                                </span>
                                                </label>
                                                <input type="text" id="v_vname" name="v_vname" class="form-control required" placeholder="Name" value="{{$records->Employee->v_vname}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        	<div class="form-group">
                                                <label class="control-label">Email
                                                <span class="redLabel">
                                                    *
                                                </span>
                                                </label>
                                                <input type="text" id="v_email" name="v_email" class="form-control email required" placeholder="Email" value="{{$records->Employee->v_email}}">
                                                <div id="error_v_email" class="duplicate-error " style="display: none;">Email already exits.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                    	<div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Phone
                                                </label>
                                                <input type="text" id="v_phone" name="v_phone" class="form-control" placeholder="Phone" value="{{$records->Employee->v_phone}}">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Fax
                                                </label>
                                            	<input type="text" id="v_fax" name="v_fax" class="form-control" 	placeholder="Fax" value="<?php echo ($records->Employee->v_fax!='')?$records->Employee->v_fax:''; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">URL
                                                </label>
                                                <input type="text" id="v_url" name="v_url" class="form-control url" placeholder="URL" value="<?php echo ($records->Employee->v_url!='')?$records->Employee->v_url:''; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Note
                                                </label>
                                                 <textarea id="v_note" name="v_note" class="form-control" placeholder="Note"><?php echo ($records->Employee->v_note!='')?$records->Employee->v_note:''; ?></textarea> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        	<div class="form-group">
                                                <label class="control-label">Profile Picture
                                                </label>
                                                <div class="clearfix"></div>
                                            	<div class="fileinput fileinput-new " data-provides="fileinput" style="margin-top:5px;">
                                                	<div class="tab-pane active" id="tab_1">
                                                    	<img width="220px" src="<?php if(File::exists(ADMIN_ADD_THUMB_IMAGE_PATH.$records->v_image) && $records->v_image !='') { echo SITE_URL.ADMIN_ADD_THUMB_IMAGE_PATH.$records->v_image; } else { echo SITE_URL.ADMIN_ADD_IMG_PATH_URL.'default-user.png'; }?>" class="img-responsive default_img_size" name="profileimg"   alt="" id="profile_pic" />
                                                        <img  width="220px" src="{{ SITE_URL.ADMIN_ADD_IMG_PATH_URL.'default-user.png'}}" class="img-responsive default_img_size" id="default_profile_pic" style="display: none;"/>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div style="margin-top:20px">
                                                      <?php   if(File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !=''){ ?>
                                                            <button class="btn btn-default" type="button" id="edit_crop">Edit Crop</button>  
                                                     <?php   } ?>
                                                        <button class="btn btn-default" type="button" id="file_trriger"><?php echo File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='' ? 'Change' : 'Select Image';?> </button>  
                                                        
                                                        <button class="btn btn-default" type="button" id="remove_image" style="display: {{  File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='' ? 'inline-block' : 'none' }};">Remove</button>
                                                    </div>
                                            	</div>
                                            	<input type="file" id="image_change" style="display: none;" />
                                        		<input type="hidden" name="frmnames" value="2"/>
                                        
                                    			<input type="hidden" id="user_profile_iamge" name="user_profile_iamge" value="<?php if( File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='') { echo SITE_URL.ADMIN_ADD_IMG_PATH_URL.$records->v_image; } else{ echo '';} ?>"/>
                                        
                                         		<input type="hidden" id="default_img" name="default_img" value="<?php if( File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='') { echo '0'; } else{ echo '1';} ?>"/>
                                                <input type="hidden" name="imgbase64" value=""  id="imgbase64" />
                                                <input type="hidden" id="x" name="x" />
                                                <input type="hidden" id="y" name="y" />
                                                <input type="hidden" id="x2" name="x2" />
                                                <input type="hidden" id="y2" name="y2" />
                                                <input type="hidden" id="w" name="w" />
                                                <input type="hidden" id="h" name="h" />
                                        	</div>
                                        </div>
                                    </div>
                                </div>
                			@elseif(isset($records->Client))
                                <div class="form-body">
                                            <h3 class="form-section">Person Details</h3>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Username
                                                        <span class="redLabel">
                                                            *
                                                        </span>
                                                        </label>
                                                        <input type="text" id="v_username" name="v_username" class="form-control required" placeholder="Username" value="{{$records->v_username}}">
                                                        <div id="error_v_username" class="duplicate-error " style="display: none;">Username already exits.</div>
                                                        <input type="hidden" name="user_type" value="client">
                                                    <input type="hidden" name="id" value="{{$records->Client->id}}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Old Password
                                                        </label>
                                                        <input type="password" name="password_old" id="password_old" class="form-control password" placeholder="Old Password">
                                                        <div id="error_old_password" class="duplicate-error " style="display: none;">Please enter correct password.</div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">New Password <span class="redLabel">(Note: Password will be changed only if entered.)</span> </label>
                                                        <input placeholder=" New Password" type="password" name="password_new" id="password_new" class="form-control password validate_password"/>
                                                    </div>
                                                  </div>  
                                                  <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Confirm Password</label>
                                                    <input placeholder=" Confirm Password" type="password" name="password_retype" class="form-control input-icon password" id="password_retype" equalTo="password_new"/>
                                                </div>

                                                </div>  
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">First Name
                                                        <span class="redLabel">
                                                            *
                                                        </span>
                                                        </label>
                                                        <input type="text" id="v_firstname" name="v_firstname" class="form-control required" placeholder="First Name" value="{{$records->Client->v_firstname}}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Last Name
                                                        <span class="redLabel">
                                                            *
                                                        </span>
                                                        </label>
                                                        <input type="text" id="v_lastname" name="v_lastname" class="form-control required" placeholder="Last Name" value="{{$records->Client->v_lastname}}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Company
                                                        <span class="redLabel">
                                                            *
                                                        </span>
                                                        </label>
                                                        <input type="text" id="v_company" name="v_company" class="form-control required" placeholder="Company" value="{{$records->Client->v_company}}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Fax
                                                        </label>
                                                        <input type="text" id="v_fax" name="v_fax" class="form-control" placeholder="Fax" value="{{$records->Client->v_fax}}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Email
                                                        <span class="redLabel">
                                                            *
                                                        </span>
                                                        </label>
                                                        <input type="text" id="v_email" name="v_email" class="form-control email required" placeholder="Email" value="{{$records->Client->v_email}}">
                                                        <div id="error_v_email" class="duplicate-error " style="display: none;">Email already exits.</div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Address line 1
                                                        <span class="redLabel">
                                                            *
                                                        </span>
                                                        </label>
                                                        <input type="text" id="v_address_1" name="v_address_1" class="form-control required" placeholder="Address line 1" value="{{$records->Client->v_address_1}}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Address line 2
                                                        </label>
                                                        <input type="text" id="v_address_2" name="v_address_2" class="form-control" placeholder="Address line 2" value="{{$records->Client->v_address_2}}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">City
                                                        <span class="redLabel">
                                                            *
                                                        </span>
                                                        </label>
                                                        <input type="text" id="v_city" name="v_city" class="form-control required" placeholder="City" value="{{$records->Client->v_city}}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">State
                                                        <span class="redLabel">
                                                            *
                                                        </span>
                                                        </label>
                                                        <input type="text" id="v_state" name="v_state" class="form-control required" placeholder="State" value="{{$records->Client->v_state}}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Zip Code
                                                        <span class="redLabel">
                                                            *
                                                        </span>
                                                        </label>
                                                        <input type="text" id="v_zipcode" name="v_zipcode" class="form-control number  required validate_zip" placeholder="Zip Code" value="{{$records->Client->v_zipcode}}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Phone
                                                        <span class="redLabel">
                                                            *
                                                        </span>
                                                        </label>
                                                        <input type="text" id="v_phone" name="v_phone" class="form-control  required" placeholder="Phone" value="{{$records->Client->v_phone}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Note
                                                        </label>
                                                        <textarea id="l_note" name="l_note" class="form-control" placeholder="Note"><?php echo ($records->Client->l_note!='')?$records->Client->l_note:''; ?></textarea> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Company Logo
                                                        </label>
                                                        <div class="input-file-box">
                                            <div data-provides="fileinput" class="fileinput" >
                                                <div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileinput-new" id="up_img"> Upload Company Logo </span>
                                                    <span class="fileinput-exists" id="change_img"> Change </span>
                                                    <input type="file" name="image_upload" id="image_upload"/>
                                                </span>
                                                    <a data-dismiss="fileinput"  class="btn red fileinput-exists" href="javascript:;" id="rmv_img">Remove </a>
                                                </div>
                                                <div style="max-width: 200px; max-height: auto; margin-top: 10px;" id="image_val1" class="fileinput-preview fileinput-exists thumbnail">
                                                    @if($records->Client->company_logo != ''  && file_exists(CLIENT_ADD_IMG_PATH_URL.$records->Client->company_logo))
                                                        <img alt="" src="{{ SITE_URL.CLIENT_ADD_IMG_PATH_URL.$records->Client->company_logo}}"/>
                                                    @else
                                                        <?php $records->Client->company_logo = ''; ?>
                                                    @endif
                                                </div>
                                            </div>
                                            <input type="hidden" value="" id="v_logo_val1" name="v_logo" />
                                            </div>
                                                        
                                            </div>
                                            </div>
                                                <!--/span-->
                                                
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                
                                            </div>
                                            <!--/row-->      
                                            <h3 class="form-section">FTP Details</h3>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>
                                                        Host Name
                                                        <span class="redLabel">
                                                            *
                                                        </span>
                                                        </label>
                                                        <input type="text" id="v_ftp_url" placeholder="Host Name" name="v_ftp_url" class="form-control required" value="{{$records->Client->v_ftp_url}}" readonly="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>
                                                        FTP Username
                                                        <span class="redLabel">
                                                            *
                                                        </span>
                                                        </label>
                                                        <input type="text" id="v_ftp_username" placeholder="FTP Username" name="v_ftp_username" class="form-control required" value="{{$records->Client->v_ftp_username}}" readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">          
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>FTP Password
                                                        </label>
                                                        <div class="input-group">
                                                            <input type="password" id="v_ftp_password" placeholder="FTP Password" name="v_ftp_password" class="form-control validate_password" value="{{$records->Client->v_ftp_password}}" readonly="">
                                                            <span class="input-group-addon" id="view_password">
                                                            <i class="fa fa-eye"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Datafeed Directory Name
                                                        </label>
                                                        <input type="text" id="v_company" placeholder="Datefeed Directory Name" name="v_company" class="form-control" value="{{$records->Client->v_company}}" readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                </div> 
                            @elseif(isset($records->Vendor))
                                <div class="form-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab1">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        User Name
                                                        <span class="required">
                                                            *
                                                        </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_username" name="v_username" class="form-control required" placeholder="User Name" value="{{ $records->v_username }}"/>
                                                        <div id="error_v_username" class="duplicate-error " style="display: none;">Username already exits.</div>
                                                        <input type="hidden" name="user_type" value="vendor">
                                                            <input type="hidden" name="id" value="{{$records->Vendor->id}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Old Password<br> <span class="redLabel"></span> </label>
                                                    <div class="col-md-3">
                                                        <input type="password" name="password_old" id="password_old" class="form-control password" placeholder=" Old Password"/>
                                                        <div id="error_old_password" class="duplicate-error " style="display: none;">Please enter correct password.</div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">New Password<br> <span class="redLabel">(Note: Password will be changed only if entered.)</span> </label>
                                                    <div class="col-md-3"><input placeholder=" New Password" type="password" name="password_new" id="password_new" class="form-control password validate_password"/></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Confirm Password</label>
                                                    <div class="col-md-3"><input placeholder=" Confirm Password" type="password" name="password_retype" class="form-control input-icon password" id="password_retype" equalTo="password_new"/></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Vendor Name
                                                        <span class="required">
                                                            *
                                                        </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_vendor_name" name="v_vendor_name" class="form-control required" placeholder="Vendor Name"  value="{{ $records->Vendor->v_vendor_name }}"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Company Name
                                                        <span class="required">
                                                            *
                                                        </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_company_name" name="v_company_name" class="form-control required" placeholder="Company Name" value="{{ $records->Vendor->v_company_name}}" />
                                                    </div>
                                                </div>
                                                <div class="form-group order_email_div">
                                                    <label class="control-label col-md-2">
                                                        Receives Dropship Order Email                                                        
                                                    </label>
                                                    <?php
                                                    $order_email = explode(',',$records->Vendor->order_email);                    
                                                    $i = 0;
                                                    ?>
                                                    @foreach($order_email as $val)
                                                    <div class="col-md-3">
                                                        <input type="text"  name="order_email[]" class="form-control email" placeholder="Receives Dropship Order Email" value="{{ $val }}" />
                                                    </div>
                                                    <?php $i++; ?>
                                                    @endforeach
                                                    @if(count($order_email) < 3)
                                                        @for($j=$i;$j<3;$j++)
                                                            <div class="col-md-3">
                                                                <input type="text"  name="order_email[]" class="form-control email" placeholder="Receives Dropship Order Email" value="" />
                                                            </div>
                                                        @endfor
                                                    @endif
                                                </div>
                                                <div class="form-group po_email_div">
                                                    <label class="control-label col-md-2">
                                                        Receives PO Email                                                        
                                                    </label>
                                                    <?php
                                                    $po_email = explode(',',$records->Vendor->po_email);                       
                                                    $i = 0;               
                                                    ?>
                                                    @foreach($po_email as $val)
                                                    <div class="col-md-3">
                                                        <input type="text"  name="po_email[]" class="form-control email" placeholder="Receives PO Email" value="{{$val}}" />
                                                    </div>
                                                    <?php $i++; ?>
                                                    @endforeach
                                                    @if(count($po_email) < 3)
                                                        @for($j=$i;$j<3;$j++)
                                                            <div class="col-md-3">
                                                                <input type="text"  name="po_email[]" class="form-control email" placeholder="Receives PO Email" value="" />
                                                            </div>
                                                        @endfor
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Email
                                                        <span class="required">
                                                            *
                                                        </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_email" name="v_email" class="form-control email required" placeholder="Email Id" value="{{ $records->Vendor->v_email }}" />
                                                        <div id="error_v_email" class="duplicate-error " style="display: none;">Email already exits.</div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Vendor ID
                                                       <!--  <span class="required">
                                                            *
                                                        </span> -->
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_vendor_id" name="v_vendor_id" class="form-control v_vendor_id" placeholder="Vendor ID" value="{{ $records->Vendor->v_vendor_id }}" />
                                                        <div id="error_v_vendor_id" class="duplicate-error " style="display: none;">Vendor Id already exits.</div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Phone
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_phone_number" name="v_phone_number" class="form-control" placeholder="Phone Number" value="{{ $records->Vendor->v_phone_number}}" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Fax
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_fax" name="v_fax" class="form-control" placeholder="Fax" value="{{ $records->Vendor->v_fax }}" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        URL
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_url" name="v_url" class="form-control url" placeholder="URL" value="{{ $records->Vendor->v_url }}" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Note
                                                    </label>
                                                    <div class="col-md-3">
                                                        <textarea id="l_note" name="l_note" class="form-control" placeholder="Note">{{ $records->Vendor->l_note }}</textarea> 
                                                    </div>
                                                    
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                       Profile Picture
                                                    </label>
                                                    <div class="fileinput fileinput-new col-md-9" data-provides="fileinput">
                                                        <div class="tab-pane active" id="tab_1">
                                                            <img width="220px" src="<?php if(File::exists(ADMIN_ADD_THUMB_IMAGE_PATH.$records->v_image) && $records->v_image !='') { echo SITE_URL.ADMIN_ADD_THUMB_IMAGE_PATH.$records->v_image; } else { echo SITE_URL.ADMIN_ADD_IMG_PATH_URL.'default-user.png'; }?>" class="img-responsive default_img_size" name="profileimg"   alt="" id="profile_pic" />
                                                            <img  width="220px" src="{{ SITE_URL.ADMIN_ADD_IMG_PATH_URL.'default-user.png'}}" class="img-responsive default_img_size" id="default_profile_pic" style="display: none;"/>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div style="margin-top:20px">
                                                            <?php if(File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !=''){ ?>
                                                                    <button class="btn btn-default" type="button" id="edit_crop">Edit Crop</button>
                                                            <?php  } ?>
                                                            <button class="btn btn-default" type="button" id="file_trriger"><?php echo File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='' ? 'Change' : 'Select Image';?> </button>  
                                                            
                                                            <button class="btn btn-default" type="button" id="remove_image" style="display: {{  File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='' ? 'inline-block' : 'none' }};">Remove</button>
                                                            
                                                        </div>
                                                    </div>
                                                        <input type="file" id="image_change" style="display: none;" />
                                                        <input type="hidden" name="frmnames" value="2"/>
                                                        
                                                        <input type="hidden" id="user_profile_iamge" name="user_profile_iamge" value="<?php if( File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='') { echo SITE_URL.ADMIN_ADD_IMG_PATH_URL.$records->v_image; } else{ echo '';} ?>"/>
                                                        
                                                         <input type="hidden" id="default_img" name="default_img" value="<?php if( File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='') { echo '0'; } else{ echo '1';} ?>"/>
                                                        <input type="hidden" name="imgbase64" value=""  id="imgbase64" />
                                                        <input type="hidden" id="x" name="x" />
                                                        <input type="hidden" id="y" name="y" />
                                                        <input type="hidden" id="x2" name="x2" />
                                                        <input type="hidden" id="y2" name="y2" />
                                                        <input type="hidden" id="w" name="w" />
                                                        <input type="hidden" id="h" name="h" />              
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                            @elseif(isset($records->DropshipEmployee))
                                <div class="form-body">
                                                <h3 class="form-section">Personal Details</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Username
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_username" name="v_username" class="form-control required" placeholder="Username" value="{{isset($records->v_username)?$records->v_username:''}}">
                                                            <div id="error_v_username" class="duplicate-error " style="display: none;">Username already exits.</div>
                                                            <input type="hidden" name="user_type" value="dropship_employee">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Old Password
                                                            </label>
                                                            <input type="password" name="password_old" id="password_old" class="form-control password" placeholder="Old Password">
                                                            <div id="error_old_password" class="duplicate-error " style="display: none;">Please enter correct password.</div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">New Password <span class="redLabel">(Note: Password will be changed only if entered.)</span> </label>
                                                <input placeholder=" New Password" type="password" name="password_new" id="password_new" class="form-control password validate_password"/>
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Confirm Password</label>
                                                <input placeholder=" Confirm Password" type="password" name="password_retype" class="form-control input-icon password" id="password_retype" equalTo="password_new"/>
                                            </div>
                                        </div>  
                                    </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"> Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_vname" name="v_vname" class="form-control required" placeholder="Name" value="{{$records->DropshipEmployee->v_vname}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Email
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_email" name="v_email" class="form-control email required" placeholder="Email" value="{{$records->DropshipEmployee->v_email}}">
                                                            <div id="error_v_email" class="duplicate-error " style="display: none;">Email already exits.</div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Phone
                                                            </label>
                                                            <input type="text" id="v_phone" name="v_phone" class="form-control" placeholder="Phone" value="{{$records->DropshipEmployee->v_phone}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Fax
                                                            </label>
                                                            <input type="text" id="v_fax" name="v_fax" class="form-control " placeholder="Fax" value="<?php echo ($records->DropshipEmployee->v_fax!='')?$records->DropshipEmployee->v_fax:''; ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Note
                                                            </label>
                                                             <textarea id="v_note" name="v_note" class="form-control" placeholder="Note"><?php echo ($records->DropshipEmployee->v_note!='')?$records->DropshipEmployee->v_note:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Profile Picture
                                                            </label>
                                                            <div class="clearfix"></div>
                                                            <div class="fileinput fileinput-new " data-provides="fileinput" style="margin-top:5px;">
                                                            <div class="tab-pane active" id="tab_1">
                                                                <img width="220px" src="<?php if(File::exists(ADMIN_ADD_THUMB_IMAGE_PATH.$records->v_image) && $records->v_image !='') { echo SITE_URL.ADMIN_ADD_THUMB_IMAGE_PATH.$records->v_image; } else { echo SITE_URL.ADMIN_ADD_IMG_PATH_URL.'default-user.png'; }?>" class="img-responsive default_img_size" name="profileimg"   alt="" id="profile_pic" />
                                                                <img  width="220px" src="{{ SITE_URL.ADMIN_ADD_IMG_PATH_URL.'default-user.png'}}" class="img-responsive default_img_size" id="default_profile_pic" style="display: none;"/>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div style="margin-top:20px">
                                                                <?php if(File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !=''){ ?>
                                                                    <button class="btn btn-default" type="button" id="edit_crop">Edit Crop</button>
                                                                <?php  } ?>
                                                                <button class="btn btn-default" type="button" id="file_trriger"><?php echo File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='' ? 'Change' : 'Select Image';?> </button>  
                                                                
                                                                <button class="btn btn-default" type="button" id="remove_image" style="display: {{  File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='' ? 'inline-block' : 'none' }};">Remove</button>
                                                                
                                                            </div>
                                                            
                                                            
                                                        </div>

                                                        <input type="file" id="image_change" style="display: none;" />
                                                    <input type="hidden" name="frmnames" value="2"/>
                                                    
                                                    <input type="hidden" id="user_profile_iamge" name="user_profile_iamge" value="<?php if( File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='') { echo SITE_URL.ADMIN_ADD_IMG_PATH_URL.$records->v_image; } else{ echo '';} ?>"/>
                                                    
                                                     <input type="hidden" id="default_img" name="default_img" value="<?php if( File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='') { echo '0'; } else{ echo '1';} ?>"/>
                                                    <input type="hidden" name="imgbase64" value=""  id="imgbase64" />
                                                    <input type="hidden" id="x" name="x" />
                                                    <input type="hidden" id="y" name="y" />
                                                    <input type="hidden" id="x2" name="x2" />
                                                    <input type="hidden" id="y2" name="y2" />
                                                    <input type="hidden" id="w" name="w" />
                                                    <input type="hidden" id="h" name="h" />
                                                        </div>
                                                    </div>
                                                </div>    
                                </div>
                            @elseif(isset($records->Admin)) 
                                <div class="form-body">
                                                <h3 class="form-section">Personal Details</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Username
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_username" name="v_username" class="form-control required" placeholder="Username" value="{{isset($records->v_username)?$records->v_username:''}}">
                                                            <div id="error_v_username" class="duplicate-error " style="display: none;">Username already exits.</div>
                                                            <input type="hidden" name="user_type" value="super">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Old Password
                                                            </label>
                                                            <input type="password" name="password_old" id="password_old" class="form-control password" placeholder="Old Password">
                                                            <div id="error_old_password" class="duplicate-error " style="display: none;">Please enter correct password.</div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">New Password <span class="redLabel">(Note: Password will be changed only if entered.)</span> </label>
                                                <input placeholder=" New Password" type="password" name="password_new" id="password_new" class="form-control password validate_password"/>
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Confirm Password</label>
                                                <input placeholder=" Confirm Password" type="password" name="password_retype" class="form-control input-icon password" id="password_retype" equalTo="password_new"/>
                                            </div>
                                        </div>  
                                    </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"> Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_vname" name="v_vname" class="form-control required" placeholder="Name" value="{{$records->Admin->v_vname}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Email
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_email" name="v_email" class="form-control email required" placeholder="Email" value="{{$records->Admin->v_email}}">
                                                            <div id="error_v_email" class="duplicate-error " style="display: none;">Email already exits.</div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Phone
                                                            </label>
                                                            <input type="text" id="v_phone" name="v_phone" class="form-control" placeholder="Phone" value="{{$records->Admin->v_phone}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Fax
                                                            </label>
                                                            <input type="text" id="v_fax" name="v_fax" class="form-control " placeholder="Fax" value="<?php echo ($records->Admin->v_fax!='')?$records->Admin->v_fax:''; ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Note
                                                            </label>
                                                             <textarea id="v_note" name="v_note" class="form-control" placeholder="Note"><?php echo ($records->Admin->v_note!='')?$records->Admin->v_note:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Profile Picture
                                                            </label>
                                                            <div class="clearfix"></div>
                                                            <div class="fileinput fileinput-new " data-provides="fileinput" style="margin-top:5px;">
                                                            <div class="tab-pane active" id="tab_1">
                                                                <img width="220px" src="<?php if(File::exists(ADMIN_ADD_THUMB_IMAGE_PATH.$records->v_image) && $records->v_image !='') { echo SITE_URL.ADMIN_ADD_THUMB_IMAGE_PATH.$records->v_image; } else { echo SITE_URL.ADMIN_ADD_IMG_PATH_URL.'default-user.png'; }?>" class="img-responsive default_img_size" name="profileimg"   alt="" id="profile_pic" />
                                                                <img  width="220px" src="{{ SITE_URL.ADMIN_ADD_IMG_PATH_URL.'default-user.png'}}" class="img-responsive default_img_size" id="default_profile_pic" style="display: none;"/>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div style="margin-top:20px">
                                                                <?php if(File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !=''){ ?>
                                                                    <button class="btn btn-default" type="button" id="edit_crop">Edit Crop</button>
                                                                <?php  } ?>
                                                                <button class="btn btn-default" type="button" id="file_trriger"><?php echo File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='' ? 'Change' : 'Select Image';?> </button>  
                                                                
                                                                <button class="btn btn-default" type="button" id="remove_image" style="display: {{  File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='' ? 'inline-block' : 'none' }};">Remove</button>
                                                                
                                                            </div>
                                                            
                                                            
                                                        </div>

                                                        <input type="file" id="image_change" style="display: none;" />
                                                    <input type="hidden" name="frmnames" value="2"/>
                                                    
                                                    <input type="hidden" id="user_profile_iamge" name="user_profile_iamge" value="<?php if( File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='') { echo SITE_URL.ADMIN_ADD_IMG_PATH_URL.$records->v_image; } else{ echo '';} ?>"/>
                                                    
                                                     <input type="hidden" id="default_img" name="default_img" value="<?php if( File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='') { echo '0'; } else{ echo '1';} ?>"/>
                                                    <input type="hidden" name="imgbase64" value=""  id="imgbase64" />
                                                    <input type="hidden" id="x" name="x" />
                                                    <input type="hidden" id="y" name="y" />
                                                    <input type="hidden" id="x2" name="x2" />
                                                    <input type="hidden" id="y2" name="y2" />
                                                    <input type="hidden" id="w" name="w" />
                                                    <input type="hidden" id="h" name="h" />
                                                        </div>
                                                    </div>
                                                </div>    
                                </div>    
                            @else   
                                <div class="tab-pane active" id="tab1">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        User Name
                                                        <span class="required">
                                                            *
                                                        </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_username" name="v_username" class="form-control required" placeholder="User Name" value="{{ $records->v_username }}"/>
                                                        <div id="error_v_username" class="duplicate-error " style="display: none;">Username already exits.</div>
                                                        <input type="hidden" name="user_type" value="">
                                                            <input type="hidden" name="id" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        First Name
                                                        <span class="required">
                                                            *
                                                        </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_firstname" name="v_firstname" class="form-control required" placeholder="First Name" value="{{ $records->v_firstname }}"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Last Name
                                                        <span class="required">
                                                            *
                                                        </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_lastname" name="v_lastname" class="form-control required" placeholder="Last Name" value="{{ $records->v_lastname }}"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Email
                                                        <span class="required">
                                                            *
                                                        </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_email" name="v_email" class="form-control email required" placeholder="Email Id" value="{{ $records->v_email }}" />
                                                        <div id="error_v_email" class="duplicate-error " style="display: none;">Email already exits.</div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Old Password<br> <span class="redLabel"></span> </label>
                                                    <div class="col-md-3">
                                                        <input type="password" name="password_old" id="password_old" class="form-control password" placeholder=" Old Password"/>
                                                        <div id="error_old_password" class="duplicate-error " style="display: none;">Please enter correct password.</div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">New Password<br> <span class="redLabel">(Note: Password will be changed only if entered.)</span> </label>
                                                    <div class="col-md-3"><input placeholder=" New Password" type="password" name="password_new" id="password_new" class="form-control password validate_password"/></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Confirm Password</label>
                                                    <div class="col-md-3"><input placeholder=" Confirm Password" type="password" name="password_retype" class="form-control input-icon password" id="password_retype" equalTo="password_new"/></div>
                                                </div>
                                                 <div class="form-group">
                                                        <label class="control-label col-md-2">Profile Picture
                                                        </label>
                                                         <div class="fileinput fileinput-new col-md-9" data-provides="fileinput">
                                                            <div class="tab-pane active" id="tab_1">
                                                                <img width="220px" height="150px" src="<?php if(File::exists(ADMIN_ADD_THUMB_IMAGE_PATH.$records->v_image) && $records->v_image !='') { echo SITE_URL.ADMIN_ADD_THUMB_IMAGE_PATH.$records->v_image; } else { echo SITE_URL.ADMIN_ADD_IMG_PATH_URL.'default-user.png'; }?>" class="img-responsive default_img_size" name="profileimg"   alt="" id="profile_pic" />
                                                                <img  width="220px" src="{{ SITE_URL.ADMIN_ADD_IMG_PATH_URL.'default-user.png'}}" class="img-responsive default_img_size" id="default_profile_pic" style="display: none;"/>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div style="margin-top:20px">
                                                                <?php if(File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !=''){ ?>
                                                                    <button class="btn btn-default" type="button" id="edit_crop">Edit Crop</button>
                                                                <?php  } ?>
                                                                  
                                                                <button class="btn btn-default" type="button" id="file_trriger"><?php echo File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='' ? 'Change' : 'Select Image';?> </button>  
                                                                
                                                                <button class="btn btn-default" type="button" id="remove_image" style="display: {{  File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='' ? 'inline-block' : 'none' }};">Remove</button>
                                                                
                                                            </div>
                                                            <!---<div class="clearfix margin-top-10">
                                                                <span class="label label-danger">NOTE! </span>
                                                                <span>&nbsp;&nbsp;Size:150px X 150px </span>
                                                            </div>-->
                                                        </div>
                                                    </div>
                                                    <input type="file" id="image_change" style="display: none;" />
                                                    <input type="hidden" name="frmnames" value="2"/>
                                                    
                                                    <input type="hidden" id="user_profile_iamge" name="user_profile_iamge" value="<?php if( File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='') { echo SITE_URL.ADMIN_ADD_IMG_PATH_URL.$records->v_image; } else{ echo '';} ?>"/>
                                                    
                                                     <input type="hidden" id="default_img" name="default_img" value="<?php if( File::exists(ADMIN_ADD_IMG_PATH_URL.$records->v_image) && $records->v_image !='') { echo '0'; } else{ echo '1';} ?>"/>
                                                    <input type="hidden" name="imgbase64" value=""  id="imgbase64" />
                                                    <input type="hidden" id="x" name="x" />
                                                    <input type="hidden" id="y" name="y" />
                                                    <input type="hidden" id="x2" name="x2" />
                                                    <input type="hidden" id="y2" name="y2" />
                                                    <input type="hidden" id="w" name="w" />
                                                    <input type="hidden" id="h" name="h" />
                                </div>
                                </div>
                                </div>                                    
                            @endif
                                <div class="form-actions <?php echo ((isset($records->Vendor))||($records->Client=='' && $records->Vendor=='' && $records->Employee=='' && $records->DropshipEmployee=='' && $records->Admin==''))?'':'right';?>">
                                    <div class="row">
                                        <div class="col-md-12">
                                           @if($records->Client != '')
                                                <div class="col-md-5"></div>
                                                <div class="col-md-7"><button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>      
                                                <a href="{{ ADMIN_URL }}dashboard" class=" btn default button-previous">Cancel </a> </div> 
                                            @elseif($records->Vendor != '')
                                                <div class="col-md-2"></div>
                                                <div class="col-md-10"><button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>      
                                                <a href="{{ ADMIN_URL }}dashboard" class=" btn default button-previous">Cancel </a> </div>   
                                            @elseif($records->Employee != '')
                                                <div class="col-md-2"></div>
                                                <div class="col-md-8"><button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>      
                                                <a href="{{ ADMIN_URL }}dashboard" class=" btn default button-previous">Cancel </a> </div>  
                                            @elseif($records->DropshipEmployee != '')
                                                <div class="col-md-2"></div>
                                                <div class="col-md-8"><button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>      
                                                <a href="{{ ADMIN_URL }}dashboard" class=" btn default button-previous">Cancel </a> </div> 
                                            @elseif($records->Admin != '')
                                                <div class="col-md-2"></div>
                                                <div class="col-md-8"><button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>      
                                                <a href="{{ ADMIN_URL }}dashboard" class=" btn default button-previous">Cancel </a> </div> 
                                            @else
                                                <div class="col-md-2"></div>
                                                <div class="col-md-10"><button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>      
                                                <a href="{{ ADMIN_URL }}dashboard" class=" btn default button-previous">Cancel </a></div>  
                                            @endif                    
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
            			
    				</div>
    			</div>
    		</div>
    	</div>
	</div>
</div>
<script>
$(document).ready(function(){
	handleProfileForm();
    $('#tab_2_visible').removeClass('tab_2_visible'); 
    var im = '<?php echo (isset($records->Client))?$records->Client->company_logo:(isset($records->v_image)?$records->v_image:''); ?>';
            //$('#error_msg').val(im);
    if(im != ''){
        $('#up_img').hide();
        $('#change_img').show();
        $('#rmv_img').show();
    } else {
        $('#up_img').show();
        $('#change_img').hide();
        $('#rmv_img').hide();
        $('#image_val1').removeClass('thumbnail');
    }
    $('#rmv_img').on('click',function(){
        $('#edit_img').hide();
        $('#up_img').show();
        $('#change_img').hide();
        $('#rmv_img').hide();
        $('#image_val1 img').attr('src','');
    });
    $('#up_img').on('click',function(){
        $('#up_img').hide();
        $('#change_img').show();
        $('#rmv_img').show();
    });
    $('#image_upload').on('change',function(){
    	var t= $('#image_upload').val();                
        if(t!= ''){
            $('#up_img').hide();
            $('#change_img').show();
            $('#rmv_img').show();
            //$('#error_msg').hide();
            //$('#error_msg_error').hide();
        }
        $("#img_div").attr('style',"display:block");
    });
    $('#view_password').on('click',function(){
        if($('#v_ftp_password').attr('type').length == 8){
            var ftp_password = '<?php echo (isset($records->Client))? base64_decode($records->Client->v_ftp_password):'' ?>';
            $('#v_ftp_password').attr('type', 'text');
            $('#v_ftp_password').val(ftp_password);
        } else {
            var ftp_password = '<?php echo (isset($records->Client))? $records->Client->v_ftp_password : '' ?>';
            $('#v_ftp_password').attr('type', 'password');
            $('#v_ftp_password').val(ftp_password);
        }
    });
});
</script>
@stop