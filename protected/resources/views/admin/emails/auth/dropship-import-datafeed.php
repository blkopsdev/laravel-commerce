<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <style>
            .success-msg{
                background-color: #dff0d8;
                border-color: #d6e9c6;
                color: #3c763d;
                padding: 15px;
                margin-bottom: 20px;
                border: 1px solid transparent;
                border-radius: 4px;
            }
            .alert-msg{
                background-color: #f2dede;
                border-color: #ebccd1;
                color: #a94442;
                padding: 15px;
                margin-bottom: 20px;
                border: 1px solid transparent;
                border-radius: 4px;
            }
            .success-span{
                color: #3c763d;
            }
            .alert-span{
                color: #a94442;
            }
        </style>
    </head>
    <body>
        <?php echo $strTemplate; ?>
    </body>
</html>