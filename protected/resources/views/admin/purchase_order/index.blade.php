@extends('admin.layouts.default')
@section('content')
<style>
a:hover {
    text-decoration:none;
    cursor:pointer;
}
/*.modal-body {
    min-height: 400px;
}*/
.scrollable-menu {
    height: auto;
    max-height: 50px;
    overflow-y: auto;
}
.safari_class {
	margin-top: 5px;
}
.error-inner{
    color: red;
}
</style>
<script>
$(document).ready(function() {
    if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0){
		$('#client_id').addClass('safari_class');
    } else if (navigator.appVersion.indexOf("Mac")!=-1){
        $('#client_id').addClass('safari_class');
    }
 });
</script>
 <?php 
$user = Auth::guard('admin')->user();
?>
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Purchase Order <small>Find and manage purchase order</small></h1>
        </div>
       
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
    <!--div class="page-content-wrapper"-->
        <div class="page-content">
            <input type="hidden" id="
            " name="del_url" value="<?php echo ADMIN_URL.'purchase_order/delete/'; ?>" />
            <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
               	    <div class="portlet light">
					   <div class="portlet-title">
                          <div class="caption"><i class="fa fa-cogs font-green-sharp"></i>
							<span class="caption-subject font-green-sharp bold uppercase">Manage Purchase Order</span></div>
                          <div class="actions purchase-order-action">
                              <div class="hide_show_po">
                                <span class="control-label">Show Hidden PO: </span>                              
                                <input type="checkbox" class="make-switch" id="show_hide_po" data-on-text="Off" data-off-text="On" checked data-on-color="success" data-off-color="warning" data-size="normal">
                              </div>
                            <div class="po-action-div">
                            @if($user->role==1 || (isset($UserPermission["6"]["e_import"]) && $UserPermission["6"]["e_import"]  == 1))                                
                                <a class="btn blue-madison btn-circle" href="#form_purchase_order_modal" data-toggle="modal">
                                    <i class="fa fa-reply"></i> <span class="hidden-480">Import Data Feed</span>  
                                </a>
                            @elseif($user->role==2)
                                <a class="btn blue-madison btn-circle" id="import_client_purchase" href="javascript:;">
                                    <i class="fa fa-reply"></i>
                                    <span class="hidden-480">Import Data Feed</span>
                                </a>
                            @endif
                            
                            @if($user->role==1 || (isset($UserPermission["6"]["e_export"]) && $UserPermission["6"]["e_export"]  == 1))
                            
                            <a href="javascript:;" action-url="<?php echo ADMIN_URL . 'purchase_order/export-data-feed'; ?>" id="export_to_excel_order" class="btn blue-madison btn-circle"> <i class="fa fa-share"></i> <span class="hidden-480">Export to Excel</span>  </a>

                            @endif
                            </div>
                        </div>
                        
					</div>                    
                <div class="portlet-body">
                    <div class="table-container ">
                     <div class="Metronic-alerts alert alert-success fade in display-hide" id="success-msg">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <i class="fa-lg fa fa-check "></i> 
                                <span class="message"></span>
                        </div>
                        @if(Session::has('success-message'))                    
                            <div class="Metronic-alerts alert alert-success">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <i class="fa-lg fa fa-check "></i> 
                                <span class="message">{!!Session::get('success-message')!!}</span>
                            </div>
                        @endif
                        @if(Session::has('alert-message'))
                            <div class="Metronic-alerts alert alert-danger fade in">
                                <button type="button" class="close" aria-hidden="true" data-dismiss="alert"></button>
                                <span class="message">{!!Session::get('alert-message')!!}</span>
                            </div>
                        @endif
                        @if($user->role==1 || (isset($UserPermission["6"]["e_delete"]) && $UserPermission["6"]["e_delete"]  == 1))
                            <div class="table-actions-wrapper">
                                <span></span>
    							<select class="table-group-action-input form-control input-inline input-small input-sm">
    								<option value="">Select...</option>
                                    <option value="Delete">Delete</option>	
                                    @if($user->role == 1 || $user->role == 2 || $user->role == 5)			
                                    <option value="Hide">Hide</option>										
                                    <option value="Unhide">Unhide</option>										
                                    @endif
    							</select>
    							<button class="btn btn-sm blue-madison table-group-action-submit btn-circle" id="bulk_action"><i class="fa fa-check"></i> Submit</button>
                                <input type="hidden"  class="table-group-action-url" value="<?php echo ADMIN_URL.'purchase_order/bulk-action';?>"/>
    					   </div>
                           @endif
                           <div id="form_purchase_order_modal" class="modal fade" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Select client for import data feed</h4>
										</div>
										<div class="modal-body">
											<form action="{{ ADMIN_URL }}purchase_order/import-to-excel-purchase-order"  class="form-horizontal" id="import_purchase_order_form" role="form">
                                                <span class="redLabel" style="text-align:center;">(FYI: Import file must be there in client specific folder to import records.)</span>
												<div class="form-group">
                                                    <div class="col-md-3"><label class="control-label col-md-4">Client
                                                    
                                                    </label></div>
													<div class="col-md-9">
														<select class="bs-select form-control scrollable-menu required" name="client_id" id="client_id" placeholder="client">
                                                            <option value="">Select Client</option>
                                                            @foreach($client_data as $client)
                                                                <option value="{{ $client->id }}">{{ $client->v_company }}</option>   
                                                            @endforeach
														</select>
													</div>
												</div>
											</form>
										</div>
										<div class="modal-footer">
											<button class="btn default" data-dismiss="modal" aria-hidden="true">Close</button>
											<button class="btn green" type="button" id="import_purchase_order">Save changes</button>
										</div>
									</div>
								</div>
							</div>
                            <table class="table table-striped table-hover table-bordered" id="datatable_ajax"> <!-- editable_resources_filter -->
                                <thead>                
                                    <tr role="row" class="heading">
                                     @if($user->role==1 || (isset($UserPermission["6"]["e_delete"]) && $UserPermission["6"]["e_delete"]  == 1))
                                        <th width="2%" style="padding-right: 0px; background-image: none ! important;" class="actions no-sort"><input type="checkbox" class="group-checkable"/></th>
                                        @endif
                                        @if($user->role!=3 && $user->role!=4)
                                        <th>
                                            Supplier Name
                                        </th>
                                        @endif
                                        <th>
                                            PO Number
                                        </th>
                                        <th>
                                            PO Order Date
                                        </th>
                                        <!--<th>
                                            PO Status
                                        </th> -->
                                        <th>
                                            Expected Delivery Date
                                        </th>
                                        <th>
                                           Vendor's Expected Delivery Date
                                        </th>
                                        <th>
                                            Item Name
                                        </th>
                                        <th>
                                            Quantity
                                        </th>
                                        <th>
                                            PO Status
                                        </th>
                                        {{--  <th>
                                            Total Cost
                                        </th>  --}}
                                        <th style="padding-right: 0px; background-image: none ! important align:right;" class="actions no-sort">
                                             Actions
                                        </th>
                                    </tr>
                                    <tr role="row" class="filter">
                                        @if($user->role==1 || (isset($UserPermission["6"]["e_delete"]) && $UserPermission["6"]["e_delete"]  == 1))
                                        <td rowspan="1" colspan="1">
            						     </td>
                                         @endif
                                         @if($user->role!=3 && $user->role!=4)
                                         <td rowspan="1" colspan="1">
                                            <select class="form-control form-filter input-sm" name="i_vendor_id">
                                            
                                                <option value="">-- Select --</option>
                                                @foreach($vendor_list as $vendor)
                                                    <option value="{{ $vendor->id }}">{{ $vendor->v_vendor_name }}</option>
                                                @endforeach
                                            </select>
            						     </td>
                                         @endif                                         
                                         <td rowspan="1" colspan="1">
                                            <input type="text" id="v_po_number" name="v_po_number" class="form-control form-filter input-sm"/>
                                            <input type="hidden" id="b_hidden" name="b_hidden" value="" class="form-control form-filter input-sm"/>
                                         </td>
                                          <td rowspan="1" colspan="1">
                                            <input type="text" name="from_order_issue_date" id="from_date" class="from_date datetimepicker form-control form-filter input-sm" placeholder="From Date" /><br>
                                            <input type="text" name="to_order_issue_date" id="end_date" class="to_date datetimepicker form-control form-filter input-sm" placeholder="To Date" />
                                         </td>
                                        <!--<td rowspan="1" colspan="1">
                                            <select class="form-control form-filter input-sm" name="e_po_status" id="e_po_status" >
                                                <option value="">Select...</option>
                                                <option value="Complete">Complete</option>
                                                <option value="Incomplete">Incomplete</option>
                                                
                                            </select>
                                         </td> -->
                                          <td rowspan="1" colspan="1">
                                            <input type="text" name="from_expected_delivery_date" id="from_date" class="from_date datetimepicker form-control form-filter input-sm" placeholder="From Date" /><br>
                                            <input type="text" name="to_expected_delivery_date" id="end_date" class="to_date datetimepicker form-control form-filter input-sm" placeholder="To Date" />
                                         </td>  
                                         <td rowspan="1" colspan="1">
                                             <input type="text" name="from_vendor_expected_delivery_date" id="from_date" class="from_date datetimepicker form-control form-filter input-sm" placeholder="From Date" /><br>
                                            <input type="text" name="to_vendor_expected_delivery_date" id="end_date" class="to_date datetimepicker form-control form-filter input-sm" placeholder="To Date" />
                                         </td>  
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="v_vendor_product_name" id="v_vendor_product_name" class="form-control form-filter input-sm"/>
                                         </td>  
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="i_quantity_from"  id="i_quantity_from" class="form-control form-filter input-sm " placeholder="From Quantity"/><br>
                                            <input type="text" name="i_quantity_to"  id="i_quantity_to" class="form-control form-filter input-sm" placeholder="To Quantity"/>                                            
                                         </td>  
                                         <td rowspan="1" colspan="1">
                                            <select class="form-control form-filter input-sm" name="e_item_status" id="e_item_status" >
                                                <option value="">Select...</option>
                                                <option value="Pending">Pending</option>
                                                <option value="Confirm and Approved">Confirm and Approved</option>
                                                <option value="Back Ordered">Back Ordered</option>
                                                 <option value="Discontinued">Discontinued</option>
                                                <option value="Partial Inventory">Partial Inventory</option>
                                                
                                            </select>
                                         </td>
                                         {{--  <td rowspan="1" colspan="1">
                                            <input type="text" name="from_order_total"  id="from_order_total" class="form-control form-filter input-sm" placeholder="From" /><br>
                                            <input type="text" name="to_order_total"  id="to_order_total" class="form-control form-filter input-sm" placeholder="To"/>
                                         </td>  --}}
                                          
                                         <td rowspan="1" class="actions" colspan="1">
                                            <div class="margin-bottom-5">
            									<button class="btn btn-sm blue-madison filter-submit margin-bottom btn-circle"><i class="fa fa-search"></i> Search</button>
                                                <button class="btn btn-sm default filter-cancel btn-circle"><i class="fa fa-times"></i> Reset</button>
            								</div>
            			                 </td>
                                    </tr>                                
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div> 
                    </div>
                </div>
            </div>
        </div>
        </div>
    <!--/div -->   
<script>
    $(document).ready(function(){    
        var url = ADMIN_URL+'purchase_order/list-ajax';
        var auth_user = '<?php echo $user->e_type; ?>';    
        if(auth_user == 'Vendor' || auth_user == 'Employee'){
            TableAjax.init(url,'desc');   
        } else {
            TableAjax.init(url);    
        }
        $('.datetimepicker').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });    
        $('#from_date').datepicker({
                format: "mm/dd/yyyy",
                autoclose: true,
                todayBtn: true,
                todayHighlight: true,
                constrainInput: false,
            }).on('change', function(e) {
                $('#end_date').val('');
                $('#end_date').datepicker('remove');
                $("#end_date").datepicker({ format: "mm/dd/yyyy", startDate: $('#from_date').val(), maxDate: 0, autoclose: true, todayBtn: true, todayHighlight: true, constrainInput: false });
            });
    });
     $(document).on('click', '#export_to_excel_order', function (e) {
        var send_data = $('#datatable_ajax').DataTable().ajax.params();
        //var send_data = grid.getAjaxParams();
        var obj = { 'page': send_data.page ,'search': send_data.search,'order': send_data.order,'i_vendor_id': send_data.i_vendor_id,'v_po_number': send_data.v_po_number,'from_order_issue_date':send_data.from_order_issue_date,'to_order_issue_date':send_data.to_order_issue_date,'from_expected_delivery_date':send_data.from_expected_delivery_date,'to_expected_delivery_date': send_data.to_expected_delivery_date,'from_vendor_expected_delivery_date':send_data.from_vendor_expected_delivery_date,'to_vendor_expected_delivery_date':send_data.to_vendor_expected_delivery_date,'v_vendor_product_name':send_data.v_vendor_product_name,'i_quantity_from':send_data.i_quantity_from,'i_quantity_to':send_data.i_quantity_to,'e_item_status':send_data.e_item_status,'from_order_total':send_data.from_order_total,'to_order_total':send_data.to_order_total};
        var send_data = $.param(obj);
        var url = $(this).attr('action-url');
        window.location.href = url+ '?' + send_data;
    });
    $('#show_hide_po').on('switchChange.bootstrapSwitch', function (event, state) {
        var x=$(this).data('on-text');
        var y=$(this).data('off-text');
        if($("#show_hide_po").is(':checked')) {
            $('#b_hidden').val('');
        } else {
            $('#b_hidden').val('0');
        }
        $('.filter-submit').trigger('click');
    });
    /*$(document).on('click', '#show_hide_po', function (e) {
        $('#b_hidden').val('1');
        $('.filter-submit').trigger('click');
    });*/
</script>
@stop