<HTML>
<HEAD>
<link rel="stylesheet" type="text/css" href="{{ ASSET_URL }}css/general_bill.css">
<link rel="stylesheet" type="text/css" href="{{ ASSET_URL }}css/layout_bill.css">
<LINK href="{{ ASSET_URL }}css/style_bill.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY text="#000000" bgColor="#ffffff" leftMargin="0" topMargin="0" style="FONT-SIZE: 11px;padding-bottom: 0px;FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Arial, Tahoma, Verdana, Helvetica, sans-serif; BACKGROUND-COLOR: #f6f6f6;">
<center>
<table cellpadding="5px" style="border:1px solid #000; margin:5px 5px 0px 5px; padding:5px;width:100%;" autosize="1" cellspacing="0">

  <tr height="35px;">
    <td colspan="6" align="center"><h1><u>Purchase Order</u></h1></td>
  </tr>
  <tr height="25px">
    <td colspan="6" style="border-bottom:solid 5px #000">From :<strong> {{$client['v_company']}}</strong></td>
  </tr>

  <tr height="25px">
    <td width="138" style="border:5px solid #000; border-top:none; font-size:16px; font-weight:bold">Ship to this Address</td>
    <td width="470" colspan="2">Black Forest Décor <br />2717 N. Van Buren St.,<br />Enid, OK. 73703<br /></td>
    <td width="138" style="border:5px solid #000 ; border-top:none; font-size:16px; font-weight:bold">Bill-to:</td>
    <td width="470" colspan="2">
    					  Black Forest Décor <br />
                          329 S Elm St #210,<br />

                          Jenks, OK. 74037<br />

                          Janet@blackforestdecor.com</td>

  </tr>

  <tr height="15px">

    <td colspan="6">Vendor : <strong>{{$order['vendor']['v_vendor_name']}}</strong></td>

  </tr>

  <tr height="20px">

    <td colspan="6">
      
      <table style="margin-bottom:-12px;" cellspacing="0" cellpadding="5px" width="100%">

          <tr>

            <td style="border:5px solid #000; border-bottom:none" >Order #</td>

            <td style="border:5px solid #000; border-left:none; border-bottom:none" width="92" >{{$order['v_po_number']}}</td>

            <td width="40" >&nbsp;</td>

            <td style="border:5px solid #000; border-bottom:none" width="65" >Order Date</td>

            <td style="border:5px solid #000; border-left:none; border-bottom:none" width="76" >{{$order['d_order_issue_date']}}</td>

            <td width="191" >&nbsp;</td>

          </tr>
        </table>
    </td>

  </tr>

  <tr height="120px">

    <td colspan="6">
    
      <table cellpadding="5px" cellspacing="0" width="100%" height="100px" style="border:solid 5px #000">
        <tr>
          <td colspan="4"><h2 style="height:2px;">Instructions</h2></td>
        </tr>
        <tr>
          <td colspan="4">
              1. Please ship the item(s) below directly to our warehouse as soon as possible.<br><br>

              2. Mail your invoice to the address listed above or email them to invoices@blackforestdecor.com.<br><br>

              3. This can be used as the packing slip for your order. Please include packing slip in shipment.<br><br>


              4.  If shipment is over 200 lbs. please quote with our freight department.

                Email the following information to freight@blackforestdecor.com:

                <table cellpadding="0" cellspacing="0" width="100%" height="100px" style="padding-left:5%;">
                  <tr><td>1. Number of pallets/cartons/crates</td></tr>

                  <tr><td>2 .Length, width, height, and weight of each pallet</td></tr>

                  <tr><td>3. Origin shipping address including name, address, contact name , email address, phone number, and dock hours/days available for pickup.</td></tr>

                  <tr><td>4. Please obtain your best rate quote as well so that we can compare with the rate quotes that we obtain.</td></tr>

                  <tr><td>5. Please confirm the freight class that you have quoted on the shipment.</td></tr>

                </table>
                @if($fedex_flg == 1)
                    If the shipment is less than 200 lbs.  Then please ship on our FedEx account.  Our account number is 283694321.  Do NOT declare a value on any FedEx ground shipment unless pre-approved by Diane Johnson- Dalrymple with Black Forest Décor.
                @else                
                    If shipment is less than 200 lbs. then please ship on our UPS account. Our account number is 688R21. Do NOT declare a value on any UPS ground shipment unless pre-approved by Diane Johnson-Dalrymple with Black Forest Décor.
                @endif
            
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>

    <td colspan="1"><h3>QTY</h3></td>
    <td colspan="4"><h3>NAME</h3></td>
    <td colspan="1"><h3>SKU</h3></td>
      
  </tr>
  @if(count($order['purchase_order_details']) > 0)
    @foreach($order['purchase_order_details'] as $val)
      <tr>
        <td colspan="1">{{$val['i_quantity']}}</td>
        <td colspan="4">{{$val['v_vendor_product_name']}}</td>
        <td colspan="1">{{$val['v_client_item_code']}}</td>
      </tr>
    @endforeach
  @endif
</table>

</center>
</BODY>
</HTML>
