@extends('admin.layouts.default')
@section('content')
<?php 
$userArr = Auth::guard('admin')->user();
?>
<style>
    .error-inner{color: red;position: absolute;}
    .duplicate-error{color: red;}
    .error-number-inner{
        color: red;
        position: absolute;
    }
</style>
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Purchase Order <small>Find and manage order</small></h1>
        </div>        
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="portlet light" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green-sharp bold uppercase">
                                    <i class="fa fa-users">
                                    </i>
                                    Edit Purchase Order
                                </span>
                            </div>
                        </div>
                        <input type="hidden" name="redirect_url" id="redirect_url" value="{{ADMIN_URL}}purchase_order/edit/{{$records[0]->PurchaseOrder->v_po_number}}">
                        <div class="portlet-body form">                            
                            <form role="form" method="post" class="horizontal-form edit_purchase_order" id="frmEdit" name="frmEdit" onsubmit="return false;" action="{{ADMIN_URL}}purchase_order/edit-order/{{$records[0]->PurchaseOrder->id}}" enctype="multipart/form-data">
                                            <div class="form-body purchase_order_form">                                                
                                                <h3 class="form-section">Edit Purchase Order : {{ $records[0]->PurchaseOrder->v_po_number }}</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Vendor Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>                                                             
                                                            <select placeholder="client" class="form-control input-icon required" name="i_vendor_id">
                                                            <option value="">-- Select --</option>
                                                            @foreach($vendor_list as $vendor)
                                                                @if($records[0]->PurchaseOrder->i_vendor_id == $vendor->id)
                                                                <option value="{{ $vendor->id }}" selected="selected">{{ $vendor->v_company_name }}</option>
                                                                @else
                                                                    <option value="{{ $vendor->id }}">{{ $vendor->v_company_name }}</option>
                                                                @endif    
                                                            @endforeach
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Order Date
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <?php
                                                                if(trim($records[0]->PurchaseOrder->d_order_issue_date) != '' && trim($records[0]->PurchaseOrder->d_order_issue_date) != '0000-00-00'){ 
                                                                        $d_order_issue_date = date('m/d/Y',strtotime($records[0]->PurchaseOrder->d_order_issue_date));
                                                                }else{
                                                                        $d_order_issue_date = '';
                                                                }
                                                             ?>
                                                            <input type="text" name="d_order_issue_date" id="d_order_issue_date" class="form-control required d_order_issue_date"  placeholder="Order Date" value="<?php echo $d_order_issue_date ?>">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Expected Delivery Date:
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <?php
                                                                if(trim($records[0]->PurchaseOrder->d_expected_delivery_date) != '' && trim($records[0]->PurchaseOrder->d_expected_delivery_date) != '0000-00-00'){ 
                                                                        $d_expected_delivery_date = date('m/d/Y',strtotime($records[0]->PurchaseOrder->d_expected_delivery_date));
                                                                }else{
                                                                        $d_expected_delivery_date = '';
                                                                }
                                                             ?>
                                                            <input type="text" name="d_expected_delivery_date" id="d_expected_delivery_date"    class="form-control required" placeholder="Expected Delivery Date" value="<?php echo $d_expected_delivery_date; ?>">
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <h3 class="form-section">Sub Details of Purchase Order</h3>    
                                                    @foreach($records as $purchase_order_details)
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">BFD Product Code
                                                                        <span class="redLabel">
                                                                            *
                                                                        </span>
                                                                    </label>
                                                                    <input type="hidden" name="purchase_order_detail_id[]" id="purchase_order_detail_id"    class="form-control" value="{{$purchase_order_details->id}}">

                                                                    <input type="text" name="v_client_item_code[]" id="v_client_item_code"    class="form-control required" placeholder="BFD Product Code" value="{{$purchase_order_details->v_vendor_item_code}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Vendor Item Code
                                                                        <span class="redLabel">
                                                                            *
                                                                        </span>
                                                                    </label>
                                                                    <input type="text" name="v_vendor_item_code[]" id="v_vendor_item_code"    class="form-control required" placeholder="Vendor Item Code" value="{{$purchase_order_details->v_client_item_code}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Vendor Product Name
                                                                        <span class="redLabel">
                                                                            *
                                                                        </span>
                                                                    </label>
                                                                    <input type="text" name="v_vendor_product_name[]" id="v_vendor_product_name"    class="form-control required" placeholder="Vendor Product Name" value="{{$purchase_order_details->v_vendor_product_name}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Quantity
                                                                        <span class="redLabel">
                                                                            *
                                                                        </span>
                                                                    </label>
                                                                    <input type="text" name="i_quantity[]" id="i_quantity_{{$purchase_order_details->id}}"  class="form-control number required i_quantity" placeholder="Quantity" value="{{$purchase_order_details->i_quantity}}" data-id = "{{ $purchase_order_details->id }}">
                                                                </div>
                                                            </div>
                                                        </div>  
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Price
                                                                        <span class="redLabel">
                                                                            *
                                                                        </span>
                                                                    </label>
                                                                    <input type="text" name="f_price[]" id="f_price_{{$purchase_order_details->id}}" class="form-control required f_price" placeholder="Price" value="{{$purchase_order_details->f_price}}" data-id = "{{ $purchase_order_details->id }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Sub Total
                                                                    </label>
                                                                    <input type="text" name="f_sub_total[]" id="f_sub_total"  class="form-control required f_sub_total_{{$purchase_order_details->id}}" placeholder="Sub Total" value="{{$purchase_order_details->f_sub_total}}" readonly>
                                                                </div>
                                                            </div>
                                                        </div>  
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Item Status
                                                                    </label>                                                                    
                                                                    <select placeholder="Item Status" class="form-control input-icon e_item_status" name="e_item_status[]" data-id = "{{ $purchase_order_details->id }}">
                                                                        {{--  <option value="">-- Select --</option>  --}}
                                                                        <option value="Pending" <?php echo ($purchase_order_details->e_item_status=='Pending')?'selected="selected"':'' ?>>Pending</option>
                                                                        <option value="Confirm and Approved" <?php echo ($purchase_order_details->e_item_status=='Confirm and Approved')?'selected="selected"':'' ?>>Confirm and Approved</option>
                                                                        <option value="Back Ordered" <?php echo ($purchase_order_details->e_item_status=='Back Ordered')?'selected="selected"':'' ?>>Back Ordered</option>
                                                                        <option value="Discontinued" <?php echo ($purchase_order_details->e_item_status=='Discontinued')?'selected="selected"':'' ?>>Discontinued</option>
                                                                        <option value="Partial Inventory" <?php echo ($purchase_order_details->e_item_status=='Partial Inventory')?'selected="selected"':'' ?>>Partial Inventory</option>
                                                                    </select>
                                                                </div>
                                                            </div>   
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Note
                                                                    </label>
                                                                    <textarea id="v_note" name="v_note[]" class="form-control" placeholder="Note"><?php echo ($purchase_order_details->v_note!='')?$purchase_order_details->v_note:''; ?></textarea> 
                                                                </div>
                                                            </div>         
                                                        </div> 
                                                        <div class="row">                                                            
                                                            <?php 
                                                            if($purchase_order_details->e_item_status=='Confirm and Approved'){
                                                                $style = '';                                                                
                                                                $expected_delivery_date = date('m/d/Y', strtotime($purchase_order_details->d_vendor_expected_delivery_date));
                                                                $class = "required";
                                                                $back_style="style=display:none";
                                                                $backclass = "";
                                                                $backorder_date_val = '';
                                                            } else if($purchase_order_details->e_item_status=='Back Ordered'){
                                                                $style="style=display:none";
                                                                $back_style = '';
                                                                $expected_delivery_date = '';
                                                                $backorder_date_val = date('m/d/Y', strtotime($purchase_order_details->d_back_ordered_date));
                                                                $backclass = "required";
                                                                $class = "";
                                                            } else if($purchase_order_details->e_item_status=='Partial Inventory') {
                                                                $back_style = '';
                                                                $backorder_date_val = date('m/d/Y', strtotime($purchase_order_details->d_back_ordered_date));
                                                                $style = '';
                                                                $expected_delivery_date = date('m/d/Y', strtotime($purchase_order_details->d_vendor_expected_delivery_date));
                                                                $class = "required";
                                                                $backclass = "required";
                                                            } else{                                                                
                                                                $style="style=display:none";
                                                                $expected_delivery_date = '';
                                                                $back_style="style=display:none";
                                                                $backorder_date_val = '';
                                                                $class = "";
                                                                $backclass = "";
                                                            }                                                            
                                                            ?>
                                                            <div class="col-md-6 partial_qty_div_{{$purchase_order_details->id}}" <?php echo ($purchase_order_details->e_item_status!='Partial Inventory')?'style="display:none;"':'' ?>>
                                                                <div class="form-group">
                                                                    <label class="control-label">Quantity On Hand
                                                                        <span class="redLabel">
                                                                            *
                                                                        </span>
                                                                    </label>
                                                                    <input type="text" name="i_qty_on_hand[]" id="i_qty_on_hand-{{$purchase_order_details->id}}"    class="form-control number <?php echo ($purchase_order_details->e_item_status=='Partial Inventory')?'required':'' ?>" placeholder="Quantity On Hand" value="{{ ($purchase_order_details->i_qty_on_hand==0)?'':$purchase_order_details->i_qty_on_hand }}">
                                                                    <div class="error-number-inner" id="i_qty_on_hand-{{$purchase_order_details->id}}number_error" style="display:none">Please enter qty on hand > 0.</div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 vendor_expected_delivery_date_{{ $purchase_order_details->id }}" {{ $style }}>
                                                                <div class="form-group">
                                                                    <label class="control-label">Vendor Expected Delivery Date
                                                                        <span class="redLabel">
                                                                            *
                                                                        </span>
                                                                    </label>
                                                                    <input type="text" name="d_vendor_expected_delivery_date[]" id="d_vendor_expected_delivery_date"  class="form-control d_vendor_expected_delivery_date {{ $class }}" placeholder="Vendor Expected Delivery Date" value="{{ $expected_delivery_date }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 back_order_delivery_date_{{ $purchase_order_details->id }}" {{ $back_style }}>
                                                                <div class="form-group">
                                                                    <label class="control-label">Back Order Delivery Date
                                                                        <span class="redLabel">
                                                                            *
                                                                        </span>
                                                                    </label>
                                                                    <input type="text" name="d_back_ordered_date[]" id="d_back_ordered_date"    class="form-control d_back_ordered_date {{ $backclass }}" placeholder="Back Order Delivery Date" value="{{ $backorder_date_val }}">
                                                                </div>
                                                            </div>
                                                        </div> 
                                                        <hr>                                        
                                                    @endforeach                                                      
                                            </div>                                            
                                            <div class="pull-right">
                                                <button type="button" class="btn blue-madison button-next pull-right add-more-product-btn"><i class="fa fa-plus"></i> Add More Product  </button>
                                            </div>                                            
                                            <div class="form-actions right" style="margin-top:50px;">                                                
                                               <button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>    
                                    <a href="{{ ADMIN_URL }}purchase_order/edit/{{$records[0]->PurchaseOrder->v_po_number}}" class=" btn default button-previous">Cancel </a>
                                            </div>
                                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    </div>
<div class="add-more-product-div" style="display:none">
    <div class="add-more-div">
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn blue-madison button-next pull-right delete-product-btn"><i class="fa fa-trash"></i> Delete Product </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">BFD Product Code
                        <span class="redLabel">
                            *
                        </span>
                    </label>
                    <input type="hidden" name="purchase_order_detail_id[]" id="purchase_order_detail_id"    class="form-control" value="">

                    <input type="text" name="v_client_item_code[]" id="v_client_item_code"    class="form-control required" placeholder="BFD Product Code" value="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Vendor Item Code
                        <span class="redLabel">
                            *
                        </span>
                    </label>
                    <input type="text" name="v_vendor_item_code[]" id="v_vendor_item_code"    class="form-control required" placeholder="Vendor Item Code" value="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Vendor Product Name
                        <span class="redLabel">
                            *
                        </span>
                    </label>
                    <input type="text" name="v_vendor_product_name[]" id="v_vendor_product_name"    class="form-control required" placeholder="Vendor Product Name" value="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Quantity
                        <span class="redLabel">
                            *
                        </span>
                    </label>
                    <input type="text" name="i_quantity[]" id="i_quantity_"  class="form-control number required i_quantity" placeholder="Quantity" value="" data-id = "">
                </div>
            </div>
        </div>  
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Price
                        <span class="redLabel">
                            *
                        </span>
                    </label>
                    <input type="text" name="f_price[]" id="f_price_" class="form-control required f_price" placeholder="Price" value="" data-id = "">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Sub Total
                    </label>
                    <input type="text" name="f_sub_total[]" id="f_sub_total"  class="form-control required f_sub_total_" placeholder="Sub Total" value="" readonly>
                </div>
            </div>
        </div>  
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Item Status
                    </label>                                                                    
                    <select placeholder="Item Status" class="form-control input-icon e_item_status" name="e_item_status[]" data-id = "">
                        {{--  <option value="">-- Select --</option>  --}}
                        <option value="Pending">Pending</option>
                        <option value="Confirm and Approved">Confirm and Approved</option>
                        <option value="Back Ordered">Back Ordered</option>
                        <option value="Discontinued">Discontinued</option>
                        <option value="Partial Inventory">Partial Inventory</option>
                    </select>
                </div>
            </div> 
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Note
                    </label>
                    <textarea id="v_note" name="v_note[]" class="form-control" placeholder="Note"></textarea> 
                </div>
            </div>                           
        </div> 
        <div class="row"> 
            <div class="col-md-6 partial_qty_div_" style="display:none;">
                <div class="form-group">
                    <label class="control-label">Quantity On Hand
                        <span class="redLabel">
                            *
                        </span>
                    </label>
                    <input type="text" name="i_qty_on_hand[]" id="i_qty_on_hand"  class="form-control number" placeholder="Quantity On Hand" value="">
                    <div class="error-number-inner" id="i_qty_on_hand-number" style="display:none">Please enter qty on hand > 0.</div>
                </div>
            </div>                                                           
            <div class="col-md-6 vendor_expected_delivery_date_" style="display:none">
                <div class="form-group">
                    <label class="control-label">Vendor Expected Delivery Date
                        <span class="redLabel">
                            *
                        </span>
                    </label>
                    <input type="text" name="d_vendor_expected_delivery_date[]" id="d_vendor_expected_delivery_date"  class="form-control d_vendor_expected_delivery_date" placeholder="Vendor Expected Delivery Date" value="">
                </div>
            </div>
            <div class="col-md-6 back_order_delivery_date_" style="display:none">
                <div class="form-group">
                    <label class="control-label">Back Order Delivery Date
                        <span class="redLabel">
                            *
                        </span>
                    </label>
                    <input type="text" name="d_back_ordered_date[]" id="d_back_ordered_date"    class="form-control d_back_ordered_date" placeholder="Back Order Delivery Date" value="">
                </div>
            </div>
        </div> 
        <hr> 
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.d_order_issue_date').datepicker({
                    format: 'mm/dd/yyyy',
                    todayHighlight:true,
                    autoclose: true
        });
        $('#d_expected_delivery_date').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight:true,
            autoclose: true
        });  
        $('.d_vendor_expected_delivery_date').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight:true,
            autoclose: true
        });  
        $('.d_back_ordered_date').datepicker({
            format: 'mm/dd/yyyy',
            todayHighlight:true,
            autoclose: true
        });                
        //$('body').find('.e_item_status').change(function(){
        $('body').on('change', '.e_item_status', function() {
            var this_val = $(this);
            var status_val = this_val.val();
            var id = this_val.attr('data-id');
            console.log(id);
            if(status_val=='Confirm and Approved'){
                $('.vendor_expected_delivery_date_'+id).show();
                $('.back_order_delivery_date_'+id).hide();
                $('.vendor_expected_delivery_date_'+id).find('#d_vendor_expected_delivery_date').datepicker({
                    format: 'mm/dd/yyyy',
                    todayHighlight:true,
                    autoclose: true
                });
                $('.vendor_expected_delivery_date_'+id).find('#d_vendor_expected_delivery_date').addClass('required').val('');
                $('.back_order_delivery_date_'+id).find('#d_back_ordered_date').removeClass('required');
                $('.back_order_delivery_date_'+id).find('#d_back_ordered_date').val('');
                $('.partial_qty_div_'+id).find('#i_qty_on_hand-'+id).val('');
                $('.partial_qty_div_'+id).find('#i_qty_on_hand-'+id).removeClass('required');
                $('.partial_qty_div_'+id).hide();
            }else if (status_val=='Back Ordered'){
                $('.vendor_expected_delivery_date_'+id).hide();
                $('.back_order_delivery_date_'+id).show();
                $('.back_order_delivery_date_'+id).find('#d_back_ordered_date').datepicker({
                    format: 'mm/dd/yyyy',
                    todayHighlight:true,
                    autoclose: true
                });
                $('.vendor_expected_delivery_date_'+id).find('#d_vendor_expected_delivery_date').removeClass('required');
                $('.vendor_expected_delivery_date_'+id).find('#d_vendor_expected_delivery_date').val('');
                $('.back_order_delivery_date_'+id).find('#d_back_ordered_date').addClass('required').val('');
                $('.partial_qty_div_'+id).find('#i_qty_on_hand-'+id).val('');
                $('.partial_qty_div_'+id).find('#i_qty_on_hand-'+id).removeClass('required');
                $('.partial_qty_div_'+id).hide();
            }else if (status_val=='Partial Inventory'){
                $('.vendor_expected_delivery_date_'+id).show();                
                $('.vendor_expected_delivery_date_'+id).find('#d_vendor_expected_delivery_date').datepicker({
                    format: 'mm/dd/yyyy',
                    todayHighlight:true,
                    autoclose: true
                });
                $('.vendor_expected_delivery_date_'+id).find('#d_vendor_expected_delivery_date').addClass('required').val('');
                $('.back_order_delivery_date_'+id).show();
                $('.back_order_delivery_date_'+id).find('#d_back_ordered_date').datepicker({
                    format: 'mm/dd/yyyy',
                    todayHighlight:true,
                    autoclose: true
                });
                $('.back_order_delivery_date_'+id).find('#d_back_ordered_date').addClass('required').val('');
                $('.partial_qty_div_'+id).show();
                $('.partial_qty_div_'+id).find('#i_qty_on_hand-'+id).addClass('required').val('');
            }else{
                $('.vendor_expected_delivery_date_'+id).hide();
                $('.back_order_delivery_date_'+id).hide();
                $('.vendor_expected_delivery_date_'+id).find('#d_vendor_expected_delivery_date').removeClass('required');
                $('.back_order_delivery_date_'+id).find('#d_back_ordered_date').removeClass('required');
                $('.back_order_delivery_date_'+id).find('#d_back_ordered_date').val('');
                $('.vendor_expected_delivery_date_'+id).find('#d_vendor_expected_delivery_date').val('');
                $('.partial_qty_div_'+id).find('#i_qty_on_hand-'+id).val('');
                $('.partial_qty_div_'+id).find('#i_qty_on_hand-'+id).removeClass('required');
                $('.partial_qty_div_'+id).hide();
            }
        });
//        $('.i_quantity , .f_price').keyup(function(){
        $('body').on('keyup', '.i_quantity , .f_price', function() {
            var this_val = $(this);
            var id = this_val.attr('data-id');
            console.log(id);
            var qty = parseFloat($('#i_quantity_'+id).val());
            var price = parseFloat($('#f_price_'+id).val());            
            var subtotal = qty * price;            
            $('.f_sub_total_'+id).val(parseFloat(subtotal).toFixed(2));
        });
        $('body').on('click', '.delete-product-btn', function() {
            var this_val = $(this);
            var id = this_val.attr('data-id');
            $('#add-more-div-'+id).remove();
        });
        var i = <?php echo $purchase_order_details->id; ?>;
        $('.add-more-product-btn').click(function(){            
            i++;  
            console.log(i);            
            $('.add-more-product-div').find('.e_item_status').attr('data-id',i);    
            $('.add-more-product-div').find('.vendor_expected_delivery_date_').addClass('vendor_expected_delivery_date_'+i);    
            $('.add-more-product-div').removeClass('vendor_expected_delivery_date_');    
            $('.add-more-product-div').find('.back_order_delivery_date_').addClass('back_order_delivery_date_'+i);    
            $('.add-more-product-div').removeClass('back_order_delivery_date_');  
            $('.add-more-product-div').find('.partial_qty_div_').addClass('partial_qty_div_'+i);
            $('.add-more-product-div').removeClass('partial_qty_div_');  
            $('.add-more-product-div').find('.i_quantity').attr('id','i_quantity_'+i);
            $('.add-more-product-div').find('.i_quantity').attr('data-id',i);
            $('.add-more-product-div').find('.f_price').attr('id','f_price_'+i);
            $('.add-more-product-div').find('.f_price').attr('data-id',i);
            $('.add-more-product-div').find('.f_sub_total_').addClass('f_sub_total_'+i);
            $('.add-more-product-div').find('.add-more-div').attr('id','add-more-div-'+i);
            $('.add-more-product-div').find('.delete-product-btn').attr('data-id',i);            
            $('.add-more-product-div').find('.partial_qty_div_'+i).find('#i_qty_on_hand').attr('id','i_qty_on_hand-'+i);
            $('.add-more-product-div').find('.partial_qty_div_'+i).find('#i_qty_on_hand-number').attr('id','i_qty_on_hand-'+i+'number_error');            
            var product_div = $('.add-more-product-div').html();   
            $('.purchase_order_form').append(product_div);
        });
    });
</script>
</div>
@stop