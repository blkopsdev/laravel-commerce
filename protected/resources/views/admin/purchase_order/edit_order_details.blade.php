@extends('admin.layouts.default')
@section('content')
<?php 
$userArr = Auth::guard('admin')->user();
?>
<style>
    .error-inner{color: red;}
    .duplicate-error{color: red;}
</style>
<div class="page-container">
    <div class="page-head">
        <div class="container-fluid">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Purchase Order <small>Find and manage order</small></h1>
            </div>        
            <!-- END PAGE TITLE -->
        </div>
    </div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light" id="form_wizard_1">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-green-sharp bold uppercase">
                                <i class="fa fa-users">
                                </i>
                                Purchase Order Details
                            </span>
                        </div>
                    </div>
                    <div class="portlet-body form">                            
                        <form role="form" method="post" class="horizontal-form" id="frmEdit" name="frmEdit" onsubmit="return false;"  enctype="multipart/form-data">
                        <div class="col-md-10">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group order_group">
                                            <label class="col-md-3">Vendor Name: 
                                            </label>
                                            <div class="col-md-9 order_detail_div">
                                                {{$records['v_vendor_name']}}    
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group order_group">
                                            <label class="col-md-3">PO Number: 
                                            </label>
                                            <div class="col-md-9 order_detail_div">
                                                {{$records['v_po_number']}}    
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group order_group">
                                            <label class="col-md-3">PO Order Date: 
                                            </label>
                                            <div class="col-md-9 order_detail_div">
                                                <?php 
                                                if($records['d_order_issue_date'] != '0000-00-00' && $records['d_order_issue_date'] != ''){
                                                    echo date('d-M-Y',strtotime($records['d_order_issue_date']));
                                                } ?>    
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group order_group">
                                            <label class="col-md-3">Expected Delivery Date: 
                                            </label>
                                            <div class="col-md-9 order_detail_div">
                                                <?php 
                                                if($records['d_expected_delivery_date'] != '0000-00-00' && $records['d_expected_delivery_date'] != ''){
                                                    echo date('d-M-Y',strtotime($records['d_expected_delivery_date'])); 
                                                }?>    
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                               <!-- <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group order_group">
                                            <label class="col-md-3">PO Status
                                            </label>
                                            <div class="col-md-9 order_detail_div">
                                                <?php 
                                                if(in_array('Confirm and Approved',$item_status)){
                                                    echo "Complete";
                                                }else if(in_array('Back Ordered',$item_status)){
                                                    echo "Complete";
                                                }else if(in_array('Discontinued',$item_status)){
                                                    echo "Complete";
                                                }else if(in_array('Partial Inventory',$item_status)){
                                                    echo "Complete";
                                                }else{
                                                    echo "Incomplete";
                                                }
                                                ?>    
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container">
                            @if(Session::has('success-message'))                    
                                <div class="Metronic-alerts alert alert-success">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                    <i class="fa-lg fa fa-check "></i> 
                                    <span class="message">{!!Session::get('success-message')!!}</span>
                                </div>
                            @endif
                            @if(Session::has('alert-message'))
                                <div class="Metronic-alerts alert alert-danger fade in">
                                    <button type="button" class="close" aria-hidden="true" data-dismiss="alert"></button>
                                    <span class="message">{!!Session::get('alert-message')!!}</span>
                                </div>
                            @endif
                            <div class="alert alert-success display-hide">
                                <span class="close" data-close="alert"></span>
                                Purchase order has been updated successfully.
                            </div>
                            <div class="alert alert-danger display-hide">
                                    <span class="close" data-close="alert"></span>
                                    You have some form errors. Please check below.
                            </div>
                            <div class="table-actions-wrapper">
                                <a class="btn blue-madison btn-circle download_pdf"  href="{{ ADMIN_URL }}purchase_order/purchase-order-pdf/{{$records['v_po_number']}}">
                                <i class="fa fa-download"></i>
                                <span class="hidden-480">Download PO PDF</span>
                                </a>
                                @if($userArr['e_type']!='Employee' && $userArr['e_type']!='Vendor')
                                 <a class="btn blue-madison btn-circle" href="{{ ADMIN_URL }}purchase_order/edit-order/{{$records['v_po_number']}}">
                                <i class="fa fa-edit"></i>
                                <span class="hidden-480">Edit Purchase Order</span>
                                </a>
                                @endif
                                <a class="btn blue-madison btn-circle" href="{{ ADMIN_URL }}purchase_order">
                                <i class="fa fa-arrow-left"></i>
                                <span class="hidden-480">Back to PO list</span>
                                </a>
                                <span></span>                                 
                                    <a href="javascript:;"  action-url="<?php echo ADMIN_URL . 'purchase_order_detail/export-to-excel/'.$records['v_po_number']; ?>" id="export_to_excel_order" class="btn blue-madison btn-circle" <i class="fa fa-share"></i> <span class="hidden-480">Export to Excel</span>  </a>

                                    {{--  a class="btn blue-madison btn-circle" href="{{ ADMIN_URL }}purchase_order_detail/export-to-excel/{{$records['v_po_number']}}">
                                    <i class="fa fa-edit"></i>
                                    <span class="hidden-480">Export To Excel</span>
                                    </a>  --}}
                                 
                            </div>
                            <form name="frmStatus" id="frmStatus" method="post">
                            <table class="table table-striped table-hover table-bordered table-po-detail" id="datatable_ajax"> <!-- editable_resources_filter -->
                                <thead>                
                                    <tr role="row" class="heading">
                                    <!--  <th width="2%" style="padding-right: 0px; background-image: none ! important;" class="actions no-sort"><input type="checkbox" class="group-checkable"/></th> -->
                                        <th style="min-width:60px;" class="">
                                            BFD Product Code
                                        </th>
                                        <th class="" style="min-width:60px;">
                                            Vendor Product Code
                                        </th>
                                        <th class="">
                                            Item Name
                                        </th>
                                        <th class="">
                                            Qty
                                        </th>
                                        <th class="" style="min-width:50px;">
                                            Cost Of Goods
                                        </th>
                                        <th>
                                            PO Status
                                        </th>
                                        <th class="" style="min-width:120px;">
                                            Vendor's Expected Delivery Date
                                        </th>
                                        <th class="">
                                            Qty On Hand
                                        </th>
                                        <th class="" style="min-width:160px;">
                                            Backordered Delivery Date
                                        </th>
                                        <th class="" style="min-width:200px;">
                                            Notes
                                        </th>
                                        <th class="">
                                            Sub Total
                                        </th>
                                        @if($userArr->role == 1)
                                        <th style="padding-right: 0px; background-image: none ! important align:right;" class="actions no-sort">
                                            
                                        </th>
                                        @endif
                                    </tr>
                                                            
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <input type="hidden" name="order_po_number" value="{{$records['v_po_number']}}">
                            </form>
                            <div class="btn_action">
                                <span></span>
                                <a class="btn blue-madison btn-circle" href="javascript:void(0)" id="edit_purchase_order_status" action-url="{{ ADMIN_URL }}purchase_order/edit-status/{{$records['i_purchase_order_id']}}" style="display: inline-block;">
                                <i class="fa fa-edit"></i>
                                <span class="hidden-480">Update</span>
                                </a>
                                @if($next != '' && $next != 'null')
                                <a class="btn blue-madison btn-circle" href="<?php echo ADMIN_URL.'purchase_order/edit/'.$next; ?>" id="edit_order_next_status" style="float: right;">
                                <i class="fa fa-arrow-right"></i>
                                <span class="hidden-480">Next</span>
                                </a>
                                @endif
                                @if($previous != '' && $previous != 'null')
                                <a class="btn blue-madison btn-circle" href="<?php echo ADMIN_URL.'purchase_order/edit/'.$previous ?>" style="float: left;">
                                <i class="fa fa-arrow-left"></i>
                                <span class="hidden-480">Previous</span>
                                </a>
                                @endif
                                
                            </div>
                        </div>
                    </div> 
                </div>
            </div>

        </div>
    </div>
</div>
<!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    </div>
    <script>
    $(document).ready(function(){
        var total_records = '<?php echo $total_records; ?>';
        if(total_records <= 100){
            var pagination = 'false';
            var info = 'false';
            var page_menu = '0';
        } else {
            var pagination = 'true';
            var info = 'true';
            var page_menu = '1';
        }
        var url = ADMIN_URL+'purchase_order/order-detail/<?php echo $records['v_po_number']; ?>';
        TableAjax.init(url,'',pagination,info,page_menu);
        setTimeout(function(){
            $('.d_vendor_expected_delivery_date').datepicker({
                format: 'mm/dd/yyyy',
                todayHighlight:true,
                autoclose: true
            });   
            $('.d_back_ordered_date').datepicker({
                format: 'mm/dd/yyyy',
                todayHighlight:true,
                autoclose: true
            });   
        },1000);
        $(document).on('click', '#export_to_excel_order', function (e) {
        var send_data = $('#datatable_ajax').DataTable().ajax.params();
        //var send_data = grid.getAjaxParams();
        var obj = { 'page': send_data.page ,'search': send_data.search,'order': send_data.order,'v_vendor_item_code': send_data.v_vendor_item_code,'v_client_item_code': send_data.v_client_item_code,'v_vendor_product_name':send_data.v_vendor_product_name,'i_quantity':send_data.i_quantity,'f_price':send_data.f_price,'e_item_status': send_data.e_item_status,'d_vendor_expected_delivery_date':send_data.d_vendor_expected_delivery_date,'i_qty_on_hand':send_data.i_qty_on_hand,'d_back_ordered_date':send_data.v_note,'f_sub_total':send_data.f_sub_total};
        var send_data = $.param(obj);
        var url = $(this).attr('action-url');
        window.location.href = url+ '?' + send_data;
    });
        
    });
</script>
</div>
@stop