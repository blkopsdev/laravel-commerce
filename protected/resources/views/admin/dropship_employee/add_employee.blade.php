@extends('admin.layouts.default')
@section('content')
<script src="{{ SITE_URL }}js/admin/jquery.passMeter.js"></script>
<style>
    .error-inner{color: red;}
    .duplicate-error{color: red;}
         .jcrop-holder div {
      -webkit-border-radius: 50% !important;
      -moz-border-radius: 50% !important;
      border-radius: 50% !important;
      margin: -1px;
    }
</style>
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Dropship Employee <small>Find and manage employee</small></h1>
        </div>        
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="portlet light" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green-sharp bold uppercase">
                                    <i class="fa fa-users">
                                    </i>
                                    Add Dropship Employee
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body form">                            
                            <form role="form" method="post" class="horizontal-form" id="frmAdd" name="frmAdd" onsubmit="return false;" action="{{ADMIN_URL}}dropship-employee/add" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <h3 class="form-section">Person Details</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Username
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_username" name="v_username" class="form-control required" placeholder="Username">
                                                            <div id="error_v_username" class="duplicate-error " style="display: none;">Username already exits.</div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Password
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="password" name="password" id="password" class="form-control required validate_password main_password" placeholder="Password">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"> Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_vname" name="v_vname" class="form-control required" placeholder="Name">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Email
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_email" name="v_email" class="form-control email required" placeholder="Email">
                                                            <div id="error_v_email" class="duplicate-error " style="display: none;">Email already exits.</div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Phone
                                                            </label>
                                                            <input type="text" id="v_phone" name="v_phone" class="form-control" placeholder="Phone">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Fax
                                                            </label>
                                                            <input type="text" id="v_fax" name="v_fax" class="form-control " placeholder="Fax">
                                                        </div>
                                                        <div class="form-group">
                                                        <label class="control-label">
                                                            Type
                                                            <span class="required">
                                                                *
                                                            </span>
                                                        </label>
                                                        <select class="form-control input-icon required" name="e_type" placeholder="type">
                                                                <option value="DropshipEmployee">Employee</option>
                                                                <option value="Super">Super Admin</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                        <label class="control-label">
                                                            Status
                                                            <span class="required">
                                                                *
                                                            </span>
                                                        </label>
                                                        <select class="form-control input-icon required" name="e_status" placeholder="status">
                                                                <option value="Active">Active</option>
                                                                <option value="Inactive">Inactive</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Note
                                                            </label>
                                                             <textarea id="v_note" name="v_note" class="form-control" placeholder="Note"></textarea> 
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                     <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Profile Picture
                                                            </label>
                                                            <div class="clearfix"></div>
                                                            <div class="fileinput fileinput-new col-md-9" data-provides="fileinput">
                                                            <div class="tab-pane active" id="tab_1">
                                                                <img width="220px" src="<?php echo SITE_URL.ADMIN_ADD_IMG_PATH_URL.'default-user.png'; ?>" class="img-responsive default_img_size" name="profileimg"   alt="" id="profile_pic" />
                                                                <img  width="220px" src="{{ SITE_URL.ADMIN_ADD_IMG_PATH_URL.'default-user.png'}}" class="img-responsive default_img_size" id="defailt_profile_pic" style="display: none;"/>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div style="margin-top:20px">
                                                                <button class="btn btn-default" type="button" id="file_trriger"><?php echo 'Select Image';?> </button>  
                                                                
                                                                <button class="btn btn-default" type="button" id="remove_image" style="display: {{  'none' }};">Remove</button>
                                                                
                                                            </div>
                                                            <!---<div class="clearfix margin-top-10">
                                                                <span class="label label-danger">NOTE! </span>
                                                                <span>&nbsp;&nbsp;Size:150px X 150px </span>
                                                            </div>-->
                                                        </div>

                                                        <input type="file" id="image_change" style="display: none;" />
                                                    <input type="hidden" name="frmnames" value="2"/>
                                                    
                                                    <input type="hidden" id="user_profile_iamge" name="user_profile_iamge" value=""/>
                                                    
                                                    <input type="hidden" id="default_img" name="default_img" value="<?php echo '1';?>"/>
                                                    <input type="hidden" name="imgbase64" value=""  id="imgbase64" />
                                                    <input type="hidden" id="x" name="x" />
                                                    <input type="hidden" id="y" name="y" />
                                                    <input type="hidden" id="x2" name="x2" />
                                                    <input type="hidden" id="y2" name="y2" />
                                                    <input type="hidden" id="w" name="w" />
                                                    <input type="hidden" id="h" name="h" />
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>      
                                                 </div>           
                                            <div class="form-actions right">
                                               <button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>
                                               <button class="btn blue-madison button-next" id="frmAddNewSubmit">Save and Add new  <i class="fa fa-check-square-o"></i></button>
                                    <a href="{{ ADMIN_URL }}dropship-employee" class=" btn default button-previous">Cancel </a>
                                            </div>
                                            </div>
                                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $.passMeter({
                // configure the divs
                'id_password'   :   '#password',

                // customize the message levels
                'bad'   :   'Bad',
                'low'   :   'Low',
                'good'  :   'Good',
                'strong'    :   'Strong'
            });
         $('#file_trriger').click(function(){
            console.log("here");
            $('#image_change').trigger('click');
        });
        });
    </script>
</div>
@stop