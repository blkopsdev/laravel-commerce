@extends('admin.layouts.default')
@section('content')
<script src="{{ SITE_URL }}js/admin/jquery.passMeter.js"></script>
<style>
    .error-inner{color: red;}
    .duplicate-error{color: red;}
</style>
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Clients <small>Find and manage clients</small></h1>
        </div>        
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="portlet light" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green-sharp bold uppercase">
                                    <i class="fa fa-users">
                                    </i>
                                    Edit Client
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body form">                            
                            <form role="form" method="post" class="horizontal-form" id="frmEdit" name="frmEdit" onsubmit="return false;" action="{{ADMIN_URL}}client/edit/{{$records->id}}" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <h3 class="form-section">Person Details</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Username
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_username" name="v_username" class="form-control required" placeholder="Username" value="{{$records->admin->v_username}}">
                                                            <div id="error_v_username" class="duplicate-error " style="display: none;">Username already exits.</div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Password
                                                            </label>
                                                            <input type="password" name="password" id="password" class="form-control validate_password main_password" placeholder="Password">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">First Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_firstname" name="v_firstname" class="form-control required" placeholder="First Name" value="{{$records->v_firstname}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Last Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_lastname" name="v_lastname" class="form-control required" placeholder="Last Name" value="{{$records->v_lastname}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Company
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_company" name="v_company" class="form-control required" placeholder="Company" value="{{$records->v_company}}">
                                                            <div id="error_v_company" class="duplicate-error " style="display: none;">Company already exits.</div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Fax
                                                            </label>
                                                            <input type="text" id="v_fax" name="v_fax" class="form-control" placeholder="Fax" value="{{$records->v_fax}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Email
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_email" name="v_email" class="form-control email required" placeholder="Email" value="{{$records->v_email}}">
                                                            <div id="error_v_email" class="duplicate-error " style="display: none;">Email already exits.</div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Address line 1
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_address_1" name="v_address_1" class="form-control required" placeholder="Address line 1" value="{{$records->v_address_1}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Address line 2
                                                            </label>
                                                            <input type="text" id="v_address_2" name="v_address_2" class="form-control" placeholder="Address line 2" value="{{$records->v_address_2}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">City
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_city" name="v_city" class="form-control required" placeholder="City" value="{{$records->v_city}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">State
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_state" name="v_state" class="form-control required" placeholder="State" value="{{$records->v_state}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Zip Code
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_zipcode" name="v_zipcode" class="form-control number  required validate_zip" placeholder="Zip Code" value="{{$records->v_zipcode}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Phone
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_phone" name="v_phone" class="form-control required" placeholder="Phone" value="{{$records->v_phone}}">
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="control-label">Note
                                                            </label>
                                                            <textarea id="l_note" name="l_note" class="form-control" placeholder="Note"><?php echo ($records->l_note!='')?$records->l_note:''; ?></textarea> 
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Status
                                                             <span class="redLabel">
                                                                *
                                                            </span></label>
                                                            <select class="form-control  input-icon" id="e_status" name="e_status">
                                                                <option value="Active" @if($records->e_status=="Active")selected="selected" @endif>Active</option>
                                                                <option value="Inactive" @if($records->e_status=="Inactive")selected="selected" @endif>Inactive</option>              
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Company Logo         
                                                            </label>
                                                            <div class="input-file-box">
                                                <div data-provides="fileinput" class="fileinput" >
                                                    <div>
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileinput-new" id="up_img"> Upload Company Logo </span>
                                                        <span class="fileinput-exists" id="change_img"> Change </span>
                                                        <input type="file" name="image_upload" id="image_upload"/>
                                                    </span>
                                                        <a data-dismiss="fileinput"  class="btn red fileinput-exists" href="javascript:;" id="rmv_img">Remove </a>
                                                    </div>
                                                    <div style="max-width: 200px; max-height: auto; margin-top: 10px;" id="image_val1" class="fileinput-preview fileinput-exists thumbnail">
                                                        @if($records->company_logo != ''  && file_exists(CLIENT_ADD_IMG_PATH_URL.$records->company_logo))
                                                            <img alt="" src="{{ SITE_URL.CLIENT_ADD_IMG_PATH_URL.$records->company_logo}}"/>
                                                        @else
                                                            <?php $records->company_logo = ''; ?>
                                                        @endif
                                                    </div>
                                                </div>
                                                <input type="hidden" value="" id="v_logo_val1" name="v_logo" />
                                            </div>
                                                            
                                                    </div>  
                                                    </div>
                                                   
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->      
                                                <h3 class="form-section">FTP Details</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>
                                                            Host Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_ftp_url" placeholder="Host Name" name="v_ftp_url" class="form-control required" value="{{$records->v_ftp_url}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>
                                                            FTP Username
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_ftp_username" placeholder="FTP Username" name="v_ftp_username" class="form-control required" value="{{$records->v_ftp_username}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">          
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>FTP Password
                                                            </label>
                                                            <div class="input-group">
                                                                <input type="password" id="v_ftp_password" placeholder="FTP Password" name="v_ftp_password" class="form-control validate_password" value="{{$records->v_ftp_password}}">
                                                                <span class="input-group-addon" id="view_password">
                                                                <i class="fa fa-eye"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Datafeed Directory Name
                                                            </label>
                                                            <input type="text" id="data_feed_company" placeholder="Datefeed Directory Name" name="v_company" class="form-control" value="{{$records->v_company}}" readonly="">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    
                                                </div>
                                            </div>
                                            <div class="form-actions right">
                                               <button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>
                                    <a href="{{ ADMIN_URL }}client" class=" btn default button-previous">Cancel </a>
                                            </div>
                                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    </div>
</div>
    <script>
$(document).ready(function(){    
       var im = '<?php echo $records->company_logo; ?>';
       $.passMeter({
            // configure the divs
            'id_password'   :   '#password',

            // customize the message levels
            'bad'   :   'Bad',
            'low'   :   'Low',
            'good'  :   'Good',
            'strong'    :   'Strong'
        });
            //$('#error_msg').val(im);
            if(im != ''){
                $('#up_img').hide();
                $('#change_img').show();
                $('#rmv_img').show();
            } else {
                $('#up_img').show();
                $('#change_img').hide();
                $('#rmv_img').hide();
                $('#image_val1').removeClass('thumbnail');
            }
            $('#rmv_img').on('click',function(){
                $('#edit_img').hide();
                $('#up_img').show();
                $('#change_img').hide();
                $('#rmv_img').hide();
            });
            $('#up_img').on('click',function(){
                $('#up_img').hide();
                $('#change_img').show();
                $('#rmv_img').show();
            });
            $('#image_upload').on('change',function(){
                var t= $('#image_upload').val();                
                if(t!= ''){
                    $('#up_img').hide();
                    $('#change_img').show();
                    $('#rmv_img').show();
                    //$('#error_msg').hide();
                    //$('#error_msg_error').hide();
                }
                $("#img_div").attr('style',"display:block");
            });
            $('#view_password').on('click',function(){
                if($('#v_ftp_password').attr('type').length == 8){
                    var ftp_password = '<?php echo base64_decode($records->v_ftp_password) ?>';
                    $('#v_ftp_password').attr('type', 'text');
                    $('#v_ftp_password').val(ftp_password);
                } else {
                    var ftp_password = '<?php echo $records->v_ftp_password ?>';
                    $('#v_ftp_password').attr('type', 'password');
                    $('#v_ftp_password').val(ftp_password);
                }
            });
});
</script>
@stop