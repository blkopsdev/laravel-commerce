@extends('admin.layouts.default')
@section('content')
<script src="{{ SITE_URL }}js/admin/jquery.passMeter.js"></script>
<style>
    .error-inner{color: red;}
    .duplicate-error{color: red;}
</style>
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Clients <small>Find and manage clients</small></h1>
        </div>        
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="portlet light" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green-sharp bold uppercase">
                                    <i class="fa fa-users">
                                    </i>
                                    Add Client
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body form">                            
                            <form role="form" method="post" class="horizontal-form" id="frmAdd" name="frmAdd" onsubmit="return false;" action="{{ADMIN_URL}}client/add" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <h3 class="form-section">Person Details</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Username
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_username" name="v_username" class="form-control required" placeholder="Username">
                                                             <div id="error_v_username" class="duplicate-error " style="display: none;">Username already exits.</div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Password
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="password" name="password" id="password" class="form-control required validate_password main_password" placeholder="Password">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">First Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_firstname" name="v_firstname" class="form-control required" placeholder="First Name">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Last Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_lastname" name="v_lastname" class="form-control required" placeholder="Last Name">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Company
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_company" name="v_company" class="form-control required" placeholder="Company">
                                                            <div id="error_v_company" class="duplicate-error " style="display: none;">Company already exits.</div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Fax
                                                            </label>
                                                            <input type="text" id="v_fax" name="v_fax" class="form-control" placeholder="Fax">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Email
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_email" name="v_email" class="form-control email required" placeholder="Email">
                                                            <div id="error_v_email" class="duplicate-error " style="display: none;">Email already exits.</div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Address line 1
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_address_1" name="v_address_1" class="form-control required" placeholder="Address line 1">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Address line 2
                                                            </label>
                                                            <input type="text" id="v_address_2" name="v_address_2" class="form-control" placeholder="Address line 2">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">City
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_city" name="v_city" class="form-control required" placeholder="City">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">State
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_state" name="v_state" class="form-control required" placeholder="State">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Zip Code
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_zipcode" name="v_zipcode" class="form-control  required validate_zip" placeholder="Zip Code">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Phone
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_phone" name="v_phone" class="form-control required" placeholder="Phone">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Note
                                                            </label>
                                                            <textarea id="l_note" name="l_note" class="form-control" placeholder="Note"></textarea> 
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Status
                                                             <span class="redLabel">
                                                                *
                                                            </span></label>
                                                            <select class="form-control  input-icon" id="e_status" name="e_status">
                                                                <option value="Active" selected="selected">Active</option>
                                                                <option value="Inactive">Inactive</option>              
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Company Logo
                                                            </label>
                                                            <div class="input-file-box">
                                                                <div data-provides="fileinput" class="fileinput fileinput-new">
                                                    <div>
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileinput-new" id="up"> Upload Company Logo </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="add_image" class="add_image" id="add_image"/>
                                                    </span>
                                                        <a data-dismiss="fileinput"  class="btn red fileinput-exists" href="javascript:;"> Remove </a>
                                                    </div>
                                                    <div>
                                                        <input type="hidden" id="error_msg" class="" placeholder="image"/>
                                                    </div>
                                                    <div style="max-width: 200px; max-height: auto; margin-top: 10px;" id="image_val" class="fileinput-preview fileinput-exists thumbnail">
                                                    </div>
                                                </div>
                                                <input type="hidden" value="" id="v_logo_val" name="v_logo" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                
                                                <!--/row-->      
                                                <h3 class="form-section">FTP Details</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>
                                                            Host Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_ftp_url" placeholder="Host Name" name="v_ftp_url" class="form-control  required">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>
                                                            FTP Username
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_ftp_username" placeholder="FTP Username" name="v_ftp_username" class="form-control required">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">          
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>FTP Password
                                                             <span class="redLabel">
                                                                *
                                                            </span></label>
                                                            <input type="password" id="v_ftp_password" placeholder="FTP Password" name="v_ftp_password" class="form-control required validate_password">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions right">
                                               <button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>
                                               <button class="btn blue-madison button-next" id="frmAddNewSubmit">Save and Add new  <i class="fa fa-check-square-o "></i></button>
                                    <a href="{{ ADMIN_URL }}client" class=" btn default button-previous">Cancel </a>
                                            </div>
                                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    </div>
    <script>
$(document).ready(function(){    
        $.passMeter({
            // configure the divs
            'id_password'   :   '#password',

            // customize the message levels
            'bad'   :   'Bad',
            'low'   :   'Low',
            'good'  :   'Good',
            'strong'    :   'Strong'
        });
      $('.add_image').on('change',function(){
         console.log($(this));
         var t= $('input[name="add_image"]').val();
         $('#error_msg').val(t);
         if(t!= ''){
             $('#error_msg').hide();
             $('.error_msg_error').hide();
         }
     });
});
</script>
</div>
@stop