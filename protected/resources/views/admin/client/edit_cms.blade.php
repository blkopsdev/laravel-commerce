@extends('admin.layouts.default')
@section('content')
<style>
    .error-inner{color: red;}
    .duplicate-error{color: red;}
</style>
<script>
$(document).ready(function(){
    $("#cms_content_1").ckeditor();
    $("#cms_content_2").ckeditor();
});
</script> 
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Content Management</h1>
        </div>        
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="portlet light" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green-sharp bold uppercase">
                                    <i class="fa fa-users">
                                    </i>
                                    Edit Content
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body form">                            
                            <form role="form" method="post" class="horizontal-form" id="frmEdit" name="frmEdit" onsubmit="return false;" action="{{ADMIN_URL}}client/cms/{{$records->id}}" enctype="multipart/form-data">
                            @if(Session::has('success-message'))                    
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <i class="fa-lg fa fa-check "></i> 
                                <span class="message">Cms has been successfully updated.</span>
                            </div>
                            @endif
                            <div class="alert alert-danger display-hide">
                                    <span class="close" data-close="alert"></span>
                                    You have some form errors. Please check below.
                            </div>
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label">Packing Slip - Contents 1 :
                                                            </label>
                                                            <textarea id="cms_content_1" name="cms_content_1" class="form-control input-icon" placeholder="CMS Content1">{{ $records->cms_content_1 }}</textarea>
                                                        </div>
                                                    </div>
                                                  
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label">Packing Slip - Contents 2 :
                                                            </label>
                                                            <textarea id="cms_content_2" name="cms_content_2" class="form-control input-icon" placeholder="CMS Content2">{{ $records->cms_content_2 }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                            
                                            <div class="form-actions right">
                                               <button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>
                                    <a href="{{ ADMIN_URL }}dashboard" class=" btn default button-previous">Cancel </a>
                                            </div>
                                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    </div>
</div>
@stop