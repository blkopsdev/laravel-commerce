@extends('admin.layouts.default')
@section('content')
<style>
    .error-inner{color: red;}
    .duplicate-error{color: red;}
</style>
<script>
        $(document).ready(function(){
            $("#t_email_content").ckeditor();
        });
    

    </script>
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Mail template <small>Find and manage mail template</small></h1>
        </div>        
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="portlet light" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green-sharp bold uppercase">
                                    <i class="fa fa-users">
                                    </i>
                                    Edit Mail Template
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body form">                            
                            <form role="form" method="post" class="form-horizontal" id="frmEdit" name="frmEdit" onsubmit="return false;" action="{{ADMIN_URL}}mail-template/edit/{{$records->id}}" enctype="multipart/form-data">
                                            <div class="form-wizard">
                                    <div class="form-body">
                                        <div class="tab-content">
                                            <div class="alert alert-danger display-none">
                                                <button class="close">
                                                </button>
                                                You have some form errors. Please check below.
                                            </div>
                                            <?php $msg = Session::get('msg'); if($msg){ ?>
                                            <div class="alert alert-danger">
                                                <button class="close"></button>
                                                {{ $msg }}
                                            </div>
                                            <?php } ?>
                                            <div class="tab-pane active" id="tab1">  
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Template Title
                                                    <span class="redLabel">
                                                        *
                                                    </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                            <input type="text" id="v_template_name" name="v_template_name" class="form-control required" placeholder="Template title" value="{{$records->v_template_name}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Template Subject
                                                    <span class="redLabel">
                                                        *
                                                    </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                            <input type="text" name="v_subject" id="v_subject" class="form-control" placeholder="Template subject" value="{{$records->v_subject}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">From Email ID
                                                    <span class="redLabel">
                                                        *
                                                    </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                           <input type="text" id="v_from_email_id" name="v_from_email_id" class="form-control email required" placeholder="From email id" value="{{$records->v_from_email_id}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Email Content
                                                    <span class="redLabel">
                                                        *
                                                    </span>
                                                    </label>
                                                    <div class="col-md-10">
                                                           <textarea id="t_email_content" name="t_email_content" class="form-control input-icon" placeholder="Email Content">{{ $records->t_email_content}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-2 col-md-8">
                                                <button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>      
                                                <a href="{{ ADMIN_URL }}mail-template" class=" btn default button-previous">Cancel </a>                        
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    </div>
    <script>
$(document).ready(function(){    
      $('.add_image').on('change',function(){
         console.log($(this));
         var t= $('input[name="add_image"]').val();
         $('#error_msg').val(t);
         if(t!= ''){
             $('#error_msg').hide();
             $('.error_msg_error').hide();
         }
     });
});
</script>
</div>
@stop