@extends('admin.layouts.default')
@section('content')
<style>
    .error-inner{color: red;}
    .duplicate-error{color: red;}
</style>
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Clients <small>Find and manage employee</small></h1>
        </div>        
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="portlet light" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green-sharp bold uppercase">
                                    <i class="fa fa-users">
                                    </i>
                                    Add Employee
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body form">                            
                            <form role="form" method="post" class="horizontal-form" id="frmAdd" name="frmAdd" onsubmit="return false;" action="{{ADMIN_URL}}employee/add" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <h3 class="form-section">Person Details</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Username
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="firstName" name="v_username" class="form-control required" placeholder="Username">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Password
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="password" name="password" id="lastName" class="form-control required" placeholder="Password">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"> Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_vname" name="v_vname" class="form-control required" placeholder="Name">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Email
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_email" name="v_email" class="form-control email required" placeholder="Email">
                                                            <div id="error_v_email" class="duplicate-error " style="display: none;">Email already exits.</div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Phone
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_phone" name="v_phone" class="form-control number required" placeholder="Company">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Note
                                                            </label>
                                                             <textarea id="v_note" name="v_note" class="form-control" placeholder="Note">
                                                            </textarea> 
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Fax
                                                            </label>
                                                            <input type="text" id="v_fax" name="v_fax" class="form-control  required" placeholder="Fax">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">URL
                                                            </label>
                                                            <input type="text" id="v_url" name="v_url" class="form-control url required" placeholder="Address line 1">
                                                             <input name="txtcompany" type="hidden" class="textboxcss" id="txtcompany" value="{{ (!empty($vendor_list))? $vendor_list[0]['id']:'' }}" size="40" maxlength="50"> 
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                            </div>
                                            <div class="form-actions right">
                                               <button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>
                                               <button class="btn blue-madison button-next" id="frmAddNewSubmit">Save and Add new  <i class="fa fa-check-square-o "></i></button>
                                    <a href="{{ ADMIN_URL }}employee" class=" btn default button-previous">Cancel </a>
                                            </div>
                                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    </div>
    <script>
$(document).ready(function(){    
      $('.add_image').on('change',function(){
         console.log($(this));
         var t= $('input[name="add_image"]').val();
         $('#error_msg').val(t);
         if(t!= ''){
             $('#error_msg').hide();
             $('.error_msg_error').hide();
         }
     });
});
</script>
</div>
@stop