@extends('admin.layouts.default')
@section('content')
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Mail template <small>Find and manage mail template</small></h1>
        </div>
       
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
    <!--div class="page-content-wrapper"-->
        <div class="page-content">
            <input type="hidden" id="del_url" name="del_url" value="<?php echo ADMIN_URL.'mail-template/delete/'; ?>" />
            <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
               	    <div class="portlet light">
					   <div class="portlet-title">
                          <div class="caption"><i class="fa fa-cogs font-green-sharp"></i>
							<span class="caption-subject font-green-sharp bold uppercase">Manage Mail Template</span></div>
					</div>                    
                <div class="portlet-body">
                    <div class="table-container ">
                        @if(Session::has('success-message'))                    
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <i class="fa-lg fa fa-check "></i> 
                                <span class="message">{!!Session::get('success-message')!!}</span>
                            </div>
                        @endif
                            <table class="table table-striped table-hover table-bordered" id="datatable_ajax"> <!-- editable_resources_filter -->
                                <thead>                
                                    <tr role="row" class="heading">
                                        <th width="2%" style="padding-right: 0px; background-image: none ! important;" class="actions no-sort"><input type="checkbox" class="group-checkable"/></th>
                                        <th>
                                            Template Title
                                        </th>                             
                                        <th style="padding-right: 0px; background-image: none ! important align:right;" class="actions no-sort">
                                             Actions
                                        </th>
                                    </tr>
                                    <tr role="row" class="filter">
                                        <td rowspan="1" colspan="1">
            						     </td>
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="v_template_name" class="form-control form-filter input-sm"/>
            						     </td>                                         
                                         <td rowspan="1" class="actions" colspan="1">
                                            <div class="margin-bottom-5">
            									<button class="btn btn-sm blue-madison filter-submit margin-bottom btn-circle"><i class="fa fa-search"></i> Search</button>
                                                <button class="btn btn-sm default filter-cancel btn-circle"><i class="fa fa-times"></i> Reset</button>
            								</div>
            			                 </td>
                                    </tr>                                
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div> 
                    </div>
                </div>
            </div>
        </div>
        </div>
    <!--/div -->   
<script>
    $(document).ready(function(){        
        var url = ADMIN_URL+'mail-template/list-ajax';
        TableAjax.init(url);    
        $('.created_at').datepicker({
                format: 'yyyy-mm-dd'
            });    
    });
</script>
@stop