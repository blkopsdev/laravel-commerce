<HTML>
<HEAD>
<TITLE>
{{$title}}
</TITLE>
<link rel="stylesheet" type="text/css" href="{{ ASSET_URL }}css/general_bill.css">
<link rel="stylesheet" type="text/css" href="{{ ASSET_URL }}css/custom_bill.css">
<link rel="stylesheet" type="text/css" href="{{ ASSET_URL }}css/layout_bill.css">
<LINK href="{{ ASSET_URL }}css/style_bill.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY text=#000000 bgColor=#ffffff leftMargin=0 topMargin=0>
<center>
<table cellpadding="5px" style="border:1px solid #000; border-bottom:1px #000 solid; margin:5px; padding:5px;" cellspacing="0" width="630px" height="850px" >
  <tr>
    <td colspan="3" style="height:10px;" align="center"></td>
  </tr>
  <tr style="height:20px;">
    <td width="47%" rowspan="2"><h2 align="left"><?php echo ucwords($order['v_website']); ?></h2></td>
    <td width="25%"><strong>Order Number</strong> :</td>
    <td width="28%">{{$order['v_shipment_number']}}</td>
  </tr>
  <tr style="height:15px;">
    <td width="25%"><strong>Order Date :</strong></td>
    <td width="28%">{{$order['d_order_date']}}</td>
  </tr>
  <tr >
    <td colspan="3" style="height:10px;" align="center"></td>
  </tr>
  <tr style="height:25px;">
    <td style="font-size:15px; font-weight:bold;">Sold To</td>
    <td style="font-size:15px; font-weight:bold;" colspan="2">Ship To</td>
  </tr>
  <tr>
    <td>
      <?php
      echo $order['v_bill_to_firstname']." ".$order['v_bill_to_lastname']."<br>";
      $add = '';
      if($order['v_bill_to_address1'])
        $add.=$order['v_bill_to_address1'].",<br /> ";
      if($order['v_bill_to_address2'])
        $add.=$order['v_bill_to_address2'].",<br /> ";
      if($order['v_bill_to_city'])
        $add.=$order['v_bill_to_city'].", ";
      if($order['v_bill_to_state'])
        $add.=$order['v_bill_to_state']." ";
      if($order['v_bill_to_zip'])
        $add.=$order['v_bill_to_zip']." ";
      echo $add."<br>USA"; ?>
    </td>
    <td colspan="2">
      <?php
            if($order['v_ship_to_company'])
            echo $order['v_ship_to_company'].'<br>';
      echo $order['v_ship_to_firstname']." ".$order['v_ship_to_lastname']."<br>";
      $add2 = '';
      if($order['v_ship_to_address1'])
        $add2.=$order['v_ship_to_address1'].", ";
      if($order['v_ship_to_address2'])
        $add2.=$order['v_ship_to_address2'].", ";
      if($order['v_ship_to_city'])
        $add2.="<br>".$order['v_ship_to_city'].", ";
      if($order['v_ship_to_state'])
        $add2.=$order['v_ship_to_state']." ";
      if($order['v_ship_to_zip'])
        $add2.=$order['v_ship_to_zip']." ";
      echo $add2."<br>USA"; ?>
    </td>
  </tr>

  <tr>
    <td colspan="3" style=""><table width="100%" >
        <tr height="15px">
          <td width="7%"><h4>QTY</h4></td>
          <td width="50%"><h4>NAME</h4></td>
          <td width="11%" >&nbsp;</td>
          <td width="17%" align="center"><h4>Backorder Date</h4></td>
          <td width="15%" align="right" ><h3>SKU</h3></td>
        </tr>
         @foreach($order['order_details'] as $val)
            @if($val['e_item_status']!='Canceled')
            <tr>
                <td height="15px">{{$val['i_quanity']}}</td>
                <td>{{$val['v_description']}}</td>
                <td align="center"><?php if($val['e_item_status'] == 'Back Ordered') { echo "<strong>backordered</strong>"; }?></td>
                <td align="center"><?php if($val['e_item_status'] == 'Back Ordered') { echo date('d-M-Y',strtotime($val['d_back_ordered_date'])); } else { echo "-";}?></td>
                <td align="right">{{$val['v_item_code']}}</td>
                </tr>
            @endif
         @endforeach
      </table></td>
  </tr>
  <?php $cms_data = 'cms_data';
  $content1 = $client['cms_content_1'];
  ?>
  <tr>
    <td colspan="3" style="padding:0px" ><table width="100%" border="0" cellpadding="3px" cellspacing="0">
        <tr>
          <td width="78%">
          <?php if($content1 != '' or trim($order['v_gift_message'])!='') { ?>
          <h3>GIFT MESSAGE</h3>
          <?php } ?></td>
          <td width="22%" rowspan="2">
            <?php if($content1 != '') {?>
            <div class="cmsBox1">
      <?php echo stripcslashes($client['cms_content_1']);?>
            </div>
            <?php } ?>
          </td>
        </tr>
        <?php if(trim($order['v_gift_message']) != '') { ?>
        <tr>
          <td style="height:200px;"><?php echo substr($order['v_gift_message'],0,1050); ?>  </td>
        </tr>
        <?php } ?>
      </table></td>
  </tr>
  <?php if(trim(stripcslashes($client['cms_content_2'])) != '') { ?>
  <tr height="5px">
    <td colspan="3">
      <div style="padding:0px;margin:0px;height:55px">
        <?php echo stripcslashes($client['cms_content_2']);?>
        </div>
    </td>
  </tr>
  <?php } ?>
  <tr height="5px">
    <td colspan="3">
    	<div style="padding:0px;margin:0px;height:145px">
			<p style="margin-bottom: 10px;"><b>DAMAGED ITEM</b> - We will replace any damaged item at no cost to the buyer, as long as we are notified within <b>5 days</b> after merchandise has been received. Please contact Customer service at info@blackforestdecor.com or call <b>1-877-208-6215</b> upon receiving any damaged or defective item.</p>
			<p><b>NOT SATISFIED</b> - If you are not satisfied with the merchandise you purchased from our site or catalog, you can return the merchandise within 60 days from the day received and we will refund your money excluding the shipping fee. Please email or call us before returning merchandise to obtain the correct return address for your package. The merchandise must be adequately packed and insured before returning and be in excellent unused condition. Custom orders, special orders, clearance products, gift certificates and bulk orders are nonrefundable. Light fixtures that have been installed cannot be returned.</p>
        </div>
    </td>
  </tr>
  <tr>
    <td colspan="3" align="center"><h1 style="margin-bottom: 0px;">Thank You For Your Order</h1></td>
  </tr>
</table>
</center>
</BODY>
</HTML>
