<HTML>
<HEAD>
<TITLE>
{{$title}}
</TITLE>
<link rel="stylesheet" type="text/css" href="{{ ASSET_URL }}css/general_bill.css">
<link rel="stylesheet" type="text/css" href="{{ ASSET_URL }}css/layout_bill.css">
<LINK href="{{ ASSET_URL }}css/style_bill.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY text=#000000 bgColor=#ffffff leftMargin=0 topMargin=0>
<center>
<table cellpadding="5px" style="border:1px solid #000; margin:5px; padding:5px;" cellspacing="0" width="630px" height="874" >
  <tr height="35px;">
    <td colspan="6" align="center"><h1><u>Drop-Ship Purchase Order</u></h1></td>
  </tr>
  <tr height="25px">
    <td colspan="6" style="border-bottom:solid 5px #000">From :<strong> {{$client['v_company']}}</strong></td>
  </tr>
  <tr height="25px">
    <td width="138" style="border:5px #000 solid; border-top:none; font-size:16px; font-weight:bold">Do Not Ship to this Address</td>
    <td width="470" colspan="5">
            @if($client['v_address_1'])
            	{{$client['v_address_1'].','}}<br />
            @endif
            @if($client['v_address_2'])
            	{{$client['v_address_2'].','}}<br />
            @endif
            {{$client['v_city']}}, {{$client['v_state']}}. {{$client['v_zipcode']}}<br />
				</td>
  </tr>
  <tr height="15px">
    <td colspan="6">Vendor : <strong>{{$order['vendor']['v_vendor_name']}}</strong></td>
  </tr>
  <tr height="20px">
    <td colspan="6"><table style="margin-bottom:-12px;" cellspacing="0" cellpadding="5px" width="100%">
        <tr>
          <td style="border:5px solid #000; border-bottom:none" >Order #</td>
          <td style="border:5px solid #000; border-left:none; border-bottom:none" width="92" >{{$order['v_shipment_number']}}</td>
          <td width="40" >&nbsp;</td>
          <td style="border:5px solid #000; border-bottom:none" width="65" >Order Date</td>
          <td style="border:5px solid #000; border-left:none; border-bottom:none" width="76" >{{$order['d_order_date']}}</td>
          <td width="191" >&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr height="120px">
    <td colspan="6"><table cellpadding="5px" cellspacing="0" width="100%" height="100px" style="border:solid 5px #000">
        <tr>
          <td colspan="4"><h2 style="height:2px;">Instructions</h2></td>
        </tr>
        <tr>
          <td colspan="4"><p>1. This is a DROP-SHIP order. Please ship the item(s) below directly to our customer as soon as possible.<br>
              2. DO NOT INCLUDE YOUR INVOICE IN THE PACKAGE! Mail your invoice to the address listed above.<br>
              3. The last page of this order is a packing slip. Please insert it in the package prior to shipping. </p></td>
        </tr>
        <tr>
          <td width="14%"><h5>4. SHIP TO </h5></td>
          <td width="35%" style="border:#000 5px solid;border-right:none;"><?php 
                                                    if($order['v_ship_to_company'])
                                                    echo $order['v_ship_to_company'].'<br>';
                                                    if($order['v_ship_to_firstname'] or $order['v_ship_to_lastname']) 
                                                        echo $order['v_ship_to_firstname']." ".$order['v_ship_to_lastname'];
													$add2 = '<br />';											
                                                    if($order['v_ship_to_address1']) 
                                                        $add2.=$order['v_ship_to_address1'];
                                                    if($order['v_ship_to_address2']) 
                                                        $add2.= "<br />".$order['v_ship_to_address2'];										
                                                    if($order['v_ship_to_city']) 
                                                        $add2.="<br>".$order['v_ship_to_city'].", ";
                                                    if($order['v_ship_to_state']) 
                                                        $add2.=$order['v_ship_to_state'];
                                                    if($order['v_ship_to_zip']) 
                                                        $add2.=" ".$order['v_ship_to_zip'];    
                                                    $add2.="<br>USA";												
                                                    echo $add2;
                                                    ?></td>
          <td width="30%" style="border:#000 5px solid;border-left:none;">Phone Number : <br />{{$order['v_ship_to_phonenumber']}}</td>
          <td width="21%">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="4"><p>5. Delays? Questions? Problems? CALL IMMEDIATELY! Diane 877-208-6215</p>
          @if($fedex_flg == 1)
            <p>6. Ship-based on Black Forest Décor routing guide.  Please use Black Forest Décor’s Fed EX ground account or for freight shipments, email freight@blackforestdecor.com per routing guide.</p>
          @else
            <p>6. Ship based on Black Forest Décor routing guide, UPS #688R21 for ground or email freight information to freight@blackforestdecor.com per routing guide.</p>
          @endif
          
           <?php /*?> <p>6. Email order acknowledgement confirming receipt of this order to vendor@blackforestdecor.com</p>
            <p>7. Email shipment confirmation to: vendor@blackforestdecor.com</p><?php */?></td>
        </tr>
      </table></td>
  <?php  $SpecialInstruction1 = '';  
  for($i=0;$i<count($order['order_details']);$i++) {
      if($order['order_details'][$i]['v_special_instruction']) {          
        $SpecialInstruction1 .= "*".$order['order_details'][$i]['v_special_instruction']."<br />";
      }
  }
  if($SpecialInstruction1 != '') { ?>
  <tr height="10px">
    <td colspan="6"><font size="+1"><strong>Special Instructions: </strong></font></td>
  </tr>
  <tr height="30px">
    <td colspan="6">
      <?php echo $SpecialInstruction1; ?>
    </td>
  </tr>
  <?php } ?>
  <tr>
  	<td colspan="6">
   		<table width="100%">
	          <tr height="16px">
                <td width="15%"><h3>QTY</h3></td>
                <td width="62%"><h3>NAME</h3></td>
                <td width="23%"><h3>SKU</h3></td>
              </tr>
              @foreach($order['order_details'] as $val)
              <tr height="14px">
                <td>{{$val['i_quanity']}}</td>
                <td>{{$val['v_description']}}</td>
                <td>{{$val['v_supplier_code']}}</td>
              </tr>
              @endforeach
        </table> 
    </td>
  </tr>
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>
</table>
</center>
</BODY>
</HTML>
