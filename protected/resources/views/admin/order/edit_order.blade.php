@extends('admin.layouts.default')
@section('content')
<style>
    .error-inner{color: red;}
    .duplicate-error{color: red;}
</style>
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Order <small>Find and manage order</small></h1>
        </div>        
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="portlet light" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green-sharp bold uppercase">
                                    <i class="fa fa-users">
                                    </i>
                                    Edit Order
                                </span>
                            </div>
                        </div>
                        <input type="hidden" name="redirect_url" id="redirect_url" value="{{ADMIN_URL}}order/edit/{{$records->v_shipment_number}}">
                        <div class="portlet-body form">                            
                            <form role="form" method="post" class="horizontal-form" id="frmEdit" name="frmEdit" onsubmit="return false;" action="{{ADMIN_URL}}order/edit-order/{{$records->id}}" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <h3 class="form-section">Edit Shipping Detail of Order : {{ $records->v_shipment_number }}</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">First Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_bill_to_firstname" name="v_bill_to_firstname" class="form-control required" placeholder="First Name" value="{{$records->v_bill_to_firstname}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Last Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" name="v_bill_to_lastname" id="v_bill_to_lastname" class="form-control required" placeholder="Last Name" value="{{$records->v_bill_to_lastname}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6">
                                                         <div class="form-group">
                                                            <label class="control-label">Address line 1
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                             <input type="text" name="v_bill_to_address1" id="v_bill_to_address1" class="form-control required" placeholder="Address Line 1" value="{{$records->v_bill_to_address1}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"> Address line 2
                                                            </label>
                                                            <input type="text" id="v_bill_to_address2" name="v_bill_to_address2" class="form-control" placeholder="Address Line 2" value="{{$records->v_bill_to_address2}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                 <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">City
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_bill_to_city" name="v_bill_to_city" class="form-control required" placeholder="City" value="{{$records->v_bill_to_city}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">State
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_bill_to_state" name="v_bill_to_state" class="form-control required" placeholder="State" value="{{$records->v_bill_to_state}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Zip Code
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_bill_to_zip" name="v_bill_to_zip" class="form-control required validate_zip" placeholder="Zip Code" value="<?php echo ($records->v_bill_to_zip!='')?$records->v_bill_to_zip:''; ?>">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Phone
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_bill_to_phonenumber" name="v_bill_to_phonenumber" class="form-control required" placeholder="Phone" value="<?php echo ($records->v_bill_to_phonenumber!='')?$records->v_bill_to_phonenumber:''; ?>">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <h3 class="form-section">Edit Gift Message for this Order</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Gift Message
                                                            </label>
                                                           <textarea id="v_gift_message" name="v_gift_message" class="form-control" placeholder="Gift Message"><?php echo ($records->v_gift_message!='')?$records->v_gift_message:''; ?></textarea> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <h3 class="form-section">Edit Special Instruction for Product(s)</h3>
                                                @foreach($records->OrderDetails as $val)
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">{{ $val->v_item_code }} : {{$val->v_description}}
                                                            </label>
                                                           <textarea id="v_special_instruction" name="v_special_instruction-{{$val['id']}}" class="form-control" placeholder="Special Instruction"><?php echo ($val->v_special_instruction!='')?$val->v_special_instruction:''; ?></textarea> 
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                            <div class="form-actions right">
                                               <button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>    
                                    <a href="{{ ADMIN_URL }}order/edit/{{$records->v_shipment_number}}" class=" btn default button-previous">Cancel </a>
                                            </div>
                                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    </div>
</div>
@stop