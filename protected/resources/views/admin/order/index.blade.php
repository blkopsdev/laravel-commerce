@extends('admin.layouts.default')
@section('content')
<style>
a:hover {
    text-decoration:none;
    cursor:pointer;
}
/*.modal-body {
    min-height: 400px;
}*/
.scrollable-menu {
    height: auto;
    max-height: 50px;
    overflow-y: auto;
}
.safari_class {
	margin-top: 5px;
}
.error-inner{
    color: red;
}
</style>
<script>
$(document).ready(function() {
    if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0){
		$('#client_id').addClass('safari_class');
    } else if (navigator.appVersion.indexOf("Mac")!=-1){
        $('#client_id').addClass('safari_class');
    }
 });
</script>
 <?php 
$user = Auth::guard('admin')->user();
?>
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Order <small>Find and manage order</small></h1>
        </div>
       
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
    <!--div class="page-content-wrapper"-->
        <div class="page-content">
            <input type="hidden" id="
            " name="del_url" value="<?php echo ADMIN_URL.'order/delete/'; ?>" />
            <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
               	    <div class="portlet light">
					   <div class="portlet-title">
                          <div class="caption"><i class="fa fa-cogs font-green-sharp"></i>
							<span class="caption-subject font-green-sharp bold uppercase">Manage Order</span></div>
                          <div class="actions">
                          @if($user->role==1 || (isset($UserPermission["1"]["e_import"]) && $UserPermission["1"]["e_import"]  == 1) || $user->role == 5 || (isset($UserPermission["5"]["e_import"]) && $UserPermission["5"]["e_import"]  == 1))
                                @if($user->role == 1 || $user->role == 5)
                                    <a class="btn yellow" href="#form_modal11" data-toggle="modal">
                                     <i class="fa fa-reply"></i> <span class="hidden-480">Import Data Feed</span>  
                                    </a>
                                @elseif($user->role==2)
                                    <a class="btn blue-madison btn-circle" id="import_client" href="javascript:;">
                                        <i class="fa fa-reply"></i>
                                        <span class="hidden-480">Import Data Feed</span>
                                    </a>
                                @endif
                            @endif
                            @if($user->role==1 || (isset($UserPermission["1"]["e_export"]) && $UserPermission["1"]["e_export"]  == 1))
                            
                            <a href="javascript:;" action-url="<?php echo
ADMIN_URL . 'order/export-data-feed'; ?>" id="export_to_excel_order" class="btn blue-madison btn-circle" <i class="fa fa-share"></i> <span class="hidden-480">Export to Excel</span>  </a>

                            @endif
                        </div>
                        
					</div>                    
                <div class="portlet-body">
                    <div class="table-container ">
                     <div class="Metronic-alerts alert alert-success fade in display-hide" id="success-msg">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <i class="fa-lg fa fa-check "></i> 
                                <span class="message"></span>
                        </div>
                        @if(Session::has('success-message'))                    
                            <div class="Metronic-alerts alert alert-success">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <i class="fa-lg fa fa-check "></i> 
                                <span class="message">{!!Session::get('success-message')!!}</span>
                            </div>
                        @endif
                        @if(Session::has('alert-message'))
                            <div class="Metronic-alerts alert alert-danger fade in">
                                <button type="button" class="close" aria-hidden="true" data-dismiss="alert"></button>
                                <span class="message">{!!Session::get('alert-message')!!}</span>
                            </div>
                        @endif
                        @if($user->role==1 || (isset($UserPermission["1"]["e_delete"]) && $UserPermission["1"]["e_delete"]  == 1))
                            <div class="table-actions-wrapper">
                                <span></span>
    							<select class="table-group-action-input form-control input-inline input-small input-sm">
    								<option value="">Select...</option>
    								<option value="Delete">Delete</option>										
    							</select>
    							<button class="btn btn-sm blue-madison table-group-action-submit btn-circle" id="bulk_action"><i class="fa fa-check"></i> Submit</button>
                                <input type="hidden"  class="table-group-action-url" value="<?php echo ADMIN_URL.'order/bulk-action';?>"/>
    					   </div>
                           @endif
                           <div id="form_modal11" class="modal fade" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Select client for import datafeed</h4>
										</div>
										<div class="modal-body">
											<form action="{{ ADMIN_URL }}order/import-to-excel-order"  class="form-horizontal" id="import_data_form" role="form">
                                                <span class="redLabel" style="text-align:center;">(FYI: Import file must be there in client specific folder to import records.)</span>
												<div class="form-group">
                                                    <div class="col-md-3"><label class="control-label col-md-4">Client
                                                    
                                                    </label></div>
													<div class="col-md-9">
														<select class="bs-select form-control scrollable-menu required" name="client_id" id="client_id" placeholder="client">
                                                            <option value="">Select Client</option>
                                                            @foreach($client_data as $client)
                                                                <option value="{{ $client->id }}">{{ $client->v_company }}</option>   
                                                            @endforeach
														</select>
													</div>
												</div>
											</form>
										</div>
										<div class="modal-footer">
											<button class="btn default" data-dismiss="modal" aria-hidden="true">Close</button>
											<button class="btn green" type="button" id="import_button">Save changes</button>
										</div>
									</div>
								</div>
							</div>
                            <table class="table table-striped table-hover table-bordered" id="datatable_ajax"> <!-- editable_resources_filter -->
                                <thead>                
                                    <tr role="row" class="heading">
                                     @if($user->role==1 || (isset($UserPermission["1"]["e_delete"]) && $UserPermission["1"]["e_delete"]  == 1))
                                        <th width="2%" style="padding-right: 0px; background-image: none ! important;" class="actions no-sort"><input type="checkbox" class="group-checkable"/></th>
                                        @endif
                                        @if($user->role!=3 &&  $user->role!=4)
                                        <th>
                                            Supplier Name
                                        </th>
                                        @endif
                                        <th>
                                            Order Id
                                        </th>
                                        <th>
                                            Order Date
                                        </th>
                                        <th>
                                            Last Name
                                        </th>
                                        <th>
                                            First Name
                                        </th>
                                        <th>
                                            Zip Code
                                        </th>
                                        <th>
                                            Product Id
                                        </th>
                                        <th>
                                            Item Name
                                        </th>
                                        <th>
                                            Item Status
                                        </th>
                                        <th style="padding-right: 0px; background-image: none ! important align:right;" class="actions no-sort">
                                             Actions
                                        </th>
                                    </tr>
                                    <tr role="row" class="filter">
                                        @if($user->role==1 || (isset($UserPermission["1"]["e_delete"]) && $UserPermission["1"]["e_delete"]  == 1))
                                        <td rowspan="1" colspan="1">
            						     </td>
                                         @endif
                                         @if($user->role!=3 && $user->role!=4)
                                         <td rowspan="1" colspan="1">
                                            <select class="form-control form-filter input-sm" name="vendor_id">
                                            
                                                <option value="">-- Select --</option>
                                                            @foreach($vendor_list as $vendor)
                                                                <option value="{{ $vendor->id }}">{{ $vendor->v_vendor_name }}</option>
                                                            @endforeach
                                            </select>
            						     </td>
                                         @endif                                         
                                         <td rowspan="1" colspan="1">
                                            <input type="text" id="v_shipment_number" name="v_shipment_number" class="form-control form-filter input-sm"/>
                                         </td>
                                          <td rowspan="1" colspan="1">
                                            <input type="text" name="from_date" id="from_date" class="from_date datetimepicker form-control form-filter input-sm" placeholder="From Date" /><br>
                                            <input type="text" name="to_date" id="end_date" class="to_date datetimepicker form-control form-filter input-sm" placeholder="To Date" />
                                         </td>
                                          <td rowspan="1" colspan="1">
                                            <input type="text" name="v_bill_to_lastname" id="v_bill_to_lastname" class="form-control form-filter input-sm"/>
                                         </td>  
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="v_bill_to_firstname"  id="v_bill_to_firstname" class="form-control form-filter input-sm"/>
                                         </td>  
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="v_bill_to_zip" id="v_bill_to_zip" class="form-control form-filter input-sm"/>
                                         </td>  
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="v_item_code"  id="v_item_code" class="form-control form-filter input-sm"/>
                                         </td>  
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="v_description"  id="v_description" class="form-control form-filter input-sm"/>
                                         </td>
                                          <td rowspan="1" colspan="1">
                                            <select class="form-control form-filter input-sm" name="e_item_status" id="e_item_status" >
                                                <option value="">Select...</option>
                                                <option value="Pending">Pending</option>
                                                <option value="Confirm and Approved">Confirm and Approved</option>
                                                <option value="Back Ordered">Back Ordered</option>
                                                <option value="Customer Canceled">Customer Canceled</option>
                                                <option value="Canceled/Discontinued">Canceled/Discontinued</option>
                                                <option value="Hold - Do not split ship">Hold - Do not split ship</option>
                                            </select>
                                         </td>
                                         <td rowspan="1" class="actions" colspan="1">
                                            <div class="margin-bottom-5">
            									<button class="btn btn-sm blue-madison filter-submit margin-bottom btn-circle"><i class="fa fa-search"></i> Search</button>
                                                <button class="btn btn-sm default filter-cancel btn-circle"><i class="fa fa-times"></i> Reset</button>
            								</div>
            			                 </td>
                                    </tr>                                
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div> 
                    </div>
                </div>
            </div>
        </div>
        </div>
    <!--/div -->   
<script>
    $(document).ready(function(){    
        var url = ADMIN_URL+'order/list-ajax';
        var auth_user = '<?php echo $user->e_type; ?>';    
        if(auth_user == 'Vendor' || auth_user == 'Employee'){
            TableAjax.init(url,'desc');   
        } else {
            TableAjax.init(url);    
        }
        $('.datetimepicker').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });    
        $('#from_date').datepicker({
                format: "mm/dd/yyyy",
                autoclose: true,
                todayBtn: true,
                todayHighlight: true,
                constrainInput: false,
            }).on('change', function(e) {
                $('#end_date').val('');
                $('#end_date').datepicker('remove');
                $("#end_date").datepicker({ format: "mm/dd/yyyy", startDate: $('#from_date').val(), maxDate: 0, autoclose: true, todayBtn: true, todayHighlight: true, constrainInput: false });
            });
    });
     $(document).on('click', '#export_to_excel_order', function (e) {
        var send_data = $('#datatable_ajax').DataTable().ajax.params();
        //var send_data = grid.getAjaxParams();
        var obj = { 'page': send_data.page ,'search': send_data.search,'order': send_data.order,'v_shipment_number': send_data.v_shipment_number,'vendor_id': send_data.vendor_id,'v_item_code':send_data.v_item_code,'v_description':send_data.v_description,'v_bill_to_zip':send_data.v_bill_to_zip,'v_bill_to_lastname': send_data.v_bill_to_lastname,'v_bill_to_firstname':send_data.v_bill_to_firstname,'to_date':send_data.to_date,'from_date':send_data.from_date,'e_item_status':send_data.e_item_status};
        var send_data = $.param(obj);
        var url = $(this).attr('action-url');
        window.location.href = url+ '?' + send_data;
    });
</script>
@stop