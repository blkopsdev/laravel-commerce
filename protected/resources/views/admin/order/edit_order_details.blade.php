@extends('admin.layouts.default')
@section('content')
<?php 
$userArr = Auth::guard('admin')->user();
?>
<style>
    .error-inner{color: red;}
    .duplicate-error{color: red;}
</style>
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Order <small>Find and manage order</small></h1>
        </div>        
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="portlet light" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green-sharp bold uppercase">
                                    <i class="fa fa-users">
                                    </i>
                                    Order Details
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body form">                            
                            <form role="form" method="post" class="horizontal-form" id="frmEdit" name="frmEdit" onsubmit="return false;"  enctype="multipart/form-data">
                                        <div class="col-md-10">
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group order_group">
                                                            <label class="col-md-3">First Name: 
                                                            </label>
                                                            <div class="col-md-9 order_detail_div">
                                                                {{$records['v_ship_to_firstname']}}    
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group order_group">
                                                            <label class="col-md-3">Last Name: 
                                                            </label>
                                                            <div class="col-md-9 order_detail_div">
                                                                {{$records['v_ship_to_lastname']}}    
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group order_group">
                                                            <label class="col-md-3">Order Number: 
                                                            </label>
                                                            <div class="col-md-9 order_detail_div">
                                                                {{$records['v_shipment_number']}}    
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group order_group">
                                                            <label class="col-md-3">Order Date: 
                                                            </label>
                                                            <div class="col-md-9 order_detail_div">
                                                                <?php echo date('d-M-Y',strtotime($records['d_order_date'])); ?>    
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group order_group">
                                                            <label class="col-md-3">Supplier Name: 
                                                            </label>
                                                            <div class="col-md-9 order_detail_div">
                                                                {{$records['v_vendor_name']}}    
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group order_group">
                                                            <label class="col-md-3">Website Name: 
                                                            </label>
                                                            <div class="col-md-9 order_detail_div">
                                                               <?php echo ucwords($records['v_website']); ?>    
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <a class="blue-madison order_invoice" target="_blank" href="{{ ADMIN_URL }}order/dropship-order/{{$records['v_shipment_number']}}">
                                            Drop Ship Order
                                            </a><br>
                                            <a class="blue-madison order_invoice" target="_blank" href="{{ ADMIN_URL }}order/bill-slip/{{$records['v_shipment_number']}}">
                                            Packing slip
                                            </a>
                                        </div>
                                        </form>
                        </div>
                        <div class="portlet-body">
                    <div class="table-container ">
                       <div class="alert alert-success display-hide">
                                <span class="close" data-close="alert"></span>
                                Order has been updated successfully.
                            </div>
                            <div class="alert alert-danger display-hide">
                                    <span class="close" data-close="alert"></span>
                                    You have some form errors. Please check below.
                            </div>
                            <div class="table-actions-wrapper">
                                <span></span>
                                @if($userArr['e_type']!='Vendor' && $userArr['e_type']!='Employee')
                                 <a class="btn blue-madison btn-circle" href="{{ ADMIN_URL }}order/edit-order/{{$records['v_shipment_number']}}">
                                <i class="fa fa-edit"></i>
                                <span class="hidden-480">Edit Order</span>
                                </a>
                                @endif
                                <a class="btn blue-madison btn-circle" href="{{ ADMIN_URL }}order">
                                <i class="fa fa-arrow-left"></i>
                                <span class="hidden-480">Back to order list</span>
                                </a>
                           </div>
                           <form name="frmStatus" id="frmStatus" method="post">
                            <table class="table table-striped table-hover table-bordered" id="datatable_ajax"> <!-- editable_resources_filter -->
                                <thead>                
                                    <tr role="row" class="heading">
                                       <!--  <th width="2%" style="padding-right: 0px; background-image: none ! important;" class="actions no-sort"><input type="checkbox" class="group-checkable"/></th> -->
                                        <th style="padding-right: 0px; background-image: none ! important;" class="actions no-sort">
                                            Client Product Code
                                        </th>
                                        <th class="no-sort">
                                            Vendor Product Code
                                        </th>
                                        <th class="no-sort">
                                            Item Name
                                        </th>
                                        <th class="no-sort">
                                            Quantity
                                        </th>
                                        <th class="no-sort">
                                            Wholesale Price
                                        </th>
                                        <th class="no-sort">
                                            Item Status
                                        </th>
                                        <th class="no-sort">
                                            Backorder Date
                                        </th>
                                        <th class="no-sort">
                                            Notes
                                        </th>
                                        <th class="no-sort">
                                            Vendor Name
                                        </th>
                                        @if($userArr->role == 1)
                                        <th style="padding-right: 0px; background-image: none ! important align:right;" class="actions no-sort">
                                            Actions
                                        </th>
                                        @endif
                                    </tr>
                                   <!--  <tr role="row" class="filter">
                                        <td rowspan="1" colspan="1">
                                         </td>
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="v_item_code" class="form-control form-filter input-sm"/>
                                         </td>                                         
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="v_description" class="form-control form-filter input-sm"/>
                                         </td>
                                          <td rowspan="1" colspan="1">
                                            <input type="text" name="from_date" id="from_date" class="from_date datetimepicker form-control form-filter input-sm" placeholder="From Date" /><br>
                                            <input type="text" name="to_date" id="end_date" class="to_date datetimepicker form-control form-filter input-sm" placeholder="To Date" />
                                         </td>
                                          <td rowspan="1" colspan="1">
                                            <input type="text" name="v_bill_to_lastname" class="form-control form-filter input-sm"/>
                                         </td>  
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="v_bill_to_firstname" class="form-control form-filter input-sm"/>
                                         </td>  
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="v_bill_to_zip" class="form-control form-filter input-sm"/>
                                         </td>  
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="v_item_code" class="form-control form-filter input-sm"/>
                                         </td>  
                                    </tr>  -->                               
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <input type="hidden" name="order_shiping_number" value="{{$records['v_shipment_number']}}">
                            </form>
                            <div class="btn_action">
                                <span></span>
                                <a class="btn blue-madison btn-circle" href="javascript:void(0)" id="edit_order_status" action-url="{{ ADMIN_URL }}order/edit-status/{{$records['i_order_id']}}" style="display: inline-block;">
                                <i class="fa fa-edit"></i>
                                <span class="hidden-480">Update</span>
                                </a>
                                 @if($next != '' && $next != 'null')
                                <a class="btn blue-madison btn-circle" href="<?php echo ADMIN_URL.'order/edit/'.$next; ?>" id="edit_order_next_status" style="float: right;">
                                <i class="fa fa-arrow-right"></i>
                                <span class="hidden-480">Next</span>
                                </a>
                                 @endif
                                @if($previous != '' && $previous != 'null')
                                <a class="btn blue-madison btn-circle" href="<?php echo ADMIN_URL.'order/edit/'.$previous ?>" style="float: left;">
                                <i class="fa fa-arrow-left"></i>
                                <span class="hidden-480">Previous</span>
                                </a>
                                @endif
                                
                        </div>
                        </div>
                        
                    </div> 
                    </div>
                </div>

                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    </div>
    <script>
    $(document).ready(function(){
        var total_records = '<?php echo $total_records; ?>';
        if(total_records <= 100){
            var pagination = 'false';
            var info = 'false';
            var page_menu = '0';
        } else {
            var pagination = 'true';
            var info = 'true';
            var page_menu = '1';
        }
        var url = ADMIN_URL+'order/order-detail/<?php echo $records['v_shipment_number']; ?>';
        TableAjax.init(url,'',pagination,info,page_menu);
        setTimeout(function(){
              $('.d_back_ordered_date').datepicker({
                    format: 'mm/dd/yyyy',
                    todayHighlight:true,
                    autoclose: true
            });   
        },2000);

        
    });
</script>
</div>
@stop