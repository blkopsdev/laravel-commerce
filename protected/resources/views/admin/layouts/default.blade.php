<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>{{ $title }} | {{ SITE_NAME }}</title>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="{{ ASSET_URL }}plugins/font-awesome/css/font-awesome.min.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <link href="{{ ASSET_URL }}plugins/simple-line-icons/simple-line-icons.min.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <link href="{{ ASSET_URL }}plugins/bootstrap/css/bootstrap.min.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <link href="{{ ASSET_URL }}plugins/uniform/css/uniform.default.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <link href="{{ ASSET_URL }}plugins/bootstrap-switch/css/bootstrap-switch.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>

    <link href="{{ ASSET_URL }}plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ ASSET_URL }}plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css{{ ADMIN_CSS_VERSION }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ ASSET_URL }}plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <!-- <link href="{{ ASSET_URL }}plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet"/>
    <link href="{{ ASSET_URL }}plugins/jquery-file-upload/css/jquery.fileupload.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet"/>
    <link href="{{ ASSET_URL }}plugins/jquery-file-upload/css/jquery.fileupload-ui.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet"/> -->
    <link href="{{ ASSET_URL }}plugins/bootstrap-fileinput/bootstrap-fileinput.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <link href="{{ ASSET_URL }}plugins/bootstrap-daterangepicker/daterangepicker-bs3.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet"
          type="text/css"/>

    <link href="{{ ASSET_URL }}plugins/bootstrap-datepicker/css/datepicker3.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <link href="{{ ASSET_URL }}plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ ASSET_URL }}plugins/fullcalendar/fullcalendar.min.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGIN STYLES -->

    <!-- BEGIN PAGE STYLES -->
    <link href="{{ ASSET_URL }}css/tasks.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE STYLES -->
    <!-- BEGIN THEME STYLES -->
    <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
    <link href="{{ ASSET_URL }}css/components.css{{ ADMIN_CSS_VERSION }}" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="{{ ASSET_URL }}css/plugins.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <link href="{{ ASSET_URL }}css/layout.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <link href="{{ ASSET_URL }}css/themes/default.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="{{ASSET_URL}}css/style_main.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <link href="{{ASSET_URL}}css/profile.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <link href="{{ ASSET_URL }}css/custom.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <link href="{{ ASSET_URL }}css/image-crop.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <link href="{{ ASSET_URL }}plugins/jcrop/css/jquery.Jcrop.min.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
    <!-- <link href="{{ ASSET_URL }}plugins/bootstrap-tour/build/css/bootstrap-tour-standalone.css" rel="stylesheet" type="text/css"/> -->
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="{{ ASSET_URL }}favicon.ico"/>
    <script src="{{ ASSET_URL }}plugins/jquery.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/jquery-migrate.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/bootstrap/js/bootstrap.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js{{ ADMIN_JS_VERSION }}"
            type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/jquery-slimscroll/jquery.slimscroll.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/jquery.blockui.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/jquery.cokie.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/uniform/jquery.uniform.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/bootstrap-switch/js/bootstrap-switch.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ ASSET_URL }}plugins/bootstrap-touchspin/bootstrap.touchspin.js{{ ADMIN_JS_VERSION }}"></script>


    <!-- END CORE PLUGINS -->
    <script type="text/javascript" src="{{ ASSET_URL }}plugins/jquery-validation/js/jquery.validate.min.js{{ ADMIN_JS_VERSION }}"></script>
    <script type="text/javascript" src="{{ ASSET_URL }}plugins/jquery-validation/js/additional-methods.js{{ ADMIN_JS_VERSION }}"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ ASSET_URL }}plugins/datatables/media/js/jquery.dataTables.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js{{ ADMIN_JS_VERSION }}"
            type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js{{ ADMIN_JS_VERSION }}"
            type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js{{ ADMIN_JS_VERSION }}"
            type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js{{ ADMIN_JS_VERSION }}"
            type="text/javascript"></script>
    <script src="{{ SITE_URL }}js/datatable.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ SITE_URL }}js/table-ajax.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/bootbox/bootbox.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/typeahead/handlebars.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/typeahead/typeahead.bundle.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/bootstrap-tour/build/js/bootstrap-tour-standalone.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>

    <script type="text/javascript" src="{{ ASSET_URL }}plugins/bootstrap-fileinput/bootstrap-fileinput.js{{ ADMIN_JS_VERSION }}"></script>
    <script type="text/javascript" src="{{ ASSET_URL }}plugins/bootstrap-select/bootstrap-select.min.js{{ ADMIN_JS_VERSION }}"></script>
    <script type="text/javascript"
            src="{{ ASSET_URL }}plugins/bootstrap-datepicker/js/bootstrap-datepicker.js{{ ADMIN_JS_VERSION }}"></script>
    <script type="text/javascript"
            src="{{ ASSET_URL }}plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js{{ ADMIN_JS_VERSION }}"></script>
    <script type="text/javascript" src="{{ ASSET_URL }}plugins/select2/select2.min.js{{ ADMIN_JS_VERSION }}"></script>
    <script type="text/javascript" src="{{ ASSET_URL }}scripts/metronic.js{{ ADMIN_JS_VERSION }}"></script>
    <script type="text/javascript" src="{{ ASSET_URL }}scripts/layout.js{{ ADMIN_JS_VERSION }}"></script>

    <script type="text/javascript" src="{{ ASSET_URL }}plugins/jcrop/js/jquery.Jcrop.min.js{{ ADMIN_JS_VERSION }}"></script>
    <script src="{{ ASSET_URL }}plugins/bootstrap-switch/js/bootstrap-switch.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}scripts/quick-sidebar.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}scripts/demo.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}scripts/components-form-tools.js{{ ADMIN_JS_VERSION }}"></script>
    <script src="{{ ASSET_URL }}scripts/index.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}scripts/tasks.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}scripts/form-image-crop.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>


    <script type="text/javascript" src="{{ ASSET_URL }}plugins/ckeditor/ckeditor.js{{ ADMIN_JS_VERSION }}"></script>
    <script type="text/javascript">CKEDITOR.env.isCompatible = true;</script>
    <script type="text/javascript" src="{{ ASSET_URL }}plugins/ckeditor/adapters/jquery.js{{ ADMIN_JS_VERSION }}"></script>
    <script src="{{SITE_URL}}js/custom_script.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{SITE_URL}}js/custom_validation.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/jcrop/js/jquery.color.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{ ASSET_URL }}plugins/jcrop/js/jquery.Jcrop.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    <script src="{{SITE_URL}}js/image_upload.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
    



    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <style>
        textarea:focus::-webkit-input-placeholder, input:focus::-webkit-input-placeholder {
            opacity: 0;
        }

        /* Mozilla Firefox 4 to 18 */
        input:focus:-moz-placeholder {
            opacity: 0;
        }

        /* Mozilla Firefox 19+ */
        input:focus::-moz-placeholder {
            opacity: 0;
        }

        /* Internet Explorer 10+ */
        input:focus:-ms-input-placeholder {
            opacity: 0;
        }

        textarea::-webkit-input-placeholder, input::-webkit-input-placeholder {
            color: #888;
        }

        input:-moz-placeholder {
            color: #888;
        }

        input::-moz-placeholder {
            color: #888;
        }

        input:-ms-input-placeholder {
            color: #888;
        }
    </style>


</head>
<body class="page-header-menu-fixed">
@include('admin.elements.header')
        <!-- BEGIN CONTAINER -->
<div class="page-container">
    
    @yield('content')
</div>
@include('admin.elements.footer')

        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
    var ADMIN_URL = "<?php echo ADMIN_URL; ?>";
    var ADMIN_PATH = "<?php echo ADMIN_PATH; ?>";
    var SITE_URL = "<?php echo SITE_URL; ?>";
    var DOC_ROOT = "<?php echo WWW_ROOT; ?>";

    var PROFILE_IMAGE_URL = "{{ USER_PROFILE_IMAGE_PATH }}";
    var PROFILE_THUMB_IMAGE_URL = "{{ USER_PROFILE_THUMB_IMAGE_PATH }}";

    $(document).ready(function () {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        Demo.init(); // init demo(theme settings page)
        Index.init(); // init index page
        Tasks.initDashboardWidget(); // init tash dashboard widget
        setTimeout(function () {
            $('.make-switch').bootstrapSwitch.defaults.size = 'large';
            $('.make-switch').bootstrapSwitch();
            $("input[type='checkbox']").uniform();
        }, 1000);
    });

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>