<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.0
Version: 3.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>{{ $title }} | {{ SITE_NAME }}</title>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="{{ ASSET_URL }}plugins/font-awesome/css/font-awesome.min.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
<link href="{{ ASSET_URL }}plugins/simple-line-icons/simple-line-icons.min.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
<link href="{{ ASSET_URL }}plugins/bootstrap/css/bootstrap.min.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
<link href="{{ ASSET_URL }}plugins/uniform/css/uniform.default.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
<link href="{{ ASSET_URL }}plugins/bootstrap-switch/css/bootstrap-switch.min.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
<link href="{{ ASSET_URL }}css/image-crop.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
<link href="{{ ASSET_URL }}plugins/jcrop/css/jquery.Jcrop.min.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<link href="{{ ASSET_URL }}css/lock.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
<!-- BEGIN THEME STYLES -->
<link href="{{ ASSET_URL }}css/components.css{{ ADMIN_CSS_VERSION }}" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{ ASSET_URL }}css/plugins.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
<link href="{{ ASSET_URL }}css/layout.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
<link id="style_color" href="{{ ASSET_URL }}css/themes/default.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
<link href="{{ ASSET_URL }}css/custom.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
<link href="{{ASSET_URL}}css/style_main.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
<link href="{{ ASSET_URL }}css/login3.css{{ ADMIN_CSS_VERSION }}" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="{{ ASSET_URL }}favicon.ico"/>

    <style>
        textarea:focus::-webkit-input-placeholder, input:focus::-webkit-input-placeholder  { opacity: 0;}
        /* Mozilla Firefox 4 to 18 */
        input:focus:-moz-placeholder {  opacity: 0;}
        /* Mozilla Firefox 19+ */
        input:focus::-moz-placeholder { opacity: 0; }
        /* Internet Explorer 10+ */
        input:focus:-ms-input-placeholder { opacity: 0;}
        textarea::-webkit-input-placeholder, input::-webkit-input-placeholder { color: #888;}
        input:-moz-placeholder { color: #888; }
        input::-moz-placeholder { color:#888; }
        input:-ms-input-placeholder { color: #888;}
    </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-menu-fixed login">
<!-- BEGIN LOGO -->
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="plugins/respond.min.js"></script>
<script src="plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="{{ ASSET_URL }}plugins/jquery.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script src="{{ ASSET_URL }}plugins/jquery-migrate.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{ ASSET_URL }}plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script src="{{ ASSET_URL }}plugins/bootstrap/js/bootstrap.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script src="{{ ASSET_URL }}plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script src="{{ ASSET_URL }}plugins/jquery-slimscroll/jquery.slimscroll.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script src="{{ ASSET_URL }}plugins/jquery.blockui.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script src="{{ ASSET_URL }}plugins/jquery.cokie.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script src="{{ ASSET_URL }}plugins/uniform/jquery.uniform.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script src="{{ ASSET_URL }}plugins/bootstrap-switch/js/bootstrap-switch.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="{{ ASSET_URL }}scripts/metronic.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script src="{{ ASSET_URL }}scripts/layout.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script src="{{ ASSET_URL }}scripts/demo.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script src="{{ ASSET_URL }}plugins/backstretch/jquery.backstretch.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script src="{{ ASSET_URL }}plugins/jquery-validation/js/jquery.validate.min.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ ASSET_URL }}plugins/select2/select2.min.js{{ ADMIN_JS_VERSION }}"></script>
<script src="{{ ASSET_URL }}scripts/login.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ ASSET_URL }}plugins/jcrop/js/jquery.Jcrop.min.js{{ ADMIN_JS_VERSION }}"></script>
<script src="{{ ASSET_URL }}scripts/form-image-crop.js{{ ADMIN_JS_VERSION }}" type="text/javascript"></script>
<script type="text/javascript" src="{{SITE_URL}}js/validate.js{{ ADMIN_JS_VERSION }}"></script>
  
@yield('content')
<script type="text/javascript">
var ADMIN_URL = "<?php echo ADMIN_URL; ?>";
        $( document ).ready( function () {
            $("input[type='checkbox']").uniform(); 
            Metronic.init();
        });
        $(window).load(function(){
            $('.page-spinner-bar').addClass('hide');
        });
    </script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>