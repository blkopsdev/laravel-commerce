@extends('admin.layouts.default')
@section('content')
<style>
    .error-inner{color: red;}
    .duplicate-error{color: red;}
</style>
<script type="text/javascript">
$(document).ready(function(){
    
    $user_id = {{$role_level_id}};
    get_user_role_data($user_id);
    function get_user_role_data(current_value){
        $selection = [];
        $.post(ADMIN_URL+'role_access/role_data/'+$user_id, function(data){
            $('.access_display').html('');
            $('#selection_list').val('');
            $('.access_role').hide();
            var checkbox = '';
            $newselection ={};
            setTimeout( function(){
                $.each(data.userAccess, function(index,val) {
                    if(data.userAccess[index]['e_add'] == '1'){
                        var id = data.userAccess[index]['i_role']+'_'+data.userAccess[index]['i_module_id']+'_Create';
                        $selection.push(id);
                        $newselection[id] = true;
                    }
                    if(data.userAccess[index]['e_edit'] == '1'){
                        var id = data.userAccess[index]['i_role']+'_'+data.userAccess[index]['i_module_id']+'_Update';
                        $selection.push(id);
                        $newselection[id] = true;
                    }
                    if(data.userAccess[index]['e_delete'] == '1'){
                       var id = data.userAccess[index]['i_role']+'_'+data.userAccess[index]['i_module_id']+'_Delete';
                        $selection.push(id);
                        $newselection[id] = true;
                    }
                    if(data.userAccess[index]['e_view'] == '1'){
                        var id = data.userAccess[index]['i_role']+'_'+data.userAccess[index]['i_module_id']+'_Read';
                        $selection.push(id);
                        $newselection[id] = true;
                    }
                    if(data.userAccess[index]['e_import'] == '1'){
                        var id = data.userAccess[index]['i_role']+'_'+data.userAccess[index]['i_module_id']+'_Import Data Feed';
                        $selection.push(id);
                        $newselection[id] = true;
                    }
                    if(data.userAccess[index]['e_export'] == '1'){
                        var id = data.userAccess[index]['i_role']+'_'+data.userAccess[index]['i_module_id']+'_Download';
                        $selection.push(id);
                        $newselection[id] = true;
                    }
                    
                });         
                data.module.forEach(function(mod,k){
                    checkbox += '<div class="form-group">\
                                    <label style="padding-top: 13px;" class="control-label col-md-2 col-xs-2">\
                                        <b>'+ mod.v_module_name+'</b>\
                                    </label>';
                        $.each(data.userAccessList, function(key,access) {
                            checkbox  += '<label class="control-label col-md-1 col-xs-2 mobile_right" style="text-align:center">\
                                                <div class="checkbox-list">\
                                                    <label class="checkbox-inline">';
                        
                            /*if((mod.id != 13 && access != 'Send'  && mod.id 
                                != 2 && mod.id 
                                != 3 && mod.id != 4 && mod.id != 19 &&  mod.id != 18 &&  mod.id != 17 &&  mod.id != 1) ||  (mod.id == 13 && access == 'Send')  || access == 'Read') {   
                                */
                            // if( access == 'Read' ||  (mod.id == 13 && access == 'Send') || ( access == 'Delete' && mod.id != 1 && mod.id != 2 && mod.id != 13 && mod.id != 3 && mod.id != 4  &&  mod.id != 17 && mod.id != 15 && mod.id != 12 && mod.id != 11) || ( access == 'Create/Update'   &&  mod.id != 1  && mod.id != 13 && mod.id!= 3 && mod.id != 4 &&  mod.id != 18 && mod.id != 19 && mod.id != 12) ||  ( access == 'Download Reports'   &&  mod.id != 1  && mod.id != 5 && mod.id != 6 && mod.id != 7 &&  mod.id != 8 &&  mod.id != 9 && mod.id != 10 && mod.id != 11 &&  mod.id != 12 &&  mod.id != 13 && mod.id != 14 && mod.id != 15)) {    
                                
                            //     if($newselection[$user_id+'_'+mod.id+'_'+access]){
                            //         checkbox  += '<input id="'+$user_id+'_'+mod.id+'_'+access+'" type="checkbox" name="selectedRole[]" value="'+$user_id+'_'+mod.id+'_'+access+'" onclick="toggleSelection('+$user_id+','+mod.id+',\''+access+'\')" checked/>'; 
                            //     }else{
                                if((mod.id == 1 && access == 'Import Data Feed') || access =='Create' || access =='Update' || access =='Delete' || access =='Download' || access =='Read'){
                                            if($newselection[$user_id+'_'+mod.id+'_'+access]){
                                                checkbox  += '<input id="'+$user_id+'_'+mod.id+'_'+access+'" type="checkbox" name="selectedRole[]" value="'+$user_id+'_'+mod.id+'_'+access+'" onclick="toggleSelection('+$user_id+','+mod.id+',\''+access+'\')" checked/>'; 
                                            }else{
                                                checkbox  += '<input id="'+$user_id+'_'+mod.id+'_'+access+'" type="checkbox" name="selectedRole[]" value="'+$user_id+'_'+mod.id+'_'+access+'" onclick="toggleSelection('+$user_id+','+mod.id+',\''+access+'\')"/>';
                                            }
                                }
                            //     }   
                                    
                            // }
                        
                            
                            checkbox  += '</label>\
                                            </div></label>'; 
                            
                        });
                    checkbox += '</div>';
                    
                })
                $('#selection_list').val($selection);
                $('.access_display').append(checkbox);
                $( ".access_role" ).fadeIn( "slow", function() { });
                setTimeout(function(){
                    $("input[type='checkbox']").uniform();    
                },0);               
            },100);
        });
        
        
        
    }
    $('#role_user_type').change(function(){
        if($('#role_user_type').val() != '') {
            
            $user_id = $('#role_user_type').val();
            get_user_role_data($user_id);
        }
    });

});
    
function toggleSelection(user_type,role,module) {
    console.log(module);
    var id =user_type+'_'+role+'_'+module;
    var idx = $selection.indexOf(id);
        if (idx > -1) {
            $selection.splice(idx, 1);
            $newselection[id]=false;
            if(module=='Delete'){
                $("#"+user_type+"_"+role+"_Create\\/Update").removeAttr('disabled');
                $("#"+user_type+"_"+role+"_Create\\/Update").parent('span').parent('div .checker').removeClass('disabled');
            }
            if(module=='Create'){
                
                $("#"+user_type+"_"+role+"_Read").removeAttr('disabled');
                $("#"+user_type+"_"+role+"_Read").parent('span').parent('div .checker').removeClass('disabled');
            }
            if(module=='Update'){
                
                $("#"+user_type+"_"+role+"_Read").removeAttr('disabled');
                $("#"+user_type+"_"+role+"_Read").parent('span').parent('div .checker').removeClass('disabled');
            }
            if(module=='Download'){
                
                $("#"+user_type+"_"+role+"_Read").removeAttr('disabled');
                $("#"+user_type+"_"+role+"_Read").parent('span').parent('div .checker').removeClass('disabled');
            }
            if(module=='Import Data Feed'){
                
                $("#"+user_type+"_"+role+"_Read").removeAttr('disabled');
                $("#"+user_type+"_"+role+"_Read").parent('span').parent('div .checker').removeClass('disabled');
            }
        }
        else {
            $selection.push(id);
            if(module=='Delete'){
                var modules='Read';
                var id =user_type+'_'+role+'_'+modules;
                $selection.push(id);
                $newselection[id]=true;
                
                var moduless='Create/Update';
                var id =user_type+'_'+role+'_'+moduless;
                $selection.push(id);
                $newselection[id]=true;
                setTimeout(function(){
                    $.uniform.update("input[type='checkbox']");  
                },0);
                $("#"+user_type+"_"+role+"_"+modules).attr('disabled',true);
                $("#"+user_type+"_"+role+"_"+modules).closest("span").addClass("checked");
                $("#"+user_type+"_"+role+"_"+modules).attr("checked",true);         
                
                $("#"+user_type+"_"+role+"_Create\\/Update").attr('disabled',true);
                $("#"+user_type+"_"+role+"_Create\\/Update").closest("span").addClass("checked"); 
                $("#"+user_type+"_"+role+"_Create\\/Update").attr("checked",true);
                
            }
          
            if(module=='Update'){
                var modules='Read';
                var id =user_type+'_'+role+'_'+modules;
                $selection.push(id);
                $newselection[id]=true;
                setTimeout(function(){
                    $.uniform.update("input[type='checkbox']");
                },0);
                $("#"+user_type+"_"+role+"_"+modules).attr('disabled',true);
                $("#"+user_type+"_"+role+"_"+modules).closest("span").addClass("checked");
                $("#"+user_type+"_"+role+"_"+modules).attr("checked",true);         
                
            }
            if(module=='Create'){
                var modules='Read';
                var id =user_type+'_'+role+'_'+modules;
                $selection.push(id);
                $newselection[id]=true;
                setTimeout(function(){
                    $.uniform.update("input[type='checkbox']");
                },0);
                $("#"+user_type+"_"+role+"_"+modules).attr('disabled',true);
                $("#"+user_type+"_"+role+"_"+modules).closest("span").addClass("checked");
                $("#"+user_type+"_"+role+"_"+modules).attr("checked",true);         
                
            }
            if(module=='Download'){
                var modules='Read';
                var id =user_type+'_'+role+'_'+modules;
                $selection.push(id);
                $newselection[id]=true;
                setTimeout(function(){
                    $.uniform.update("input[type='checkbox']");
                },0);
                $("#"+user_type+"_"+role+"_"+modules).attr('disabled',true);
                $("#"+user_type+"_"+role+"_"+modules).closest("span").addClass("checked");
                $("#"+user_type+"_"+role+"_"+modules).attr("checked",true);         
                
            }
                if(module=='Import Data Feed'){
                var modules='Read';
                var id =user_type+'_'+role+'_'+modules;
                $selection.push(id);
                $newselection[id]=true;
                setTimeout(function(){
                    $.uniform.update("input[type='checkbox']");
                },0);
                $("#"+user_type+"_"+role+"_"+modules).attr('disabled',true);
                $("#"+user_type+"_"+role+"_"+modules).closest("span").addClass("checked");
                $("#"+user_type+"_"+role+"_"+modules).attr("checked",true);         
                
            }
        }
        $('#selection_list').val($selection); 
        
}

</script>
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Role Management <small>Find and manage Role</small></h1>
        </div>        
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="portlet light" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green-sharp bold uppercase">
                                    <i class="fa fa-users">
                                    </i>
                                    Edit Role
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body form">
                <div class="col-md-12">
                    <div class="Metronic-alerts alert alert-danger fade in" style="margin-top:20px;display: none;">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                        <span class="message"></span>
                    </div>
                </div>
                <form role="form" method="post" class="form-horizontal role_form" id="frmEdit" name="frmEdit"  action="{{action('RoleController@anyEdit')}}"  redirect_url="{{action('RoleController@anyEdit')}}" >
                   
                      <input type="hidden" value="" id="redirect_url" value="{{action('RoleController@anyEdit')}}" name="redirect_url"/> 
                   <div class="form-wizard">
                     @if(Session::has('success-message'))
                          <div class="alert alert-success edit-success">
                            <span class="close" data-close="alert"></span>
                           {!!Session::get('success-message')!!}
                        </div>
                    @endif
                        <div class="form-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">
                                           User Type:
                                           <span class="redLabel">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-4">
                                            <select class="form-control input-icon required" id="role_user_type" name="role_user_type">
                                            
                                                @foreach($adminType as $key => $admin)
                                                    <option @if($role_level_id == $key) selected='selected' @endif value="{{$key}}">{{$admin}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                     </div>
                                    <div>
                                        <div class="access_role" style="display: none;">
                                            <div class="form-group mobile_hidden">
                                                <label class="control-label col-md-2 col-xs-2">
                                                    &nbsp;
                                                </label>
                                                @foreach($userAccessList as $value)
                                                    <label class="control-label col-md-1 col-xs-2" style="text-align: center;">
                                                        <b>
                                                            {{$value}}
                                                        </b>
                                                    </label>
                                                @endforeach
                                            </div>
                                            
                                            <div class="form-group access_display">
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <input type="hidden" name="selection_list" id="selection_list" value=""/>
                                </div>
                             </div>
                        </div>
                        <div class="form-actions right">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-9">
                                 <button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>
                                  <a href="{{ ADMIN_URL }}dashboard" class=" btn default button-previous">Cancel </a>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    </div>
</div>
@stop