<?php 
$userArr = Auth::guard('admin')->user();
if(!empty($userArr))
{
	$user_info=Session::get('USER_DATA');
	$mail_flag=Session::get('SETTING_DATA');
	if(!empty($user_info)) {	
	if($user_info->Client)
	{
		if(file_exists(CLIENT_ADD_IMG_PATH_URL.$user_info->Client->company_logo) && !empty($user_info->Client->company_logo)){
			$company_logo=LOGIN_IMAGE_PATH.$user_info->Client->company_logo;		
		}else{
			$company_logo=ASSET_URL.'img/default-user.png';	
		}
		
	}else {
		if($user_info->v_image=='')
		{
			$company_logo=ASSET_URL.'img/default-user.png';	
		}else
		{
			$company_logo=SITE_URL.ADMIN_ADD_THUMB_IMAGE_PATH.$user_info->v_image;	
		}
	}
	if(isset($UserPermission)){
		// dd($UserPermission);	
	}
	

?>
<!-- BEGIN HEADER -->
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<div class="page-header-top" style="text-align: center;">
		<div class="container-fluid">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="<?php echo ADMIN_URL.'dashboard'; ?>"><img src="{{SITE_URL}}img/logo.png" alt="Logo" class="logo-default" style="height: auto; vertical-align: middle; width: 250px; margin-top: 15px;"></a>
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler"></a>
			<span class="text-heading">{{($userArr['e_type']=="Super")?"Super Admin":$userArr['e_type']}} Control Panel</span>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<!-- <div class="top-menu-heading">			
				<div>
					Admin Control Panel
				</div>
			</div> -->
			<div class="top-menu">			
				<ul class="nav navbar-nav pull-right">
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown dropdown-user dropdown-dark">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<img alt="" class="img-circle" src="{{$company_logo}}">
							<span class="username username-hide-mobile" id="name">{{ $userArr['v_username'] }}</span>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="<?php echo ADMIN_URL.'my_profile'; ?>">
								<i class="icon-user"></i> My Profile </a>
							</li>
							<li>
								<a href="<?php echo ADMIN_URL.'logout'; ?>">
								<i class="icon-key"></i> Log Out </a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
	</div>
	<!-- END HEADER TOP -->
	<!-- BEGIN HEADER MENU -->
	<div class="page-header-menu">
		<div class="container-fluid">
			<!-- BEGIN HEADER SEARCH BOX -->
			<!--form class="search-form" action="extra_search.html" method="GET">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search" name="query">
					<span class="input-group-btn">
					<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
					</span>
				</div>
			</form-->
			<!-- END HEADER SEARCH BOX -->
			<!-- BEGIN MEGA MENU -->
			<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
			<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
			<div class="hor-menu ">
				<?php
				$data = getCurrentControllerAction();
				$explode_data = explode("||", $data);
				$curr_controller = $explode_data[0];
				$curr_action = $explode_data[1];				
				$userArr = Auth::guard('admin')->user();
				if(!empty($userArr))
				{
				?>
				<ul class="nav navbar-nav">
					<li class="<?php if($curr_controller == 'Authenticate' && $curr_action == 'dashboard') { echo 'active'; } ?>">
						<a href="{{ ADMIN_URL }}dashboard">
							<i class="icon-home"></i>
							<span class="title">Dashboard</span>
						</a>
					</li>
					<?php 
					if((isset($UserPermission["1"]["e_view"]) && $UserPermission["1"]["e_view"]  == 1) || $userArr['role']==1)
					{ 	
				    ?>
                    <li class="<?php if($curr_controller == 'Order' && ($curr_action == 'Index' || $curr_action == 'anyEdit' || $curr_action == 'anyAdd' )) { echo 'active'; } ?>">
						<a href="{{ ADMIN_URL }}order">
							<i class="fa fa-history"></i>
							<span class="title">Order</span>
						</a>
					</li>
					<?php }?>
					<?php 
					if((isset($UserPermission["6"]["e_view"]) && $UserPermission["6"]["e_view"]  == 1) || $userArr['role']==1 || $userArr['role'] == 2)
					{ 	
						if($user_info->Client || $user_info->Admin || $user_info->DropshipEmployee || $user_info->Vendor || $user_info->Employee){
				    ?>
                    <li class="<?php if($curr_controller == 'PurchaseOrder' && ($curr_action == 'Index' || $curr_action == 'anyEdit' || $curr_action == 'anyAdd' || $curr_action == 'anyEditOrder' )) { echo 'active'; } ?>">
						<a href="{{ ADMIN_URL }}purchase_order">
							<i class="fa fa-history"></i>
							<span class="title">Purchase Order</span>
						</a>
					</li>
					<?php } } ?>
					<?php 
					if((isset($UserPermission["2"]["e_view"]) && $UserPermission["2"]["e_view"]  == 1) || $userArr['role']==1)
					{ 	
				    ?>
					<li class="<?php if($curr_controller == 'Client' && ($curr_action == 'Index' || $curr_action == 'anyEdit' || $curr_action == 'anyAdd' )) { echo 'active'; } ?>">
						<a href="{{ ADMIN_URL }}client">
							<i class="icon-users"></i>
							<span class="title">Client</span>
						</a>
					</li>
					<?php } ?>
					<?php 
					if((isset($UserPermission["3"]["e_view"]) && $UserPermission["3"]["e_view"]  == 1) || $userArr['role']==1)
					{ 	
				    ?>
                    <li class="<?php if($curr_controller == 'Vendor' && ($curr_action == 'Index' || $curr_action == 'anyEdit' || $curr_action == 'anyAdd' )) { echo 'active'; } ?>">
						<a href="{{ ADMIN_URL }}vendor">
							<i class="icon-users"></i>
							<span class="title">Vendor</span>
						</a>
					</li>
					<?php } ?>
					<?php 
					if((isset($UserPermission["4"]["e_view"]) && $UserPermission["4"]["e_view"]  == 1) || $userArr['role']==1)
					{ 	
				    ?>
					<li class="<?php if($curr_controller == 'Employee' && ($curr_action == 'Index' || $curr_action == 'anyEdit' || $curr_action == 'anyAdd' )) { echo 'active'; } ?>">
						<a href="{{ ADMIN_URL }}employee">
							<i class="icon-users"></i>
							<span class="title">Vendor Employee</span>
						</a>
					</li>
					<?php }?>
					<?php 
					if($userArr['role']==1)
					{ 	
				    ?>
					<li class="<?php if($curr_controller == 'DropshipEmployee' && ($curr_action == 'Index' || $curr_action == 'anyEdit' || $curr_action == 'anyAdd' )) { echo 'active'; } ?>">
						<a href="{{ ADMIN_URL }}dropship-employee">
							<i class="icon-users"></i>
							<span class="title">Dropship Employee</span>
						</a>
					</li>
					<?php }?>
					@if($userArr['role']==2)
					<li class="<?php if($curr_controller == 'Client' && ($curr_action == 'Index' || $curr_action == 'anyEdit' || $curr_action == 'anyAdd' )) { echo 'active'; } ?>">
						<a href="{{ ADMIN_URL }}client/cms/{{$userArr['i_client_id']}}">
							<i class="fa fa-list"></i>
							<span class="title">CMS</span>
						</a>
					</li>
					@endif
					<!-- <li class="<?php if($curr_controller == 'RoleManagement' && ($curr_action == 'Index' || $curr_action == 'anyEdit' || $curr_action == 'anyAdd' )) { echo 'active'; } ?>">
						<a href="{{ ADMIN_URL }}role_access">
							<i class="fa fa-history"></i>
							<span class="title">Role Management</span>
						</a>
					</li> -->
					@if($mail_flag->mail_template_flg!=0)
					<li class="<?php if($curr_controller == 'MailTemplate' && ($curr_action == 'Index' || $curr_action == 'anyEdit' || $curr_action == 'anyAdd' )) { echo 'active'; } ?>">
						<a href="{{ ADMIN_URL }}mail-template">
							<i class="fa fa-envelope"></i>
							<span class="title">Mail Template</span>
						</a>
					</li>
					@endif
				</ul>
				<?php
				}
				else
				{
					?>
					<?php
				}
				?>
			</div>
			<!-- END MEGA MENU -->
		</div>
	</div>
	<!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
<?php } } ?>