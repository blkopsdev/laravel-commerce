<div class="page-sidebar navbar-collapse collapse">
    <?php
    $data = getCurrentControllerAction();
    $explode_data = explode("||", $data);
    $curr_controller = $explode_data[0];
    $curr_action = $explode_data[1];
    $userArr = Auth::guard('admin')->user();
    if(!empty($userArr) && count($userArr) > 0)
    {
    ?>
    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
        <li class="sidebar-search-wrapper">
            <form class="sidebar-search sidebar-search-bordered" action="extra_search.html" method="POST">
                <a href="javascript:;" class="remove"><i class="icon-close"></i></a>
            </form>
        </li>
        <li class="<?php if($curr_controller == "Authenticate" && $curr_action == "dashboard") { echo 'active'; } ?>">
            <a  href="{{ ADMIN_URL }}dashboard">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
            </a>
        </li>
        <li>
            <a href="{{ ADMIN_URL }}logout">
                <i class="icon-users"></i>
                <span class="title">Logout</span>
            </a>
        </li>
    </ul>
</div>
<?php
}
else
{
    ?>
    <?php
}
?>
