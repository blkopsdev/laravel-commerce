<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="container-fluid">
		 {{ date('Y') }} &copy; {{ SITE_NAME }}.&nbsp;All Rights Reserved.
	</div>
</div>
<div class="scroll-to-top">
	<i class="icon-arrow-up"></i>
</div>