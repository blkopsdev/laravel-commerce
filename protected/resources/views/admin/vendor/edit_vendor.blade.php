@extends('admin.layouts.default')
@section('content')
<script src="{{ SITE_URL }}js/admin/jquery.passMeter.js"></script>
<style>
    .error-inner{color: red;}
    .duplicate-error{color: red;}
</style>
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Vendor <small>Find and manage vendors</small></h1>
        </div>        
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="portlet light" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green-sharp bold uppercase">
                                    <i class="fa fa-users">
                                    </i>
                                    Edit Vendor
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body form">                            
                            <form role="form" method="post" class="form-horizontal" id="frmEdit" name="frmEdit" onsubmit="return false;" action="{{ADMIN_URL}}vendor/edit/{{$records->id}}" enctype="multipart/form-data">
                                <div class="form-wizard">
									<div class="form-body">
										<div class="tab-content">
											<div class="alert alert-danger display-none">
												<button class="close">
												</button>
												You have some form errors. Please check below.
											</div>
                                            <?php $msg = Session::get('msg'); if($msg){ ?>
											<div class="alert alert-danger">
												<button class="close"></button>
												{{ $msg }}
											</div>
                                            <?php } ?>
											<div class="tab-pane active" id="tab1">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        User Name
                                                        <span class="required">
                                                            *
                                                        </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_username" name="v_username" class="form-control required" placeholder="User Name" value="{{ (isset($records->admin->v_username))?$records->admin->v_username:'' }}"/>
                                                        <div id="error_v_username" class="duplicate-error " style="display: none;">Username already exits.</div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Password
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="password" name="password" id="password" class="form-control validate_password" placeholder="Password"/>
                                                    </div>
                                                </div>
                                                @if($auth_user->e_type != 'Client')
                                                <div class="form-group">
													<label class="control-label col-md-2">
														Client 
														<span class="required">
															*
														</span>
													</label>
													<div class="col-md-3">
                                                            <select placeholder="client" class="form-control input-icon required" name="i_client_id">
                                                            <option value="">-- Select --</option>
                                                            @foreach($client_list as $client)
                                                                @if($records->i_client_id == $client->id)
                                                                <option value="{{ $client->id }}" selected="selected">{{ $client->v_company }}</option>
                                                                @else
                                                                    <option value="{{ $client->id }}">{{ $client->v_company }}</option>
                                                                @endif    
                                                            @endforeach
                                                        </select>
                                                        
													</div>
												</div>
                                                @endif
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Vendor Name
                                                        <span class="required">
                                                            *
                                                        </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_vendor_name" name="v_vendor_name" class="form-control required" placeholder="Vendor Name"  value="{{ $records->v_vendor_name }}"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Company Name
                                                        <span class="required">
                                                            *
                                                        </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_company_name" name="v_company_name" class="form-control required" placeholder="Company Name" value="{{ $records->v_company_name}}" />
                                                    </div>
                                                </div>
                                                <div class="form-group order_email_div">
                                                    <label class="control-label col-md-2">
                                                        Receives Dropship Order Email                                                        
                                                    </label>
                                                    <?php
                                                    $order_email = explode(',',$records->order_email);                    
                                                    $i = 0;
                                                    ?>
                                                    @foreach($order_email as $val)
                                                    <div class="col-md-3">
                                                        <input type="text"  name="order_email[]" class="form-control email" placeholder="Receives Dropship Order Email" value="{{ $val }}" />
                                                    </div>
                                                    <?php $i++; ?>
                                                    @endforeach
                                                    @if(count($order_email) < 3)
                                                        @for($j=$i;$j<3;$j++)
                                                            <div class="col-md-3">
                                                                <input type="text"  name="order_email[]" class="form-control email" placeholder="Receives Dropship Order Email" value="" />
                                                            </div>
                                                        @endfor
                                                    @endif
                                                </div>
                                                <div class="form-group po_email_div">
                                                    <label class="control-label col-md-2">
                                                        Receives PO Email                                                        
                                                    </label>
                                                    <?php
                                                    $po_email = explode(',',$records->po_email);                          $i = 0;               
                                                    ?>
                                                    @foreach($po_email as $val)
                                                    <div class="col-md-3">
                                                        <input type="text"  name="po_email[]" class="form-control email" placeholder="Receives PO Email" value="{{$val}}" />
                                                    </div>
                                                    <?php $i++; ?>
                                                    @endforeach
                                                    @if(count($po_email) < 3)
                                                        @for($j=$i;$j<3;$j++)
                                                            <div class="col-md-3">
                                                                <input type="text"  name="po_email[]" class="form-control email" placeholder="Receives PO Email" value="" />
                                                            </div>
                                                        @endfor
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Email
                                                        <span class="required">
                                                            *
                                                        </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_email" name="v_email" class="form-control email required" placeholder="Email Id" value="{{ $records->v_email }}" />
                                                        <div id="error_v_email" class="duplicate-error " style="display: none;">Email already exits.</div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Vendor ID   
                                                        <span class="required">
                                                            *
                                                        </span>                                                     
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_vendor_id" name="v_vendor_id" class="form-control required" placeholder="Vendor Id" value="{{ $records->v_vendor_id }}"/>
                                                        <div id="error_v_vendor_id" class="duplicate-error " style="display: none;">Vendor id already exits.</div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Phone
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_phone_number" name="v_phone_number" class="form-control" placeholder="Phone Number" value="{{ $records->v_phone_number}}" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Fax
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_fax" name="v_fax" class="form-control" placeholder="Fax" value="{{ $records->v_fax }}" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        URL
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="v_url" name="v_url" class="form-control url" placeholder="URL" value="{{ $records->v_url }}" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Status
                                                        <span class="required">
                                                            *
                                                        </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <select class="form-control input-icon required" name="e_status" placeholder="status">
                                                            <option value="Active"  @if($records->e_status == 'Active'){{ 'selected' }}@endif>Active</option>
                                                            <option value="Inactive" @if($records->e_status == 'Inactive'){{ 'selected' }}@endif>Inactive</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">
                                                        Note
                                                    </label>
                                                    <div class="col-md-3">
                                                        <textarea id="l_note" name="l_note" class="form-control" placeholder="Note">{{ $records->l_note }}</textarea> 
                                                    </div>
                                                    
                                                </div>
											</div>
										</div>
                                    </div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-2 col-md-8">
                                                <button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>
                                                <a href="{{ ADMIN_URL }}vendor" class=" btn default button-previous">Cancel </a>						
											</div>
										</div>
									</div>
								</div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    </div>
    <script>
$(document).ready(function(){    
    $.passMeter({
        // configure the divs
        'id_password'   :   '#password',

        // customize the message levels
        'bad'   :   'Bad',
        'low'   :   'Low',
        'good'  :   'Good',
        'strong'    :   'Strong'
    }); 
       var im = '<?php echo $records->company_logo; ?>';
            //$('#error_msg').val(im);
            if(im != ''){
                $('#up_img').hide();
                $('#change_img').show();
                $('#rmv_img').show();
            } else {
                $('#up_img').show();
                $('#change_img').hide();
                $('#rmv_img').hide();
                $('#image_val1').removeClass('thumbnail');
            }
            $('#rmv_img').on('click',function(){
                $('#edit_img').hide();
                $('#up_img').show();
                $('#change_img').hide();
                $('#rmv_img').hide();
            });
            $('#up_img').on('click',function(){
                $('#up_img').hide();
                $('#change_img').show();
                $('#rmv_img').show();
            });
            $('#image_upload').on('change',function(){
                var t= $('#image_upload').val();                
                if(t!= ''){
                    $('#up_img').hide();
                    $('#change_img').show();
                    $('#rmv_img').show();
                    //$('#error_msg').hide();
                    //$('#error_msg_error').hide();
                }
                $("#img_div").attr('style',"display:block");
            });
});
</script>
</div>
@stop