@extends('admin.layouts.default')
@section('content')
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Vendors <small>Find and manage Vendors</small></h1>
        </div>
       
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
    <!--div class="page-content-wrapper"-->
        <div class="page-content">
            <input type="hidden" id="del_url" name="del_url" value="<?php echo ADMIN_URL.'vendor/delete/'; ?>" />
            <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
               	    <div class="portlet light">
					   <div class="portlet-title">
                          <div class="caption"><i class="fa fa-cogs font-green-sharp"></i>
							<span class="caption-subject font-green-sharp bold uppercase">Manage Vendors</span></div>
                          <div class="actions">
                            <a class="btn blue-madison btn-circle" href="{{ ADMIN_URL }}vendor/add">
                                <i class="fa fa-plus"></i>
                                <span class="hidden-480">Add New Vendor</span>
                            </a>
                             <?php 
                           $user = Auth::guard('admin')->user();
                           ?>
                            @if($user->role==1 || (isset($UserPermission["3"]["e_export"]) && $UserPermission["3"]["e_export"]  == 1))
                                <a href="javascript:;" class="btn blue-madison btn-circle" id="export_to_excel" action-url="<?php echo
                                ADMIN_URL . 'vendor/export-to-excel'; ?>"> <i class="fa fa-share"></i> <span class="hidden-480">Export to Excel</span> </a>
                            @endif
                        </div>
					</div>                    
                <div class="portlet-body">
                    <div class="table-container ">
                     <div class="Metronic-alerts alert alert-success fade in display-hide" id="success-msg">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <i class="fa-lg fa fa-check "></i> 
                                <span class="message"></span>
                        </div>
                        @if(Session::has('success-message'))                    
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <i class="fa-lg fa fa-check "></i> 
                                <span class="message">{!!Session::get('success-message')!!}</span>
                            </div>
                        @endif
                        @if(Session::has('alert-message'))
                            <div class="Metronic-alerts alert alert-danger fade in">
                                <button type="button" class="close" aria-hidden="true" data-dismiss="alert"></button>
                                <span class="message">{!!Session::get('alert-message')!!}</span>
                            </div>
                        @endif
                            <div class="table-actions-wrapper">
                                <span></span>
    							<select class="table-group-action-input form-control input-inline input-small input-sm">
    								<option value="">Select...</option>
                                    <option value="Active">Active</option>
                                    <option value="Inactive">Inactive</option>
    								<option value="Delete">Delete</option>										
    							</select>
    							<button class="btn btn-sm blue-madison table-group-action-submit btn-circle" id="bulk_action"><i class="fa fa-check"></i> Submit</button>
                                <input type="hidden"  class="table-group-action-url" value="<?php echo ADMIN_URL.'vendor/bulk-action';?>"/>
    					   </div>
                            <table class="table table-striped table-hover table-bordered" id="datatable_ajax"> <!-- editable_resources_filter -->
                                <thead>                
                                    <tr role="row" class="heading">
                                        <th width="2%" style="padding-right: 0px; background-image: none ! important;" class="actions no-sort"><input type="checkbox" class="group-checkable"/></th>
                                        @if($user->e_type != 'Client')
                                        <th>
                                            Client Name
                                        </th>
                                        @endif
                                        <th>
                                            Vendor ID
                                        </th>
                                        <th>
                                            Vendor Name
                                        </th>
                                        <th>
                                            Company Name
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        
                                        <th>
                                            Phone Number
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                        <th style="padding-right: 0px; background-image: none ! important align:right;" class="actions no-sort">
                                             Actions
                                        </th>
                                    </tr>
                                    <tr role="row" class="filter">
                                        <td rowspan="1" colspan="1">
            						     </td>
                                         @if($user->e_type != 'Client')
                                            <td rowspan="1" colspan="1">
                                                <input type="text" name="v_company" class="form-control form-filter input-sm"/>
                                            </td>  
                                         @endif  
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="v_vendor_id" class="form-control form-filter input-sm"/>
                                         </td>
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="v_vendor_name" class="form-control form-filter input-sm"/>
                                         </td>
                                          <td rowspan="1" colspan="1">
                                            <input type="text" name="v_company_name" class="form-control form-filter input-sm"/>
                                         </td>
                                          <td rowspan="1" colspan="1">
                                            <input type="text" name="v_email" class="form-control form-filter input-sm"/>
                                         </td>
                                          
                                         <td rowspan="1" colspan="1">
                                            <input type="text" name="v_phone_number" class="form-control form-filter input-sm"/>
                                         </td>
                                         <td rowspan="1" colspan="1">
                                            <select class="form-control form-filter input-sm" name="e_status" >
                                                <option value="">Select...</option>
                                                <option value="Active">Active</option>
                                                <option value="Inactive">Inactive</option>
                                            </select>
                                         </td>
                                         <td rowspan="1" class="actions" colspan="1">
                                            <div class="margin-bottom-5">
            									<button class="btn btn-sm blue-madison filter-submit margin-bottom btn-circle"><i class="fa fa-search"></i> Search</button>
                                                <button class="btn btn-sm default filter-cancel btn-circle"><i class="fa fa-times"></i> Reset</button>
            								</div>
            			                 </td>
                                    </tr>                                
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div> 
                    </div>
                </div>
            </div>
        </div>
        </div>
    <!--/div -->   
<script>
    $(document).ready(function(){        
        var url = ADMIN_URL+'vendor/list-ajax';
        TableAjax.init(url);   
    });
</script>
@stop