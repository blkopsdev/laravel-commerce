@extends('admin.layouts.default')
@section('content')
<script src="{{ SITE_URL }}js/admin/jquery.passMeter.js"></script>
<style>
    .error-inner{color: red;}
    .duplicate-error{color: red;}
</style>
<div class="page-container">
    <div class="page-head">
    <div class="container-fluid">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Employee <small>Find and manage employee</small></h1>
        </div>        
        <!-- END PAGE TITLE -->
    </div>
</div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="portlet light" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green-sharp bold uppercase">
                                    <i class="fa fa-users">
                                    </i>
                                    Edit Employee
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body form">                            
                            <form role="form" method="post" class="horizontal-form" id="frmEdit" name="frmEdit" onsubmit="return false;" action="{{ADMIN_URL}}employee/edit/{{$records->id}}" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <h3 class="form-section">Person Details</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Username
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_username" name="v_username" class="form-control required" placeholder="Username" value="{{$records->admin->v_username}}">
                                                            <div id="error_v_username" class="duplicate-error " style="display: none;">Username already exits.</div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Password
                                                            </label>
                                                            <input type="password" name="password" id="password" class="form-control validate_password main_password" placeholder="Password">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                 <?php 
                                               $user = Auth::guard('admin')->user();
                                               ?>
                                                @if($user->role!=3 || $flag==1)
                                                <div class="row">
                                                <div class="col-md-6">
                                                         <div class="form-group">
                                                            <label class="control-label">Vendor Company
                                                            @if($flag!=1)
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            @endif
                                                            </label>
                                                                <select placeholder ="Vendor" class="form-control input-icon <?php echo ($flag!=1)?'required':'' ?>" name="i_vendor_id">
                                                                    <option value="">-- Select --</option>
                                                                    @foreach($vendor_list as $vendor)
                                                                        <option value="{{ $vendor->id }}" {{($records->i_vendor_id==$vendor->id)?"selected=selected":""}}>{{ $vendor->v_vendor_name }}</option>
                                                                    @endforeach
                                                                </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"> Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_vname" name="v_vname" class="form-control required" placeholder="Name" value="{{$records->v_vname}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                 <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Email
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_email" name="v_email" class="form-control email required" placeholder="Email" value="{{$records->v_email}}">
                                                            <div id="error_v_email" class="duplicate-error " style="display: none;">Email already exits.</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Phone
                                                            </label>
                                                            <input type="text" id="v_phone" name="v_phone" class="form-control" placeholder="Phone" value="{{$records->v_phone}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Fax
                                                            </label>
                                                            <input type="text" id="v_fax" name="v_fax" class="form-control " placeholder="Fax" value="<?php echo ($records->v_fax!='')?$records->v_fax:''; ?>">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">URL
                                                            </label>
                                                            <input type="text" id="v_url" name="v_url" class="form-control url"  placeholder= "URL" value="<?php echo ($records->v_url!='')?$records->v_url:''; ?>">
                                                             <!-- <input name="txtcompany" type="hidden" class="textboxcss" id="txtcompany" value="{{ (!empty(count($records->Vendor)>0))? $records->Vendor[0]['id']:$vendor_list[0]['id'] }}" size="40" maxlength="50"> --> 
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Status
                                                                <span class="required">
                                                                    *
                                                                </span>
                                                            </label>
                                                            <select class="form-control input-icon required" name="e_status" placeholder="status">
                                                            <option value="Active"  @if($records->e_status == 'Active'){{ 'selected' }}@endif>Active</option>
                                                            <option value="Inactive" @if($records->e_status == 'Inactive'){{ 'selected' }}@endif>Inactive</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Note
                                                            </label>
                                                             <textarea id="v_note" name="v_note" class="form-control" placeholder="Note"><?php echo ($records->v_note!='')?$records->v_note:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                 @else
                                                    <input type="hidden" id="i_vendor_id" name="i_vendor_id" class="form-control" value="{{$user->i_vendor_id}}" placeholder="Vendor Name">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"> Name
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_vname" name="v_vname" class="form-control required" placeholder="Name" value="{{$records->v_vname}}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Email
                                                            <span class="redLabel">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input type="text" id="v_email" name="v_email" class="form-control email required" placeholder="Email" value="{{$records->v_email}}">
                                                            <div id="error_v_email" class="duplicate-error " style="display: none;">Email already exits.</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                 
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Phone
                                                            </label>
                                                            <input type="text" id="v_phone" name="v_phone" class="form-control" placeholder="Phone" value="{{$records->v_phone}}">
                                                        </div>
                                                    </div>
                                                     <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Fax
                                                            </label>
                                                            <input type="text" id="v_fax" name="v_fax" class="form-control " placeholder="Fax" value="<?php echo ($records->v_fax!='')?$records->v_fax:''; ?>">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                   
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">URL
                                                            </label>
                                                            <input type="text" id="v_url" name="v_url" class="form-control url" placeholder="URL" value="<?php echo ($records->v_url!='')?$records->v_url:''; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Status
                                                                <span class="required">
                                                                    *
                                                                </span>
                                                            </label>
                                                            <select class="form-control input-icon required" name="e_status" placeholder="status">
                                                            <option value="Active"  @if($records->e_status == 'Active'){{ 'selected' }}@endif>Active</option>
                                                            <option value="Inactive" @if($records->e_status == 'Inactive'){{ 'selected' }}@endif>Inactive</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <div class="row">
                                                     <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Note
                                                            </label>
                                                             <textarea id="v_note" name="v_note" class="form-control" placeholder="Note"><?php echo ($records->v_note!='')?$records->v_note:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                            @endif
                                            </div>
                                            <div class="form-actions right">
                                               <button type="submit" class="btn blue-madison button-next">Save  <i class="fa fa-check-square-o "></i></button>    
                                    <a href="{{ ADMIN_URL }}employee" class=" btn default button-previous">Cancel </a>
                                            </div>
                                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    </div>
    <script>
$(document).ready(function(){    
    $.passMeter({
        // configure the divs
        'id_password'   :   '#password',

        // customize the message levels
        'bad'   :   'Bad',
        'low'   :   'Low',
        'good'  :   'Good',
        'strong'    :   'Strong'
    });
      $('.add_image').on('change',function(){
         console.log($(this));
         var t= $('input[name="add_image"]').val();
         $('#error_msg').val(t);
         if(t!= ''){
             $('#error_msg').hide();
             $('.error_msg_error').hide();
         }
     });
});
</script>
</div>
@stop