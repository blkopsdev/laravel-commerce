<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="//use.typekit.net/jsm6bbw.js"></script><script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<title></title>
<style type="text/css">
body {
	background: none repeat scroll 0 0 #EDEFF0;
    font-family: "myriad-pro",sans-serif;
    margin: 0;
    padding: 0;
}
.clear{ clear:both;}
img{ border:none;}</style>
<table bgcolor="#edeff0" border="0" cellpadding="0" cellspacing="0" style="padding: 50px 0 0 0;" width="100%">
	<tbody>
		<tr>
			<td>
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
					<tbody>
						<tr>
							<td>
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tbody>
										<tr>
											<td style="background:#FFFFFF; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;">
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tbody>
														<tr>
															<td style="background:#333333;border-bottom:#333333 solid 1px; padding:20px 28px">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td>
																			<a href="{{ SITE_URL }}"><img alt="IMAGE" src="{{ SITE_URL }}img/logo.png" style="width: 530px; height: 76px;" /></a></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td style="padding:28px;">
																<p style="font-size:15px; color:#555555; line-height:23px; margin:0; padding:0 0 18px 0;">
																	Hey {{ $full_name }},<br />
																	<br />
																	A password reset request was submitted for your profile on <a href="{{ SITE_URL }}" style=" color:#349cc6; font-size:16px; font-weight:700;">{{ SITE_NAME }}</a>. Please click the link below in order to reset your password.</p>
																<p style="font-size:15px; color:#555555; line-height:23px; margin:0; padding:0 0 18px 0;">
																	<a href="{{ $link }}" style=" color:#349cc6; font-size:16px; font-weight:700;">{{$link }}</a></p>
																<p style="font-size:15px; color:#555555; line-height:23px; margin:0; padding:0 0 18px 0;">
																	If clicking the link above does not work, please copy and paste the URL into your browser instead.</p>
																<p style="font-size:15px; color:#555555; line-height:23px; margin:0; padding:0 0 18px 0;">
																	If you did not make this request, you can ignore this message and your password will remain the same.</p>
																<p style="font-size:15px; color:#555555; line-height:23px; margin:0; padding:0 0 18px 0;">
																	<strong>Thank you,<br />
																	{{ SITE_NAME }}</strong></p>
															</td>
														<tr>
											<td style="padding:20px 0; text-align:center;">
												<p style="font-size:14px; color:#8d9aa5; margin:0; padding:0;">
													<strong>{{ date('Y') }} &copy; {{ SITE_NAME }}. All Rights Reserved.</strong></p>
												<ul style="margin:0 0 5px 0; padding:0;">
													<li style="margin:0; padding:0 7px 0 0; list-style:none; color:#8d9aa5; font-size:12px; display:inline-block; *display:inline; zoom:1; /*border-right:#aab3bb solid 1px;*/ line-height:1;">
													<!--<li style="margin:0; padding:0 0 0 7px; list-style:none; color:#8d9aa5; font-size:12px; display:inline-block; *display:inline; zoom:1; line-height:1;">
														Unsubscribe</li>-->
												</ul>
												<div class="clear">
													&nbsp;</div>
												</td>
										</tr>
                                                        
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<br />
</html>