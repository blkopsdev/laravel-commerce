<?php
use Illuminate\Support\Str;
function pr($data){
    echo '<pre>';
    print_r($data);
}
function getCurrentControllerAction() {
    $routeArray = Str::parseCallback(Route::currentRouteAction(), null);
    $controller = '';
    $action = '';
    if (last($routeArray) != null) {
        // Remove 'controller' from the controller name.
        $controller = str_replace('Controller', '', class_basename(head($routeArray)));
         
        // Take out the method from the action.
        $action = str_replace(['get', 'post', 'patch', 'put', 'delete'], '', last($routeArray));
        return $controller . '||' . $action;
    }
 
    return '';
}


define('JS_VERSION','?ver=v.1.3.4');
define('CSS_VERSION','?ver=v.1.3.4');
define('HTML_VERSION','?ver=v.1.3.3');
define('ADMIN_JS_VERSION','?ver=v.1.3.0');
define('ADMIN_CSS_VERSION','?ver=v.1.3.0');

$SITE_URL = url('')."/";
//$SITE_URL = "/";
define('LABEL_SITE_NAME','DropShipAdministrator');
define('SITE_NAME','Drop Ship Administrator');
define('PROJECT_NAME','Drop Ship Administrator');
define('BASE_PATH','/');
define('SITE_URL',$SITE_URL);
define('ASSET_URL',SITE_URL."assets/");
define('WWW_ROOT',$_SERVER['CONTEXT_DOCUMENT_ROOT'].BASE_PATH);
define('ADMIN_PATH',"admin");
define('ADMIN_URL', SITE_URL);
define('ADMIN_URL_LOGIN', SITE_URL);
define('REC_PER_PAGE','10');
/*define('TEMP_PATH','files/temp/');
define('TEMP_THUMB_PATH','files/temp/thumb/');
define('PROFILE_PATH','files/profile/');
define('PROFILE_THUMB_PATH','files/profile/thumb/');
define('COMPANY_LOGO_PATH','files/company/');*/
define('ERR_PWS','Invalid Emailid or Password.');
define('ERR_USERNAME_PWS','Please enter username or password for login.');
define('FORGOT_ERR_USERNAME_PWS','Please enter username for forgot password.');
define('FORGOT_ERR_EMAIL_PWS','Please enter email for forgot password.');
define('INVALID_EMAIL','Invalid email address or username.');
define('PWD_SENT','Reset password link has been successfully sent to your email address.');
define('PASSWORD_SUCCESS','Password has been set successfully');
define('PROFILE_SUCCESS','Your personal information is successfully updated!');
/*define('TEMP_IMG_PATH','files/bank/');
define('TEMP_SERVICE_VOUCHER_PATH','files/voucher_file/');
define('SERVICE_LOGO_IMG_PATH','files/service_logo/');

define('CLIENT_IMG_PATH','files/client/');*/

define('TEMP_ZIP_FILE_PATH','files/order/');

define('TEMP_IMG_PATH_URL','files/bank/thumb/');
define('HEADER_IMG_PATH_URL','files/header/');
define('HEADER_ADD_IMG_PATH_URL','files/header/uploads/');
define('USER_PROFILE_IMAGE_PATH','files/profile/');
define('USER_PROFILE_THUMB_IMAGE_PATH','files/profile/thumb/');
define('BANNER_IMG_PATH_URL','files/banner/');
define('INVALID_USERNAME_OR_PASSWORD','Email Id and Password do not match. Please try again!');

define('RECAPTCHA_SITE_KEY','6LcrRhATAAAAAJzOJteMwqAOr21pNyRJs93-2yeI');
define('RECAPTCHA_SECRET_KEY','6LcrRhATAAAAALMZsMLJYGRv7R-0XVyBnLONF9cQ');
define("VERIFY_RECAPTCHA_URL", "https://www.google.com/recaptcha/api/siteverify");
define('CLIENT_ADD_IMG_PATH_URL','files/client/');
define('ADMIN_ADD_IMG_PATH_URL','files/admin/');
define('ADMIN_ADD_THUMB_IMAGE_PATH','files/admin/thumb/');
define('VENDOR_ADD_IMG_PATH_URL','files/vendor/');
define('VENDOR_ADD_THUMB_IMAGE_PATH','files/vendor/thumb/');
define('DROPSHIP_EMPLOYEE_ADD_IMG_PATH_URL','files/dropship_employee/');
define('DROPSHIP_EMPLOYEE_ADD_THUMB_IMAGE_PATH','files/dropship_employee/thumb/');
// define('CLIENT_FOLDER_PATH','files/feed/');
define('CLIENT_FOLDER_PATH','feed/');
define('FEED_BACKUP_PATH','feed_backup_files/');
define('PURCHASE_FEED_PATH','purchase_order_feed/');
define('PURCHASE_FEED_BACKUP_PATH','purchase_order_feed/processed');
    /* Mail Credentials*/
define('EMAIL_LOGO_IMAGE_PATH',SITE_URL.'img/logo.png');
define('LOGIN_IMAGE_PATH',SITE_URL.'files/client/');
//define('TESTING_MAILID','cyle@blackforestdecor.com');
define('TESTING_MAILID','');
define('REPORT_TEST_MAILID','');
define('ORDER_FILES_PATH','files/order/');
define('ADMIN_EMAIL','sales@dropshipadministrator2717.com');