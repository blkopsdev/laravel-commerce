<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use Mail, Session, Redirect, Auth, Validator,Excel,Cookie,Request,Zipper;
use App\Models\User, App\Models\Client,App\Models\Admin,App\Models\Employee,App\Models\Vendor,App\Models\MailTemplate,App\Models\Order,App\Models\OrderDetail, App\Models\DataFeed;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use File,PHPExcel_IOFactory;
use Illuminate\Support\Facades\Hash;
class OrderController extends BaseController
{
    public function getIndex()
    {
        $auth_user = Auth::guard('admin')->user();
        $vendor_data = Vendor::orderBy('v_vendor_name')->get();
        $client_data = Client::where('e_status','Active')->get();
        return View('admin.order.index', array('title' => 'Order List','vendor_list' => $vendor_data,'client_data'=>$client_data));
    }

    public function anyListAjax(Request $request) //Employee Listing
    {
        $data = Request::all();
        $user = Auth::guard('admin')->user();
        $module_name = "1";
        if ($data) {

            $query = new OrderDetail();
            if($user->e_type== 'Employee' || $user->e_type== 'Vendor'){
                $sortColumn = array('v_shipment_number','d_order_date', 'v_bill_to_lastname','v_bill_to_firstname','v_bill_to_zip','v_item_code','v_description','e_item_status');
            } else {
                $sortColumn = array('', 'v_vendor_name','v_shipment_number','d_order_date', 'v_bill_to_lastname','v_bill_to_firstname','v_bill_to_zip','v_item_code','v_description','e_item_status');
            }
            
            // $query = $query->with("Order","Order.Vendor");


            $query = $query->join('tbl_order', function ($join){
                            $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                        });
            $query = $query->join('tbl_vendor', function ($join){
                            $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        });

            if($user->role==2){
                $query=$query->where("tbl_order.i_client_id",$user->i_client_id);
            }else if($user->role==3){
                 $query=$query->where("tbl_order.i_vendor_id",$user->i_vendor_id);
            }else if($user->role==4){
                $employee_data=Employee::where('id',$user->i_emp_id)->first();
                $query=$query->where("tbl_order.i_vendor_id",$employee_data->i_vendor_id);
            }
            if(isset($data['vendor_id']) && $data['vendor_id'] != '') {
                 $query = $query->whereHas('order.Vendor', function($q) use($data){
                    $q->where('tbl_vendor.id', '=',  trim($data['vendor_id']));
                });
            }           
            if (isset($data['v_shipment_number']) && $data['v_shipment_number'] != '') {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_shipment_number', 'LIKE', '%' . trim($data['v_shipment_number']) . '%');
                });
            }            
            if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "" && $data['from_date'] == $data['to_date'] ) {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.d_order_date','LIKE','%'. date('Y-m-d',strtotime($data['from_date'])).'%');
                });
               // $query = $query->where('tbl_order.d_order_date','LIKE','%'. date('Y-m-d',strtotime($data['from_date'])).'%');
            }else if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "") {
                $data['to_date'] = $data['to_date']. "23:59:59";
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->whereBetween('tbl_order.d_order_date',  array(date('Y-m-d H:i:s',strtotime($data['from_date'])),date('Y-m-d H:i:s',strtotime($data['to_date']))));
                });

                // $query = $query->whereBetween('tbl_order.d_order_date',  array(date('Y-m-d H:i:s',strtotime($data['from_date'])),date('Y-m-d H:i:s',strtotime($data['to_date']))));
            }else  if (isset($data['from_date']) && $data['from_date'] != ""){
                 $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.d_order_date','>=',date('Y-m-d',strtotime($data['from_date'])));
                });
                  // $query = $query->where('tbl_order.d_order_date','>=',date('Y-m-d',strtotime($data['from_date'])));
            }else if (isset($data['to_date']) && $data['to_date'] != ""){
                 $data['to_date'] = $data['to_date']. "23:59:59";
                 $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.d_order_date','<=',date('Y-m-d H:i:s',strtotime($data['to_date'])));
                });
                  // $query = $query->where('tbl_order.d_order_date','<=',date('Y-m-d H:i:s',strtotime($data['to_date'])));
            } 
            if(isset($data['v_bill_to_lastname']) && $data['v_bill_to_lastname'] != '') {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_bill_to_lastname', 'LIKE', '%' . trim($data['v_bill_to_lastname']) . '%');
                });
            }           
            if(isset($data['v_bill_to_firstname']) && $data['v_bill_to_firstname'] != '') {
                 $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_bill_to_firstname', 'LIKE', '%' . trim($data['v_bill_to_firstname']) . '%');
                });
            }           
            if(isset($data['v_bill_to_zip']) && $data['v_bill_to_zip'] != '') {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_bill_to_zip', 'LIKE', '%' . trim($data['v_bill_to_zip']) . '%');
                });
            }
            if(isset($data['v_item_code']) && $data['v_item_code'] != '') {
                 $query = $query->where('v_item_code',  'LIKE', '%' .trim($data['v_item_code']). '%');
            }
            if(isset($data['v_description']) && $data['v_description'] != '') {
                $data['v_description'] = htmlspecialchars($data['v_description']);
                $query = $query->where('v_description',  'LIKE', '%' .trim($data['v_description']). '%');
            }
            if(isset($data['e_item_status']) && $data['e_item_status'] != '') {
                $query = $query->where('e_item_status',  trim($data['e_item_status']));
            }         
            $rec_per_page = REC_PER_PAGE;

            if (isset($data['length'])) {
                if ($data['length'] == '-1') {
                    $rec_per_page = '';
                } else {
                    $rec_per_page = $data['length'];
                }
            }

            $sort_order = $data['order']['0']['dir'];
            $order_field = $sortColumn[$data['order']['0']['column']];
            
            if ($sort_order != '' && $order_field != '') {                
                if($order_field == 'v_vendor_name'){
                    $query = $query->orderBy('tbl_vendor.v_vendor_name', $sort_order);
                }else if($order_field == 'v_shipment_number'){                    
                    $query = $query->orderBy('tbl_order.v_shipment_number', $sort_order);
                    
                }else if($order_field == 'd_order_date'){
                    $query = $query->orderBy('tbl_order.d_order_date', $sort_order);
                }else if($order_field == 'v_bill_to_lastname'){
                    $query = $query->orderBy('tbl_order.v_bill_to_lastname', $sort_order);
                }else if($order_field == 'v_bill_to_firstname'){
                    $query = $query->orderBy('tbl_order.v_bill_to_firstname', $sort_order);
                }else if($order_field == 'v_bill_to_zip'){
                    $query = $query->orderBy('tbl_order.v_bill_to_zip', $sort_order);
                }else
                {
                    $query = $query->orderBy($order_field, $sort_order);
                }
                
            } else {
                $query = $query->orderBy('tbl_order.v_shipment_number', 'desc');
            }
            
            if($rec_per_page == ''){
                $temp_records = $query->get();
                $rec_per_page = count($temp_records);
                $users = $query->paginate($rec_per_page);
            } else {
                $users = $query->paginate($rec_per_page);
            }
            
            $arrUsers = $users->toArray();                        
            $data = array();
            foreach ($arrUsers['data'] as $key => $val) {                
                $index = 0;
                if($user->role != 1)
                 {
                    if(isset($this->permissions[$module_name]['e_delete']) && $this->permissions[$module_name]['e_delete'] == '1')
                    {   
                    $data[$key][$index++] = '<input type="checkbox" name="id[]" value="'.$val['i_order_id'].'" class="delete_'.$val['i_order_id'].'">';
                    }else{
                        // $data[$key][$index++]='';
                    }
                 }
                else
                {
                     $data[$key][$index++] = '<input type="checkbox" name="id[]" value="'.$val['i_order_id'].'" class="delete_'.$val['i_order_id'].'">'; 
                }
                if($user->role!=3 && $user->role!=4){
                    $data[$key][$index++] = $val['v_vendor_name'];
                }
                $data[$key][$index++] = '<a rel="' . $val['i_order_id'] . '" href="' . ADMIN_URL . 'order/edit/' . $val['v_shipment_number'] . '">'. $val["v_shipment_number"] .'</a>';
                $data[$key][$index++] = date('m/d/Y', strtotime($val['d_order_date']));
                $data[$key][$index++] = $val['v_bill_to_lastname'];
                $data[$key][$index++] = $val['v_bill_to_firstname'];
                $data[$key][$index++] = $val['v_bill_to_zip'];
                $data[$key][$index++] = (!empty($val['v_item_code']))?$val['v_item_code']:'';
                $data[$key][$index++] = (!empty($val['v_description']))?$val['v_description']:'';
                $data[$key][$index++] = (!empty($val['e_item_status']))?$val['e_item_status']:'';
                $action='';
                if($user->role != 1)
                 {
                    if(isset($this->permissions[$module_name]['e_edit']) && $this->permissions[$module_name]['e_edit'] == '1' && isset($this->permissions[$module_name]['e_delete']) && $this->permissions[$module_name]['e_delete'] == '0')
                    {   
                        $action = '<div class="actions"><a class="edit btn default btn-xs black" rel="' . $val['i_order_id'] . '" href="' . ADMIN_URL . 'order/edit/' . $val['v_shipment_number'] . '"  title="Edit"><i class="fa fa-edit"></i></a>';
                    }else if(isset($this->permissions[$module_name]['e_delete']) && $this->permissions[$module_name]['e_delete'] == '1' || isset($this->permissions[$module_name]['e_edit']) && $this->permissions[$module_name]['e_edit'] == '1'){
                        $action = '<div class="actions"><a class="edit btn default btn-xs black" rel="' . $val['id'] . '" href="' . ADMIN_URL . 'order/edit/' . $val['v_shipment_number'] . '"  title="Edit"><i class="fa fa-edit"></i></a><a href="javascript:;" id="delete_record" rel="' . $val['id'] . '" delete-url="' . ADMIN_URL . 'order/delete/' . $val['id'] . '" class="btn default btn-xs black delete" title="Delete"><i class="icon-trash"></i></a>';
                    }
                 }
                else
                {
                     $action = '<div class="actions"><a class="edit btn default btn-xs black" rel="' . $val['id'] . '" href="' . ADMIN_URL . 'order/edit/' . $val['v_shipment_number'] . '"  title="Edit"><i class="fa fa-edit"></i></a><a href="javascript:;" id="delete_record" rel="' . $val['id'] . '" delete-url="' . ADMIN_URL . 'order/delete/' . $val['id'] . '" class="btn default btn-xs black delete" title="Delete"><i class="icon-trash"></i></a>';
                }
                
                $data[$key][$index++] = $action;
            }
            $return_data['data'] = $data;
            $return_data['recordsTotal'] = $arrUsers['total'];
            $return_data['recordsFiltered'] = $arrUsers['total'];
            $return_data['data_array'] = $arrUsers['data'];
            return $return_data;
        }
    }

    public function getDelete($id)
    {
        $order = Order::find($id);
        if (!empty($order)) {
            $order_details = OrderDetail::where('i_order_id',$id)->delete();
            $order->delete();
            return 'TRUE';
        } else {
            return 'FALSE';
        }  
    }
     public function getOrderDetailsDelete($id)
    {
        $OrderDetail = OrderDetail::find($id);
        if (!empty($OrderDetail)) {
            $order_id = $OrderDetail['i_order_id'];
            $OrderDetail->delete();
            $order_count = OrderDetail::where('i_order_id',$order_id)->count();
            if($order_count <= 0){
                    $order = Order::find($order_id);
                    $order = $order->delete();
            }
            
            return 'TRUE';
        } else {
            return 'FALSE';
        }  
    }
    public function anyEdit($id)
    {
        
        if (Input::all()) {
            $inputs = Input::all();  
            $order = Order::find($id); 
            $order_details = OrderDetail::where('i_order_id',$id)->first();   
            $order->v_bill_to_firstname = trim($inputs['v_bill_to_firstname']);
            $order->v_bill_to_lastname = trim($inputs['v_bill_to_lastname']);
            $order->v_bill_to_address1 = trim($inputs['v_bill_to_address1']);
            $order->v_bill_to_address2 = trim($inputs['v_bill_to_address2']);
            $order->v_bill_to_city = trim($inputs['v_bill_to_city']);
            $order->v_bill_to_state = trim($inputs['v_bill_to_state']);
            $order->v_bill_to_zip = trim($inputs['v_bill_to_zip']);
            $order->v_bill_to_phonenumber = trim($inputs['v_bill_to_phonenumber']);
            $order->v_gift_message = trim($inputs['v_gift_message']);
            $order_details->v_special_instruction = trim($inputs['v_special_instruction']);
            $order->updated_at=date('Y-m-d h:i:s');     
            if ($order->save()) {                
                if($order_details->save())
                {
                    Session::flash('success-message', 'Order has been updated successfully.');
                    return '';    
                }
            }

        } else {

            $value= '';
        foreach($_COOKIE as $key => $val){
            if (strpos($key, 'list-ajax') !== false) {
                $value = $val;
            }
        }
        $data = json_decode($value);
        if ($data) {
            $user = Auth::guard('admin')->user();
            $module_name = "1";
            $query = new OrderDetail();
            if($user->e_type== 'Employee' || $user->e_type== 'Vendor'){
                $sortColumn = array('v_shipment_number','d_order_date', 'v_bill_to_lastname','v_bill_to_firstname','v_bill_to_zip','v_item_code','v_description','e_item_status');
            } else {
                $sortColumn = array('', 'v_vendor_name','v_shipment_number','d_order_date', 'v_bill_to_lastname','v_bill_to_firstname','v_bill_to_zip','v_item_code','v_description','e_item_status');
            }
            $data= (array) $data;
            
            
            
            // $query = $query->with("Order","Order.Vendor");


            $query = $query->join('tbl_order', function ($join){
                            $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                        });
            $query = $query->join('tbl_vendor', function ($join){
                            $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        });

            if($user->role==2){
                $query=$query->where("tbl_order.i_client_id",$user->i_client_id);
            }else if($user->role==3){
                 $query=$query->where("tbl_order.i_vendor_id",$user->i_vendor_id);
            }else if($user->role==4){
                $employee_data=Employee::where('id',$user->i_emp_id)->first();
                $query=$query->where("tbl_order.i_vendor_id",$employee_data->i_vendor_id);
            }
            if(isset($data['vendor_id']) && $data['vendor_id'] != '') {
                 $query = $query->whereHas('order.Vendor', function($q) use($data){
                    $q->where('tbl_vendor.id', '=',  trim($data['vendor_id']));
                });
            }           
            if (isset($data['v_shipment_number']) && $data['v_shipment_number'] != '') {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_shipment_number', 'LIKE', '%' . trim($data['v_shipment_number']) . '%');
                });
            }  
            if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "" && $data['from_date'] == $data['to_date'] ) {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.d_order_date','LIKE','%'. date('Y-m-d',strtotime($data['from_date'])).'%');
                });
               // $query = $query->where('tbl_order.d_order_date','LIKE','%'. date('Y-m-d',strtotime($data['from_date'])).'%');
            }else if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "") {
                $data['to_date'] = $data['to_date']. "23:59:59";
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->whereBetween('tbl_order.d_order_date',  array(date('Y-m-d H:i:s',strtotime($data['from_date'])),date('Y-m-d H:i:s',strtotime($data['to_date']))));
                });

                // $query = $query->whereBetween('tbl_order.d_order_date',  array(date('Y-m-d H:i:s',strtotime($data['from_date'])),date('Y-m-d H:i:s',strtotime($data['to_date']))));
            }else  if (isset($data['from_date']) && $data['from_date'] != ""){
                 $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.d_order_date','>=',date('Y-m-d',strtotime($data['from_date'])));
                });
                  // $query = $query->where('tbl_order.d_order_date','>=',date('Y-m-d',strtotime($data['from_date'])));
            }else if (isset($data['to_date']) && $data['to_date'] != ""){
                 $data['to_date'] = $data['to_date']. "23:59:59";
                 $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.d_order_date','<=',date('Y-m-d H:i:s',strtotime($data['to_date'])));
                });
                  // $query = $query->where('tbl_order.d_order_date','<=',date('Y-m-d H:i:s',strtotime($data['to_date'])));
            } 
            if(isset($data['v_bill_to_lastname']) && $data['v_bill_to_lastname'] != '') {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_bill_to_lastname', 'LIKE', '%' . trim($data['v_bill_to_lastname']) . '%');
                });
            }           
            if(isset($data['v_bill_to_firstname']) && $data['v_bill_to_firstname'] != '') {
                 $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_bill_to_firstname', 'LIKE', '%' . trim($data['v_bill_to_firstname']) . '%');
                });
            }           
            if(isset($data['v_bill_to_zip']) && $data['v_bill_to_zip'] != '') {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_bill_to_zip', 'LIKE', '%' . trim($data['v_bill_to_zip']) . '%');
                });
            }
            if(isset($data['v_item_code']) && $data['v_item_code'] != '') {
                 $query = $query->where('v_item_code',  'LIKE', '%' .trim($data['v_item_code']). '%');
            }
            if(isset($data['v_description']) && $data['v_description'] != '') {
                $data['v_description'] = htmlspecialchars($data['v_description']);
                $query = $query->where('v_description',  'LIKE', '%' .trim($data['v_description']). '%');
            }
            if(isset($data['e_item_status']) && $data['e_item_status'] != '') {
                $query = $query->where('e_item_status',  trim($data['e_item_status']));
            }         
         //   $rec_per_page = REC_PER_PAGE;

           /* if (isset($data['length'])) {
                if ($data['length'] == '-1') {
                    $rec_per_page = '';
                } else {
                    $rec_per_page = $data['length'];
                }
            }*/
           // $query = $query->where('tbl_order.v_shipment_number', '<', $id);
           $data['order_sort'] = (array) $data['order_sort'];
            if(isset($data['order_sort']) && !empty($data['order_sort'])){
                 $sort_order = $data['order_sort']->sort;
                $order_field = $sortColumn[$data['order_sort']->column];
            
                if ($sort_order != '' && $order_field != '') {                
                    if($order_field == 'v_vendor_name'){
                        $query = $query->orderBy('tbl_vendor.v_vendor_name', $sort_order);
                    }else if($order_field == 'v_shipment_number'){                    
                        $query = $query->orderBy('tbl_order.v_shipment_number', $sort_order);
                        
                    }else if($order_field == 'd_order_date'){
                        $query = $query->orderBy('tbl_order.d_order_date', $sort_order);
                    }else if($order_field == 'v_bill_to_lastname'){
                        $query = $query->orderBy('tbl_order.v_bill_to_lastname', $sort_order);
                    }else if($order_field == 'v_bill_to_firstname'){
                        $query = $query->orderBy('tbl_order.v_bill_to_firstname', $sort_order);
                    }else if($order_field == 'v_bill_to_zip'){
                        $query = $query->orderBy('tbl_order.v_bill_to_zip', $sort_order);
                    }else
                    {
                        $query = $query->orderBy($order_field, $sort_order);
                    }
                } else {
                    $query = $query->orderBy('tbl_order.v_shipment_number', 'desc');
                }
            } else {
                $query = $query->orderBy('tbl_order.v_shipment_number', 'desc');
            }
            $query1 = $query;
            $records_1 = clone $query1;
            $query2 = clone $query1;
            $query3 = clone $query2;
            $query4 = clone $query3;
            $records = $records_1->where('tbl_order.v_shipment_number',$id)->first();
       
            $previous = $query2->where('tbl_order.v_shipment_number', '<', $id)->max('tbl_order.v_shipment_number');
            $next = $query3->where('tbl_order.v_shipment_number', '>', $id)->min('tbl_order.v_shipment_number');
            if($previous == ''){
                $previous = 0;
            }
            if($next == ''){
                $next = 0;
            }
            $total_records = $records_1->where('tbl_order.v_shipment_number',$id)->count();
        }
           /* $user = Auth::guard('admin')->user();            
            if($user->role==2){
                $records = OrderDetail::join('tbl_order', function ($join){
                            $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                        })->join('tbl_vendor', function ($join){
                            $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        })->where("tbl_order.i_client_id",$user->i_client_id)->where('tbl_order.v_shipment_number',$id)->first();                
                if(!empty($records)){
                    $records=$records->toArray();                
                    $previous = OrderDetail::join('tbl_order', function ($join){
                                $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                            })->join('tbl_vendor', function ($join){
                                $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                            })->where("tbl_order.z",$user->i_client_id)->where('tbl_order.v_shipment_number', '<', $records['v_shipment_number'])->max('tbl_order.v_shipment_number');
                    $next = OrderDetail::join('tbl_order', function ($join){
                        $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                    })->join('tbl_vendor', function ($join){
                        $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                    })->where("tbl_order.i_client_id",$user->i_client_id)->where('tbl_order.v_shipment_number', '>', $records['v_shipment_number'])->min('tbl_order.v_shipment_number');
                }else{
                    $previous=0;
                    $next=0;
                }
            }else if($user->role==3){
                $records = OrderDetail::join('tbl_order', function ($join){
                            $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                        })->join('tbl_vendor', function ($join){
                            $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        })->where("tbl_order.i_vendor_id",$user->i_vendor_id)->where('tbl_order.v_shipment_number',$id)->first();
                if(!empty($records)){
                    $records=$records->toArray();                
                    $previous = OrderDetail::join('tbl_order', function ($join){
                                $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                            })->join('tbl_vendor', function ($join){
                                $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                            })->where("tbl_order.i_vendor_id",$user->i_vendor_id)->where('tbl_order.v_shipment_number', '<', $records['v_shipment_number'])->max('tbl_order.v_shipment_number');                 
                    $next = OrderDetail::join('tbl_order', function ($join){
                        $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                    })->join('tbl_vendor', function ($join){
                        $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                    })->where("tbl_order.i_vendor_id",$user->i_vendor_id)->where('tbl_order.v_shipment_number', '>', $records['v_shipment_number'])->min('tbl_order.v_shipment_number');
                }else{
                    $previous=0;
                    $next=0;
                }

            }else if($user->role==4){
                $employee_data=Employee::where('id',$user->i_emp_id)->first();
                $records = OrderDetail::join('tbl_order', function ($join){
                            $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                        })->join('tbl_vendor', function ($join){
                            $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        })->where("tbl_order.i_vendor_id",$employee_data->i_vendor_id)->where('tbl_order.v_shipment_number',$id)->first();   
                if(!empty($records)){
                    $records=$records->toArray();   

                    $previous = OrderDetail::join('tbl_order', function ($join){
                                $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                            })->join('tbl_vendor', function ($join){
                                $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                            })->where("tbl_order.i_vendor_id",$employee_data->i_vendor_id)->where('tbl_order.v_shipment_number', '<', $records['v_shipment_number'])->max('tbl_order.v_shipment_number');    

                    $next = OrderDetail::join('tbl_order', function ($join){
                        $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                    })->join('tbl_vendor', function ($join){
                        $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                    })->where("tbl_order.i_vendor_id",$employee_data->i_vendor_id)->where('tbl_order.v_shipment_number', '>', $records['v_shipment_number'])->min('tbl_order.v_shipment_number');
                }else{
                    $previous=0;
                    $next=0;

                }
            }else{
                $records = OrderDetail::join('tbl_order', function ($join){
                            $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                        })->join('tbl_vendor', function ($join){
                            $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        })->where('tbl_order.v_shipment_number',$id)->first();
                if(!empty($records)){
                    $records=$records->toArray();
                    $previous = OrderDetail::join('tbl_order', function ($join){
                                $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                            })->join('tbl_vendor', function ($join){
                                $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                            })->where('tbl_order.v_shipment_number', '<', $records['v_shipment_number'])->max('tbl_order.v_shipment_number');
                    $next = OrderDetail::join('tbl_order', function ($join){
                        $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                    })->join('tbl_vendor', function ($join){
                        $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                    })->where('tbl_order.v_shipment_number', '>', $records['v_shipment_number'])->min('tbl_order.v_shipment_number');
                 }else{
                    $previous=0;
                    $next=0;
                }
            }
            $total_records = OrderDetail::with("Order")->whereHas('Order', function($q) use($records){
                    $q->where('tbl_order.v_shipment_number',$records['v_shipment_number']);
            })->count();*/
       
            if(isset($records) && !empty($records)){
                 return View('admin.order.edit_order_details', array('records' => $records, 'title' => 'Edit Order','previous'=>$previous,'next'=>$next,'total_records'=>$total_records));
            }else{
                return Redirect(ADMIN_URL . 'order');        
            }
        }
        return Redirect(ADMIN_URL . 'order');
    }
    public function anyEditOrder($id)
    {
        if (Input::all()) {
            $inputs = Input::all(); 
            $order = Order::find($id); 
            $order_details = OrderDetail::where('i_order_id',$id)->get();   
            $order->v_bill_to_firstname = trim($inputs['v_bill_to_firstname']);
            $order->v_bill_to_lastname = trim($inputs['v_bill_to_lastname']);
            $order->v_bill_to_address1 = trim($inputs['v_bill_to_address1']);
            $order->v_bill_to_address2 = trim($inputs['v_bill_to_address2']);
            $order->v_bill_to_city = trim($inputs['v_bill_to_city']);
            $order->v_bill_to_state = trim($inputs['v_bill_to_state']);
            $order->v_bill_to_zip = trim($inputs['v_bill_to_zip']);
            $order->v_bill_to_phonenumber = trim($inputs['v_bill_to_phonenumber']);
            $order->v_gift_message = trim($inputs['v_gift_message']);
            foreach ($order_details as $key => $value) {
                $order_details_obj=OrderDetail::find($value->id);
                $order_details_obj->v_special_instruction = trim($inputs['v_special_instruction-'.$value->id]);
                $order_details_obj->save();
            }
            $order->updated_at=date('Y-m-d h:i:s');     
            if ($order->save()) {                
                    Session::flash('success-message', 'Order has been updated successfully.');
                    return '';    
            }

        } else {
            $records = Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->first();
            return View('admin.order.edit_order', array('records' => $records, 'title' => 'Edit Order'));
        }
        return Redirect(ADMIN_URL . 'order');
    }

    public function postBulkAction()
    {
        $data = Request::all();
        if(count($data) > 0) {
            if ($data['action'] == 'Delete') {
                $orderdetials = OrderDetail::whereIn('i_order_id',$data['ids'])->delete();
                $order = Order::whereIn('id',$data['ids'])->delete();
                if($order){
                    return 'TRUE';
                } else { return 'FALSE'; }
            } else if ($data['action'] == 'edit_order') {   
               // $id='639782-3';
                $records = Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->first();
                return View('admin.order.edit_order', array('records' => $records, 'title' => 'Edit Order')); 
            }
        } 
    }
     public function anyExportDataFeed(){
     
        $data = Input::all();
       
       // $data = json_decode($data, true);
       
        $user = Auth::guard('admin')->user();
        $module_name = "1";
            $query = new OrderDetail();
            if($user->e_type== 'Employee' || $user->e_type== 'Vendor'){
                $sortColumn = array('v_shipment_number','d_order_date', 'v_bill_to_lastname','v_bill_to_firstname','v_bill_to_zip','v_item_code','v_description','e_item_status');
            } else {
                $sortColumn = array('', 'v_vendor_name','v_shipment_number','d_order_date', 'v_bill_to_lastname','v_bill_to_firstname','v_bill_to_zip','v_item_code','v_description','e_item_status');
            }
            
            // $query = $query->with("Order","Order.Vendor");


            $query = $query->join('tbl_order', function ($join){
                            $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                        });
            $query = $query->join('tbl_vendor', function ($join){
                            $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        });

            if($user->role==2){
                $query=$query->where("tbl_order.i_client_id",$user->i_client_id);
            }else if($user->role==3){
                 $query=$query->where("tbl_order.i_vendor_id",$user->i_vendor_id);
            }else if($user->role==4){
                $employee_data=Employee::where('id',$user->i_emp_id)->first();
                $query=$query->where("tbl_order.i_vendor_id",$employee_data->i_vendor_id);
            }
            if(isset($data['vendor_id']) && $data['vendor_id'] != '') {
                 $query = $query->whereHas('order.Vendor', function($q) use($data){
                    $q->where('tbl_vendor.id', '=',  trim($data['vendor_id']));
                });
            }           
            if (isset($data['v_shipment_number']) && $data['v_shipment_number'] != '') {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_shipment_number', 'LIKE', '%' . trim($data['v_shipment_number']) . '%');
                });
            }            
            if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "" && $data['from_date'] == $data['to_date'] ) {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.d_order_date','LIKE','%'. date('Y-m-d',strtotime($data['from_date'])).'%');
                });
               // $query = $query->where('tbl_order.d_order_date','LIKE','%'. date('Y-m-d',strtotime($data['from_date'])).'%');
            }else if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "") {
                $data['to_date'] = $data['to_date']. "23:59:59";
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->whereBetween('tbl_order.d_order_date',  array(date('Y-m-d H:i:s',strtotime($data['from_date'])),date('Y-m-d H:i:s',strtotime($data['to_date']))));
                });

                // $query = $query->whereBetween('tbl_order.d_order_date',  array(date('Y-m-d H:i:s',strtotime($data['from_date'])),date('Y-m-d H:i:s',strtotime($data['to_date']))));
            }else  if (isset($data['from_date']) && $data['from_date'] != ""){
                 $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.d_order_date','>=',date('Y-m-d',strtotime($data['from_date'])));
                });
                  // $query = $query->where('tbl_order.d_order_date','>=',date('Y-m-d',strtotime($data['from_date'])));
            }else if (isset($data['to_date']) && $data['to_date'] != ""){
                 $data['to_date'] = $data['to_date']. "23:59:59";
                 $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.d_order_date','<=',date('Y-m-d H:i:s',strtotime($data['to_date'])));
                });
                  // $query = $query->where('tbl_order.d_order_date','<=',date('Y-m-d H:i:s',strtotime($data['to_date'])));
            } 
            if(isset($data['v_bill_to_lastname']) && $data['v_bill_to_lastname'] != '') {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_bill_to_lastname', 'LIKE', '%' . trim($data['v_bill_to_lastname']) . '%');
                });
            }           
            if(isset($data['v_bill_to_firstname']) && $data['v_bill_to_firstname'] != '') {
                 $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_bill_to_firstname', 'LIKE', '%' . trim($data['v_bill_to_firstname']) . '%');
                });
            }           
            if(isset($data['v_bill_to_zip']) && $data['v_bill_to_zip'] != '') {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_bill_to_zip', 'LIKE', '%' . trim($data['v_bill_to_zip']) . '%');
                });
            }
            if(isset($data['v_item_code']) && $data['v_item_code'] != '') {
                 $query = $query->where('v_item_code',  'LIKE', '%' .trim($data['v_item_code']). '%');
            }
            if(isset($data['v_description']) && $data['v_description'] != '') {
                $data['v_description'] = htmlspecialchars($data['v_description']);
                $query = $query->where('v_description',  'LIKE', '%' .trim($data['v_description']). '%');
            }
            if(isset($data['e_item_status']) && $data['e_item_status'] != '') {
                $query = $query->where('e_item_status',  trim($data['e_item_status']));
            }         
            $rec_per_page = REC_PER_PAGE;

            if (isset($data['length'])) {
                if ($data['length'] == '-1') {
                    $rec_per_page = '';
                } else {
                    $rec_per_page = $data['length'];
                }
            }
            $sort_order = $data['order']['0']['dir'];
            $order_field = $sortColumn[$data['order']['0']['column']];
            
            if ($sort_order != '' && $order_field != '') {                
                if($order_field == 'v_vendor_name'){
                    $query = $query->orderBy('tbl_vendor.v_vendor_name', $sort_order);
                }else if($order_field == 'v_shipment_number'){                    
                    $query = $query->orderBy('tbl_order.v_shipment_number', $sort_order);
                    
                }else if($order_field == 'd_order_date'){
                    $query = $query->orderBy('tbl_order.d_order_date', $sort_order);
                }else if($order_field == 'v_bill_to_lastname'){
                    $query = $query->orderBy('tbl_order.v_bill_to_lastname', $sort_order);
                }else if($order_field == 'v_bill_to_firstname'){
                    $query = $query->orderBy('tbl_order.v_bill_to_firstname', $sort_order);
                }else if($order_field == 'v_bill_to_zip'){
                    $query = $query->orderBy('tbl_order.v_bill_to_zip', $sort_order);
                }else
                {
                    $query = $query->orderBy($order_field, $sort_order);
                }
                
            } else {
                $query = $query->orderBy('tbl_order_details.id', 'desc');
            }
            $data = '';
          
            $arrUsers = $query->get()->toArray();                        
     
              if($user->e_type == 'Vendor' or $user->e_type == 'Employee') {
                    $header = "Order ID \t Order Date \t Last Name \t First Name \t Zip Code \t Product Id \t Item Name \t Item Status \t Quantity \t Ship To - Company \t Ship To - Name \t Ship To - Address \t Ship To - City \t Ship To - State \t Ship To - Zip \t Phone Number \t Email \t Website Name \t Wholesale Price \t Back Order Date \t Cancel Reason";
                } else {
                    $header = "Supplier Name \t Order ID \t Order Date \t Last Name \t First Name \t Zip Code \t Product Id \t Item Name \t Item Status \t Quantity \t Ship To - Company \t Ship To - Name \t Ship To - Address \t Ship To - City \t Ship To - State \t Ship To - Zip \t Phone Number \t Email \t Website Name \t Wholesale Price \t Back Order Date \t Cancel Reason";
                }
            foreach ($arrUsers as $key => $val) {                
              
                $SupplierName = $val['v_company_name'];
                $ShipmentNumber = $val['v_shipment_number'];
                $OrderDate = date('m/d/Y', strtotime($val['d_order_date']));
                $BillToLastName = $val['v_bill_to_lastname'];
                $BillToFirstName = $val['v_bill_to_firstname'];
                $BillToZip = $val['v_bill_to_zip'];
                $ItemCode = $val['v_item_code'];
                $Description = $val['v_description'];
                $ItemStatus = $val['e_item_status'];
                $Quantity = $val['i_quanity'];                        
                $ShipToCompany = $val['v_ship_to_company'];
                $ShipToFirstName = $val['v_ship_to_firstname'];
                $ShipToAddress1 = $val['v_ship_to_address1'];
                $ShipToCity = $val['v_ship_to_city'];
                $ShipToState = $val['v_ship_to_state'];
                $ShipToZip = $val['v_ship_to_zip'];
                $ShipToPhoneNumber = $val['v_ship_to_phonenumber'];
                $ShipToEmail = $val['v_ship_to_email'];
                $Website = $val['v_website'];
                $WholesalePrice = $val['f_cogs'];
                $SpecialInstruction = $val['v_special_instruction'];
                if($val['d_back_ordered_date'] != '' && $val['d_back_ordered_date'] != '0000-00-00'){ $back_order_Date = date('m/d/Y', strtotime($val['d_back_ordered_date'])); } else { $back_order_Date =''; }
                $BackOrderedDate = $back_order_Date;
                $CanceledReason = $val['v_canceled_reason']; 
                $SupplierCode = $val['v_supplier_code'];
            
                $ShipToName = trim($val['v_ship_to_firstname'])." ".trim($val['v_ship_to_lastname']);
                if(trim($val['v_ship_to_address1']) != '') { $ShipToAddress = trim($val['v_ship_to_address1']).", "; }
                if(trim($val['v_ship_to_address2']) != '') { $ShipToAddress.= trim($val['v_ship_to_address2']); }
                $ShipToAddress = rtrim($ShipToAddress,", ");
               // $vals['created_at'] = date('m/d/Y h:i:s', strtotime($val['created_at']));
              //  $vals['updated_at'] = date('m/d/Y h:i:s', strtotime($val['updated_at']));

              if($user->e_type == 'Vendor' or $user->e_type == 'Employee') {
                $line = "$ShipmentNumber \t $OrderDate \t $BillToLastName \t $BillToFirstName \t $BillToZip \t $SupplierCode \t $Description \t $ItemStatus \t $Quantity \t $ShipToCompany \t $ShipToName \t $ShipToAddress \t $ShipToCity \t $ShipToState \t $ShipToZip \t $ShipToPhoneNumber \t $ShipToEmail \t $Website \t $WholesalePrice \t $BackOrderedDate  \t $CanceledReason ";
            } else {
                $line = "$SupplierName \t $ShipmentNumber \t $OrderDate \t $BillToLastName \t $BillToFirstName \t $BillToZip \t $SupplierCode \t $Description \t $ItemStatus \t $Quantity \t $ShipToCompany \t $ShipToName \t $ShipToAddress \t $ShipToCity \t $ShipToState \t $ShipToZip \t $ShipToPhoneNumber \t $ShipToEmail \t $Website \t $WholesalePrice \t $BackOrderedDate \t $CanceledReason";
            }
            $line = str_replace("\t \t","\t",$line);
            $line = str_replace('"', '""', stripslashes($line));
            $data .= trim($line)."\n";
        }
            $data = str_replace("\r", "", $data);
            if ($data == "") {
                $data = "\nNo records found.\n";
            }

            $output = $header."\n".$data;
            // Convert to UTF-16LE
            $output = mb_convert_encoding($output, 'UTF-16LE', 'UTF-8');
            // Prepend BOM
            $output = "\xFF\xFE" . $output;

            header('Pragma: public');
            header("Content-type: application/x-msexcel");
            $filename = "Orders_list_".$user->e_type."_export_".date("d_M_Y")."_".time().rand(0,999999);
            header('Content-Disposition: attachment;  filename="'.$filename.'.xls"');
            echo $output;
        
    }
    public function anyExportToExcel()
    {        
        $data = Input::all();        
        $user = Auth::guard('admin')->user();
        $loginid = $user->id;
        $files = glob(TEMP_ZIP_FILE_PATH.$loginid."/*"); // get all file names
        foreach($files as $file){ // iterate files
          if(is_file($file))
            @unlink($file); // delete file
        }
        if(is_file( (public_path() . '/order'.$loginid.'.zip'))){
            $download_path =(public_path() . '/order'.$loginid.'.zip');
            @unlink($download_path);
        }

        $time = date('Y_m_d_h_i_s_A');
        $query = new OrderDetail();
        if($user->e_type== 'Employee' || $user->e_type== 'Vendor'){
            $sortColumn = array('v_shipment_number','d_order_date', 'v_bill_to_lastname','v_bill_to_firstname','v_bill_to_zip','v_item_code','v_description','e_item_status');
        } else {
            $sortColumn = array('', 'v_vendor_name','v_shipment_number','d_order_date', 'v_bill_to_lastname','v_bill_to_firstname','v_bill_to_zip','v_item_code','v_description','e_item_status');
        }
            // $query = $query->with("Order","Order.Vendor");

            $query = $query->join('tbl_order', function ($join){
                            $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                        });
            $query = $query->join('tbl_vendor', function ($join){
                            $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        });

            if($user->role==2){
                $query=$query->where("tbl_order.i_client_id",$user->i_client_id);
            }else if($user->role==3){
                 $query=$query->where("tbl_order.i_vendor_id",$user->i_vendor_id);
            }else if($user->role==4){
                $employee_data=Employee::where('id',$user->i_emp_id)->first();
                $query=$query->where("tbl_order.i_vendor_id",$employee_data->i_vendor_id);
            }
            if(isset($data['vendor_id']) && $data['vendor_id'] != '') {
                 $query = $query->whereHas('order.Vendor', function($q) use($data){
                    $q->where('tbl_vendor.id', '=',  trim($data['vendor_id']));
                });
            }           
            if (isset($data['v_shipment_number']) && $data['v_shipment_number'] != '') {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_shipment_number', 'LIKE', '%' . trim($data['v_shipment_number']) . '%');
                });
            }            
            if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "" && $data['from_date'] == $data['to_date'] ) {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.d_order_date','LIKE','%'. date('Y-m-d',strtotime($data['from_date'])).'%');
                });
               // $query = $query->where('tbl_order.d_order_date','LIKE','%'. date('Y-m-d',strtotime($data['from_date'])).'%');
            }else if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "") {
                $data['to_date'] = $data['to_date']. "23:59:59";
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->whereBetween('tbl_order.d_order_date',  array(date('Y-m-d H:i:s',strtotime($data['from_date'])),date('Y-m-d H:i:s',strtotime($data['to_date']))));
                });

                // $query = $query->whereBetween('tbl_order.d_order_date',  array(date('Y-m-d H:i:s',strtotime($data['from_date'])),date('Y-m-d H:i:s',strtotime($data['to_date']))));
            }else  if (isset($data['from_date']) && $data['from_date'] != ""){
                 $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.d_order_date','>=',date('Y-m-d',strtotime($data['from_date'])));
                });
                  // $query = $query->where('tbl_order.d_order_date','>=',date('Y-m-d',strtotime($data['from_date'])));
            }else if (isset($data['to_date']) && $data['to_date'] != ""){
                 $data['to_date'] = $data['to_date']. "23:59:59";
                 $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.d_order_date','<=',date('Y-m-d H:i:s',strtotime($data['to_date'])));
                });
                  // $query = $query->where('tbl_order.d_order_date','<=',date('Y-m-d H:i:s',strtotime($data['to_date'])));
            } 
            if(isset($data['v_bill_to_lastname']) && $data['v_bill_to_lastname'] != '') {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_bill_to_lastname', 'LIKE', '%' . trim($data['v_bill_to_lastname']) . '%');
                });
            }           
            if(isset($data['v_bill_to_firstname']) && $data['v_bill_to_firstname'] != '') {
                 $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_bill_to_firstname', 'LIKE', '%' . trim($data['v_bill_to_firstname']) . '%');
                });
            }           
            if(isset($data['v_bill_to_zip']) && $data['v_bill_to_zip'] != '') {
                $query = $query->whereHas('Order', function($q) use($data){
                    $q->where('tbl_order.v_bill_to_zip', 'LIKE', '%' . trim($data['v_bill_to_zip']) . '%');
                });
            }
            if(isset($data['v_item_code']) && $data['v_item_code'] != '') {
                 $query = $query->where('v_item_code',  'LIKE', '%' .trim($data['v_item_code']). '%');
            }
            if(isset($data['v_description']) && $data['v_description'] != '') {
                $query = $query->where('v_description',  'LIKE', '%' .trim($data['v_description']). '%');
            }
            if(isset($data['e_item_status']) && $data['e_item_status'] != '') {
                $query = $query->where('e_item_status',  trim($data['e_item_status']));
            }         
            $rec_per_page = REC_PER_PAGE;

            if (isset($data['length'])) {
                if ($data['length'] == '-1') {
                    $rec_per_page = '';
                } else {
                    $rec_per_page = $data['length'];
                }
            }

            $sort_order = $data['order']['0']['dir'];
            $order_field = $sortColumn[$data['order']['0']['column']];
            
            if ($sort_order != '' && $order_field != '') {                
                if($order_field == 'v_vendor_name'){
                    $query = $query->orderBy('tbl_vendor.v_vendor_name', $sort_order);
                }else if($order_field == 'v_shipment_number'){                    
                    $query = $query->orderBy('tbl_order.v_shipment_number', $sort_order);
                    
                }else if($order_field == 'd_order_date'){
                    $query = $query->orderBy('tbl_order.d_order_date', $sort_order);
                }else if($order_field == 'v_bill_to_lastname'){
                    $query = $query->orderBy('tbl_order.v_bill_to_lastname', $sort_order);
                }else if($order_field == 'v_bill_to_firstname'){
                    $query = $query->orderBy('tbl_order.v_bill_to_firstname', $sort_order);
                }else if($order_field == 'v_bill_to_zip'){
                    $query = $query->orderBy('tbl_order.v_bill_to_zip', $sort_order);
                }else
                {
                    $query = $query->orderBy($order_field, $sort_order);
                }
                
            } else {
                $query = $query->orderBy('tbl_order_details.id', 'desc');
            }

        $max=1000;
        //$users = $query->get();
        $total = $query->count();
         
        //  $records = $users->toArray();
        // $records = $records['data'];
        if($total > 0){
            $pages = ceil($total / $max);
            if($total >= $max) {
                for ($i = 1; $i < ($pages + 1); $i++) {
                    $offset = (($i - 1)  * $max);
                    $start = ($offset == 0 ? 0 : ($offset + 1));
                    $records = $query->skip($start)->take($max)->get();
                    Excel::create('Order_List_'.time(), function ($excel) use ($records) {
                        $excel->sheet('Order_List', function ($sheet) use ($records) {
                            $field = array();
                            $field['v_vendor_name'] = 'Supplier Name';
                            $field['id'] = ' Order ID';
                            $field['d_order_date'] = ' Order Date';
                            $field['v_bill_to_lastname'] = 'Last Name';
                            $field['v_bill_to_firstname'] = 'First Name';
                            $field['v_bill_to_zip'] = ' Zip Code ';
                            $field['v_item_code'] = ' Product Id ';
                            $field['v_description'] = ' Item Name ';
                            $field['e_item_status'] = '  Item Status  ';
                            $field['i_quanity'] = 'Quantity';
                            $field['v_ship_to_company'] = 'Ship To - Company';
                            $field['v_ship_to_firstname'] = 'Ship To - Name';
                            $field['v_ship_to_address1'] = 'Ship To - Address';
                            $field['v_ship_to_city'] = 'Ship To - City';
                            $field['v_ship_to_state'] = 'Ship To - State';
                            $field['v_ship_to_zip'] = 'Ship To - Zip';
                            $field['v_ship_to_phonenumber'] = 'Phone Number';
                            $field['v_ship_to_email'] = 'Email';
                            $field['v_website'] = 'Website Name';
                            $field['f_cogs'] = 'Wholesale Price';
                            $field['v_special_instruction'] = 'Special Instruction';
                            $field['d_back_ordered_date'] = 'Back Order Date';
                            $field['v_canceled_reason'] = 'Notes';
                            $field['created_at'] = 'Created At';
                            $field['updated_at'] = 'Updated At';
                            $sheet->setOrientation('landscape');
                            $sheet->setHeight(1, 30);
                            $sheet->mergeCells('A1:Y1');
                        

                            $sheet->cells('A1:Y1', function ($cell) {
                                $cell->setAlignment('center');
                                $cell->setValignment('middle');
                                $cell->setFontSize('20');
                                $cell->setFontWeight('bold');
                            });
                            $sheet->row(1, array('Order List'));
                            $sheet->cells('A2:Y2', function ($cell) {
                                $cell->setAlignment('center');
                                $cell->setValignment('middle');
                                $cell->setFontSize('12');
                                $cell->setFontWeight('bold');
                            });
                            $sheet->getStyle('D70')->getAlignment()->setWrapText(true);
                            $sheet->row(2, $field);
                            
                            $intCount = 3;
                            $srNo = 1;
                            foreach ($records as $index => $val) {
                                $vals=array();
                                $vals['v_vendor_name'] = $val['v_vendor_name'];
                                $vals['id'] = $val['v_shipment_number'];
                                $vals['d_order_date'] = date('m/d/Y', strtotime($val['d_order_date']));
                                $vals['v_bill_to_lastname'] = $val['v_bill_to_lastname'];
                                $vals['v_bill_to_firstname'] = $val['v_bill_to_firstname'];
                                $vals['v_bill_to_zip'] = $val['v_bill_to_zip'];
                                $vals['v_item_code'] = $val['v_item_code'];
                                $vals['v_description'] = $val['v_description'];
                                $vals['e_item_status'] = $val['e_item_status'];
                                $vals['i_quanity'] = $val['i_quanity'];                        
                                $vals['v_ship_to_company'] = $val['v_ship_to_company'];
                                $vals['v_ship_to_firstname'] = $val['v_ship_to_firstname'];
                                $vals['v_ship_to_address1'] = $val['v_ship_to_address1'];
                                $vals['v_ship_to_city'] = $val['v_ship_to_city'];
                                $vals['v_ship_to_state'] = $val['v_ship_to_state'];
                                $vals['v_ship_to_zip'] = $val['v_ship_to_zip'];
                                $vals['v_ship_to_phonenumber'] = $val['v_ship_to_phonenumber'];
                                $vals['v_ship_to_email'] = $val['v_ship_to_email'];
                                $vals['v_website'] = $val['v_website'];
                                $vals['f_cogs'] = $val['f_cogs'];
                                $vals['v_special_instruction'] = $val['v_special_instruction'];
                                if($val['d_back_ordered_date'] != '' && $val['d_back_ordered_date'] != '0000-00-00'){ $back_order_Date = date('m/d/Y', strtotime($val['d_back_ordered_date'])); } else { $back_order_Date =''; }
                                $vals['d_back_ordered_date'] = $back_order_Date;
                                $vals['v_canceled_reason'] = $val['v_canceled_reason']; 
                                $vals['created_at'] = date('m/d/Y h:i:s', strtotime($val['created_at']));
                                $vals['updated_at'] = date('m/d/Y h:i:s', strtotime($val['updated_at']));
                                $sheet->row($intCount, $vals);
                                $intCount++;
                                $srNo++;
                            }
                        }); 
                    })->store('xls',TEMP_ZIP_FILE_PATH.$loginid."/", true);
                }
                $files = glob(TEMP_ZIP_FILE_PATH.$loginid."/*");
            
                Zipper::make(public_path().'/order'.$loginid.'.zip')->add($files)->close();
                $download_path = ( public_path() . '/order'.$loginid.'.zip');
                return response()->download($download_path);
            } else {
                $records = $query->get();
                Excel::create('Order_List_'.$time, function ($excel) use ($records) {
                    $excel->sheet('Order_List', function ($sheet) use ($records) {
                        $field = array();
                        $field['v_vendor_name'] = 'Supplier Name';
                        $field['id'] = ' Order ID';
                        $field['d_order_date'] = ' Order Date';
                        $field['v_bill_to_lastname'] = 'Last Name';
                        $field['v_bill_to_firstname'] = 'First Name';
                        $field['v_bill_to_zip'] = ' Zip Code ';
                        $field['v_item_code'] = ' Product Id ';
                        $field['v_description'] = ' Item Name ';
                        $field['e_item_status'] = '  Item Status  ';
                        $field['i_quanity'] = 'Quantity';
                        $field['v_ship_to_company'] = 'Ship To - Company';
                        $field['v_ship_to_firstname'] = 'Ship To - Name';
                        $field['v_ship_to_address1'] = 'Ship To - Address';
                        $field['v_ship_to_city'] = 'Ship To - City';
                        $field['v_ship_to_state'] = 'Ship To - State';
                        $field['v_ship_to_zip'] = 'Ship To - Zip';
                        $field['v_ship_to_phonenumber'] = 'Phone Number';
                        $field['v_ship_to_email'] = 'Email';
                        $field['v_website'] = 'Website Name';
                        $field['f_cogs'] = 'Wholesale Price';
                        $field['v_special_instruction'] = 'Special Instruction';
                        $field['d_back_ordered_date'] = 'Back Order Date';
                        $field['v_canceled_reason'] = 'Notes';
                        $field['created_at'] = 'Created At';
                        $field['updated_at'] = 'Updated At';
                        $sheet->setOrientation('landscape');
                        $sheet->setHeight(1, 30);
                        $sheet->mergeCells('A1:Y1');
                        $sheet->cells('A1:Y1', function ($cell) {
                            $cell->setAlignment('center');
                            $cell->setValignment('middle');
                            $cell->setFontSize('20');
                            $cell->setFontWeight('bold');
                        });
                        $sheet->row(1, array('Order List'));
                        $sheet->cells('A2:Y2', function ($cell) {
                            $cell->setAlignment('center');
                            $cell->setValignment('middle');
                            $cell->setFontSize('12');
                            $cell->setFontWeight('bold');
                        });
                        $sheet->getStyle('D70')->getAlignment()->setWrapText(true);
                        $sheet->row(2, $field);
                            
                        $intCount = 3;
                        $srNo = 1;
                        foreach ($records as $index => $val) {
                            $vals=array();
                            $vals['v_vendor_name'] = $val['v_vendor_name'];
                            $vals['id'] = $val['v_shipment_number'];
                            $vals['d_order_date'] = date('m/d/Y', strtotime($val['d_order_date']));
                            $vals['v_bill_to_lastname'] = $val['v_bill_to_lastname'];
                            $vals['v_bill_to_firstname'] = $val['v_bill_to_firstname'];
                            $vals['v_bill_to_zip'] = $val['v_bill_to_zip'];
                            $vals['v_item_code'] = $val['v_item_code'];
                            $vals['v_description'] = $val['v_description'];
                            $vals['e_item_status'] = $val['e_item_status'];
                            $vals['i_quanity'] = $val['i_quanity'];                        
                            $vals['v_ship_to_company'] = $val['v_ship_to_company'];
                            $vals['v_ship_to_firstname'] = $val['v_ship_to_firstname'];
                            $vals['v_ship_to_address1'] = $val['v_ship_to_address1'];
                            $vals['v_ship_to_city'] = $val['v_ship_to_city'];
                            $vals['v_ship_to_state'] = $val['v_ship_to_state'];
                            $vals['v_ship_to_zip'] = $val['v_ship_to_zip'];
                            $vals['v_ship_to_phonenumber'] = $val['v_ship_to_phonenumber'];
                            $vals['v_ship_to_email'] = $val['v_ship_to_email'];
                            $vals['v_website'] = $val['v_website'];
                            $vals['f_cogs'] = $val['f_cogs'];
                            $vals['v_special_instruction'] = $val['v_special_instruction'];
                            if($val['d_back_ordered_date'] != '' && $val['d_back_ordered_date'] != '0000-00-00'){ $back_order_Date = date('m/d/Y', strtotime($val['d_back_ordered_date'])); } else { $back_order_Date =''; }
                            $vals['d_back_ordered_date'] = $back_order_Date;
                            $vals['v_canceled_reason'] = $val['v_canceled_reason']; 
                            $vals['created_at'] = date('m/d/Y h:i:s', strtotime($val['created_at']));
                            $vals['updated_at'] = date('m/d/Y h:i:s', strtotime($val['updated_at']));
                            $sheet->row($intCount, $vals);
                            $intCount++;
                            $srNo++;
                        }
                    }); 
                })->export('xls');
            }
       } else{
            Session::flash('alert-message', 'No data found.');
            return Redirect(ADMIN_URL . 'order');
        }
    }
    public function anyOrderDetail(Request $request,$id) //Employee Listing
    {
        $data = Request::all();
        $user = Auth::guard('admin')->user();
        if ($data) {

            $query = new OrderDetail();
            $sortColumn = array('', 'v_vendor_name','v_shipment_number','d_order_date', 'v_bill_to_lastname','v_bill_to_firstname','v_bill_to_zip','v_item_code','v_description','e_item_status');
                $query=$query->with("Order",'Order.Vendor')->whereHas('Order', function($q) use($id){
                    $q->where('tbl_order.v_shipment_number',$id);
                });
            if(isset($data['v_vendor_name']) && $data['v_vendor_name'] != '') {
                 $query = $query->whereHas('Vendor', function($q) use($data){
                    $q->where('tbl_vendor.v_vendor_name', 'LIKE', '%' . trim($data['v_vendor_name']) . '%');
                });
            }
            if (isset($data['v_shipment_number']) && $data['v_shipment_number'] != '') {
                $query = $query->where('v_shipment_number', 'LIKE', '%' . trim($data['v_shipment_number']) . '%');
            }            
            if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "" && $data['from_date'] == $data['to_date'] ) {
               $query = $query->where('tbl_order.d_order_date','LIKE','%'. date('Y-m-d',strtotime($data['from_date'])).'%');
            }else if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "") {
                $data['to_date'] = $data['to_date']. "23:59:59";
                $query = $query->whereBetween('tbl_order.d_order_date',  array(date('Y-m-d H:i:s',strtotime($data['from_date'])),date('Y-m-d H:i:s',strtotime($data['to_date']))));
            }else  if (isset($data['from_date']) && $data['from_date'] != ""){
                  $query = $query->where('tbl_order.d_order_date','>=',date('Y-m-d',strtotime($data['from_date'])));
            }else if (isset($data['to_date']) && $data['to_date'] != ""){
                 $data['to_date'] = $data['to_date']. "23:59:59";
                  $query = $query->where('tbl_order.d_order_date','<=',date('Y-m-d H:i:s',strtotime($data['to_date'])));
            } 
            if(isset($data['v_bill_to_lastname']) && $data['v_bill_to_lastname'] != '') {
                $query = $query->where('v_bill_to_lastname', 'LIKE', '%' . trim($data['v_bill_to_lastname']) . '%');
            }           
            if(isset($data['v_bill_to_firstname']) && $data['v_bill_to_firstname'] != '') {
                 $query = $query->where('v_bill_to_firstname', 'LIKE', '%' . trim($data['v_bill_to_firstname']) . '%');
            }           
            if(isset($data['v_bill_to_zip']) && $data['v_bill_to_zip'] != '') {
                $query = $query->where('v_bill_to_zip', 'LIKE', '%' . trim($data['v_bill_to_zip']) . '%');
            }
            if(isset($data['v_item_code']) && $data['v_item_code'] != '') {
                 $query = $query->whereHas('OrderDetails', function($q) use($data){
                    $q->where('tbl_order_details.v_item_code', 'LIKE', '%' . trim($data['v_item_code']) . '%');
                });
            }
            if(isset($data['v_description']) && $data['v_description'] != '') {
                  $query = $query->whereHas('OrderDetails', function($q) use($data){
                    $q->where('tbl_order_details.v_description', 'LIKE', '%' . trim($data['v_description']) . '%');
                });
            }
            if(isset($data['e_item_status']) && $data['e_item_status'] != '') {
                 $query = $query->whereHas('OrderDetails', function($q) use($data){
                    $q->where('tbl_order_details.e_item_status', 'LIKE', '%' . trim($data['e_item_status']) . '%');
                });
            }         
            $rec_per_page = REC_PER_PAGE;

            if (isset($data['length'])) {
                if ($data['length'] == '-1') {
                    $rec_per_page = '';
                } else {
                    $rec_per_page = $data['length'];
                }
            }

            $sort_order = $data['order']['0']['dir'];
            $order_field = $sortColumn[$data['order']['0']['column']];

            if ($sort_order != '' && $order_field != '') {
                if($order_field == 'v_vendor_name'){
                     $query = $query->join('tbl_vendor','tbl_vendor.id','=','tbl_order.i_vendor_id')->orderBy('tbl_vendor.v_vendor_name',$sort_order);
                }else if($order_field == 'v_item_code'){
                     $query = $query->join('tbl_order_details','tbl_order.id','=','tbl_order_details.i_order_id')->orderBy('tbl_order_details.v_item_code',$sort_order);
                }else if($order_field == 'v_description'){
                     $query = $query->join('tbl_order_details','tbl_order.id','=','tbl_order_details.i_order_id')->orderBy('tbl_order_details.v_description',$sort_order);
                }else if($order_field == 'e_item_status'){
                     $query = $query->join('tbl_order_details','tbl_order.id','=','tbl_order_details.i_order_id')->orderBy('tbl_order_details.e_item_status',$sort_order);
                }else
                {
                    $query = $query->orderBy($order_field, $sort_order);
                }
                
            } else {
                // $query = $query->join('tbl_order_details','tbl_order.id','=','tbl_order_details.i_order_id')->orderBy('tbl_order_details.id','desc');
                $query = $query->orderBy('id', 'desc');
            }
            if($rec_per_page == ''){
                $temp_records = $query->get();
                $rec_per_page = count($temp_records);
                $users = $query->paginate($rec_per_page);
            } else {
                $users = $query->paginate($rec_per_page);
            }
            //$users = $query->paginate($rec_per_page);
            $arrUsers = $users->toArray();  
            // dd($arrUsers);
            $data = array();
            foreach ($arrUsers['data'] as $key => $val) {
                $index = 0;
               // $subTotal=0;
               // $subTotal=$val['i_quanity'] * $val['f_cogs'];
                // $data[$key][$index++] = '<input type="checkbox" name="id[]" value="' . $val['id'] . '" class="delete_' . $val['id'] . '">';
                $data[$key][$index++] = $val['v_item_code'];
                $data[$key][$index++] = $val['v_supplier_code'];
                $data[$key][$index++] = $val["v_description"];
                $data[$key][$index++] = $val['i_quanity'];
                $data[$key][$index++] = $val['f_cogs'];
                $status='<select class="form-control form-filter input-sm item_status" name="e_item_status-'.$val['id'].'" id="e_item_status-'.$val['id'].'">
                            <option value="">Select...</option>
                            <option value="Pending"'.(($val['e_item_status']=="Pending")?"selected='selected'":"").'>Pending</option>
                            <option value="Confirm and Approved" '.(($val['e_item_status']=="Confirm and Approved")?"selected='selected'":"").'>Confirm and Approved</option>
                            <option value="Back Ordered" '.(($val['e_item_status']=="Back Ordered")?"selected='selected'":"").'>Back Ordered</option>
                            <option value="Canceled" '.(($val['e_item_status']=="Canceled")?"selected='selected'":"").'>Canceled</option>
                            <option value="Hold - Do not split ship" '.(($val['e_item_status']=="Hold - Do not split ship")?"selected='selected'":"").'>Hold - Do not split ship</option>
                        </select>';
                $data[$key][$index++] = $status;
                $back_order_date='';
                if($val['d_back_ordered_date']!='0000-00-00' && $val['d_back_ordered_date']!='')
                {
                    $back_order_date=date('m/d/Y', strtotime($val['d_back_ordered_date']));
                }else{
                    $back_order_date='';
                }
                $d_back_ordered_date='';
                if($val['e_item_status']=='Back Ordered'){
                    $back_order_date='<input type="text" name="d_back_ordered_date-'.$val['id'].'" id="d_back_ordered_date-'.$val['id'].'" class="d_back_ordered_date datetimepicker form-control form-filter input-sm" placeholder="Back order date" value="'.$back_order_date.'" />';
                }else
                {
                    $back_order_date='<input type="text" name="d_back_ordered_date-'.$val['id'].'" id="d_back_ordered_date-'.$val['id'].'" class="form-control form-filter input-sm" placeholder="Back order date" value="'.$back_order_date.'" readonly/>';
                }
                $data[$key][$index++] = $back_order_date;
                $cancel_rsn='';
                /*if($val['e_item_status']=='Canceled'){
                    $cancel_rsn='<textarea id="v_canceled_reason-'.$val['id'].'" name="v_canceled_reason-'.$val['id'].'" class="form-control v_canceled_reason" placeholder="Canceled reason">'.$val['v_canceled_reason'].'</textarea>';
                }else
                {
                    $cancel_rsn='<textarea id="v_canceled_reason-'.$val['id'].'" name="v_canceled_reason-'.$val['id'].'" class="form-control v_canceled_reason" placeholder="Canceled reason" readonly>'.$val['v_canceled_reason'].'</textarea>';
                }*/
                $cancel_rsn='<textarea id="v_canceled_reason-'.$val['id'].'" name="v_canceled_reason-'.$val['id'].'" class="form-control v_canceled_reason" placeholder="Notes">'.$val['v_canceled_reason'].'</textarea>';
                
                $data[$key][$index++] = $cancel_rsn;
                $data[$key][$index++] = $val['order']['vendor']['v_vendor_name'];
                if($user->role == 1){
                    $action = '<div class="actions"><a href="javascript:;" id="delete_record" rel="' . $val['id'] . '" delete-url="' . ADMIN_URL . 'order/order-details-delete/' . $val['id'] . '" class="btn default btn-xs black delete" title="Delete"><i class="icon-trash"></i></a>';
                    $data[$key][$index++] = $action;
                }
            }
            $return_data['data'] = $data;
            $return_data['recordsTotal'] = $arrUsers['total'];
            $return_data['recordsFiltered'] = $arrUsers['total'];
            $return_data['data_array'] = $arrUsers['data'];
            return $return_data;
        }
    }
    public function anyEditStatus($id)
    {
        $inputs=Input::all();
        $flag=0;
        $order=Order::select('id')->where('v_shipment_number',$inputs['order_shiping_number'])->get()->toArray();        
        $order_ids=array();
        foreach ($order as $key => $value) {            
            $order_ids[]=$value['id'];
        }        
        $order_details = OrderDetail::whereIn('i_order_id',$order_ids)->get();
        foreach ($order_details as $key => $value) {
            $order_details_obj=OrderDetail::find($value->id);
            $order_details_obj->e_item_status = trim($inputs['e_item_status-'.$value->id]);
            if($inputs['e_item_status-'.$value->id]=='Back Ordered' && $inputs['d_back_ordered_date-'.$value->id]!=''){
                $order_details_obj->d_back_ordered_date = date('Y-m-d',strtotime(trim($inputs['d_back_ordered_date-'.$value->id]))); 
                $order_details_obj->v_canceled_reason = trim($inputs['v_canceled_reason-'.$value->id]);
            }else if($inputs['e_item_status-'.$value->id]=='Canceled' && $inputs['v_canceled_reason-'.$value->id]!=''){
                $order_details_obj->v_canceled_reason = trim($inputs['v_canceled_reason-'.$value->id]);
                $order_details_obj->d_back_ordered_date='';
            }else{
                $order_details_obj->d_back_ordered_date='';
                $order_details_obj->v_canceled_reason = trim($inputs['v_canceled_reason-'.$value->id]);
            }
            
            if($order_details_obj->save())
            {                
                $flag=1;
            }else
            {                
                $flag=0;
                exit;
            }

        }
        if($flag==1)
        {
            Session::flash('success-message', 'Order has been updated successfully.');
            return "TRUE";
        }else
        {
            return "FALSE";
        }
    }
    public function anyDropshipOrder($id)
    {
        $user = Auth::guard('admin')->user();    
        if($user->role==2){            
            $order=Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->where("i_client_id",$user->i_client_id)->first();
                if((!empty($order)) && $order!=''){
                $order=$order->toArray();
            }
        }else if($user->role==3){             
             $order=Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->where("i_vendor_id",$user->i_vendor_id)->first();
             if((!empty($order)) && $order!=''){
                $order=$order->toArray();
            }
        }else if($user->role==4){
            $employee_data=Employee::where('id',$user->i_emp_id)->first();            
            $order=Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->where("i_vendor_id",$employee_data->i_vendor_id)->first();
            if((!empty($order)) && $order!=''){
                $order=$order->toArray();
            }
        }else{
            $order=Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->first();
            if((!empty($order)) && $order!=''){
                $order=$order->toArray();
            }
        }     
        if((!empty($order)) && $order!=''){
                $client=Client::with("Admin")->where('id',$order['i_client_id'])->first()->toArray();
            }        
        if((!empty($order)) && $order!=''){
            return View('admin.order.dropship_order', array('order' => $order,'client'=>$client, 'title' => 'Dropship Order')); 
        }else{            
            return Redirect(ADMIN_URL . 'order');
        }       
    }
    public function anyBillSlip($id)
    {
        $user = Auth::guard('admin')->user();    
        if($user->role==2){            
            $order=Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->where("i_client_id",$user->i_client_id)->first();
                if((!empty($order)) && $order!=''){
                $order=$order->toArray();
            }
        }else if($user->role==3){             
             $order=Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->where("i_vendor_id",$user->i_vendor_id)->first();
             if((!empty($order)) && $order!=''){
                $order=$order->toArray();
            }
        }else if($user->role==4){
            $employee_data=Employee::where('id',$user->i_emp_id)->first();            
            $order=Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->where("i_vendor_id",$employee_data->i_vendor_id)->first();
            if((!empty($order)) && $order!=''){
                $order=$order->toArray();
            }
        }else{
            $order=Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->first();
            if((!empty($order)) && $order!=''){
                $order=$order->toArray();
            }
        }     
        if((!empty($order)) && $order!=''){
                $client=Client::with("Admin")->where('id',$order['i_client_id'])->first()->toArray();
            }        
        if((!empty($order)) && $order!=''){
            return View('admin.order.billslip', array('order' => $order,'client'=>$client, 'title' => 'BillSlip Order'));                
        }else{
            // return Redirect()->back();
            return Redirect(ADMIN_URL . 'order');
        }
    }
    public function anyImportToExcelOrder()
    {
        $auth_user = Auth::guard('admin')->user();
        $data = Input::all();
        if(!empty($data)){
            if(isset($data['id']) && $data['id'] != ''){
                $client_id = $data['id'];
            } else {
                $client_id = $auth_user->i_client_id; 
            }
        } else {
            $client_id = $auth_user->i_client_id; 
        }
        
        $file_flag = 0;
        $folder_name = Client::find($client_id);
        $folder_name = $folder_name->v_folder_name;
        $dir = WWW_ROOT.CLIENT_FOLDER_PATH.$folder_name;
        $destination =  WWW_ROOT.FEED_BACKUP_PATH;
        $source =  WWW_ROOT.CLIENT_FOLDER_PATH;
        $cFileArr_Err = array();
        $cFileArr = array();
        if(is_dir($dir)){
            $files =  glob($dir . "/*.txt");
            foreach($files as $file) {
                rename($file, str_replace(".txt","_".time().".csv",$file));	 //convert text file to csv
            }
            $files = glob($dir . "/*.csv");
            $file_num = 1;
            if (!empty($files)) {
                $total_files = 0;
                $total_data_feed = 0;
                $total_order = 0;
                $total_order_details = 0;
                $if_count = 0;
                $else_count = 0;
                for($f = 0; $f < count($files); $f++) {
                    $pathtofile = $files[$f];
                    $file_name_copy = str_replace($source,'',$files[$f]);
                    $extension = pathinfo($pathtofile,PATHINFO_EXTENSION);
                    if($extension == 'csv') {
                        $total_count = 0;
                        $objPHPExcel = PHPExcel_IOFactory::load($files[$f]);
                        $no_of_colums = "30";
                        $columns_empty = $objPHPExcel->getActiveSheet(0)->toArray()[0]; 
                        $columns = array_filter($columns_empty);
                        if($no_of_colums == count($columns)) {
                            foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
                                if($key == 0) {
                                    $datafeed_data = $worksheet->toArray();
                                    $total_count = count($datafeed_data);
                                }
                            }
                            $i = 1;
                            foreach($datafeed_data as $key=>$val){
                                if ($key != 0 && !empty($val) && array_filter($val)) {
                                    $data_feed = new DataFeed;
                                    $insert_array = array('i_client_id', 'v_bill_to_firstname', 'v_bill_to_lastname', 'v_bill_to_address1', 'v_bill_to_address2', 'v_bill_to_city', 'v_bill_to_state', 'v_bill_to_zip', 'v_bill_to_phonenumber', 'v_bill_to_email', 'v_ship_to_firstname', 'v_ship_to_lastname', 'v_ship_to_company', 'v_ship_to_address1', 'v_ship_to_address2', 'v_ship_to_city', 'v_ship_to_state', 'v_ship_to_zip', 'v_ship_to_phonenumber', 'v_ship_to_email', 'v_shipment_number', 'd_order_date', 'v_website', 'v_item_code', 'v_supplier_name', 'v_supplier_code', 'v_description', 'i_quanity', 'f_cogs', 'v_special_instruction', 'v_gift_message', 'i_file_number','created_at','updated_at');
                                    $data_feed->i_client_id = $client_id;
                                    $data_feed->v_bill_to_firstname = (isset($val[0]))?$val[0]:'';
                                    $data_feed->v_bill_to_lastname = (isset($val[1]))?$val[1]:'';
                                    $data_feed->v_bill_to_address1 = (isset($val[2]))?$val[2]:'';
                                    $data_feed->v_bill_to_address2 = (isset($val[3]))?$val[3]:'';
                                    $data_feed->v_bill_to_city = (isset($val[4]))?$val[4]:'';
                                    $data_feed->v_bill_to_state = (isset($val[5]))?$val[5]:'';
                                    $data_feed->v_bill_to_zip = (isset($val[6]))?$val[6]:'';
                                    $data_feed->v_bill_to_phonenumber = (isset($val[7]))?$val[7]:'';
                                    $data_feed->v_bill_to_email = (isset($val[8]))?$val[8]:'';
                                    $data_feed->v_ship_to_firstname = (isset($val[9]))?$val[9]:'';
                                    $data_feed->v_ship_to_lastname = (isset($val[10]))?$val[10]:'';
                                    $data_feed->v_ship_to_company = (isset($val[11]))?$val[11]:'';
                                    $data_feed->v_ship_to_address1 = (isset($val[12]))?$val[12]:'';
                                    $data_feed->v_ship_to_address2 = (isset($val[13]))?$val[13]:'';
                                    $data_feed->v_ship_to_city = (isset($val[14]))?$val[14]:'';
                                    $data_feed->v_ship_to_state = (isset($val[15]))?$val[15]:'';
                                    $data_feed->v_ship_to_zip = (isset($val[16]))?$val[16]:'';
                                    $data_feed->v_ship_to_phonenumber = (isset($val[17]))?$val[17]:'';
                                    $data_feed->v_ship_to_email = (isset($val[18]))?$val[18]:'';
                                    $data_feed->v_shipment_number = (isset($val[19]))?$val[19]:'';
                                    $data_feed->d_order_date = (isset($val[20]))?$val[20]:'';
                                    $data_feed->v_website = (isset($val[21]))?$val[21]:'';
                                    $data_feed->v_item_code = (isset($val[22]))?$val[22]:'';
                                    $data_feed->v_supplier_name = (isset($val[23]))?$val[23]:'';
                                    $data_feed->v_supplier_code = (isset($val[24]))?$val[24]:'';
                                    $data_feed->v_description = (isset($val[25]))?$val[25]:'';
                                    $data_feed->i_quanity = (isset($val[26]))?$val[26]:'';
                                    $data_feed->f_cogs = (isset($val[27]))?$val[27]:'';
                                    $data_feed->v_special_instruction = (isset($val[28]))?$val[28]:'';
                                    $data_feed->v_gift_message =(isset($val[29]))?$val[29]:'';
                                    $data_feed->i_file_number = $f + 1;
                                    $data_feed->created_at = date("Y-m-d h:i:s", time() + $i);
                                    $data_feed->updated_at = date("Y-m-d h:i:s", time() + $i);
                                    $data_feed->save();
                                    $i++;
                                }
                                $filename 	= str_replace(WWW_ROOT.CLIENT_FOLDER_PATH,"",$files[$f]);
                                $cFileArr[$f]['filename']	= $filename;
                                $cFileArr[$f]['line_count'] = $total_count-1;
                                $cFileArr[$f]['line_get'] = $i - 1;
                                //$cFileArr[$f]['line_get'] = 0 ;
                            }
                        } else {
                            $filename_error	= str_replace(WWW_ROOT.CLIENT_FOLDER_PATH,"",$files[$f]);
                            $cFileArr_Err[$f]['filename']	= $filename_error;
                        }
                    }
                    //copy($files[$f], $destination.$file_name_copy);
                    @unlink($files[$f]);
                    $file_flag = 1;
                    $file_num++;
                    $total_files++;
                }     
                $selQry = DataFeed::where('i_client_id',$client_id)->select(DB::raw('DISTINCT(v_shipment_number)'))->get();
                $rec = $selQry->toArray();
                for($i = 0; $i < count($rec); $i++) {
                    $flag = 1;
                    $subSelQry = DataFeed::where('v_shipment_number',$rec[$i]['v_shipment_number'])->where('i_client_id',$client_id)->get();
                    $subrec = $subSelQry->toArray();
                    //$cFileArr[$f]['line_get'] = 0;
                    for($j= 0; $j < count($subrec); $j++) {
                        if(trim($subrec[$j]['v_item_code']) != '' and trim($subrec[$j]['v_supplier_code'])!='') {
                            if($flag == 1) {
                               $iClientId = $client_id;
                                $iVendorId  = $this->getVendorRelationId($subrec[$j]['v_supplier_name'],$client_id);
                                if(trim($iClientId)!='' and trim($iVendorId)!='')
                                {
                                   // $order_duplicate = Order::where('v_shipment_number',$subrec[$j]['v_shipment_number'])->count();
                                   // if($order_duplicate > 0){
                                        $order =  new Order;
                                        $order->i_client_id = (isset($iClientId))?$iClientId:'';
                                        $order->i_vendor_id = (isset($iVendorId))?$iVendorId:''; 
                                        $order->v_bill_to_firstname = (isset($subrec[$j]['v_bill_to_firstname']))?$subrec[$j]['v_bill_to_firstname']:''; 
                                        $order->v_bill_to_lastname = (isset($subrec[$j]['v_bill_to_lastname']))?$subrec[$j]['v_bill_to_lastname']:''; 
                                        $order->v_bill_to_address1 = (isset($subrec[$j]['v_bill_to_address1']))?$subrec[$j]['v_bill_to_address1']:''; 
                                        $order->v_bill_to_address2 = (isset($subrec[$j]['v_bill_to_address2']))?$subrec[$j]['v_bill_to_address2']:''; 
                                        $order->v_bill_to_city = (isset($subrec[$j]['v_bill_to_city']))?$subrec[$j]['v_bill_to_city']:''; 
                                        $order->v_bill_to_state = (isset($subrec[$j]['v_bill_to_state']))?$subrec[$j]['v_bill_to_state']:''; 
                                        $order->v_bill_to_zip = (isset($subrec[$j]['v_bill_to_zip']))?$subrec[$j]['v_bill_to_zip']:''; 
                                        $order->v_bill_to_phonenumber = (isset($subrec[$j]['v_bill_to_phonenumber']))?$subrec[$j]['v_bill_to_phonenumber']:''; 
                                        $order->v_bill_to_email = (isset($subrec[$j]['v_bill_to_email']))?$subrec[$j]['v_bill_to_email']:''; 
                                        $order->v_ship_to_firstname = (isset($subrec[$j]['v_ship_to_firstname']))?$subrec[$j]['v_ship_to_firstname']:''; 
                                        $order->v_ship_to_lastname = (isset($subrec[$j]['v_ship_to_lastname']))?$subrec[$j]['v_ship_to_lastname']:''; 
                                        $order->v_ship_to_company = (isset($subrec[$j]['v_ship_to_company']))?$subrec[$j]['v_ship_to_company']:''; 
                                        $order->v_ship_to_address1 = (isset($subrec[$j]['v_ship_to_address1']))?$subrec[$j]['v_ship_to_address1']:''; 
                                        $order->v_ship_to_address2 = (isset($subrec[$j]['v_ship_to_address2']))?$subrec[$j]['v_ship_to_address2']:''; 
                                        $order->v_ship_to_city = (isset($subrec[$j]['v_ship_to_city']))?$subrec[$j]['v_ship_to_city']:''; 
                                        $order->v_ship_to_state = (isset($subrec[$j]['v_ship_to_state']))?$subrec[$j]['v_ship_to_state']:''; 
                                        $order->v_ship_to_zip = (isset($subrec[$j]['v_ship_to_zip']))?$subrec[$j]['v_ship_to_zip']:''; 
                                        $order->v_ship_to_phonenumber = (isset($subrec[$j]['v_ship_to_phonenumber']))?$subrec[$j]['v_ship_to_phonenumber']:''; 
                                        $order->v_ship_to_email = (isset($subrec[$j]['v_ship_to_email']))?$subrec[$j]['v_ship_to_email']:''; 
                                        $order->v_shipment_number = (isset($subrec[$j]['v_shipment_number']))?$subrec[$j]['v_shipment_number']:''; 
                                        $order->d_order_date = (isset($subrec[$j]['d_order_date']))?$subrec[$j]['d_order_date']:''; 
                                        $order->v_website = (isset($subrec[$j]['v_website']))?$subrec[$j]['v_website']:''; 
                                        $order->v_gift_message = (isset($subrec[$j]['v_gift_message']))?$subrec[$j]['v_gift_message']:''; 
                                        $order->save();
                                        $orderid = $order->id;
                                        $flag = 0;
                                   // }
                                }
                            }
                            if(trim($iClientId)!='' and trim($iVendorId)!='')
                            {
                                $FileNum = $subrec[$j]['i_file_number'] - 1;
                               //$cFileArr[$FileNum]['line_get'] = $cFileArr[$FileNum]['line_get'] + 1;
                                $order_detail =  New OrderDetail;
                                $order_detail->i_order_id = (isset($orderid))?$orderid:'';
                                $order_detail->v_item_code = (isset($subrec[$j]['v_item_code']))?$subrec[$j]['v_item_code']:'';
                                $order_detail->v_supplier_code = (isset($subrec[$j]['v_supplier_code']))?$subrec[$j]['v_supplier_code']:'';
                                $order_detail->i_quanity = (isset($subrec[$j]['i_quanity']))?$subrec[$j]['i_quanity']:'';
                                $order_detail->v_description = (isset($subrec[$j]['v_description']))?$subrec[$j]['v_description']:'';
                                $order_detail->f_cogs = (isset($subrec[$j]['f_cogs']))?$subrec[$j]['f_cogs']:'';
                                $order_detail->v_special_instruction = (isset($subrec[$j]['v_special_instruction']))?$subrec[$j]['v_special_instruction']:'';
                                $order_detail->save();
                                
                            }
                    }
                }
            } 
            DataFeed::where('i_client_id',$client_id)->delete();
            $message = '';
            if(isset($cFileArr) && !empty($cFileArr)){
                foreach($cFileArr as $file){
                    $message.= "Total <strong>".$file['line_get']."</strong> records are imported <strong>out of ".$file['line_count']."</strong> from file name '".$file['filename']."'.<br />";
                }
            }
            $message_err = '';
            if(isset($cFileArr_Err) && !empty($cFileArr_Err)){
                foreach($cFileArr_Err as $file_err){
                    $message_err.= "File name '".$file_err['filename']."' is not imported. Please check the format of file.<br />";
                }
            }
            if($message_err != ''){
                Session::flash('alert-message', $message_err);
            }
            Session::flash('success-message', $message);
            return 'TRUE';
        } else {
                Session::flash('alert-message', 'There is no file to import. Please try again later.');
                return 'TRUE';
            }
        } else {
            Session::flash('alert-message', ' Folder '.$folder_name.' does not exists.');
            return 'TRUE';
        }
}
public function anyCronForImportFeed() {
    $day = date('D');
    $time = date('h:i A');
    if((($day == 'Mon' || $day == 'Sun') && ($time ==  '06:30 AM' || $time == '03:30 PM')) || ($day == 'Fri' && $time == '12:30 PM' )) {
    $path =  WWW_ROOT.CLIENT_FOLDER_PATH;
    $dirs = array();
    $dir = dir($path);
    while (false !== ($entry = $dir->read())) {
        if ($entry != '.' && $entry != '..') {
            if (is_dir($path . '/' .$entry)) {
                $dirs[] = $entry; 
            }
        }
    }
    $alert_msg = '';
    $success_msg = '';
    $total_datafeed = 0;
    $client_data = Client::where('e_status','Active')->pluck('id','v_folder_name')->toArray();
    $source =  WWW_ROOT.CLIENT_FOLDER_PATH;
    $message_err = '';
    $message = '';
    $no_data_folder = '';
    $no_folder = '';
    $folder_arr = array();
    foreach($dirs as $key => $folder_name){
        if(array_key_exists($folder_name, $client_data)){
            array_push($folder_arr,$folder_name);
        }
    }
    foreach($folder_arr as $key => $folder_name){
        $cFileArr_Err = array();
        $cFileArr = array();
        if(array_key_exists($folder_name, $client_data)){
            $client_id = $client_data[$folder_name];
        }
        $dir = WWW_ROOT.CLIENT_FOLDER_PATH.$folder_name;
    
       if(is_dir($dir)){
            $files =  glob($dir . "/*.txt");
            foreach($files as $file) {
                rename($file, str_replace(".txt","_".time().".csv",$file));	 //convert text file to csv
            }
            $files = glob($dir . "/*.csv");
            $file_num = 1;
            if (!empty($files)) {
                $total_files = 0;
                $total_data_feed = 0;
                $total_order = 0;
                $total_order_details = 0;
                $if_count = 0;
                $else_count = 0;
                for($f = 0; $f < count($files); $f++) {
                    $pathtofile = $files[$f];
                    $file_name_copy = str_replace($source,'',$files[$f]);
                    $extension = pathinfo($pathtofile,PATHINFO_EXTENSION);
                    if($extension == 'csv') {
                        $total_count = 0;
                        $objPHPExcel = PHPExcel_IOFactory::load($files[$f]);
                        $no_of_colums = "30";
                        $columns_empty = $objPHPExcel->getActiveSheet(0)->toArray()[0]; 
                        $columns = array_filter($columns_empty);
                        if($no_of_colums == count($columns)) {
                            foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
                                if($key == 0) {
                                    $datafeed_data = $worksheet->toArray();
                                    $total_count = count($datafeed_data);
                                }
                            }
                            $i = 1;
                            foreach($datafeed_data as $key=>$val){
                                if ($key != 0 && !empty($val) && array_filter($val)) {
                                    $data_feed = new DataFeed;
                                    $insert_array = array('i_client_id', 'v_bill_to_firstname', 'v_bill_to_lastname', 'v_bill_to_address1', 'v_bill_to_address2', 'v_bill_to_city', 'v_bill_to_state', 'v_bill_to_zip', 'v_bill_to_phonenumber', 'v_bill_to_email', 'v_ship_to_firstname', 'v_ship_to_lastname', 'v_ship_to_company', 'v_ship_to_address1', 'v_ship_to_address2', 'v_ship_to_city', 'v_ship_to_state', 'v_ship_to_zip', 'v_ship_to_phonenumber', 'v_ship_to_email', 'v_shipment_number', 'd_order_date', 'v_website', 'v_item_code', 'v_supplier_name', 'v_supplier_code', 'v_description', 'i_quanity', 'f_cogs', 'v_special_instruction', 'v_gift_message', 'i_file_number','created_at','updated_at');
                                    $data_feed->i_client_id = $client_id;
                                    $data_feed->v_bill_to_firstname = (isset($val[0]))?$val[0]:'';
                                    $data_feed->v_bill_to_lastname = (isset($val[1]))?$val[1]:'';
                                    $data_feed->v_bill_to_address1 = (isset($val[2]))?$val[2]:'';
                                    $data_feed->v_bill_to_address2 = (isset($val[3]))?$val[3]:'';
                                    $data_feed->v_bill_to_city = (isset($val[4]))?$val[4]:'';
                                    $data_feed->v_bill_to_state = (isset($val[5]))?$val[5]:'';
                                    $data_feed->v_bill_to_zip = (isset($val[6]))?$val[6]:'';
                                    $data_feed->v_bill_to_phonenumber = (isset($val[7]))?$val[7]:'';
                                    $data_feed->v_bill_to_email = (isset($val[8]))?$val[8]:'';
                                    $data_feed->v_ship_to_firstname = (isset($val[9]))?$val[9]:'';
                                    $data_feed->v_ship_to_lastname = (isset($val[10]))?$val[10]:'';
                                    $data_feed->v_ship_to_company = (isset($val[11]))?$val[11]:'';
                                    $data_feed->v_ship_to_address1 = (isset($val[12]))?$val[12]:'';
                                    $data_feed->v_ship_to_address2 = (isset($val[13]))?$val[13]:'';
                                    $data_feed->v_ship_to_city = (isset($val[14]))?$val[14]:'';
                                    $data_feed->v_ship_to_state = (isset($val[15]))?$val[15]:'';
                                    $data_feed->v_ship_to_zip = (isset($val[16]))?$val[16]:'';
                                    $data_feed->v_ship_to_phonenumber = (isset($val[17]))?$val[17]:'';
                                    $data_feed->v_ship_to_email = (isset($val[18]))?$val[18]:'';
                                    $data_feed->v_shipment_number = (isset($val[19]))?$val[19]:'';
                                    $data_feed->d_order_date = (isset($val[20]))?$val[20]:'';
                                    $data_feed->v_website = (isset($val[21]))?$val[21]:'';
                                    $data_feed->v_item_code = (isset($val[22]))?$val[22]:'';
                                    $data_feed->v_supplier_name = (isset($val[23]))?$val[23]:'';
                                    $data_feed->v_supplier_code = (isset($val[24]))?$val[24]:'';
                                    $data_feed->v_description = (isset($val[25]))?$val[25]:'';
                                    $data_feed->i_quanity = (isset($val[26]))?$val[26]:'';
                                    $data_feed->f_cogs = (isset($val[27]))?$val[27]:'';
                                    $data_feed->v_special_instruction = (isset($val[28]))?$val[28]:'';
                                    $data_feed->v_gift_message =(isset($val[29]))?$val[29]:'';
                                    $data_feed->i_file_number = $f + 1;
                                    $data_feed->created_at = date("Y-m-d h:i:s", time() + $i);
                                    $data_feed->updated_at = date("Y-m-d h:i:s", time() + $i);
                                    $data_feed->save();
                                    $i++;
                                }
                                $filename 	= str_replace(WWW_ROOT.CLIENT_FOLDER_PATH,"",$files[$f]);
                                $cFileArr[$f]['filename']	= $filename;
                                $cFileArr[$f]['line_count'] = $total_count-1;
                                $cFileArr[$f]['line_get'] = $i - 1;
                                //$cFileArr[$f]['line_get'] = 0 ;
                            }
                        } else {
                            $filename_error	= str_replace(WWW_ROOT.CLIENT_FOLDER_PATH,"",$files[$f]);
                            $cFileArr_Err[$f]['filename']	= $filename_error;
                        }
                    }
                    //copy($files[$f], $destination.$file_name_copy);
                    @unlink($files[$f]);
                    $file_flag = 1;
                    $file_num++;
                    $total_files++;
                }     
                $selQry = DataFeed::where('i_client_id',$client_id)->select(DB::raw('DISTINCT(v_shipment_number)'))->get();
                $rec = $selQry->toArray();
                for($i = 0; $i < count($rec); $i++) {
                    $flag = 1;
                    $subSelQry = DataFeed::where('v_shipment_number',$rec[$i]['v_shipment_number'])->where('i_client_id',$client_id)->get();
                    $subrec = $subSelQry->toArray();
                    //$cFileArr[$f]['line_get'] = 0;
                    for($j= 0; $j < count($subrec); $j++) {
                        if(trim($subrec[$j]['v_item_code']) != '' and trim($subrec[$j]['v_supplier_code'])!='') {
                            if($flag == 1) {
                               $iClientId = $client_id;
                                $iVendorId  = $this->getVendorRelationId($subrec[$j]['v_supplier_name'],$client_id);
                                if(trim($iClientId)!='' and trim($iVendorId)!='')
                                {
                                   // $order_duplicate = Order::where('v_shipment_number',$subrec[$j]['v_shipment_number'])->count();
                                   // if($order_duplicate > 0){
                                        $order =  new Order;
                                        $order->i_client_id = (isset($iClientId))?$iClientId:'';
                                        $order->i_vendor_id = (isset($iVendorId))?$iVendorId:''; 
                                        $order->v_bill_to_firstname = (isset($subrec[$j]['v_bill_to_firstname']))?$subrec[$j]['v_bill_to_firstname']:''; 
                                        $order->v_bill_to_lastname = (isset($subrec[$j]['v_bill_to_lastname']))?$subrec[$j]['v_bill_to_lastname']:''; 
                                        $order->v_bill_to_address1 = (isset($subrec[$j]['v_bill_to_address1']))?$subrec[$j]['v_bill_to_address1']:''; 
                                        $order->v_bill_to_address2 = (isset($subrec[$j]['v_bill_to_address2']))?$subrec[$j]['v_bill_to_address2']:''; 
                                        $order->v_bill_to_city = (isset($subrec[$j]['v_bill_to_city']))?$subrec[$j]['v_bill_to_city']:''; 
                                        $order->v_bill_to_state = (isset($subrec[$j]['v_bill_to_state']))?$subrec[$j]['v_bill_to_state']:''; 
                                        $order->v_bill_to_zip = (isset($subrec[$j]['v_bill_to_zip']))?$subrec[$j]['v_bill_to_zip']:''; 
                                        $order->v_bill_to_phonenumber = (isset($subrec[$j]['v_bill_to_phonenumber']))?$subrec[$j]['v_bill_to_phonenumber']:''; 
                                        $order->v_bill_to_email = (isset($subrec[$j]['v_bill_to_email']))?$subrec[$j]['v_bill_to_email']:''; 
                                        $order->v_ship_to_firstname = (isset($subrec[$j]['v_ship_to_firstname']))?$subrec[$j]['v_ship_to_firstname']:''; 
                                        $order->v_ship_to_lastname = (isset($subrec[$j]['v_ship_to_lastname']))?$subrec[$j]['v_ship_to_lastname']:''; 
                                        $order->v_ship_to_company = (isset($subrec[$j]['v_ship_to_company']))?$subrec[$j]['v_ship_to_company']:''; 
                                        $order->v_ship_to_address1 = (isset($subrec[$j]['v_ship_to_address1']))?$subrec[$j]['v_ship_to_address1']:''; 
                                        $order->v_ship_to_address2 = (isset($subrec[$j]['v_ship_to_address2']))?$subrec[$j]['v_ship_to_address2']:''; 
                                        $order->v_ship_to_city = (isset($subrec[$j]['v_ship_to_city']))?$subrec[$j]['v_ship_to_city']:''; 
                                        $order->v_ship_to_state = (isset($subrec[$j]['v_ship_to_state']))?$subrec[$j]['v_ship_to_state']:''; 
                                        $order->v_ship_to_zip = (isset($subrec[$j]['v_ship_to_zip']))?$subrec[$j]['v_ship_to_zip']:''; 
                                        $order->v_ship_to_phonenumber = (isset($subrec[$j]['v_ship_to_phonenumber']))?$subrec[$j]['v_ship_to_phonenumber']:''; 
                                        $order->v_ship_to_email = (isset($subrec[$j]['v_ship_to_email']))?$subrec[$j]['v_ship_to_email']:''; 
                                        $order->v_shipment_number = (isset($subrec[$j]['v_shipment_number']))?$subrec[$j]['v_shipment_number']:''; 
                                        $order->d_order_date = (isset($subrec[$j]['d_order_date']))?$subrec[$j]['d_order_date']:''; 
                                        $order->v_website = (isset($subrec[$j]['v_website']))?$subrec[$j]['v_website']:''; 
                                        $order->v_gift_message = (isset($subrec[$j]['v_gift_message']))?$subrec[$j]['v_gift_message']:''; 
                                        $order->save();
                                        $orderid = $order->id;
                                        $flag = 0;
                                   // }
                                }
                            }
                            if(trim($iClientId)!='' and trim($iVendorId)!='')
                            {
                                $FileNum = $subrec[$j]['i_file_number'] - 1;
                               //$cFileArr[$FileNum]['line_get'] = $cFileArr[$FileNum]['line_get'] + 1;
                                $order_detail =  New OrderDetail;
                                $order_detail->i_order_id = (isset($orderid))?$orderid:'';
                                $order_detail->v_item_code = (isset($subrec[$j]['v_item_code']))?$subrec[$j]['v_item_code']:'';
                                $order_detail->v_supplier_code = (isset($subrec[$j]['v_supplier_code']))?$subrec[$j]['v_supplier_code']:'';
                                $order_detail->i_quanity = (isset($subrec[$j]['i_quanity']))?$subrec[$j]['i_quanity']:'';
                                $order_detail->v_description = (isset($subrec[$j]['v_description']))?$subrec[$j]['v_description']:'';
                                $order_detail->f_cogs = (isset($subrec[$j]['f_cogs']))?$subrec[$j]['f_cogs']:'';
                                $order_detail->v_special_instruction = (isset($subrec[$j]['v_special_instruction']))?$subrec[$j]['v_special_instruction']:'';
                                $order_detail->save();
                                
                            }
                    }
                }
            } 
            DataFeed::where('i_client_id',$client_id)->delete();
            if(isset($cFileArr) && !empty($cFileArr)){
                foreach($cFileArr as $file){
                    $message.= "Total <strong>".$file['line_get']."</strong> records are imported <strong>out of ".$file['line_count']."</strong> from file name '".$file['filename']."'.<br />";
                }
            }
            if(isset($cFileArr_Err) && !empty($cFileArr_Err)){
                foreach($cFileArr_Err as $file_err){
                    $message_err.= "File name '".$file_err['filename']."' is not imported. Please check the format of file.<br />";
                }
            }
        } else {
            $no_data_folder.= 'There is no file to import from '.$folder_name.'. Please try again later. <br/>';
        }
        } else {
             $no_folder.= ' Folder '.$folder_name.' does not exists. <br/>';
        }
        
    }
    $mail_msg = '';
    if(isset($message) && !empty($message)){
        $mail_msg.= '<div class="success-msg"><span class="success-span">'.$message.' </span></div>';
    }
    if(isset($message_err) && !empty($message_err)){
        $mail_msg.= '<div class="alert-msg"><span class="alert-span">'.$message_err.' </span></div>';
    }
    if(isset($no_data_folder) && !empty($no_data_folder)){
        $mail_msg.= '<div class="alert-msg"><span class="alert-span">'.$no_data_folder.' </span></div>';
    }
    if(isset($no_folder) && !empty($no_folder)){
        $mail_msg.= '<div class="alert-msg"><span class="alert-span">'.$no_folder.' </span></div>';
    }
    /*$to = 'edward.shane@icloud.com';
    $subject = 'Dropship - Cron job import feed';
    Mail::send('admin.emails.auth.dropship-import-datafeed', array('strTemplate'=>$mail_msg), function($msg) use ($subject,$to){ 
                    $msg->to($to);   
                    $msg->subject($subject);                                
    });*/
    $to = 'testing.demo@gmail.com';
    $subject = 'Dropship - Cron job import feed';
    //$admin_email = ADMIN_EMAIL;
    $admin_email = 'testing.kancy@gmail.com';
    Mail::send('admin.emails.auth.dropship-import-datafeed', array('strTemplate'=>$mail_msg), function($msg) use ($subject,$to){ 
                    $msg->to($to);   
                    $msg->subject($subject);                                
    });
    echo "Done";
    }
}
/*public function anyCronForImportFeed() {
    $path =  WWW_ROOT.CLIENT_FOLDER_PATH;
    $dirs = array();
    $dir = dir($path);
    while (false !== ($entry = $dir->read())) {
        if ($entry != '.' && $entry != '..') {
            if (is_dir($path . '/' .$entry)) {
                $dirs[] = $entry; 
            }
        }
    }
    $alert_msg = '';
    $success_msg = '';
    $total_datafeed = 0;
    $client_data = Client::pluck('id','v_folder_name')->toArray();
    foreach($dirs as $key => $folder_name){
        if(array_key_exists($folder_name, $client_data)){
            $client_id = $client_data[$folder_name];
        }
        $dir = WWW_ROOT.CLIENT_FOLDER_PATH.$folder_name;
    
        if(is_dir($dir)){
            $files =  glob($dir . "/*.txt");
            foreach($files as $file) {
                rename($file, str_replace(".txt","_".time().".csv",$file));	 //convert text file to csv
            }
            $files = glob($dir . "/*.csv");
            $file_num = 1;
            if (!empty($files)) {
                $total_files = 0;
                $total_data_feed = 0;
                $total_order = 0;
                $total_order_details = 0;
                for($f = 0; $f < count($files); $f++) {
                    $pathtofile = $files[$f];
                    $extension = pathinfo($pathtofile,PATHINFO_EXTENSION);
                    if($extension == 'csv') {
                        $objPHPExcel = PHPExcel_IOFactory::load($files[$f]);
                        foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
                            if($key == 0) {
                                $datafeed_data = $worksheet->toArray();
                                $total_count = count($datafeed_data) - 1;
                            }
                        }
                        $i = 1;
                        foreach($datafeed_data as $key=>$val){
                            if ($key != 0 && !empty($val)) {
                                
                            $data_feed = new DataFeed;
                            $insert_array = array('i_client_id', 'v_bill_to_firstname', 'v_bill_to_lastname', 'v_bill_to_address1', 'v_bill_to_address2', 'v_bill_to_city', 'v_bill_to_state', 'v_bill_to_zip', 'v_bill_to_phonenumber', 'v_bill_to_email', 'v_ship_to_firstname', 'v_ship_to_lastname', 'v_ship_to_company', 'v_ship_to_address1', 'v_ship_to_address2', 'v_ship_to_city', 'v_ship_to_state', 'v_ship_to_zip', 'v_ship_to_phonenumber', 'v_ship_to_email', 'v_shipment_number', 'd_order_date', 'v_website', 'v_item_code', 'v_supplier_name', 'v_supplier_code', 'v_description', 'i_quanity', 'f_cogs', 'v_special_instruction', 'v_gift_message', 'i_file_number','created_at','updated_at');
                            $data_feed->i_client_id = $client_id;
                            $data_feed->v_bill_to_firstname = $val[0];
                            $data_feed->v_bill_to_lastname = $val[1];
                            $data_feed->v_bill_to_address1 = $val[2];
                            $data_feed->v_bill_to_address2 = $val[3];
                            $data_feed->v_bill_to_city = $val[4];
                            $data_feed->v_bill_to_state = $val[5];
                            $data_feed->v_bill_to_zip = $val[6];
                            $data_feed->v_bill_to_phonenumber = $val[7];
                            $data_feed->v_bill_to_email = $val[8];
                            $data_feed->v_ship_to_firstname = $val[9];
                            $data_feed->v_ship_to_lastname = $val[10];
                            $data_feed->v_ship_to_company = $val[11];
                            $data_feed->v_ship_to_address1 = $val[12];
                            $data_feed->v_ship_to_address2 = $val[13];
                            $data_feed->v_ship_to_city = $val[14];
                            $data_feed->v_ship_to_state = $val[15];
                            $data_feed->v_ship_to_zip = $val[16];
                            $data_feed->v_ship_to_phonenumber = $val[17];
                            $data_feed->v_ship_to_email = $val[18];
                            $data_feed->v_shipment_number = $val[19];
                            $data_feed->d_order_date = $val[20];
                            $data_feed->v_website = $val[21];
                            $data_feed->v_item_code = $val[22];
                            $data_feed->v_supplier_name = $val[23];
                            $data_feed->v_supplier_code = $val[24];
                            $data_feed->v_description = $val[25];
                            $data_feed->i_quanity = $val[26];
                            $data_feed->f_cogs = $val[27];
                            $data_feed->v_special_instruction = $val[28];
                            $data_feed->v_gift_message = $val[29];
                            $data_feed->i_file_number = $f;
                            $data_feed->created_at = date("Y-m-d h:i:s", time() + $i);
                            $data_feed->updated_at = date("Y-m-d h:i:s", time() + $i);
                            $data_feed->save();
                           
                            $filename 	= str_replace(CLIENT_FOLDER_PATH,"",$files[$f]);
                            $cFileArr[$f]['file_name']	= $files[$f];
                            $cFileArr[$f]['line_get'] = count($key);
                            $cFileArr[$f]['line_count'] = $total_datafeed;
                            $i++;
                            }
                        }
                        
                    }
                    
                    //@unlink($files[$f]);
                    $file_flag = 1;
                    $file_num++;
                    $total_files++;
                }
                $selQry = DataFeed::where('i_client_id',$client_id)->groupBy('v_shipment_number')->get();
                $rec = $selQry->toArray();
                for($i = 0; $i < count($rec); $i++) {
                    $flag = 1;
                    $subSelQry = DataFeed::where('v_shipment_number',$rec[$i]['v_shipment_number'])->where('i_client_id',$client_id)->get();
                    $subrec = $subSelQry->toArray();
                    for($j= 0; $j < count($subrec); $j++) {
                        if(trim($subrec[$j]['v_item_code']) != '' and trim($subrec[$j]['v_supplier_code'])!='') {
                            if($flag == 1) {
                                $subrec[$j]['v_supplier_name'];
                                $iClientId = $client_id;
                                $iVendorId  = $this->getVendorRelationId($subrec[$j]['v_supplier_name'],$client_id);
                                if(trim($iClientId)!='' and trim($iVendorId)!='')
                                {
                                    $order =  new Order;
                                    $order->i_client_id = $iClientId;
                                    $order->i_vendor_id = $iVendorId; 
                                    $order->v_bill_to_firstname = $subrec[$j]['v_bill_to_firstname']; 
                                    $order->v_bill_to_lastname = $subrec[$j]['v_bill_to_lastname']; 
                                    $order->v_bill_to_address1 = $subrec[$j]['v_bill_to_address1']; 
                                    $order->v_bill_to_address2 = $subrec[$j]['v_bill_to_address2']; 
                                    $order->v_bill_to_city = $subrec[$j]['v_bill_to_city']; 
                                    $order->v_bill_to_state = $subrec[$j]['v_bill_to_state']; 
                                    $order->v_bill_to_zip = $subrec[$j]['v_bill_to_zip']; 
                                    $order->v_bill_to_phonenumber = $subrec[$j]['v_bill_to_phonenumber']; 
                                    $order->v_bill_to_email = $subrec[$j]['v_bill_to_email']; 
                                    $order->v_ship_to_firstname = $subrec[$j]['v_ship_to_firstname']; 
                                    $order->v_ship_to_lastname = $subrec[$j]['v_ship_to_lastname']; 
                                    $order->v_ship_to_company = $subrec[$j]['v_ship_to_company'];
                                    $order->v_ship_to_address1 = $subrec[$j]['v_ship_to_address1']; 
                                    $order->v_ship_to_address2 = $subrec[$j]['v_ship_to_address2']; 
                                    $order->v_ship_to_city = $subrec[$j]['v_ship_to_city']; 
                                    $order->v_ship_to_state = $subrec[$j]['v_ship_to_state']; 
                                    $order->v_ship_to_zip = $subrec[$j]['v_ship_to_zip']; 
                                    $order->v_ship_to_phonenumber = $subrec[$j]['v_ship_to_phonenumber']; 
                                    $order->v_ship_to_email = $subrec[$j]['v_ship_to_email']; 
                                    $order->v_shipment_number = $subrec[$j]['v_shipment_number']; 
                                    $order->d_order_date = $subrec[$j]['d_order_date']; 
                                    $order->v_website = $subrec[$j]['v_website']; 
                                    $order->v_gift_message = $subrec[$j]['v_gift_message'];
                                    $order->save();
                                    $orderid = $order->id;
                                    $flag = 0;
                                }
                            }
                            if(trim($iClientId)!='' and trim($iVendorId)!='')
                            {
                                $FileNum = $subrec[$j]['i_file_number'];
                                //$cFileArr[$FileNum]['line_get'] = $cFileArr[$FileNum]['line_get'] + 1;
                               // $FileNum = $subrec[$j][FileNum];
				                $cFileArr[$FileNum]['line_get'] = $cFileArr[$FileNum]['line_get'] + 1;
                                $order_detail =  New OrderDetail;
                                $order_detail->i_order_id = $orderid;
                                $order_detail->v_item_code = $subrec[$j]['v_item_code'];
                                $order_detail->v_supplier_code = $subrec[$j]['v_supplier_code'];
                                $order_detail->i_quanity = $subrec[$j]['i_quanity'];
                                $order_detail->v_description = $subrec[$j]['v_description'];
                                $order_detail->f_cogs = $subrec[$j]['f_cogs'];
                                $order_detail->v_special_instruction = $subrec[$j]['v_special_instruction'];
                                $order_detail->save();
                            }
                    }
                }
                    DataFeed::where('i_client_id',$client_id)->delete();
                } 
                echo  'Folder '.$folder_name.'  files imported successfully.<br/>';
        } else {
                echo  'There is no file to import. Please try again later.<br/>';
            }
        } else {
            echo 'Folder '.$folder_name.' does not exists.<br/>';
            
        }
    }
}*/
public function getVendorRelationId($SupplierName,$client_id)
{
	$qry = Vendor::select('id')->where('v_company_name',$SupplierName)->where('i_client_id', $client_id)->get()->toArray();    
    if(isset($qry) && !empty($qry)){
        return $qry[0]['id'];
    } else {
        return '';
    }
	
}
public function anyCronDeleteOrders(){
    $orders = Order::select('id','d_order_date')->get();
    foreach ($orders as $key => $value) {
        $TimeGap   = round(abs(strtotime(date("Y-m-d"))-strtotime($value->d_order_date))/86400); 
        $OrderId   = $value->id;
        if($TimeGap >= 120)
        {            
            $order_details = OrderDetail::where('i_order_id',$OrderId)->select('id','e_item_status')->get();
            if(count($order_details)>0){
                $del_flag = 0;
                foreach($order_details as $val){
                    if($val->e_item_status == 'Confirm and Approved')
                    {
                      	$order_del_qry = OrderDetail::where('id',$val->id)->delete();
                        $del_flag++;
                    }
                }
                if($del_flag == count($order_details))
                {                    
                    $order_detail_del_qry = Order::where('id',$OrderId)->delete();
                }
            }
        }
    }
    $to = 'testing.demo@gmail.com';
    $subject = 'Run delete order cron';
    //$admin_email = ADMIN_EMAIL;
    $admin_email = 'testing.kancy@gmail.com';
    $message = "Successfully run deleted order cron";
    Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$message), function($msg) use ($subject,$to){ 
                    $msg->to($to);   
                    $msg->subject($subject);                                
    });
    echo "Done";
}
public function anyCronMailing(){
    $vendor = Vendor::where('e_status','Active')->select('id','v_email','v_vendor_name')->get();
    if(count($vendor)>0){
        $ordArrListNew = '';  
        $emailCounter = 0;
        $emailIds = 'EMAIL ID List <br>';
        foreach ($vendor as $key => $value) {
            $vendor_id = $value->id;
            $vendor_name = $value->v_vendor_name;
            $vendor_mail = $value->v_email;
            //$vendor_mail = 'testing.demo@gmail.com';
            $OrderRec = Order::where('i_vendor_id',$vendor_id)->where('t_is_mail_sent','0')->select('id','v_shipment_number')->get();
            if(count($OrderRec)>0){
                $OrderIdList = '';          
                $ordArr = array();
                foreach ($OrderRec as $order_key => $order_value) {
                    $ordArr[$order_value->v_shipment_number] = $order_value->v_shipment_number;
                    $OrderUpdate = Order::find($order_value->id);
                    $OrderUpdate->t_is_mail_sent = '1';
                    $OrderUpdate->save();
                }
                if(count($ordArr)>0){
                    foreach($ordArr as $ordId) {
                    $OrderIdList.=  "<br />&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;".$ordId."\r\n";  
                    }
                    $OrderIdList = trim($OrderIdList);
                }
                $ordArrListNew.= $OrderIdList;
                if($OrderIdList)
                {
                    $objEmailTemplate = MailTemplate::find(9)->toArray();
                    $strTemplate = $objEmailTemplate['t_email_content'];
                    $strTemplate = str_replace('[PROJECT_NAME]',PROJECT_NAME,$strTemplate);
                    $strTemplate = str_replace('[USER_NAME]',trim($vendor_name),$strTemplate);
                    $strTemplate = str_replace('[ORDER_IDS]',trim($OrderIdList),$strTemplate);
                    $strTemplate = str_replace('[SITE_NAME]',SITE_NAME,$strTemplate);
                    $strTemplate = str_replace('[SITE_URL]',SITE_URL,$strTemplate);
                    $subject='New Orders at '.PROJECT_NAME;
                    // mail sent to user with new link
                    $send_mailid='';
                    if(REPORT_TEST_MAILID!=''){
                        $send_mailid=REPORT_TEST_MAILID;
                    }else{
                        $send_mailid=$vendor_mail;
                    }  
                    Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$strTemplate), function($message) use ($subject,$send_mailid){ 
                        $message->to($send_mailid);   
                        $message->subject($subject);                                
                    });
                    $emailIds.= $vendor_mail."<br>";
                    $emailCounter++;
                }
                
            }
        }
        $to = 'testing.demo@gmail.com';
        $subject = 'Dropship - Cron job mailing';
        //$admin_email = ADMIN_EMAIL;
        $admin_email = 'testing.kancy@gmail.com';
        $message = "cron Hello! This is a simple email message from drop ship.".date('Y-m-d H:i:s')."<br />".$ordArrListNew."<br>--------".$emailCounter."<br>--------".$emailIds;
        Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$message), function($msg) use ($subject,$to){ 
                        $msg->to($to);   
                        $msg->subject($subject);                                
        });
    }
    echo 'Done';
}
public function anyCronOldOrdersMail(){
    $vendor = Vendor::where('e_status','Active')->select('id','v_email','v_vendor_name')->get();
    if(count($vendor)>0){
        foreach ($vendor as $key => $value) {
            $vendor_id = $value->id;
            $vendor_name = $value->v_vendor_name;
            $vendor_mail = $value->v_email;
            //$vendor_mail = 'testing.demo@gmail.com';            
            $OrderRec = Order::leftjoin('tbl_order_details','tbl_order.id','=','tbl_order_details.i_order_id')->where('tbl_order_details.e_item_status','=','Pending')->where('i_vendor_id',$vendor_id)->select('tbl_order.id','tbl_order.d_order_date','tbl_order.v_shipment_number')->get();
            if(count($OrderRec)>0){
                $OrderIdList = '';          
                $ordArr = array();
                foreach ($OrderRec as $order_key => $order_value) {
                    $TimeGap   = round(abs(strtotime(date("Y-m-d"))-strtotime($order_value->d_order_date))/86400);
                    if($TimeGap > 3)
                    {
                        $ordArr[$order_value->v_shipment_number] = $order_value->v_shipment_number;             
                    }   
                }
                foreach($ordArr as $ordId) {
                    $OrderIdList.=  "<br>&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;".$ordId;   
                }
                //echo "----<br>".$vendor_id."<br>----";
                $OrderIdList = trim($OrderIdList);
                if($OrderIdList)
                {
                    $objEmailTemplate = MailTemplate::find(10)->toArray();
                    $strTemplate = $objEmailTemplate['t_email_content'];
                    $strTemplate = str_replace('[PROJECT_NAME]',PROJECT_NAME,$strTemplate);
                    $strTemplate = str_replace('[USER_NAME]',trim($vendor_name),$strTemplate);
                    $strTemplate = str_replace('[ORDER_IDS]',trim($OrderIdList),$strTemplate);
                    $strTemplate = str_replace('[SITE_NAME]',SITE_NAME,$strTemplate);
                    $strTemplate = str_replace('[SITE_URL]',SITE_URL,$strTemplate);
                    $subject='Reminder for Pending Orders at '.PROJECT_NAME;
                    // mail sent to user with new link
                    $send_mailid='';
                    if(REPORT_TEST_MAILID!=''){
                        $send_mailid=REPORT_TEST_MAILID;
                    }else{
                        $send_mailid=$vendor_mail;
                    } 
                    Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$strTemplate), function($message) use ($subject,$send_mailid){ 
                        $message->to($send_mailid);   
                        $message->subject($subject);                                
                    });
                }
                
            }
        }
    }
    $to = 'testing.demo@gmail.com';
    $subject = 'Run old order delete cron';
    //$admin_email = ADMIN_EMAIL;
    $admin_email = 'testing.kancy@gmail.com';
    $message = "Successfully run cron";
    Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$message), function($msg) use ($subject,$to){ 
                    $msg->to($to);   
                    $msg->subject($subject);                                
    });
    echo 'Done';
}
   
}

