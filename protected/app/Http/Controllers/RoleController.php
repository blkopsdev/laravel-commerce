<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Module,App\Models\RoleAccess,App\Models\Languages, App\Models\AccessLevel, App\Models\UserAccessLevel;
use Illuminate\Support\Facades\Input;
use DB,Excel,Session,Auth,Response,Mail,Hash,Image,Config;

class RoleController extends BaseController
{
    public function getIndex(){
        $langID = Session::get('language_id');
        $langCode = Session::get('lang_code');
        $user = Auth::guard('admin')->user();
		$allUserType =BaseController::admin_types();
		$user_access_list = BaseController::access_levels();
		$access_levels = BaseController::accessLevels();
		$module = new Module;
		$module =$module->get();
	 	$alllang = '';
	 	// dd($user);
		if($user->role == 1)
		{
		  $role_id = Session::get('role_level_id');
		  if(!isset($role_id) && empty($role_id))
		  {
			 $role_id = 2;  
		  }	  
		 return View('admin.role_access.edit_role',array('title' => 'User Role', 'langID' => $langID, 'langCode' => $langCode,'alllang' => $alllang,'adminType' => $allUserType, 'module' => $module,'userAccessList'=>$user_access_list,'accessLevels' => $access_levels,'role_level_id'=>$role_id));	
		}	
		else
		{
			return Redirect(action('AuthenticateController@dashboard',array('langcode'=>Session::get('lang_code'))));
			
		}		
     	 
    }
	public function anyRoleData($id){
		$user = Auth::guard('admin')->user();
		$user_access_list = BaseController::access_levels();
		$access_levels = BaseController::accessLevels();
		$module = new Module;
		$module =$module->get();
		foreach($module as $mod){
			$user_access[] = AccessLevel::where('i_role','=',$id)->where('i_module_id','=',$mod['id'])->first();
        }
		return array('userAccess'=>$user_access,'module' => $module,'userAccessList'=>$user_access_list,'accessLevels' => $access_levels);
    }
	/*  public function anyRoleData($id){
		$data = Input::all();
		$user = Auth::guard('admin')->user();
		$module = new Module;
		$module =$module->get();
		foreach($module as $mod){
			$user_access[] = AccessLevel::where('i_role','=',$data['id'])->where('i_module_id','=',$mod['id'])->first();
        }
		return ['userAccess' => $user_access];
    }
	 */
	public function anyEdit(Request $request)
    {
        $allUserType = BaseController::admin_types();
		$langID = Session::get('language_id');
        $langCode = Session::get('lang_code');
        $current_user = Auth::guard('admin')->user();
		
        if(Input::all())
        {
            $data = Input::all();
			// dd($data);
			if(!empty($data)){
				$user_modules = Module::get();
				if(isset($data['role_user_type']) && $data['role_user_type'] != ''){
					$id = $data['role_user_type'];
				}else{
					$id = 2;
				}
				$useraccess = AccessLevel::where('i_role','=',$id)->get();
				$user_access_list = BaseController::access_levels();
				//$access_levels = BaseController::accessLevels();
				
				$selection_list = explode(',',$data['selection_list']);
				foreach($selection_list as $selection) {
					$myarray[$selection] = $selection;
				} 
				
				foreach($user_modules as $module) {
					$user_access = AccessLevel::where('i_module_id','=',$module->id)->where('i_role','=',$id)->first();
					
					if(empty($user_access) && $user_access == '') {
						$user_access = new AccessLevel;
						$user_access->i_role = $id;
						$user_access->i_module_id = $module['id'];
					}
					foreach($user_access_list as $access) {
						
						if(isset($myarray[$id.'_'.$module->id.'_'.$access])) {
							if($access == 'Read') {
								$user_access->e_view = '1';
                            } else if($access == 'Delete') {
                                $user_access->e_delete = '1';
                                $user_access->e_view = '1';
                                $user_access->e_add = '1';
                                $user_access->e_edit = '1';
                            }  else if($access == 'Create') {
                                $user_access->e_add = '1';
								$user_access->e_view = '1';
							}else if($access == 'Update') {
                                $user_access->e_edit = '1';
								$user_access->e_view = '1';
							}  else if($access == 'Import Data Feed') {
                                $user_access->e_import = '1';
                            } 
							 else if($access == 'Download') {
                                $user_access->e_export = '1';
                            } 
							
                            
                            $user_new_access = UserAccessLevel::where('i_module_id','=',$module->id)->where('i_role','=',$id)->get();
						    
                            if(count($user_new_access)>0) {
                                foreach($user_new_access as $new_access){
                                  $users_new = UserAccessLevel::where('i_module_id','=',$new_access->i_module_id)->where('i_role','=',$new_access->i_role)->where('i_user_id','=',$new_access->i_user_id)->first();
                                  
									if($access == 'Read' ){
										$users_new->e_view = '1';
									} else if($access == 'Delete' ){
										$users_new->e_delete = '1';
										$users_new->e_view = '1';
										$users_new->e_edit = '1';
										$users_new->e_add = '1';
									} else if($access == 'Create' ){
										$users_new->e_add = '1';
										$users_new->e_view = '1';
									} 
									else if($access == 'Update' ){
										$users_new->e_edit = '1';
										$users_new->e_view = '1';
									} else if($access == "Import Data Feed"){
										$users_new->e_import = '1';
									} 
									else if($access == "Download"){
										$users_new->e_export = '1';
									} 
									
									$users_new->save();
                                }
                                
                               
                            } 
                           
                        } else {
                            if($access == 'Read') {
                                $user_access->e_view ='0';
                            } else if($access == 'Delete') {
                                $user_access->e_delete = '0';
                            } else if($access == 'Create') {
                                $user_access->e_add = '0';
                            }else if($access == 'Update') {
                                $user_access->e_edit = '0';
                            } else if($access == 'Import Data Feed') {
                                $user_access->e_import = '0';
                            } 
							else if($access == 'Download') {
                                $user_access->e_export = '0';
                            } 
                            $user_new_access = UserAccessLevel::where('i_module_id','=',$module->id)->where('i_role','=',$id)->get();
							
                          	if(count($user_new_access)>0) {
                                foreach($user_new_access as $new_access){
                                  	$users_new = UserAccessLevel::where('i_module_id','=',$new_access->i_module_id)->where('i_role','=',$new_access->i_role)->where('i_user_id','=',$new_access->i_user_id)->first();
                                  	
									if($access == 'Read' ){
										$users_new->e_view = '0';
									} else if($access == 'Delete' ){
										$users_new->e_delete = '0';
										
									} else if($access == 'Create' ){
										$users_new->e_add = '0';
									
									}else if($access == 'Update' ){
										$users_new->e_edit = '0';
									
									} else if($access == "Import Data Feed"){
										$users_new->e_import = '0';
									} 
									else if($access == 'Download') {
										$users_new->e_export = '0';
                                      } 
									$users_new->save();
                                }
                                
                               
                            } 
                        }
                    }
					
					$user_access->save();
					
				}
				Session::put('role_level_id',$id);
				Session::flash('success-message',"Record has been updated successfully.");
				 
				return '';  
			}else{
				return 'FALSE';
			}
		}
    }
    
	/* public function getUserResultView($lang,$id){
        $langID = Session::get('language_id');
        $langCode = Session::get('lang_code');
        $user = Auth::guard('admin')->user();
        $get_user = RoleAccess::find($id);
        $data_user = [
            'firstname' => $get_user->firstname,
            'lastname' => $get_user->lastname,
            'email' => $get_user->email
        ];
    	return View('admin.user.view',array('title' => 'User Result View','UserID' => $id,'data_user'=>$data_user,'langCode' => $langCode));
    }
 */
    
}
