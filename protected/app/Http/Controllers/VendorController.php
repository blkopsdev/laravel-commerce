<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use Request, Session;
use App\Models\Client, App\Models\Vendor, App\Models\admin,App\Models\MailTemplate,App\Models\TblAdmin;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Excel,Auth,Validator;
use Illuminate\Support\Facades\Hash;
use Mail,FFMPEG;

class VendorController extends BaseController
{
    public function getIndex()
    {
        return View('admin.vendor.index', array('title' => 'Vendor List'));
    }

    public function anyListAjax(Request $request) //client Listing
    {
        $data = Request::all();
        if ($data) {

            $query = new Vendor;
            $query = $query->with('Client');
            $user = Auth::guard('admin')->user();
            if($user->e_type == 'Client') {
                $sortColumn = array('', 'v_vendor_id', 'v_vendor_name','v_company_name','v_email',  'v_phone_number','e_status');
            } else {
                $sortColumn = array('', 'v_company', 'v_vendor_id', 'v_vendor_name','v_company_name','v_email', 'v_phone_number','e_status');
            }
              
            $query=$query->select('tbl_vendor.id', 'i_client_id', 'tbl_vendor.v_vendor_id', 'v_vendor_name','v_company_name','tbl_vendor.v_email',  'v_phone_number','tbl_vendor.e_status');
            if($user->role==2){
                $query=$query->where('i_client_id',$user->i_client_id);
            }
            if (isset($data['v_company']) && $data['v_company'] != '') {
                $query = $query->whereHas('client', function($q) use($data){
                    $q->where('tbl_client.v_company', 'LIKE', '%' . trim($data['v_company']) . '%');
                });
            }
            if (isset($data['v_vendor_name']) && $data['v_vendor_name'] != '') {
                $query = $query->where('v_vendor_name', 'LIKE', '%' . trim($data['v_vendor_name']) . '%');
            }
            if (isset($data['v_vendor_id']) && $data['v_vendor_id'] != '') {
                $query = $query->where('v_vendor_id', 'LIKE', '%' . trim($data['v_vendor_id']) . '%');
            }
            if (isset($data['v_company_name']) && $data['v_company_name'] != '') {
                $query = $query->where('v_company_name', 'LIKE', '%' . trim($data['v_company_name']) . '%');
            }            
            if (isset($data['v_email']) && $data['v_email'] != '') {
                $query = $query->where('tbl_vendor.v_email', 'LIKE', '%' . trim($data['v_email']) . '%');
            }
            if (isset($data['v_phone_number']) && $data['v_phone_number'] != '') {
                $query = $query->where('v_phone_number', 'LIKE', '%' . trim($data['v_phone_number']) . '%');
            }
            if (isset($data['e_status']) && $data['e_status'] != '') {
                $query = $query->where('e_status', 'LIKE',trim($data['e_status']));
            }
            $rec_per_page = REC_PER_PAGE;

            if (isset($data['length'])) {
                if ($data['length'] == '-1') {
                    $rec_per_page = '';
                } else {
                    $rec_per_page = $data['length'];
                }
            }

            $sort_order = $data['order']['0']['dir'];
            $order_field = $sortColumn[$data['order']['0']['column']];

            if ($sort_order != '' && $order_field != '') {
                if($order_field == 'v_company'){
                     $query = $query->join('tbl_client','tbl_client.id','=','tbl_vendor.i_client_id')->orderBy('tbl_client.v_company',$sort_order);
                }else
                {
                   $query = $query->orderBy($order_field, $sort_order);
                }
            } else {
                $query = $query->orderBy('id', 'desc');
            }
            if($rec_per_page == ''){
                $temp_records = $query->get();
                $rec_per_page = count($temp_records);
                $users = $query->paginate($rec_per_page);
            } else {
                $users = $query->paginate($rec_per_page);
            }
            //$users = $query->paginate($rec_per_page);
            $arrUsers = $users->toArray();
            $data = array();
            foreach ($arrUsers['data'] as $key => $val) {
                $index = 0;
                $data[$key][$index++] = '<input type="checkbox" name="id[]" value="' . $val['id'] . '" class="delete_' . $val['id'] . '">';
                if($user->e_type != 'Client'){
                    if(isset($val['client']['v_company'])){
                        $data[$key][$index++] = $val['client']['v_company'];
                    } else {
                        $data[$key][$index++] = '';
                    }
                }
                $data[$key][$index++] = $val['v_vendor_id'];
                $data[$key][$index++] = $val["v_vendor_name"];
                $data[$key][$index++] = $val['v_company_name'];
                $data[$key][$index++] = $val['v_email'];
                $data[$key][$index++] = $val['v_phone_number'];            
                if ($val['e_status'] == 'Active') {
                    $intStatus = 'Active';
                } else {
                    $intStatus = 'Inactive';
                }
                $data[$key][$index++] = '<a href="javascript:void(0);" data-id="' . $val['id'] . '" id="change_status" rel="' . $intStatus . '" change-url="' . ADMIN_URL . 'vendor/change-status">' . $intStatus . '</a>';
                $action = '<div class="actions"><a class="edit btn default btn-xs black" rel="' . $val['id'] . '" href="' . ADMIN_URL . 'vendor/edit/' . $val['id'] . '"  title="Edit"><i class="fa fa-edit"></i></a><a href="javascript:;" id="delete_record" rel="' . $val['id'] . '" delete-url="' . ADMIN_URL . 'vendor/delete/' . $val['id'] . '" class="btn default btn-xs black delete" title="Delete"><i class="icon-trash"></i></a>';
                $data[$key][$index++] = $action;
            }
            $return_data['data'] = $data;
            $return_data['recordsTotal'] = $arrUsers['total'];
            $return_data['recordsFiltered'] = $arrUsers['total'];
            $return_data['data_array'] = $arrUsers['data'];
            return $return_data;
        }
    }

    public function anyAdd()
    {        
        $auth_user = Auth::guard('admin')->user();
        if (Input::all()) {
            $inputs = Input::all();                        
            $vendor = new Vendor;            
            $validator = Validator::make(Input::all(), array(
                "v_email" => 'required',
                "v_username" => 'required|unique:tbl_admin,v_username,NULL,id,deleted_at,NULL',
                "v_vendor_id" => 'required|unique:tbl_vendor,v_vendor_id,NULL,id,deleted_at,NULL'));
            if ($validator->fails()) {
                return $validator->errors();
            } else {                    
                    $vendor->v_vendor_name = trim($inputs['v_vendor_name']);
                    $vendor->v_email = trim($inputs['v_email']); 
                    $vendor->v_vendor_id = trim($inputs['v_vendor_id']); 
                    if(!empty($inputs['order_email'])) {
                        $order_email = array_filter($inputs['order_email']);
                        $order_email_str = implode (", ", $order_email);                        
                        $vendor->order_email = trim($order_email_str);
                    } 
                    if(!empty($inputs['po_email'])) {
                        $po_email = array_filter($inputs['po_email']);
                        $po_email_str = implode (", ", $po_email);                                                
                        $vendor->po_email = trim($po_email_str);
                    }                    
                    if($auth_user->e_type=="Client"){
                        $vendor->i_client_id  = $auth_user->i_client_id;   
                    }else{
                        $vendor->i_client_id  = $inputs['i_client_id'];       
                    }
                    $vendor->v_company_name = trim($inputs['v_company_name']);       
                    if(Input::has('v_phone_number')){
                        $vendor->v_phone_number = trim($inputs['v_phone_number']);
                    }  
                    if(Input::has('v_cell_number')){
                        $vendor->v_cell_number = trim($inputs['v_cell_number']);
                    }  
                    if(Input::has('l_note')){
                        $vendor->l_note = trim($inputs['l_note']);
                    }  
                    if(Input::has('v_fax')){
                        $vendor->v_fax = trim($inputs['v_fax']);
                    }  
                    if(Input::has('v_url')){
                        $vendor->v_url = trim($inputs['v_url']);
                    }      
                    $vendor->e_status = trim($inputs['e_status']);     
                    if ($vendor->save()) {
                        $last_inserted_id = $vendor->id;
                        $admin = new Admin;
                        $admin->v_username = trim($inputs['v_username']);
                        $admin->password = Hash::make(trim($inputs['password']));
                        $admin->v_email = trim($inputs['v_email']);
                        $admin->i_vendor_id = $last_inserted_id;
                        $admin->e_type = 'Vendor';  
                        $admin->role='3';     
                        $admin->e_status = trim($inputs['e_status']);            
                        if($admin->save())
                        {
                            $objEmailTemplate = MailTemplate::find(7)->toArray();
                            $strTemplate = $objEmailTemplate['t_email_content'];
                            $strTemplate = str_replace('[PROJECT_NAME]',PROJECT_NAME,$strTemplate);
                            $strTemplate = str_replace('[USER_NAME]',trim($inputs['v_username']),$strTemplate);
                            $strTemplate = str_replace('[PASSWORD]',trim($inputs['password']),$strTemplate);
                            $strTemplate = str_replace('[IMAGE_PATH]', EMAIL_LOGO_IMAGE_PATH, $strTemplate);
                            $strTemplate = str_replace('[SITE_NAME]',SITE_NAME,$strTemplate);
                            $strTemplate = str_replace('[SITE_URL]',SITE_URL,$strTemplate);
                            $subject='New vendor registration at '.PROJECT_NAME;
                            // mail sent to user with new link
                            $mail_flag=Session::get('SETTING_DATA');
                            $send_mailid='';
                            if(TESTING_MAILID!=''){
                                $send_mailid=TESTING_MAILID;
                            }else{
                                $send_mailid=$inputs['v_email'];
                            }
                            // Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$strTemplate), function($message) use ($inputs,$subject,$send_mailid){ 
                            //     $message->to($send_mailid);   
                            //     $message->subject($subject);                                
                            // }); 
                            
                            Session::flash('success-message', 'Vendor has been added successfully.');
                            return '';    
                        }
                        
                    }
                }
        } else {            
            $client_list = Client::select('id','v_company')->get();           
            return View('admin.vendor.add_vendor', array('title' => 'Add Vendor','client_list'=> $client_list,'auth_user' =>$auth_user));
        }
        return Redirect(ADMIN_URL . 'vendor');

    }

    public function getDelete($id)
    {
        $vendor = Vendor::find($id);
        if (!empty($vendor)) {
            $admin = Admin::where('i_vendor_id',$id)->delete();
            $vendor->delete();
            return 'TRUE';
        } else {
            return 'FALSE';
        }  
    }
    public function anyEdit($id)
    {
        $auth_user = Auth::guard('admin')->user();
        if (Input::all()) {
            $inputs = Input::all();                                    
            $vendor = Vendor::find($id);    
            $admin=Admin::where('i_vendor_id',$id)->first();        
            $validator = Validator::make(Input::all(), array(
                "v_email" =>'required',
                "v_username" =>'required|unique:tbl_admin,v_username,' .$admin->id. ',id,deleted_at,NULL',
                "v_vendor_id" => 'required|unique:tbl_vendor,v_vendor_id,'.$vendor->id.',id,deleted_at,NULL'));
            if ($validator->fails()) {
                return $validator->errors();
            } else {
                    $vendor->v_vendor_name = trim($inputs['v_vendor_name']);
                    $vendor->v_email = trim($inputs['v_email']); 
                    $vendor->v_vendor_id = trim($inputs['v_vendor_id']); 
                    if(!empty($inputs['order_email'])) {
                        $order_email = array_filter($inputs['order_email']);
                        $order_email_str = implode (", ", $order_email);                        
                        $vendor->order_email = trim($order_email_str);
                    } 
                    if(!empty($inputs['po_email'])) {
                        $po_email = array_filter($inputs['po_email']);
                        $po_email_str = implode (", ", $po_email);                                                                        
                        $vendor->po_email = trim($po_email_str);
                    } 
                    if($auth_user->e_type=="Client"){
                        $vendor->i_client_id  = $auth_user->i_client_id;   
                    }else{
                        $vendor->i_client_id  = $inputs['i_client_id'];       
                    }
                    
                    $vendor->v_company_name = trim($inputs['v_company_name']);       
                    if(Input::has('v_phone_number')){
                        $vendor->v_phone_number = trim($inputs['v_phone_number']);
                    }  
                    $vendor->l_note = trim($inputs['l_note']);
                    $vendor->v_fax = trim($inputs['v_fax']);
                    $vendor->v_url = trim($inputs['v_url']);
                    $vendor->e_status = trim($inputs['e_status']);                         
                    if ($vendor->save()) {
                        $admin = Admin::where('i_vendor_id',$vendor->id)->first();
                        $admin->v_username=trim($inputs['v_username']);
                        $admin->e_status = trim($inputs['e_status']);  
                        if(Input::has('password')){
                            $admin->password=Hash::make(trim($inputs['password']));
                            $objEmailTemplate = MailTemplate::find(8)->toArray();
                            $strTemplate = $objEmailTemplate['t_email_content'];
                            $strTemplate = str_replace('[PROJECT_NAME]',PROJECT_NAME,$strTemplate);
                            $strTemplate = str_replace('[USER_NAME]',trim($inputs['v_username']),$strTemplate);
                            $strTemplate = str_replace('[PASSWORD]',trim($inputs['password']),$strTemplate);
                            $strTemplate = str_replace('[IMAGE_PATH]', EMAIL_LOGO_IMAGE_PATH, $strTemplate);
                            $strTemplate = str_replace('[SITE_NAME]',SITE_NAME,$strTemplate);
                            $strTemplate = str_replace('[SITE_URL]',SITE_URL,$strTemplate);
                            $subject='Update vendor profile at '.PROJECT_NAME;
                            // mail sent to user with new link
                            $mail_flag=Session::get('SETTING_DATA');
                            $send_mailid='';
                            if(TESTING_MAILID!=''){
                                $send_mailid=TESTING_MAILID;
                            }else{
                                $send_mailid=$inputs['v_email'];
                            }
                            // Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$strTemplate), function($message) use ($inputs,$subject,$send_mailid){ 
                            //     $message->to($send_mailid);   
                            //     $message->subject($subject);                                
                            // });
                            
                        }
                        $admin->v_email = trim($inputs['v_email']);
                        $admin->e_type = 'Vendor';              
                        if($admin->save())
                        {
                            Session::flash('success-message', 'Vendor has been updated successfully.');
                            return '';    
                        }
                        
                    }
                }
        } else {            
            $client_list = Client::select('id','v_company')->get();
            $records = Vendor::with('Client','Admin')->find($id);
            if(isset($records) && !empty($records)){
                return View('admin.vendor.edit_vendor', array('records' => $records,'title' => 'Edit Vendor','client_list'=> $client_list,'auth_user'=>$auth_user));
            } else {
                return Redirect(ADMIN_URL . 'vendor');
            }
        }
        
        
    }

    public function postBulkAction()
    {
        $data = Request::all();        
        if(count($data) > 0) {
            if ($data['action'] == 'Delete') {                
                $admin = Admin::whereIn('i_vendor_id',$data['ids'])->delete();
                $vendor = Vendor::whereIn('id',$data['ids'])->delete();
                if($vendor){
                    return 'TRUE';
                } else { return 'FALSE'; }
            }else if($data['action'] == 'Active'){
                $vendor = Vendor::whereIn('id',$data['ids'])->update(['e_status' => 'Active']);
                $admin = Admin::whereIn('i_vendor_id',$data['ids'])->update(['e_status' => 'Active']);
                if($vendor){
                    return 'TRUE';
                } else { return 'FALSE'; }
            }else if($data['action'] ==  'Inactive'){
                $vendor = Vendor::whereIn('id',$data['ids'])->update(['e_status' => 'Inactive']);
                $admin = Admin::whereIn('i_vendor_id',$data['ids'])->update(['e_status' => 'Inactive']);
                if($vendor){
                    return 'TRUE';
                } else { return 'FALSE'; }
            }
        } 
    }


    public function postChangeStatus()
    {
        $data = Input::all();
        $vendor = Vendor::find($data['id']);
        $admin=Admin::where('i_vendor_id',$data['id'])->first();
        if($vendor->e_status=='Active')
                $status='Inactive';
        else
                $status='Active';
        $vendor->e_status = $status;
        $admin->e_status = $status;
        if ($vendor->save()) {
            $admin->save();
            return 'TRUE';
        } else {
            return 'FALSE';
        }  
    }
    public function postCopy() {
       $result=TblAdmin::where('eType','Vendor')->get();
       foreach ($result as $key => $value) {
            $admin=new Admin();
            $admin->v_username=trim($value->vUsername);
            $admin->password=Hash::make(trim($value->vPassword));
            $admin->v_email=trim($value->vEmailAdd);
            $admin->i_vendor_id=trim($value->iVendorId);
            $admin->e_type='Vendor';
            $admin->save();
       }
       echo "done";
    }
    public function anyExportToExcel()
    {
        $time = date('Y_m_d_h_i_A');
        $data = Input::all();
        $query = new Vendor;
        $user = Auth::guard('admin')->user();
  
        if($user->e_type == 'Client') {
            $sortColumn = array('', 'v_vendor_id','v_vendor_name','v_company_name','v_email', 'v_phone_number','e_status');
        } else {
            $sortColumn = array('', 'v_company', 'v_vendor_id', 'v_vendor_name','v_company_name','v_email',  'v_phone_number','e_status');
            //$sortColumn = array('', 'v_company', 'v_vendor_id', 'v_vendor_name','v_company_name','v_email', 'v_phone_number','e_status');
        }  
        
        if($user->role==2){
                $query=$query->where('tbl_vendor.i_client_id',$user->i_client_id);
            }
        if (isset($data['v_company']) && $data['v_company'] != '') {
             $query = $query->where('tbl_client.v_company', 'LIKE', '%' . trim($data['v_company']) . '%');
        }
        if (isset($data['v_vendor_id']) && $data['v_vendor_id'] != '') {
            $query = $query->where('tbl_vendor.v_vendor_id', 'LIKE', '%' . trim($data['v_vendor_id']) . '%');
        }
        if (isset($data['v_vendor_name']) && $data['v_vendor_name'] != '') {
            $query = $query->where('tbl_vendor.v_vendor_name', 'LIKE', '%' . trim($data['v_vendor_name']) . '%');
        }
        if (isset($data['v_company_name']) && $data['v_company_name'] != '') {
            $query = $query->where('tbl_vendor.v_company_name', 'LIKE', '%' . trim($data['v_company_name']) . '%');
        }            
        if (isset($data['v_email']) && $data['v_email'] != '') {
            $query = $query->where('tbl_vendor.v_email', 'LIKE', '%' . trim($data['v_email']) . '%');
        }
        
        if (isset($data['v_phone_number']) && $data['v_phone_number'] != '') {
            $query = $query->where('tbl_vendor.v_phone_number', 'LIKE', '%' . trim($data['v_phone_number']) . '%');
        }
        if (isset($data['e_status']) && $data['e_status'] != '') {
            $query = $query->where('tbl_vendor.e_status', 'LIKE', '%' . trim($data['e_status']) . '%');
        }
        $query = $query->join('tbl_admin', function ($join){
                            $join->on('tbl_vendor.id', '=', 'tbl_admin.i_vendor_id')->whereNull('tbl_admin.deleted_at');
                        });
         $query = $query->join('tbl_client', function ($join){
                            $join->on('tbl_vendor.i_client_id', '=', 'tbl_client.id')->whereNull('tbl_client.deleted_at');
                        });
       /* $query = $query->join('tbl_admin','tbl_admin.i_vendor_id','=','tbl_vendor.id');
        $query= $query->join('tbl_client','tbl_client.id','=','tbl_vendor.i_client_id');*/
        $query= $query->select('tbl_vendor.id','tbl_client.v_company','tbl_admin.v_username', 'tbl_vendor.v_vendor_id', 'tbl_vendor.v_vendor_name','tbl_vendor.v_company_name','tbl_vendor.v_email', 'tbl_vendor.v_phone_number','tbl_vendor.v_fax','tbl_vendor.v_url','tbl_vendor.e_status','tbl_vendor.l_note','tbl_vendor.created_at','tbl_vendor.updated_at')->where('tbl_admin.deleted_at',"=",NULL)->where('tbl_client.deleted_at',"=",NULL);;
        $sort_order = $data['order']['0']['dir'];
        $order_field = $sortColumn[$data['order']['0']['column']];

        if ($sort_order != '' && $order_field != '') {
               $query = $query->orderBy($order_field, $sort_order);
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $users = $query->get();
        $records = $users->toArray();
        if (count($records) > 0) {
            $field = array();
            $field['id'] = 'ID';
            $field['v_company'] = 'Client Name';
            $field['v_username'] = 'User Name';
            $field['v_vendor_id'] = 'Vendor ID';
            $field['v_vendor_name'] = 'Vendor Name';            
            $field['v_company_name'] = 'Company Name';
            $field['v_email'] = 'Email';
            $field['v_phone_number'] = 'Phone';
            $field['v_fax'] = 'FAX';
            $field['v_url'] = 'URL';
            $field['e_status'] = 'Status';
            $field['l_note'] = 'Notes';
            $field['created_at'] = 'Created At';
            $field['updated_at'] = 'Updated At';
            
            Excel::create('Vendor_List_'.$time, function ($excel) use ($records, $field) {
                $excel->sheet('Vendor_List', function ($sheet) use ($records, $field) {
                    $sheet->setOrientation('landscape');
                    $sheet->setHeight(1, 30);
                    $sheet->mergeCells('A1:N1');

                    $sheet->cells('A1:N1', function ($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('middle');
                        $cell->setFontSize('20');
                        $cell->setFontWeight('bold');
                    });
                    $sheet->row(1, array('Vendor List'));
                    $sheet->cells('A2:N2', function ($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('middle');
                        $cell->setFontSize('12');
                        $cell->setFontWeight('bold');
                    });
                    $sheet->row(2, $field);
                    $intCount = 3;
                    $srNo = 1;
                    foreach ($records as $index => $val) {
                        if($val['created_at']){
                            $val['created_at'] = date('m/d/Y h:i:s', strtotime($val['created_at']));
                        }
                        if($val['updated_at']){
                            $val['updated_at'] = date('m/d/Y h:i:s', strtotime($val['updated_at']));
                        }
                        $sheet->row($intCount, $val);
                        $intCount++;
                        $srNo++;
                    }
                });
            })->export('xls');
        }else{
             Session::flash('alert-message', 'No data found.');
            return Redirect(ADMIN_URL . 'vendor');
        }
    }

}
