<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request, Hash, Mail, Redirect, Validator,Excel,Cookie,Auth,Session,DB,URL;
use App\Models\Admin,App\Models\Setting,App\Models\Client,App\Models\Vendor,App\Models\Employee,App\Models\MailTemplate,App\Models\Order,App\Models\DropshipEmployee,App\Models\OrderDetail, App\Models\PurchaseOrderDetail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use File;

class AuthenticateController extends BaseController {

    public function home(){
        return view('admin.authenticate.home', array('title' => 'Home'));   
    }
    public function index(){
        
        $ip_address = Request::ip();
		$forgotpass_flag = '0';
        if(Session::get('FORGOTPASS_FLAG') == '1' || Session::has('forgot_message') || Session::has('forgot_msg')) {
                $forgotpass_flag = "1";
		}
		$data = Input::all();
        if(!empty($data) && isset($data['uname']) &&  isset($data['password'])) {

            $logAccess = filter_var($data['uname'], FILTER_VALIDATE_EMAIL) ? 'v_email' : 'v_username';

            $admin_data = Admin::where($logAccess,$data['uname'])->where('e_status', 'Active')->first();            

            if(!empty($admin_data) && ($admin_data->i_vendor_id != 0 || $admin_data->i_emp_id != 0) && $logAccess == 'v_email') {
                Session::remove('msg');
                Session::flash('message',($logAccess == "v_email" ? ERR_USERNAME_PWS : ERR_PWS));
                return redirect(ADMIN_URL_LOGIN);
                exit;
            }
            if(!empty($admin_data) && $admin_data->is_reset == 0) {
                $access_code = $this->randomPassword();
                
                $admin_data->remember_token = $access_code;
                $admin_data->save();

                $link = ADMIN_URL.'reset-password/'.$access_code.'/force-reset';
                return Redirect($link);
            }

            $remember = (Input::has('remember')) ? true : false;
            
            $admindata = Auth::guard('admin')->attempt([ $logAccess => $data['uname'], 'password' => $data['password'],'e_status' => 'Active'],$remember);
            if($remember == 1){
                $cookie_username = Cookie::make('user_name', $data['uname']);
                $cookie_password = Cookie::make('password', $data['password']);
                //$cookie = $cookie_username.$cookie_password;
            
            } else {
                $cookie_username = Cookie::forget('user_name');
                $cookie_password = Cookie::forget('password');
            }
            $admin_data = Auth::guard('admin')->user();
            if(!empty($admin_data)){
                $date = date('Y-m-d h:i:s');
                Session::put('CURRENTLOGIN',$date);
                $admin_data= Admin::find($admin_data['id']);
                
                if($admin_data['i_vendor_id']==0 && $admin_data['i_client_id']==0 && $admin_data['i_emp_id']==0 && $admin_data['i_dropship_emp_id']==0 && $admin_data['i_admin_id'] == 0){   
                        $user_data= Admin::find($admin_data['id']);
                    
                }else if($admin_data['i_vendor_id']!=0 && $admin_data['i_client_id']==0 && $admin_data['i_emp_id']==0 && $admin_data['i_dropship_emp_id']==0)
                {
                    //vendor
                    $user_data= Admin::with("Vendor")->find($admin_data['id']);
                }else if($admin_data['i_vendor_id']==0 && $admin_data['i_client_id']!=0 && $admin_data['i_emp_id']==0 && $admin_data['i_dropship_emp_id']==0){
                    //client
                    $user_data= Admin::with("Client")->find($admin_data['id']);
                }else if($admin_data['i_vendor_id']==0 && $admin_data['i_client_id']==0 && $admin_data['i_emp_id']!=0 && $admin_data['i_dropship_emp_id']==0){
                    //employee
                    $user_data= Admin::with("Employee")->find($admin_data['id']);
                }else if($admin_data['i_vendor_id']==0 && $admin_data['i_client_id']==0 && $admin_data['i_emp_id']==0 && $admin_data['i_dropship_emp_id']!=0){
                    $user_data= Admin::with("DropshipEmployee")->find($admin_data['id']);
                    
                } 
                else if($admin_data['i_vendor_id']==0 && $admin_data['i_client_id']==0 && $admin_data['i_emp_id']==0 && $admin_data['i_dropship_emp_id']==0 && $admin_data['i_admin_id']!=0){
                    $user_data= Admin::with("Admin")->find($admin_data['id']);
                    
                } 
                $admin_data->d_last_login_time = date('Y-m-d H:i:s');
                $admin_data->v_ip_address = $ip_address;
                $admin_data->save();
                Session::remove('FORGOTPASS_FLAG');
    			$forgotpass_flag = '0';
                if (!empty($admindata)){
                    Session::remove('USER_DATA');
                    Session::remove('SETTING_DATA');
                    $setting_data=Setting::select('mail_template_flg')->first();
                    Session::put('SETTING_DATA',$setting_data);
                    Session::put('USER_DATA',$user_data);
                    return Redirect(ADMIN_URL.'dashboard')->withCookie($cookie_username)->withCookie($cookie_password);
                    exit;
                }
            } else{
                Session::remove('msg');
                Session::flash('message',($logAccess == "username" ? ERR_USERNAME_PWS : ERR_PWS));
                return redirect(ADMIN_URL_LOGIN);
                exit;
            }	
		}
		return view('admin.authenticate.login', array('forgotpass_flag' => $forgotpass_flag,'title' => 'Login'));	
	}

    public function dashboard(){
        
        $userArr = Auth::guard('admin')->user();
        $client = new Client;
        $vendor = new Vendor;
        $employee = new Employee;
        $order = new OrderDetail();
        $purchaseOrder = new PurchaseOrderDetail();
        if($userArr->role==1){
            $client = $client->count();
            $vendor = $vendor->count();
            $employee = $employee->count();
            $order = $order->join('tbl_order', function ($join){
                            $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                        })->join('tbl_vendor', function ($join){
                            $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        })->count();
            $purchaseOrder = $purchaseOrder->join('tbl_purchase_order', function ($join){
                            $join->on('tbl_purchase_order_details.i_purchase_order_id', '=', 'tbl_purchase_order.id')->whereNull('tbl_purchase_order.deleted_at');
                        })->join('tbl_vendor', function ($join){
                            $join->on('tbl_purchase_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        })->count();
        }else if($userArr->role==2){
            $client = $client->count();
            $vendor = $vendor->where('i_client_id',$userArr->i_client_id)->count();
            $employee = $employee->with("Vendor")->whereHas('Vendor', function($q) use($userArr){
                    $q->where('tbl_vendor.i_client_id', $userArr->i_client_id);
                })->count();                
            $vendor_ids = Vendor::where('i_client_id',$userArr->i_client_id)->pluck('id');
            $order = $order->join('tbl_order', function ($join){
                            $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                        })->join('tbl_vendor', function ($join){
                            $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        })->where('tbl_order.i_client_id',$userArr->i_client_id)->count();
            $purchaseOrder = $purchaseOrder->join('tbl_purchase_order', function ($join){
                            $join->on('tbl_purchase_order_details.i_purchase_order_id', '=', 'tbl_purchase_order.id')->whereNull('tbl_purchase_order.deleted_at');
                        })->join('tbl_vendor', function ($join){
                            $join->on('tbl_purchase_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        })->whereIn('tbl_purchase_order.i_vendor_id',$vendor_ids)->count();
        }else if($userArr->role==3){
            $client = $client->count();
            $vendor = $vendor->count();
            $vendor_ids = Vendor::where('v_email',$userArr->v_email)->pluck('id');
            
            $employee = $employee->where("i_vendor_id",$vendor_ids)->count();
            $order = $order->join('tbl_order', function ($join){
                            $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                        })->join('tbl_vendor', function ($join){
                            $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        })->where("tbl_order.i_vendor_id",$vendor_ids)->count();
            $purchaseOrder = $purchaseOrder->join('tbl_purchase_order', function ($join){
                            $join->on('tbl_purchase_order_details.i_purchase_order_id', '=', 'tbl_purchase_order.id')->whereNull('tbl_purchase_order.deleted_at');
                        })->join('tbl_vendor', function ($join){
                            $join->on('tbl_purchase_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        })->whereIn('tbl_purchase_order.i_vendor_id',$vendor_ids)->count();
        
        }else if($userArr->role==4){
            $client = $client->count();
            $vendor = $vendor->count();
            $employee = $employee->count();
            $employee_data=Employee::where('id',$userArr->i_emp_id)->first();
            $order = $order->join('tbl_order', function ($join){
                            $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                        })->join('tbl_vendor', function ($join){
                            $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        })->where("tbl_order.i_vendor_id",$employee_data->i_vendor_id)->count();
        
            $purchaseOrder = $purchaseOrder->join('tbl_purchase_order', function ($join){
                            $join->on('tbl_purchase_order_details.i_purchase_order_id', '=', 'tbl_purchase_order.id')->whereNull('tbl_purchase_order.deleted_at');
                        })->join('tbl_vendor', function ($join){
                            $join->on('tbl_purchase_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        })->where('tbl_purchase_order.i_vendor_id',$employee_data->i_vendor_id)->count();
        
        }else if($userArr->role==5){
            $client = $client->count();
            $vendor = $vendor->count();
            $employee = $employee->count();
            $order = $order->join('tbl_order', function ($join){
                            $join->on('tbl_order_details.i_order_id', '=', 'tbl_order.id')->whereNull('tbl_order.deleted_at');
                        })->join('tbl_vendor', function ($join){
                            $join->on('tbl_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        })->count();
            $purchaseOrder = $purchaseOrder->join('tbl_purchase_order', function ($join){
                            $join->on('tbl_purchase_order_details.i_purchase_order_id', '=', 'tbl_purchase_order.id')->whereNull('tbl_purchase_order.deleted_at');
                        })->join('tbl_vendor', function ($join){
                            $join->on('tbl_purchase_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        })->count();
        }
        $responseData = array();
        $responseData['Client'] = $client;
        $responseData['Employee'] = $employee;
        $responseData['Vendor'] = $vendor;
        $responseData['Order'] = $order;
        $responseData['PurchaseOrder'] = $purchaseOrder;
        return view('admin.authenticate.dashboard', array('title' => 'Dashboard','responseData' => $responseData));  
    }

    public function logout(){
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time()-1000);
                setcookie($name, '', time()-1000, '/');
            }
        }
        $ip_address = Request::ip();
        $admin_data = Auth::guard('admin')->user();
        $admin_data->d_last_login_time = date('Y-m-d H:i:s');
        $admin_data->v_ip_address = $ip_address;
        $admin_data->save();
        Auth::guard('admin')->logout();
        Session::remove('admin_logged_id');
        Session::remove('admin_array');
        Session::remove('lock_url');
        Session::remove('USER_DATA');
        Session::remove('SETTING_DATA');
        Session::flash('msg', "You are successfully logged out.");
        return Redirect::to('login');            
    }

    public function randomPassword() { // this I have used for creating link for forgot password
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array(); 
	    $alphaLength = strlen($alphabet) - 1; 
	    for ($i = 0; $i < 60; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); 
	}

    public function forgot_password(){
	   if(Input::all())
        {
            $data = Input::all();            
            $logAccess = filter_var($data['email'], FILTER_VALIDATE_EMAIL) ? 'v_email' : 'v_username';
            $admindata = Admin::where($logAccess,$data['email'])->where('e_status', 'Active')->first();                
            if(!empty($admindata) && ($admindata->i_vendor_id != 0 || $admindata->i_emp_id != 0) && $logAccess == 'v_email') {                
                Session::remove('forgot_message');
                Session::flash('forgot_message',($logAccess == "v_email" ? FORGOT_ERR_USERNAME_PWS : FORGOT_ERR_USERNAME_PWS));                                
                return redirect(ADMIN_URL_LOGIN);
                exit;
            } else if(!empty($admindata) && ($admindata->i_dropship_emp_id != 0 || $admindata->i_client_id != 0 || $admindata->i_admin_id != 0) && $logAccess == 'v_username') {                 
                Session::remove('forgot_message');
                Session::flash('forgot_message',($logAccess == "v_username" ? FORGOT_ERR_EMAIL_PWS : FORGOT_ERR_EMAIL_PWS));                
                return redirect(ADMIN_URL_LOGIN);
                exit;
            }                          
            // $admindata = Admin::where('v_email','=',trim(Input::get('email')))->where('e_status','=','Active')->first();
			if($admindata!=""){
                $admindata = $admindata->toArray();
                //$username = $admindata['v_username'];
                $first_name = ucfirst($admindata['v_username']);
                //$last_name = ucfirst($admindata['v_lastname']);
                $full_name = $first_name;
                $admin_id = $admindata['id'];
                $access_code = $this->randomPassword();
                $u = Admin::find($admin_id);
                $u->remember_token = $access_code;
                $u->save();

                $link = ADMIN_URL.'reset-password/'.$access_code; // link for forgot password
                $date = date("j M Y, H:i A");
                $to = $admindata['v_email'];
                $user = array(
                    'email' => $to,
                    'name' => 'Dropship',
                );
                if ($admindata['v_email']) {
                    $adminemail = $admindata['v_email'];
                } 
                else {
                    $adminemail = '';
                }
                $site_name = SITE_NAME;
                $site_url = ADMIN_URL;
                // sending mail
                $send_mailid='';
                if(TESTING_MAILID!=''){
                    $send_mailid=TESTING_MAILID;
                }else{
                    $send_mailid=$user['email'];
                }                               
                // $send_mailid = 'testing.newdemo@gmail.com';
                Mail::send('emails.forgetPassMail' , array('link' => $link , 'date' => $date , 'site_name'=> $site_name , 'full_name'=> $full_name, 'site_url' => $site_url , 'admin_id' => $admin_id , 'code' => $access_code), function($message) use ($user,$send_mailid){
                    $message->to($send_mailid,$user['name'])->subject("Forgot Password request");
                });
                Session::remove('forgot_msg');
                Session::flash('forgot_msg',PWD_SENT);
                Session::remove('FORGOTPASS_FLAG');
                return Redirect(ADMIN_URL_LOGIN);
            } else {
                Session::remove('forgot_message');
                Session::flash('forgot_message',INVALID_EMAIL); // Message of invalid email
				//Session::set('FORGOTPASS_FLAG', '1');
                return Redirect(ADMIN_URL_LOGIN);
                
			} 
		} 
	}

    public function reset_password($code,$status = '')
    {
        $records = Admin::where('remember_token' , '=' , $code)->first();
        if( $records['remember_token'] == ''){
            return Redirect(ADMIN_URL_LOGIN);
            exit();
        }
        
        $id = $records['id'];
        $rec = Admin::find($id);
        
        if(Input::all()){
            $inputs = Input::all();
            if($inputs['password'] != "" && $inputs['rpassword'] != "" && $inputs['password'] == $inputs['rpassword']) {
                $rec->password = Hash::make($inputs['password']);
                $rec->remember_token = '';  
                if($status == 'force-reset'){
                    $rec->is_reset = '1';
                }   
                $rec->save();

                if($status == 'force-reset'){
                    $remember = true;
                    $ip_address = Request::ip();
                    $admindata = Auth::guard('admin')->attempt([ 'v_email' => $rec->v_email, 'password' => $inputs['password'],'e_status' => 'Active'],$remember);
                    if($remember == 1){
                        $cookie_username = Cookie::make('user_name', $rec->uname);
                        $cookie_password = Cookie::make('password', $inputs['password']);
                        //$cookie = $cookie_username.$cookie_password;
                    
                    } else {
                        $cookie_username = Cookie::forget('user_name');
                        $cookie_password = Cookie::forget('password');
                    }
                    $admin_data = Auth::guard('admin')->user();
                    if(!empty($admin_data)){
                        $date = date('Y-m-d h:i:s');
                        Session::put('CURRENTLOGIN',$date);
                        $admin_data= Admin::find($admin_data['id']);
                        
                        if($admin_data['i_vendor_id']==0 && $admin_data['i_client_id']==0 && $admin_data['i_emp_id']==0 && $admin_data['i_dropship_emp_id']==0 && $admin_data['i_admin_id'] == 0){   
                                $user_data= Admin::find($admin_data['id']);
                            
                        }else if($admin_data['i_vendor_id']!=0 && $admin_data['i_client_id']==0 && $admin_data['i_emp_id']==0 && $admin_data['i_dropship_emp_id']==0)
                        {
                            //vendor
                            $user_data= Admin::with("Vendor")->find($admin_data['id']);
                        }else if($admin_data['i_vendor_id']==0 && $admin_data['i_client_id']!=0 && $admin_data['i_emp_id']==0 && $admin_data['i_dropship_emp_id']==0){
                            //client
                            $user_data= Admin::with("Client")->find($admin_data['id']);
                        }else if($admin_data['i_vendor_id']==0 && $admin_data['i_client_id']==0 && $admin_data['i_emp_id']!=0 && $admin_data['i_dropship_emp_id']==0){
                            //employee
                            $user_data= Admin::with("Employee")->find($admin_data['id']);
                        }else if($admin_data['i_vendor_id']==0 && $admin_data['i_client_id']==0 && $admin_data['i_emp_id']==0 && $admin_data['i_dropship_emp_id']!=0){
                            $user_data= Admin::with("DropshipEmployee")->find($admin_data['id']);
                            
                        } 
                        else if($admin_data['i_vendor_id']==0 && $admin_data['i_client_id']==0 && $admin_data['i_emp_id']==0 && $admin_data['i_dropship_emp_id']==0 && $admin_data['i_admin_id']!=0){
                            $user_data= Admin::with("Admin")->find($admin_data['id']);
                            
                        } 
                        
                        $admin_data->d_last_login_time = date('Y-m-d H:i:s');
                        $admin_data->v_ip_address = $ip_address;
                        $admin_data->save();
                        Session::remove('FORGOTPASS_FLAG');
                        $forgotpass_flag = '0';
                        if (!empty($admindata)){
                            Session::remove('USER_DATA');
                            Session::remove('SETTING_DATA');
                            $setting_data=Setting::select('mail_template_flg')->first();
                            Session::put('SETTING_DATA',$setting_data);
                            Session::put('USER_DATA',$user_data);
                            return Redirect(ADMIN_URL.'dashboard')->withCookie($cookie_username)->withCookie($cookie_password);
                            exit;
                        }
                    }
                } else {
                    Session::flash('msg', PASSWORD_SUCCESS);
                    Session::remove('FORGOTPASS_FLAG');
                    return Redirect(ADMIN_URL_LOGIN);
                    exit();
                }

            } else{
                Session::flash('message', 'Invalid Password');
            }
            
        }

        return View('admin.authenticate.reset_password')->with(array('record'=>$records, 'status'=>$status))->with('title' , 'Reset Password');
    }

    public function my_profile()
	{
        
        
        $records = Auth::guard('admin')->user();
        $id = $records['id'];
		if(Input::all()) {
            $flag = 1;
            $inputs = Input::all();
            $validator = Validator::make(Request::all(), array(
                //"v_email" =>'required|unique:tbl_admin,v_email,' .$id. ',id,deleted_at,NULL',
                "v_username" =>'required|unique:tbl_admin,v_username,' .$id. ',id,deleted_at,NULL'));
               
            if ($validator->fails()) {
                $flag = 0;
                return $validator->errors();
            } else{
                if(isset($inputs['password_old']) && $inputs['password_old'] != ''){
                        if($inputs['password_new'] != ''){
                            if(!(Hash::check($inputs['password_old'], $records->password))) {
                                $flag = 0;
                                return 'false_password';
                            } else {
                                $flag = 1;
                            }
                        }
                } if($flag == 1){
                    $records->v_email = trim($inputs['v_email']);
                    $records->v_username = trim($inputs['v_username']);
                    if(Input::has("password_new"))
               {
                    $records->password = Hash::make($inputs['password_new']);
                    $objEmailTemplate = MailTemplate::find(8)->toArray();
                    $strTemplate = $objEmailTemplate['t_email_content'];
                    $strTemplate = str_replace('[PROJECT_NAME]',PROJECT_NAME,$strTemplate);
                    $strTemplate = str_replace('[USER_NAME]',trim($inputs['v_username']),$strTemplate);
                    $strTemplate = str_replace('[PASSWORD]',trim($inputs['password_new']),$strTemplate);
                    $strTemplate = str_replace('[IMAGE_PATH]', EMAIL_LOGO_IMAGE_PATH, $strTemplate);
                    $strTemplate = str_replace('[SITE_NAME]',SITE_NAME,$strTemplate);
                    $strTemplate = str_replace('[SITE_URL]',SITE_URL,$strTemplate);
                    if($inputs['user_type']=="client")
                    {
                        $subject='Update client profile at '.PROJECT_NAME;
                    }else if($inputs['user_type']=="employee")
                    {
                        $subject='Update employee profile at '.PROJECT_NAME;
                    }else if($inputs['user_type']=="vendor"){
                        $subject='Update vendor profile at '.PROJECT_NAME;
                    }else if($inputs['user_type']=="dropship_employee"){
                        $subject='Update Dropship Employee profile at '.PROJECT_NAME;
                    }else{
                        $subject='Update Superadmin profile at '.PROJECT_NAME;
                    }
                    
                    // mail sent to user with new link
                    $mail_flag=Session::get('SETTING_DATA');
                    $send_mailid='';
                    if(TESTING_MAILID!=''){
                        $send_mailid=TESTING_MAILID;
                    }else{
                        $send_mailid=$inputs['v_email'];
                    }                       
                    Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$strTemplate), function($message) use ($inputs,$subject,$send_mailid){ 
                        $message->to($send_mailid);   
                        $message->subject($subject);                                
                    });
                    
                     
               }
                if($inputs['user_type']=="client"){
                   $client = Client::where('id',$inputs['id'])->first();    
                    $client->v_firstname = trim($inputs['v_firstname']);
                    $client->v_lastname = trim($inputs['v_lastname']);
                    $client->v_company = trim($inputs['v_company']);
                    $client->v_fax = trim($inputs['v_fax']);
                    $client->v_email = trim($inputs['v_email']);
                    $client->v_address_1 = trim($inputs['v_address_1']);
                    $client->v_address_2 = trim($inputs['v_address_2']);
                    $client->v_city = trim($inputs['v_city']);
                    $client->v_state = trim($inputs['v_state']);
                    $client->v_zipcode = trim($inputs['v_zipcode']);
                    $client->v_phone = trim($inputs['v_phone']);
                    $client->l_note = trim($inputs['l_note']);    
                    if($inputs['v_logo'] != '') {
                        if($inputs['v_logo'] != SITE_URL.CLIENT_ADD_IMG_PATH_URL.$client->company_logo)
                         {
                            @unlink(WWW_ROOT.CLIENT_ADD_IMG_PATH_URL.($client->company_logo));
                            $client->company_logo =$this->saveImage($inputs['v_logo'],CLIENT_ADD_IMG_PATH_URL);
                         }
                    } else{
                        @unlink(WWW_ROOT.CLIENT_ADD_IMG_PATH_URL.($client->company_logo));
                        $client->company_logo ='';
                    }         
                    $client->updated_at=date('Y-m-d h:i:s');
                    if(Input::has('v_company')){
                        $folder = $this->getFolderName(trim($inputs['v_company']));
                        $result=File::deleteDirectory(CLIENT_FOLDER_PATH.$client->v_folder_name, false);
                        $result = File::makeDirectory(CLIENT_FOLDER_PATH.$folder, 0777, true, true);
                        $client->v_folder_name=$folder;
                    }
                    $client->save();
                    $user_data= Admin::with("Client")->find($id);
                    Session::remove('USER_DATA');
                    Session::put('USER_DATA',$user_data);

                }else if($inputs['user_type']=="employee"){                   
                    $employee = Employee::where('id',$inputs['id'])->first();    
                    $employee->v_vname = trim($inputs['v_vname']);
                    $employee->v_email = trim($inputs['v_email']);            
                    $employee->v_phone = trim($inputs['v_phone']);
                    $employee->v_note = trim($inputs['v_note']);
                    $employee->v_fax = trim($inputs['v_fax']);
                    $employee->v_url = trim($inputs['v_url']);
                    $employee->updated_at=date('Y-m-d h:i:s');
                    $employee->save();
                    $user_data= Admin::with("Employee")->find($id);
                    Session::remove('USER_DATA');
                    Session::put('USER_DATA',$user_data);
                    if(isset($inputs['default_img']) && $inputs['default_img'] == '0'){
                        if(isset($inputs['imgbase64']) && $inputs['imgbase64'] != '') {
                          //  if(isset($inputs['x']) && $inputs['x'] != 0 && isset($inputs['y']) && $inputs['y'] != 0) {
                                $profileImgPath = ADMIN_ADD_IMG_PATH_URL;
                                $profileImgThumbPath = ADMIN_ADD_THUMB_IMAGE_PATH;
                                $imageName = $this->cropImages($inputs['imgbase64'],$inputs['x'],$inputs['y'],$inputs['h'],$inputs['w'],$profileImgPath,$profileImgThumbPath);
                                @unlink(WWW_ROOT.ADMIN_ADD_THUMB_IMAGE_PATH.$records->v_image);
                                @unlink(WWW_ROOT.ADMIN_ADD_IMG_PATH_URL.($records->v_image));
                                $records->v_image =$imageName;
                          //  }
                        }
                    } else{
                        $records->v_image = '';
                    }

                }else if($inputs['user_type']=="dropship_employee"){   
                    $employee = DropshipEmployee::find($records['i_dropship_emp_id']);  
                    $admin = Admin::where('i_dropship_emp_id',$records['i_dropship_emp_id'])->orWhere('i_admin_id',$records['i_dropship_emp_id'])->first();   
                    $validator = Validator::make(Input::all(), array(
                        "v_email" =>'required|unique:tbl_dropship_employee,v_email,' .$records['i_dropship_emp_id']. ',id,deleted_at,NULL',
                        "v_username" =>'required|unique:tbl_admin,v_username,' .$admin->id. ',id,deleted_at,NULL'));

                    if ($validator->fails()) {
                        return $validator->errors();
                    }else{
                        $employee->v_vname = trim($inputs['v_vname']);
                        $employee->v_email = trim($inputs['v_email']);            
                        $employee->v_phone = trim($inputs['v_phone']);
                        $employee->v_note = trim($inputs['v_note']);
                        $employee->v_fax = trim($inputs['v_fax']);                    
                        $employee->updated_at = date('Y-m-d h:i:s');
                        if(isset($inputs['imgbase64']) && $inputs['imgbase64'] != '') {
                           // if(isset($inputs['x']) && $inputs['x'] != 0 && isset($inputs['y']) && $inputs['y'] != 0) {
                                $profileImgPath = ADMIN_ADD_IMG_PATH_URL;
                                $profileImgThumbPath = ADMIN_ADD_THUMB_IMAGE_PATH;
                                $imageName = $this->cropImages($inputs['imgbase64'],$inputs['x'],$inputs['y'],$inputs['h'],$inputs['w'],$profileImgPath,$profileImgThumbPath);
                                @unlink(WWW_ROOT.ADMIN_ADD_THUMB_IMAGE_PATH.$records->v_image);
                                @unlink(WWW_ROOT.ADMIN_ADD_IMG_PATH_URL.($records->v_image));
                           // }
                            
                            $records->v_image =$imageName;
                        }
                        else{
                            $records->v_image = '';
                                            
                        }
                        $employee->save();
                     }
                    

                } else if($inputs['user_type']=="super"){   
                    $employee = DropshipEmployee::find($records['i_admin_id']);  
                    $admin = Admin::where('i_admin_id',$records['i_admin_id'])->first();   
                    $validator = Validator::make(Input::all(), array(
                        "v_email" =>'required|unique:tbl_dropship_employee,v_email,' .$records['i_admin_id']. ',id,deleted_at,NULL',
                        "v_username" =>'required|unique:tbl_admin,v_username,' .$admin->id. ',id,deleted_at,NULL'));

                    if ($validator->fails()) {
                        return $validator->errors();
                    }else{
                        $employee->v_vname = trim($inputs['v_vname']);
                        $employee->v_email = trim($inputs['v_email']);            
                        $employee->v_phone = trim($inputs['v_phone']);
                        $employee->v_note = trim($inputs['v_note']);
                        $employee->v_fax = trim($inputs['v_fax']);                    
                        $employee->updated_at = date('Y-m-d h:i:s');
                        if(isset($inputs['imgbase64']) && $inputs['imgbase64'] != '') {
                           // if(isset($inputs['x']) && $inputs['x'] != 0 && isset($inputs['y']) && $inputs['y'] != 0) {
                                $profileImgPath = ADMIN_ADD_IMG_PATH_URL;
                                $profileImgThumbPath = ADMIN_ADD_THUMB_IMAGE_PATH;
                                $imageName = $this->cropImages($inputs['imgbase64'],$inputs['x'],$inputs['y'],$inputs['h'],$inputs['w'],$profileImgPath,$profileImgThumbPath);
                                @unlink(WWW_ROOT.ADMIN_ADD_THUMB_IMAGE_PATH.$records->v_image);
                                @unlink(WWW_ROOT.ADMIN_ADD_IMG_PATH_URL.($records->v_image));
                           // }
                            
                            $records->v_image =$imageName;
                        }
                        else{
                            $records->v_image = '';
                                            
                        }
                        $employee->save();
                     }
                    

                } 
                else if($inputs['user_type']=="vendor")
                {
                    
                    $vendor = Vendor::where('id', $inputs['id'])->first();
                    $validator = Validator::make(Input::all(), array(
                        "v_vendor_id" =>'nullable|unique:tbl_vendor,v_vendor_id,' .$vendor['id']. ',id,deleted_at,NULL'));
                    
                    if ($validator->fails()) {
                        return $validator->errors();
                    }else{
                            if(!empty($inputs['order_email'])) {
                                $order_email = array_filter($inputs['order_email']);
                                $order_email_str = implode (", ", $order_email);                        
                                $vendor->order_email = trim($order_email_str);
                            } 
                            if(!empty($inputs['po_email'])) {
                                $po_email = array_filter($inputs['po_email']);
                                $po_email_str = implode (", ", $po_email);                                                                        
                                $vendor->po_email = trim($po_email_str);
                            }
                            $vendor->v_vendor_name = trim($inputs['v_vendor_name']);
                            $vendor->v_email = trim($inputs['v_email']);
                            $vendor->v_vendor_id = trim($inputs['v_vendor_id']);
                            $vendor->v_company_name = trim($inputs['v_company_name']);
                            if (Input::has('v_phone_number')) {
                                $vendor->v_phone_number = trim($inputs['v_phone_number']);
                            }
                            $vendor->l_note = trim($inputs['l_note']);
                            $vendor->v_fax = trim($inputs['v_fax']);
                            $vendor->v_url = trim($inputs['v_url']);
                            $vendor->save();
                            $user_data = Admin::with("Vendor")->find($id);
                            Session::remove('USER_DATA');
                            Session::put('USER_DATA', $user_data);
                            if (isset($inputs['default_img']) && $inputs['default_img'] == '0') {
                                if (isset($inputs['imgbase64']) && $inputs['imgbase64'] != '') {
                                    // if(isset($inputs['x']) && $inputs['x'] != 0 && isset($inputs['y']) && $inputs['y'] != 0) {
                                    $profileImgPath = ADMIN_ADD_IMG_PATH_URL;
                                    $profileImgThumbPath = ADMIN_ADD_THUMB_IMAGE_PATH;
                                    $imageName = $this->cropImages($inputs['imgbase64'], $inputs['x'], $inputs['y'], $inputs['h'], $inputs['w'], $profileImgPath, $profileImgThumbPath);
                                    @unlink(WWW_ROOT . ADMIN_ADD_THUMB_IMAGE_PATH . $records->v_image);
                                    @unlink(WWW_ROOT . ADMIN_ADD_IMG_PATH_URL . ($records->v_image));
                                    $records->v_image = $imageName;
                                    // }
                                }
                            } else {
                                $records->v_image = '';
                            }
                        }
                    }else{
                    
                    $records->v_email = trim($inputs['v_email']);
                    $records->v_username = trim($inputs['v_username']);
                    $records->v_firstname = trim($inputs['v_firstname']);
                    $records->v_lastname = trim($inputs['v_lastname']);
                    if($inputs['password_new'] != "" && $inputs['password_retype'] != "" && $inputs['password_new'] == $inputs['password_retype']) {
                         $records->password = Hash::make($inputs['password_new']);
                         $records->remember_token = '';   
                    }    
                    if(isset($inputs['default_img']) && $inputs['default_img'] == '0'){
                        if(isset($inputs['imgbase64']) && $inputs['imgbase64'] != '') {
                            $profileImgPath = ADMIN_ADD_IMG_PATH_URL;
                            $profileImgThumbPath = ADMIN_ADD_THUMB_IMAGE_PATH;
                            $imageName = $this->cropImages($inputs['imgbase64'],$inputs['x'],$inputs['y'],$inputs['h'],$inputs['w'],$profileImgPath,$profileImgThumbPath);
                            @unlink(WWW_ROOT.ADMIN_ADD_THUMB_IMAGE_PATH.$records->v_image);
                            @unlink(WWW_ROOT.ADMIN_ADD_IMG_PATH_URL.($records->v_image));
                            $records->v_image =$imageName;
                        }
                    } else {
                        $records->v_image = '';
                                        
                    }
                }
    			
    			if($records->save()){
                    if($inputs['user_type']=='')
                    {                        
                        $user_data = $records;
                        Session::remove('USER_DATA');
                        Session::put('USER_DATA',$user_data);
                    }else{
                        $user_data = $records;
                        Session::remove('USER_DATA');
                        Session::put('USER_DATA',$user_data);
                    }
                    Session::flash('success-message', 'Record has been updated successfully.');
                    return 'TRUE';
                }
            }
            }
		} else{
            $records=Session::get('USER_DATA');
		    return View('admin.authenticate.my_profile',array('title'=>'My Profile','records' => $records));
		} 
	}

    public function my_settings()
    {
        $setting_data = Setting::first();
        $admin_data = Admin::where('e_type','Super')->first();
        if (Input::all()) {
            $inputs = Input::all();
            if($setting_data != '') {
               $query = Setting::query(); 
            } else {
                $setting_data = new Setting;
            }
            $setting_data->v_site_url = trim($inputs['v_site_url']);
            if($setting_data->save()){
                if(!empty($admin_data)){
                    $admin_data->v_email = trim($inputs['v_email']);
                    if($admin_data->save()){
                        return 'TRUE';
                    }
                }
            }
        }else{
		      return View('admin.authenticate.my_settings',array('title'=>'My Settings','admin_data' => $admin_data, 'setting_data' => $setting_data));
		} 
        
    }
    public function getFolderName($temp)
    {
        $temp = strtolower(trim($temp));        
        $temp = str_replace("   ", " ",$temp);
        $temp = str_replace("  ", " ",$temp);
        $temp = str_replace(" ", "_",$temp);
        $temp = str_replace("'", "",$temp);
        $temp = str_replace("\\", "",$temp);
        $temp = str_replace("\"", "",$temp);
        $temp = str_replace("?", "",$temp);
        $temp = str_replace("/", "_or_",$temp);
        $temp = str_replace("&", "and",$temp);
        $temp = str_replace("(", "",$temp);
        $temp = str_replace(")", "",$temp);
        $temp = str_replace("`", "",$temp);
        $temp = str_replace("%", "",$temp);
        $temp = str_replace("^", "",$temp);
        $temp = str_replace(";", "",$temp);
        $temp = str_replace(":", "",$temp);
        $temp = str_replace("<", "",$temp);
        $temp = str_replace(">", "",$temp);
        $temp = str_replace("~", "",$temp);
        
        return trim(stripslashes($temp));
    }
    public function saveImage($base64img,$path) {
        $v_random_image = time().'-'.str_random(6);
        $tmpFile = $v_random_image;
        if (strpos($base64img,'data:image/jpeg;base64,') !== false) {
            $base64img = str_replace('data:image/jpeg;base64,', '', $base64img);
            $tmpFile = $v_random_image.'.jpg';
        }
        if (strpos($base64img,'data:image/png;base64,') !== false) {
            $base64img = str_replace('data:image/png;base64,', '', $base64img);
            $tmpFile = $v_random_image.'.png';
        }
        if (strpos($base64img,'data:image/webp;base64,') !== false) {
            $base64img = str_replace('data:image/webp;base64,', '', $base64img);
            $tmpFile = $v_random_image.'.png';
        }
        if (strpos($base64img,'data:image/jpg;base64,') !== false) {
            $base64img = str_replace('data:image/jpg;base64,', '', $base64img);
            $tmpFile = $v_random_image.'.jpg';
        }
        if (strpos($base64img,'data:image/gif;base64,') !== false) {
            $base64img = str_replace('data:image/gif;base64,', '', $base64img);
            $tmpFile = $v_random_image.'.gif';
        }        
        $data = base64_decode($base64img);
        $file = $path.$tmpFile;
        file_put_contents($file, $data);

        return $tmpFile;
    }
    /*public function change_password() {
        $records = Auth::guard('admin')->user();
        $id = $records['id'];
        if(Input::all()) {
            $inputs = Input::all();
            $validator = Validator::make(Request::all(), array(
                "v_email" =>'required|unique:tbl_admin,v_email,' .$id. ',id,deleted_at,NULL'));
            if ($validator->fails()) {
                return $validator->errors();
            } else{
                $records->email = trim($inputs['v_email']);
                $records->firstname = trim($inputs['firstname']);
                $records->lastname = trim($inputs['lastname']);
                // checking two passwords
                if($inputs['password_new'] != "" && $inputs['password_retype'] != "" && $inputs['password_new'] == $inputs['password_retype']) {
                    $records->password = Hash::make($inputs['password_new']);
                    $records->remember_token = '';  
                }
                if($records->save()){
                    return 'TRUE';
                }
            }
        } else{
              return View('admin.authenticate.change_password',array('title'=>'Change Password','records' => $records));
        } 
    }*/
    public function hashMake(){
        $records=Admin::where('e_type','=','Super')->get();
        foreach ($records as $key => $value) {
            $admin_obj=Admin::find($value->id);
            $admin_obj->password=Hash::make($value->password);
            $admin_obj->save();
        }
        echo "done";
        // dd($records);
    }
    public function vendorEmail(){
        $records=Vendor::get();        
        foreach ($records as $key => $value) {
            $admin_obj=Admin::where('i_vendor_id',$value->id)->first();            
            $admin_obj->v_email = $value->v_email;
            $admin_obj->e_status = $value->e_status;
            $admin_obj->save();
        }
        echo "done";
        // dd($records);
    }
 }