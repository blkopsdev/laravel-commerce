<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request, Hash, Mail, Session, Redirect, Auth, Validator,Excel,Input,Cookie,DB;
use Illuminate\Support\Str,View;
use Route;

class BaseController extends Controller {

	public $tools_list = array('Adodb Photoshop','PHP Editor','Wamp');
    protected $auth;
    public $permissions = [];
    public function __construct() {
          $this->auth =Auth::guard('admin');
         if(!empty($this->auth->user()->id) && ($this->auth->user()->role != '1'))
         {
            $permissions = \App\Models\AccessLevel::where('i_role',$this->auth->user()->role)->with('module')->whereHas('module', function($q1){
            })->get();
            $resultarr = [];
            foreach($permissions->toArray() as $key=>$val)
            {
             $resultarr[$val["module"]['id']]= $val;        
            }
            $this->permissions = $resultarr;
            View::share('UserPermission', $resultarr);
         }
    }
    public function saveImage($base64img,$path) {
        $v_random_image = time().'-'.str_random(6).'.png';
        $tmpFile = $v_random_image;
        if (strpos($base64img,'data:image/jpeg;base64,') !== false) {
                $base64img = str_replace('data:image/jpeg;base64,', '', $base64img);
                //$tmpFile = $v_random_image.'.jpeg';
            }
            if (strpos($base64img,'data:image/png;base64,') !== false) {
                $base64img = str_replace('data:image/png;base64,', '', $base64img);
                //$tmpFile = $v_random_image.'.png';
            }
            if (strpos($base64img,'data:image/webp;base64,') !== false) {
                $base64img = str_replace('data:image/webp;base64,', '', $base64img);
                //$tmpFile = $v_random_image.'.png';
            }
            
            if (strpos($base64img,'data:image/jpg;base64,') !== false) {
                $base64img = str_replace('data:image/jpg;base64,', '', $base64img);
                //$tmpFile = $v_random_image.'.jpg';
            }
            if (strpos($base64img,'data:image/gif;base64,') !== false) {
                $base64img = str_replace('data:image/gif;base64,', '', $base64img);
                //$tmpFile = $v_random_image.'.gif';
            }
        //$tmpFile = $v_random_image.'.png';
        $data = base64_decode($base64img);
        $file = $path.$tmpFile;
        
        file_put_contents($file, $data);
        
        return $tmpFile;
    }
    
    /* For Crop Image */
    public function crop($file_thumb,$x , $y , $w , $h)
    {
        $targ_w = $targ_h = 550;
        $original_info = getimagesize($file_thumb);
        $type = $original_info['mime'];
        
        if($type == 'image/jpeg' || $type == 'image/jpg')
        {
            $img_r = imagecreatefromjpeg($file_thumb);
        }
        if($type == 'image/png')
        {
            $img_r = imagecreatefrompng($file_thumb);
        }
        if($type == 'image/gif')
        {
            $img_r = imagecreatefromgif($file_thumb);
        }

        $dst_r = imagecreatetruecolor( $targ_w, $targ_h );
        
        imagecopyresampled($dst_r,$img_r,0,0,intval($x),intval($y), $targ_w,$targ_h, intval($w),intval($h));
        header("Content-type: image/jpg");
        
        if($type == 'image/jpeg' || $type == 'image/jpg')
        {
            imagejpeg($dst_r, $file_thumb);
        }
        if($type == 'image/png')
        {
            imagepng($dst_r, $file_thumb);
        }
        if($type == 'image/gif')
        {
            imagegif($dst_r, $file_thumb);
        }
        
    } 
    

     function random_number($digit){
        $allowed_characters = array(1,2,3,4,5,6,7,8,9,0);
        $pass = '';
        for($i = 1;$i <= $digit; $i++){
            $pass .= $allowed_characters[rand(0, count($allowed_characters) - 1)];
        }
        $count = Vendor::where('v_vendor_number',$pass)->count();
        if($count > 0){
            return $this->random_number(8);
        } else {
            return $pass;
        }
      
    }
    public function admin_types(){
                $adminType = [
            2 => 'Client',
            3 => 'Vendor',
            4 => 'Employee',
            5 => 'Dropship Employee'
        ];
                return $adminType;
    }
    public function access_levels(){
                $access_levels = ['Read','Create','Update','Delete','Download','Import Data Feed'];
                return $access_levels;
    }
    public function accessLevels(){
                $access_levels = ['e_view','e_add','e_edit','e_delete','e_export','e_import'];
                return $access_levels;
    }
}