<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use Mail, Session, Redirect, Auth, Validator,Excel,Cookie,Request;
use App\Models\User, App\Models\Client,App\Models\Admin,App\Models\Employee,App\Models\Vendor,App\Models\MailTemplate,App\Models\DropshipEmployee;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use File;
use Illuminate\Support\Facades\Hash;
class DropshipEmployeeController extends BaseController
{
    public function getIndex()
    {
        return View('admin.dropship_employee.index', array('title' => 'Employee List'));
    }

    public function anyListAjax(Request $request) //Employee Listing
    {
        $data = Request::all();
        $auth_user = Auth::guard('admin')->user();
        if ($data) {

            $query = new DropshipEmployee();
            $sortColumn = array('', 'v_vname','v_email', 'v_phone','e_type','e_status');
            $query=$query->select('id', 'v_vname','v_email', 'v_phone','e_type','e_status');
            if (isset($data['v_vname']) && $data['v_vname'] != '') {
                $query = $query->where('v_vname', 'LIKE', '%' . trim($data['v_vname']) . '%');
            }
            if (isset($data['v_email']) && $data['v_email'] != '') {
                $query = $query->where('v_email', 'LIKE', '%' . trim($data['v_email']) . '%');
            }            
            if (isset($data['v_phone']) && $data['v_phone'] != '') {
                $query = $query->where('v_phone', 'LIKE', '%' . trim($data['v_phone']) . '%');
            }           
            if (isset($data['e_status']) && $data['e_status'] != '') {
                $query = $query->where('e_status', '=',trim($data['e_status']));
            } 
            if (isset($data['e_type']) && $data['e_type'] != '') {
                $query = $query->where('e_type', '=',trim($data['e_type']));
            } 
            $query = $query->where('v_email', '!=',$auth_user->v_email);
            $rec_per_page = REC_PER_PAGE;

            if (isset($data['length'])) {
                if ($data['length'] == '-1') {
                    $rec_per_page = '';
                } else {
                    $rec_per_page = $data['length'];
                }
            }

            $sort_order = $data['order']['0']['dir'];
            $order_field = $sortColumn[$data['order']['0']['column']];

            if ($sort_order != '' && $order_field != '') {
                $query = $query->orderBy($order_field, $sort_order);
            } else {
                $query = $query->orderBy('id', 'desc');
            }
            if($rec_per_page == ''){
                $temp_records = $query->get();
                $rec_per_page = count($temp_records);
                $users = $query->paginate($rec_per_page);
            } else {
                $users = $query->paginate($rec_per_page);
            }
            //$users = $query->paginate($rec_per_page);
            $arrUsers = $users->toArray();                        
            $data = array();
            foreach ($arrUsers['data'] as $key => $val) {
                $index = 0;
                $data[$key][$index++] = '<input type="checkbox" name="id[]" value="' . $val['id'] . '" class="delete_' . $val['id'] . '">';
                $data[$key][$index++] = $val['v_vname'];
                $data[$key][$index++] = $val['v_email'];
                $data[$key][$index++] = $val['v_phone'];
                if($val['e_type'] == 'Super'){
                    $data[$key][$index++] = 'Super Admin';
                } else {
                    $data[$key][$index++] = 'Employee';
                }
                
                if ($val['e_status'] == 'Active') {
                    $intStatus = 'Active';
                } else {
                    $intStatus = 'Inactive';
                }
                $data[$key][$index++] = '<a href="javascript:void(0);" data-id="' . $val['id'] . '" id="change_status" rel="' . $intStatus . '" change-url="' . ADMIN_URL . 'dropship-employee/change-status">' . $intStatus . '</a>';


                $action = '<div class="actions"><a class="edit btn default btn-xs black" rel="' . $val['id'] . '" href="' . ADMIN_URL . 'dropship-employee/edit/' . $val['id'] . '"  title="Edit"><i class="fa fa-edit"></i></a><a href="javascript:;" id="delete_record" rel="' . $val['id'] . '" delete-url="' . ADMIN_URL . 'dropship-employee/delete/' . $val['id'] . '" class="btn default btn-xs black delete" title="Delete"><i class="icon-trash"></i></a>';
                $data[$key][$index++] = $action;
            }
            $return_data['data'] = $data;
            $return_data['recordsTotal'] = $arrUsers['total'];
            $return_data['recordsFiltered'] = $arrUsers['total'];
            $return_data['data_array'] = $arrUsers['data'];
            return $return_data;
        }
    }

    public function postChangeStatus()
    {
        $data = Input::all();
        $employee = DropshipEmployee::find($data['id']);
        $admin=Admin::where('i_dropship_emp_id',$data['id'])->orWhere('i_admin_id',$data['id'])->first();
        
        if($employee->e_status=='Active')
                $status='Inactive';
        else
                $status='Active';
        $employee->e_status = $status;
        $admin->e_status = $status;
        if ($employee->save()) {
            $admin->save();
            return 'TRUE';
        } else {
            return 'FALSE';
        }  
    }

    public function anyAdd()
    {
        $auth_user = Auth::guard('admin')->user();
        if (Input::all()) {
            $inputs = Input::all();                        
            $employee = new DropshipEmployee();
            $validator = Validator::make(Input::all(), array(
                "v_email" => 'required|unique:tbl_dropship_employee,v_email,NULL,id,deleted_at,NULL',
                "v_username" => 'required|unique:tbl_admin,v_username,NULL,id,deleted_at,NULL'));
            if ($validator->fails()) {
                return $validator->errors();
            } else {
                    $employee->v_vname = trim($inputs['v_vname']);
                    $employee->v_email = trim($inputs['v_email']);            
                    if(Input::has('v_phone')){
                        $employee->v_phone = trim($inputs['v_phone']);
                    }  
                    if(Input::has('v_note')){
                        $employee->v_note = trim($inputs['v_note']);
                    }  
                    if(Input::has('v_fax')){
                        $employee->v_fax = trim($inputs['v_fax']);
                    }  
                    if(Input::has('v_url')){
                        $employee->v_url = trim($inputs['v_url']);
                    }  
                    $employee->e_status = trim($inputs['e_status']);    
                    $employee->e_type = trim($inputs['e_type']);    
                    $employee->created_at=date('Y-m-d h:i:s');
                    $employee->updated_at=date('Y-m-d h:i:s');            
                    if ($employee->save()) {
                        $last_inserted_id=$employee->id;
                        $admin=new Admin();
                        $admin->v_username=trim($inputs['v_username']);
                        $admin->password=Hash::make(trim($inputs['password']));
                        $admin->v_email=trim($inputs['v_email']);
                        
                        $admin->e_status = trim($inputs['e_status']);  
                        if($employee->e_type == 'Super'){
                           $admin->role='1'; 
                           $admin->i_admin_id = $last_inserted_id;
                        } else {
                            $admin->role='5'; 
                            $admin->i_dropship_emp_id=$last_inserted_id;
                        }
                        $admin->e_type= $employee->e_type;
                        
                        $admin->created_at=date('Y-m-d h:i:s');
                        $admin->updated_at=date('Y-m-d h:i:s');
                        if(isset($inputs['default_img']) && $inputs['default_img'] == '0'){
                        if(isset($inputs['imgbase64']) && $inputs['imgbase64'] != '') {
                            $profileImgPath = ADMIN_ADD_IMG_PATH_URL;
                            $profileImgThumbPath = ADMIN_ADD_THUMB_IMAGE_PATH;
                            $imageName = $this->cropImages($inputs['imgbase64'],$inputs['x'],$inputs['y'],$inputs['h'],$inputs['w'],$profileImgPath,$profileImgThumbPath);
                            @unlink(WWW_ROOT.ADMIN_ADD_THUMB_IMAGE_PATH.$admin->v_image);
                            @unlink(WWW_ROOT.ADMIN_ADD_IMG_PATH_URL.($admin->v_image));
                            $admin->v_image =$imageName;
                        }
                        } else{
                            $admin->v_image = '';
                                            
                        }                
                        if($admin->save())
                        {
                            $objEmailTemplate = MailTemplate::find(7)->toArray();
                            $strTemplate = $objEmailTemplate['t_email_content'];
                            $strTemplate = str_replace('[PROJECT_NAME]',PROJECT_NAME,$strTemplate);
                            $strTemplate = str_replace('[USER_NAME]',trim($inputs['v_username']),$strTemplate);
                            $strTemplate = str_replace('[PASSWORD]',trim($inputs['password']),$strTemplate);
                            $strTemplate = str_replace('[IMAGE_PATH]', EMAIL_LOGO_IMAGE_PATH, $strTemplate);
                            $strTemplate = str_replace('[SITE_NAME]',SITE_NAME,$strTemplate);
                            $strTemplate = str_replace('[SITE_URL]',SITE_URL,$strTemplate);
                            $subject='New dropship employee registration at '.PROJECT_NAME;
                            // mail sent to user with new link
                            $mail_flag=Session::get('SETTING_DATA');
                            $send_mailid='';
                            if(TESTING_MAILID!=''){
                                $send_mailid=TESTING_MAILID;
                            }else{
                                $send_mailid=$inputs['v_email'];
                            }                            
//                            Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$strTemplate), function($message) use ($inputs,$subject,$send_mailid){ 
//                                $message->to($send_mailid);   
//                                $message->subject($subject);                                
//                            });                        
                            Session::flash('success-message', 'Employee has been added successfully.');
                            return '';    
                        }
                        
                    }
                }
        } else {            
            //$vendor_data=Vendor::where('id',$auth_user->i_vendor_id)->get();            
            $vendor_data=Vendor::orderBy('v_vendor_name')->get();
            return View('admin.dropship_employee.add_employee', array('title' => 'Add Employee','vendor_list'=> $vendor_data));
        }
        return Redirect(ADMIN_URL . 'dropship-employee');

    }

    public function getDelete($id)
    {
        $employee = DropshipEmployee::find($id);
        if (!empty($employee)) {
            $admin = Admin::where('i_dropship_emp_id',$id)->orWhere('i_admin_id',$id)->delete();
            $employee->delete();
            return 'TRUE';
        } else {
            return 'FALSE';
        }  
    }
    public function anyEdit($id)
    {
        $auth_user = Auth::guard('admin')->user();

        if (Input::all()) {
            $inputs = Input::all();                    
            $employee = DropshipEmployee::find($id);  
            $admin = Admin::where('i_dropship_emp_id',$id)->orWhere('i_admin_id',$id)->first(); 
            $validator = Validator::make(Input::all(), array(
                "v_email" =>'required|unique:tbl_dropship_employee,v_email,' .$id. ',id,deleted_at,NULL',
                "v_username" =>'required|unique:tbl_admin,v_username,' .$admin->id. ',id,deleted_at,NULL'));

            if ($validator->fails()) {
                return $validator->errors();
            }else{
            $employee->v_vname = trim($inputs['v_vname']);
            $employee->v_email = trim($inputs['v_email']);            
            $employee->v_phone = trim($inputs['v_phone']);
            $employee->v_note = trim($inputs['v_note']);
            $employee->v_fax = trim($inputs['v_fax']);
            $employee->e_status = trim($inputs['e_status']);
            $employee->e_type = trim($inputs['e_type']);
            $employee->updated_at=date('Y-m-d h:i:s');           
            if ($employee->save()) {                
                $admin=Admin::where('i_dropship_emp_id',$employee->id)->orWhere('i_admin_id',$id)->first();
                $admin->e_status = trim($inputs['e_status']);
                $admin->v_username = trim($inputs['v_username']);
                if(Input::has('password')){
                    $admin->password=Hash::make(trim($inputs['password']));
                    $objEmailTemplate = MailTemplate::find(8)->toArray();
                    $strTemplate = $objEmailTemplate['t_email_content'];
                    $strTemplate = str_replace('[PROJECT_NAME]',PROJECT_NAME,$strTemplate);
                    $strTemplate = str_replace('[USER_NAME]',trim($inputs['v_username']),$strTemplate);
                    $strTemplate = str_replace('[PASSWORD]',trim($inputs['password']),$strTemplate);
                    $strTemplate = str_replace('[IMAGE_PATH]', EMAIL_LOGO_IMAGE_PATH, $strTemplate);
                    $strTemplate = str_replace('[SITE_NAME]',SITE_NAME,$strTemplate);
                    $strTemplate = str_replace('[SITE_URL]',SITE_URL,$strTemplate);
                    $subject='Update dropship employee profile at '.PROJECT_NAME;
                    // mail sent to user with new link
                    $mail_flag=Session::get('SETTING_DATA');
                    $send_mailid='';
                    if(TESTING_MAILID!=''){
                        $send_mailid=TESTING_MAILID;
                    }else{
                        $send_mailid=$inputs['v_email'];
                    }
                    /* Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$strTemplate), function($message) use ($inputs,$subject,$send_mailid){ 
                        $message->to($send_mailid);   
                        $message->subject($subject);                                
                    }); */
                    
                }
                if(isset($inputs['default_img']) && $inputs['default_img'] == '0'){
                        if(isset($inputs['imgbase64']) && $inputs['imgbase64'] != '') {
                            $profileImgPath = ADMIN_ADD_IMG_PATH_URL;
                            $profileImgThumbPath = ADMIN_ADD_THUMB_IMAGE_PATH;
                            $imageName = $this->cropImages($inputs['imgbase64'],$inputs['x'],$inputs['y'],$inputs['h'],$inputs['w'],$profileImgPath,$profileImgThumbPath);
                            @unlink(WWW_ROOT.ADMIN_ADD_THUMB_IMAGE_PATH.$admin->v_image);
                            @unlink(WWW_ROOT.ADMIN_ADD_IMG_PATH_URL.($admin->v_image));
                            $admin->v_image =$imageName;
                        }
                        } else{
                            $admin->v_image = '';
                                            
                        }
                $admin->v_email=trim($inputs['v_email']);
                $admin->updated_at=date('Y-m-d h:i:s');                
                if($admin->save())
                {
                    Session::flash('success-message', 'Employee has been updated successfully.');
                    return '';    
                }
            }
            }             
            

        } else {
            $records = DropshipEmployee::find($id);
            if($records->e_type == 'Super'){
                $records = DropshipEmployee::with('Admin')->find($id);
            } else {
                $records = DropshipEmployee::with('AdminEmp')->find($id);
            }
            if(isset($records) && !empty($records)){
                //$vendor_data=Vendor::where('id',$auth_user->i_vendor_id)->get();
                if(isset($records->admin->v_image)){
                    $v_image = $records->admin->v_image;
                } elseif(isset($records->admin_emp->v_image)){
                    $v_image = $records->admin_emp->v_image;
                } else {
                    $v_image = '';
                }
                $vendor_data=Vendor::orderBy('v_vendor_name')->get();
                return View('admin.dropship_employee.edit_employee', array('records' => $records, 'title' => 'Edit Employee','vendor_list'=> $vendor_data, 'v_image' => $v_image));
            } else {
                return Redirect(ADMIN_URL . 'dropship-employee');
            }
        }
        return Redirect(ADMIN_URL . 'dropship-employee');
    }

    public function postBulkAction()
    {
        $data = Request::all();        
        if(count($data) > 0) {
            if ($data['action'] == 'Delete') {                                
                $admin_emp = Admin::whereIn('i_dropship_emp_id',$data['ids'])->delete();
                $admin = Admin::whereIn('i_admin_id',$data['ids'])->delete();
                $employee = DropshipEmployee::whereIn('id',$data['ids'])->delete();
                if($employee){
                    return 'TRUE';
                } else { return 'FALSE'; }
            } else if($data['action'] == 'Active'){
                $employee = DropshipEmployee::whereIn('id',$data['ids'])->update(['e_status' => 'Active']);
                $admin_emp = Admin::whereIn('i_dropship_emp_id',$data['ids'])->update(['e_status' => 'Active']);
                $admin = Admin::whereIn('i_admin_id',$data['ids'])->update(['e_status' => 'Active']);
                if($employee){
                    return 'TRUE';
                } else { return 'FALSE'; }
            } else if($data['action'] ==  'Inactive'){
                $employee = DropshipEmployee::whereIn('id',$data['ids'])->update(['e_status' => 'Inactive']);
                $admin_emp = Admin::whereIn('i_dropship_emp_id',$data['ids'])->update(['e_status' => 'Inactive']);
                $admin = Admin::whereIn('i_admin_id',$data['ids'])->update(['e_status' => 'Inactive']);
                if($employee){
                    return 'TRUE';
                } else { return 'FALSE'; }
            }
        } 
    }
    public function anyExportToExcel()
    {        
        $data = Input::all();
        $query = new DropshipEmployee();
        $time = date('Y_m_d_h_i_A');
        $sortColumn = array('', 'v_vname','v_email', 'v_phone','e_type','e_status');
        $query = $query->with("Admin")->with('AdminEmp');
        $auth_user = Auth::guard('admin')->user();
        $query = $query->where('v_email', '!=',$auth_user->v_email);
        if (isset($data['v_vname']) && $data['v_vname'] != '') {
            $query = $query->where('tbl_dropship_employee.v_vname', 'LIKE', '%' . trim($data['v_vname']) . '%');
        }
        if (isset($data['v_email']) && $data['v_email'] != '') {
            $query = $query->where('tbl_dropship_employee.v_email', 'LIKE', '%' . trim($data['v_email']) . '%');
        }            
        if (isset($data['v_phone']) && $data['v_phone'] != '') {
            $query = $query->where('tbl_dropship_employee.v_phone', 'LIKE', '%' . trim($data['v_phone']) . '%');
        }   
        if (isset($data['e_type']) && $data['e_type'] != '') {
            $query = $query->where('tbl_dropship_employee.e_type', '=', trim($data['e_type']));
        }  
        if (isset($data['e_status']) && $data['e_status'] != '') {
            $query = $query->where('tbl_dropship_employeee_status', '=', trim($data['e_status']));
        }  
        $rec_per_page = REC_PER_PAGE;

        if (isset($data['length'])) {
            if ($data['length'] == '-1') {
                $rec_per_page = '';
            } else {
                $rec_per_page = $data['length'];
            }
        }

        $sort_order = $data['order']['0']['dir'];
        $order_field = $sortColumn[$data['order']['0']['column']];

        if ($sort_order != '' && $order_field != '') {
            $query = $query->orderBy($order_field, $sort_order);
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $users = $query->get();
        $records = $users->toArray();
        if (count($records) > 0) {
            $field = array();
            $field['id'] = 'ID';
            $field['v_username'] = 'User Name';
            $field['v_vname'] = 'Employee Name';
            $field['v_email'] = 'Email';
            $field['v_phone'] = 'Phone';                        
            $field['v_fax'] = 'Fax';
            $field['v_note'] = 'Note';
            $field['e_type'] = 'Type';
            $field['e_status'] = 'Status';
            $field['created_at'] = 'Created At';
            $field['updated_at'] = 'Updated At';
            Excel::create('Dropship_Employee_List_'.$time, function ($excel) use ($records, $field) {
                $excel->sheet('Dropship_Employee_List', function ($sheet) use ($records, $field) {
                    $sheet->setOrientation('landscape');
                    $sheet->setHeight(1, 30);
                    $sheet->mergeCells('A1:J1');
                    /*$sheet->setWidth(array('A' => 10, 'B' => 25, 'C' => 30, 'D' => 30, 'E' => 20, 'F' => 20, 'G' => 30,'H' => 30,'I' => 30,'J' => 30));*/

                    $sheet->cells('A1:J1', function ($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('middle');
                        $cell->setFontSize('20');
                        $cell->setFontWeight('bold');
                    });
                    $sheet->row(1, array('Dropship Employee List'));
                    $sheet->cells('A2:J2', function ($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('middle');
                        $cell->setFontSize('12');
                        $cell->setFontWeight('bold');
                    });
                    $sheet->row(2, $field);
                    $intCount = 3;
                    $srNo = 1;
                    foreach ($records as $index => $val) {
                        $vals=array();
                        $vals['id'] = $srNo;
                        if(isset($val['admin']['v_username'])) {  
                            $vals['v_username'] = $val['admin']['v_username'];
                        } elseif(isset($val['admin_emp']['v_username'])) {  
                            $vals['v_username'] = $val['admin_emp']['v_username'];
                        } else {
                            $vals['v_username'] = '';
                        }
                        $vals['v_vname'] = $val['v_vname'];
                        $vals['v_email'] = $val['v_email'];
                        $vals['v_phone'] = $val['v_phone'];
                        $vals['v_fax'] = $val['v_fax'];
                        $vals['v_note'] = $val['v_note'];
                        if($val['e_type'] == 'Super'){
                            $vals['e_type'] = 'Super Admin';
                        } else {
                            $vals['e_type'] = 'Employee';
                        }
                        $vals['e_status'] = $val['e_status'];
                        $vals['created_at'] = date('m/d/Y h:i:s', strtotime($val['created_at']));
                        $vals['updated_at'] =  date('m/d/Y h:i:s', strtotime($val['updated_at']));
                        $sheet->row($intCount, $vals);
                        $intCount++;
                        $srNo++;
                    }
                });
            })->export('xls');
        }else{
             Session::flash('alert-message', 'No data found.');
            return Redirect(ADMIN_URL . 'dropship-employee');
        }
    }
}
