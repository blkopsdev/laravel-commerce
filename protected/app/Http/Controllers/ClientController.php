<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use Request, Session,Validator;
use App\Http\Controllers\Controller;
use App\Models\User, App\Models\Client,App\Models\Admin,App\Models\MailTemplate;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Excel;
use File,Auth;
use Illuminate\Support\Facades\Hash;
use Mail,FFMPEG;

class ClientController extends BaseController
{
    protected $auth;
    public function getIndex()
    {
        return View('admin.client.index', array('title' => 'Client List'));
    }

    public function anyListAjax(Request $request) //client Listing
    {
        $data = Request::all();
        $user = Auth::guard('admin')->user();
       // $query = $query->where('v_email', '!=',$user->v_email);
        $module_name = "2";
        if ($data) {

            $query = new Client();
            $sortColumn = array('', 'v_company','v_firstname','v_lastname', 'v_email', 'v_phone', 'created_at', 'v_folder_name', 'e_status');  
            $query=$query->select('id', 'v_company','v_firstname','v_lastname', 'v_email', 'v_phone', 'created_at', 'v_folder_name', 'e_status');
            if (isset($data['v_company']) && $data['v_company'] != '') {
                $query = $query->where('v_company', 'LIKE', '%' . trim($data['v_company']) . '%');
            }
            if (isset($data['v_firstname']) && $data['v_firstname'] != '') {
                $query = $query->where('v_firstname', 'LIKE', '%' . trim($data['v_firstname']) . '%');
            }
            if (isset($data['v_lastname']) && $data['v_lastname'] != '') {
                $query = $query->where('v_lastname', 'LIKE', '%' . trim($data['v_lastname']) . '%');
            }            
            if (isset($data['v_email']) && $data['v_email'] != '') {
                $query = $query->where('v_email', 'LIKE', '%' . trim($data['v_email']) . '%');
            }
            if (isset($data['v_phone']) && $data['v_phone'] != '') {
                $query = $query->where('v_phone', 'LIKE', '%' . trim($data['v_phone']) . '%');
            }
            if (isset($data['created_at']) && $data['created_at'] != '') {
                $query = $query->where('created_at', 'LIKE', '%' . trim($data['created_at']) . '%');
            }
            if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "" && $data['from_date'] == $data['to_date'] ) {
               $query = $query->where('tbl_client.created_at','LIKE','%'. date('Y-m-d',strtotime($data['from_date'])).'%');
            }else if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "") {
                $data['to_date'] = $data['to_date']. "23:59:59";
                $query = $query->whereBetween('tbl_client.created_at',  array(date('Y-m-d H:i:s',strtotime($data['from_date'])),date('Y-m-d H:i:s',strtotime($data['to_date']))));
            }else  if (isset($data['from_date']) && $data['from_date'] != ""){
                  $query = $query->where('tbl_client.created_at','>=',date('Y-m-d',strtotime($data['from_date'])));
            }else if (isset($data['to_date']) && $data['to_date'] != ""){
                 $data['to_date'] = $data['to_date']. "23:59:59";
                  $query = $query->where('tbl_client.created_at','<=',date('Y-m-d H:i:s',strtotime($data['to_date'])));
            }
            if (isset($data['v_folder_name']) && $data['v_folder_name'] != '') {
                $query = $query->where('v_folder_name', 'LIKE', '%' . trim($data['v_folder_name']) . '%');
            }
            if (isset($data['e_status']) && $data['e_status'] != '') {
                $query = $query->where('e_status', $data['e_status']);
            }
            $rec_per_page = REC_PER_PAGE;

            if (isset($data['length'])) {
                if ($data['length'] == '-1') {
                    $rec_per_page = '';
                } else {
                    $rec_per_page = $data['length'];
                }
            }

            $sort_order = $data['order']['0']['dir'];
            $order_field = $sortColumn[$data['order']['0']['column']];

            if ($sort_order != '' && $order_field != '') {
                $query = $query->orderBy($order_field, $sort_order);
            } else {
                $query = $query->orderBy('id', 'desc');
            }
            if($rec_per_page == ''){
                $temp_records = $query->get();
                $rec_per_page = count($temp_records);
                $users = $query->paginate($rec_per_page);
            } else {
                $users = $query->paginate($rec_per_page);
            }
            //$users = $query->paginate($rec_per_page);
            $arrUsers = $users->toArray();
            $data = array();
            foreach ($arrUsers['data'] as $key => $val) {
                $index = 0;
                if($user->role != 1)
                 {
                    if(isset($this->permissions[$module_name]['e_delete']) && $this->permissions[$module_name]['e_delete'] == '1')
                    {   
                    $data[$key][$index++] = '<input type="checkbox" name="id[]" value="'.$val['id'].'" class="delete_'.$val['id'].'">';
                    }
                 }
                else
                {
                     $data[$key][$index++] = '<input type="checkbox" name="id[]" value="'.$val['id'].'" class="delete_'.$val['id'].'">'; 
                }
                $data[$key][$index++] = $val['v_company'];
                $data[$key][$index++] = $val["v_firstname"];
                $data[$key][$index++] = $val['v_lastname'];
                $data[$key][$index++] = $val['v_email'];
                $data[$key][$index++] = $val['v_phone'];
                $data[$key][$index++] = date('m/d/Y', strtotime($val['created_at']));
                $data[$key][$index++] = $val['v_folder_name'];                
                if ($val['e_status'] == 'Active') {
                    $intStatus = 'Active';
                } else {
                    $intStatus = 'Inactive';
                }
                $data[$key][$index++] = '<a href="javascript:void(0);" data-id="' . $val['id'] . '" id="change_status" rel="' . $intStatus . '" change-url="' . ADMIN_URL . 'client/change-status">' . $intStatus . '</a>';
                $action='';
                 if($user->role != 1)
                 {
                    if(isset($this->permissions[$module_name]['e_delete']) && $this->permissions[$module_name]['e_delete'] == '1' || isset($this->permissions[$module_name]['e_edit']) && $this->permissions[$module_name]['e_edit'] == '1')
                    {   
                    $action = '<div class="actions"><a class="edit btn default btn-xs black" rel="' . $val['id'] . '" href="' . ADMIN_URL . 'client/edit/' . $val['id'] . '"  title="Edit"><i class="fa fa-edit"></i></a><a href="javascript:;" id="delete_record" rel="' . $val['id'] . '" delete-url="' . ADMIN_URL . 'client/delete/' . $val['id'] . '" class="btn default btn-xs black delete" title="Delete"><i class="icon-trash"></i></a>';
                    }
                 }
                else
                {
                     $action = '<div class="actions"><a class="edit btn default btn-xs black" rel="' . $val['id'] . '" href="' . ADMIN_URL . 'client/edit/' . $val['id'] . '"  title="Edit"><i class="fa fa-edit"></i></a><a href="javascript:;" id="delete_record" rel="' . $val['id'] . '" delete-url="' . ADMIN_URL . 'client/delete/' . $val['id'] . '" class="btn default btn-xs black delete" title="Delete"><i class="icon-trash"></i></a>';
                }
                
                $data[$key][$index++] = $action;
            }
            $return_data['data'] = $data;
            $return_data['recordsTotal'] = $arrUsers['total'];
            $return_data['recordsFiltered'] = $arrUsers['total'];
            $return_data['data_array'] = $arrUsers['data'];
            return $return_data;
        }
    }

    public function anyAdd()
    {
        if (Input::all()) {                
                $inputs = Input::all();            
                $client = new Client();   
                $validator = Validator::make(Input::all(), array(
                    "v_email" => 'required|unique:tbl_client,v_email,NULL,id,deleted_at,NULL',
                    "v_username" => 'required|unique:tbl_admin,v_username,NULL,id,deleted_at,NULL',
                    "v_company" => 'required|unique:tbl_client,v_company,NULL,id,deleted_at,NULL'));
                if ($validator->fails()) {
                    return $validator->errors();
                }else{         
                    $client->v_firstname = trim($inputs['v_firstname']);
                    $client->v_lastname = trim($inputs['v_lastname']);
                    $client->v_company = trim($inputs['v_company']);
                    if(Input::has('v_fax')){
                        $client->v_fax = trim($inputs['v_fax']);
                    }
                    $client->v_email = trim($inputs['v_email']);
                    $client->v_address_1 = trim($inputs['v_address_1']);
                    $client->v_address_2 = trim($inputs['v_address_2']);
                    $client->v_city = trim($inputs['v_city']);
                    $client->v_state = trim($inputs['v_state']);
                    $client->v_zipcode = trim($inputs['v_zipcode']);
                    $client->v_phone = trim($inputs['v_phone']);
                    if(Input::has('l_note')){
                        $client->l_note = trim($inputs['l_note']);    
                    }            
                    $client->v_ftp_url = trim($inputs['v_ftp_url']);
                    $client->v_ftp_username = trim($inputs['v_ftp_username']);
                    $client->v_ftp_password = base64_encode(trim($inputs['v_ftp_password']));
                    if(Input::has('v_logo'))
                    {
                        $client->company_logo =$this->saveImage($inputs['v_logo'],CLIENT_ADD_IMG_PATH_URL);    
                    }                    
                    $client->e_status = trim($inputs['e_status']);
                    $client->created_at=date('Y-m-d h:i:s');
                    $client->updated_at=date('Y-m-d h:i:s');
                    $folder = $this->getFolderName(trim($inputs['v_company']));
                    $result = File::makeDirectory(CLIENT_FOLDER_PATH.$folder, 0777, true, true);
                    $result = File::makeDirectory(PURCHASE_FEED_PATH.$folder, 0777, true, true);
                    $client->v_folder_name=$folder;
                    if ($client->save()) {
                        $last_inserted_id=$client->id;
                        $admin=new Admin();
                        $admin->v_username=trim($inputs['v_username']);
                        $admin->password=Hash::make(trim($inputs['password']));
                        $admin->v_email=trim($inputs['v_email']);
                        $admin->i_client_id=$last_inserted_id;
                        $admin->e_type='Client';
                        $admin->role='2';
                        $admin->created_at=date('Y-m-d h:i:s');
                        $admin->updated_at=date('Y-m-d h:i:s');
                        $admin->e_status=trim($inputs['e_status']);
                        if($admin->save())
                        {
                            $mail_flag=Session::get('SETTING_DATA');
                            $objEmailTemplate = MailTemplate::find(7)->toArray();
                            $strTemplate = $objEmailTemplate['t_email_content'];
                            $strTemplate = str_replace('[PROJECT_NAME]',PROJECT_NAME,$strTemplate);
                            $strTemplate = str_replace('[USER_NAME]',trim($inputs['v_username']),$strTemplate);
                            $strTemplate = str_replace('[PASSWORD]',trim($inputs['password']),$strTemplate);
                            $strTemplate = str_replace('[IMAGE_PATH]', EMAIL_LOGO_IMAGE_PATH, $strTemplate);
                            $strTemplate = str_replace('[SITE_NAME]',SITE_NAME,$strTemplate);
                            $strTemplate = str_replace('[SITE_URL]',SITE_URL,$strTemplate);
                            $subject='New client registration at '.PROJECT_NAME;
                            // mail sent to user with new link
                            $send_mailid='';
                            if(TESTING_MAILID!=''){
                                $send_mailid=TESTING_MAILID;
                            }else{
                                $send_mailid=$inputs['v_email'];
                            }                            
                            /* Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$strTemplate), function($message) use ($inputs,$subject,$send_mailid){ 
                                $message->to($send_mailid);   
                                $message->subject($subject);                                
                            }); */
                            
                            Session::flash('success-message', 'client has been added successfully.');
                            return '';    
                        }
                        
                    }
            }
        } else {
       //  $send_mailid=TESTING_MAILID;
       //  $subject="test";
       //  $strTemplate="hi";
       // Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$strTemplate), function($message) use ($subject,$send_mailid){ 
       //      $message->to($send_mailid);   
       //      $message->subject($subject);                                
       //  });
       // echo "done";
       // exit;
            return View('admin.client.add_client', array('title' => 'Add Client'));
        }
        return Redirect(ADMIN_URL . 'client');

    }

    public function getDelete($id)
    {
        $client = Client::find($id);
        if (!empty($client)) {
            $admin = Admin::where('i_client_id',$id)->delete();
            $client->delete();
            $result=File::deleteDirectory(CLIENT_FOLDER_PATH.$client->v_folder_name, false);
            $result=File::deleteDirectory(PURCHASE_FEED_PATH.$client->v_folder_name, false);
            @unlink(WWW_ROOT.CLIENT_ADD_IMG_PATH_URL.($client->company_logo));
            return 'TRUE';
        } else {
            return 'FALSE';
        }  
    }
    public function anyEdit($id)
    {
        if (Input::all()) {
            $inputs = Input::all();                    
            $client = Client::find($id);
            $admin=Admin::where('i_client_id',$id)->first();
            $validator = Validator::make(Input::all(), array(
                "v_email" =>'required|unique:tbl_client,v_email,' .$id. ',id,deleted_at,NULL',
                "v_username" =>'required|unique:tbl_admin,v_username,' .$admin->id. ',id,deleted_at,NULL',
                "v_company" =>'required|unique:tbl_client,v_company,' .$id. ',id,deleted_at,NULL'));
            if ($validator->fails()) {
                return $validator->errors();
            }else{           
                    $client->v_firstname = trim($inputs['v_firstname']);
                    $client->v_lastname = trim($inputs['v_lastname']);
                    $client->v_company = trim($inputs['v_company']);
                    $client->v_fax = trim($inputs['v_fax']);
                    $client->v_email = trim($inputs['v_email']);
                    $client->v_address_1 = trim($inputs['v_address_1']);
                    $client->v_address_2 = trim($inputs['v_address_2']);
                    $client->v_city = trim($inputs['v_city']);
                    $client->v_state = trim($inputs['v_state']);
                    $client->v_zipcode = trim($inputs['v_zipcode']);
                    $client->v_phone = trim($inputs['v_phone']);
                    $client->l_note = trim($inputs['l_note']);    
                    $client->v_ftp_url = trim($inputs['v_ftp_url']);
                    $client->v_ftp_username = trim($inputs['v_ftp_username']);
                    if(Input::has('v_ftp_password')){
                    $client->v_ftp_password = base64_encode(trim($inputs['v_ftp_password']));
                    }
                    if($inputs['v_logo'] != '') {
                         if($inputs['v_logo'] != SITE_URL.CLIENT_ADD_IMG_PATH_URL.$client->company_logo)
                         {
                            @unlink(WWW_ROOT.CLIENT_ADD_IMG_PATH_URL.($client->company_logo));
                            $client->company_logo =$this->saveImage($inputs['v_logo'],CLIENT_ADD_IMG_PATH_URL);
                         }
                    }else{
                        @unlink(WWW_ROOT.CLIENT_ADD_IMG_PATH_URL.($client->company_logo));
                        $client->company_logo ='';
                    }
                    $client->e_status = trim($inputs['e_status']);            
                    $client->updated_at=date('Y-m-d h:i:s');
                    if(Input::has('v_company')){
                        $folder = $this->getFolderName(trim($inputs['v_company']));
                        $result=File::deleteDirectory(CLIENT_FOLDER_PATH.$client->v_folder_name, false);
                        $result = File::makeDirectory(CLIENT_FOLDER_PATH.$folder, 0777, true, true);
                        $result=File::deleteDirectory(PURCHASE_FEED_PATH.$client->v_folder_name, false);
                        $result = File::makeDirectory(PURCHASE_FEED_PATH.$folder, 0777, true, true);
                        $client->v_folder_name=$folder;
                    }            
                    if ($client->save()) {                
                        $admin=Admin::where('i_client_id',$client->id)->first();
                        $admin->v_username=trim($inputs['v_username']);
                        if(Input::has('password')){
                            $admin->password=Hash::make(trim($inputs['password']));
                            $objEmailTemplate = MailTemplate::find(8)->toArray();
                            $strTemplate = $objEmailTemplate['t_email_content'];
                            $strTemplate = str_replace('[PROJECT_NAME]',PROJECT_NAME,$strTemplate);
                            $strTemplate = str_replace('[USER_NAME]',trim($inputs['v_username']),$strTemplate);
                            $strTemplate = str_replace('[PASSWORD]',trim($inputs['password']),$strTemplate);
                            $strTemplate = str_replace('[IMAGE_PATH]', EMAIL_LOGO_IMAGE_PATH, $strTemplate);
                            $strTemplate = str_replace('[SITE_NAME]',SITE_NAME,$strTemplate);
                            $strTemplate = str_replace('[SITE_URL]',SITE_URL,$strTemplate);
                            $subject='Update Client profile at '.PROJECT_NAME;
                            // mail sent to user with new link
                            $mail_flag=Session::get('SETTING_DATA');
                            $send_mailid='';
                            if(TESTING_MAILID!=''){
                                $send_mailid=TESTING_MAILID;
                            }else{
                                $send_mailid=$inputs['v_email'];
                            }                           
                            /* Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$strTemplate), function($message) use ($inputs,$subject,$send_mailid){ 
                                $message->to($send_mailid);   
                                $message->subject($subject);                                
                            });    */                         
                        }
                        $admin->v_email=trim($inputs['v_email']);                
                        $admin->e_type='Client';                
                        $admin->updated_at=date('Y-m-d h:i:s');
                        $admin->e_status=trim($inputs['e_status']);
                        if($admin->save())
                        {
                            Session::flash('success-message', 'client has been updated successfully.');
                            return '';    
                        }
                    }
                }

        } else {

            $records = Client::with('admin')->find($id);         
            if(isset($records) && !empty($records)){   
                return View('admin.client.edit_client', array('records' => $records, 'title' => 'Edit Client'));
            } else {
                return Redirect(ADMIN_URL . 'client');
            }
        }
        return Redirect(ADMIN_URL . 'client');
    }

    public function postBulkAction()
    {
        $data = Request::all();        
        if(count($data) > 0) {
            if ($data['action'] == 'Delete') {                
                foreach ($data['ids'] as $key => $value) {
                    $client_img=Client::where('id',$value)->first();                    
                    $result=File::deleteDirectory(CLIENT_FOLDER_PATH.$client_img->v_folder_name, false);
                    $result=File::deleteDirectory(PURCHASE_FEED_PATH.$client_img->v_folder_name, false);
                    @unlink(WWW_ROOT.CLIENT_ADD_IMG_PATH_URL.($client_img->company_logo));
                }
                $admin = Admin::whereIn('i_client_id',$data['ids'])->delete();
                $client = Client::whereIn('id',$data['ids'])->delete();
                if($client){
                    return 'TRUE';
                } else { return 'FALSE'; }
            }else if($data['action'] == 'Active'){
                $client = Client::whereIn('id',$data['ids'])->update(['e_status' => 'Active']);
                $admin = Admin::whereIn('i_client_id',$data['ids'])->update(['e_status' => 'Active']);
                if($client){
                    return 'TRUE';
                } else { return 'FALSE'; }
            }else if($data['action'] ==  'Inactive'){
                $client = Client::whereIn('id',$data['ids'])->update(['e_status' => 'Inactive']);
                $admin = Admin::whereIn('i_client_id',$data['ids'])->update(['e_status' => 'Inactive']);
                if($client){
                    return 'TRUE';
                } else { return 'FALSE'; }
            }
        } 
    }


    public function postChangeStatus()
    {               
        $data = Input::all();
        $client = Client::find($data['id']);
        $admin=Admin::where('i_client_id',$data['id'])->first();
        if($client->e_status=='Active')

                $status='Inactive';
        else
                $status='Active';
        $client->e_status = $status;
        $admin->e_status = $status;
        if ($client->save()) {
            $admin->save();
            return 'TRUE';
        } else {
            return 'FALSE';
        }
    }
    public function saveImage($base64img,$path) {
        $v_random_image = time().'-'.str_random(6);
        $tmpFile = $v_random_image;
        if (strpos($base64img,'data:image/jpeg;base64,') !== false) {
            $base64img = str_replace('data:image/jpeg;base64,', '', $base64img);
            $tmpFile = $v_random_image.'.jpg';
        }
        if (strpos($base64img,'data:image/png;base64,') !== false) {
            $base64img = str_replace('data:image/png;base64,', '', $base64img);
            $tmpFile = $v_random_image.'.png';
        }
        if (strpos($base64img,'data:image/webp;base64,') !== false) {
            $base64img = str_replace('data:image/webp;base64,', '', $base64img);
            $tmpFile = $v_random_image.'.png';
        }
        if (strpos($base64img,'data:image/jpg;base64,') !== false) {
            $base64img = str_replace('data:image/jpg;base64,', '', $base64img);
            $tmpFile = $v_random_image.'.jpg';
        }
        if (strpos($base64img,'data:image/gif;base64,') !== false) {
            $base64img = str_replace('data:image/gif;base64,', '', $base64img);
            $tmpFile = $v_random_image.'.gif';
        }        
        $data = base64_decode($base64img);
        $file = $path.$tmpFile;
        file_put_contents($file, $data);

        return $tmpFile;
    }
    public function getFolderName($temp)
    {
        $temp = strtolower(trim($temp));        
        $temp = str_replace("   ", " ",$temp);
        $temp = str_replace("  ", " ",$temp);
        $temp = str_replace(" ", "_",$temp);
        $temp = str_replace("'", "",$temp);
        $temp = str_replace("\\", "",$temp);
        $temp = str_replace("\"", "",$temp);
        $temp = str_replace("?", "",$temp);
        $temp = str_replace("/", "_or_",$temp);
        $temp = str_replace("&", "and",$temp);
        $temp = str_replace("(", "",$temp);
        $temp = str_replace(")", "",$temp);
        $temp = str_replace("`", "",$temp);
        $temp = str_replace("%", "",$temp);
        $temp = str_replace("^", "",$temp);
        $temp = str_replace(";", "",$temp);
        $temp = str_replace(":", "",$temp);
        $temp = str_replace("<", "",$temp);
        $temp = str_replace(">", "",$temp);
        $temp = str_replace("~", "",$temp);
        
        return trim(stripslashes($temp));
    }
    public function anyExportToExcel()
    {        
        $data = Input::all();
        $query = new Client();
        $time = date('Y_m_d_h_i_A');
        $user = Auth::guard('admin')->user();
        $query = $query->where('tbl_client.v_email', '!=',$user->v_email);
        $sortColumn = array('', 'v_company','v_firstname','v_lastname', 'v_email', 'v_phone', 'created_at', 'v_folder_name', 'e_status'); 
        if (isset($data['v_company']) && $data['v_company'] != '') {
            $query = $query->where('tbl_client.v_company', 'LIKE', '%' . trim($data['v_company']) . '%');
        }
        if (isset($data['v_firstname']) && $data['v_firstname'] != '') {
            $query = $query->where('tbl_client.v_firstname', 'LIKE', '%' . trim($data['v_firstname']) . '%');
        }
        if (isset($data['v_lastname']) && $data['v_lastname'] != '') {
            $query = $query->where('tbl_client.v_lastname', 'LIKE', '%' . trim($data['v_lastname']) . '%');
        }            
        if (isset($data['v_email']) && $data['v_email'] != '') {
            $query = $query->where('tbl_client.v_email', 'LIKE', '%' . trim($data['v_email']) . '%');
        }
        if (isset($data['v_phone']) && $data['v_phone'] != '') {
            $query = $query->where('tbl_client.v_phone', 'LIKE', '%' . trim($data['v_phone']) . '%');
        }
        if (isset($data['created_at']) && $data['created_at'] != '') {
            $query = $query->where('tbl_client.created_at', 'LIKE', '%' . trim($data['created_at']) . '%');
        }
        if(isset($data['from_date']) && $data['from_date'] != ''){
            $d_from_date = date('Y-m-d',strtotime($data['from_date']));
            $query = $query->where("tbl_client.created_at", '>=', $d_from_date);                    
        }
        if(isset($data['to_date']) && $data['to_date'] != ''){
            $d_to_date = date('Y-m-d',strtotime($data['to_date']. "+1 days"));
            $query = $query->where("tbl_client.created_at", '<=', $d_to_date);                    
        }
        if (isset($data['v_folder_name']) && $data['v_folder_name'] != '') {
            $query = $query->where('tbl_client.v_folder_name', 'LIKE', '%' . trim($data['v_folder_name']) . '%');
        }
        if (isset($data['e_status']) && $data['e_status'] != '') {
            $query = $query->where('tbl_client.e_status', $data['e_status']);
        }
        $query = $query->join('tbl_admin','tbl_admin.i_client_id','=','tbl_client.id');
        $query = $query->select('tbl_client.id','tbl_admin.v_username','tbl_client.v_firstname','tbl_client.v_lastname','tbl_client.v_company','tbl_client.v_email','tbl_client.v_address_1','tbl_client.v_address_2','tbl_client.v_city','tbl_client.v_state','tbl_client.v_zipcode', 'tbl_client.v_phone','tbl_client.l_note','tbl_client.v_fax', 'tbl_client.v_ftp_url','tbl_client.v_ftp_username','tbl_client.v_ftp_password','tbl_client.v_folder_name','tbl_client.e_status','tbl_client.cms_content_1','tbl_client.cms_content_2','tbl_client.created_at','tbl_client.updated_at')->where('tbl_admin.deleted_at',"=",NULL);
       

        $sort_order = $data['order']['0']['dir'];
        $order_field = $sortColumn[$data['order']['0']['column']];

        if ($sort_order != '' && $order_field != '') {
            $query = $query->orderBy($order_field, $sort_order);
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $users = $query->get();
        $records = $users->toArray();
        if (count($records) > 0) {
            $field = array();
            $field['no'] = 'Sr.No';
            $field['v_username'] = 'User Name';
            $field['v_firstname'] = 'First Name';
            $field['v_lastname'] = 'Last Name';
            $field['v_company'] = 'Company'; 
            $field['v_email'] = 'Email'; 
            $field['v_address_1'] = 'Address Line 1'; 
            $field['v_address_2'] = 'Address Line 2'; 
            $field['v_city'] = 'City'; 
            $field['v_state'] = 'State'; 
            $field['v_zipcode'] = 'Zipcode'; 
            $field['v_phone'] = 'Phone'; 
            $field['l_note'] = 'Note'; 
            $field['v_fax'] = 'Fax'; 
            $field['v_ftp_url'] = 'FTP Url'; 
            $field['v_ftp_username'] = 'FTP Username'; 
            $field['v_ftp_password'] = 'FTP Password'; 
            $field['v_folder_name'] = 'Folder Name'; 
            $field['e_status'] = 'Status'; 
            $field['cms_content_1'] = 'Cms Content 1'; 
            $field['cms_content_2'] = 'Cms Content 2';
            $field['created_at'] = 'Created At'; 
            $field['updated_at'] = 'Upadate At';
            Excel::create('Client_List_'.$time, function ($excel) use ($records, $field) {
                $excel->sheet('Client_List', function ($sheet) use ($records, $field) {
                    $sheet->setOrientation('landscape');
                    $sheet->setHeight(1, 30);
                    $sheet->mergeCells('A1:W1');
                   /* $sheet->setWidth(array('A' => 10, 'B' => 25, 'C' => 30, 'D' => 30, 'E' => 20, 'F' => 20, 'G' => 30, 'H' => 10,'I' => 10));*/

                    $sheet->cells('A1:W1', function ($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('middle');
                        $cell->setFontSize('20');
                        $cell->setFontWeight('bold');
                    });
                    $sheet->row(1, array('Client List'));
                    $sheet->cells('A2:W2', function ($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('middle');
                        $cell->setFontSize('12');
                        $cell->setFontWeight('bold');
                    });
                    $sheet->row(2, $field);
                    $intCount = 3;
                    $srNo = 1;
                    // dd($records);
                    foreach ($records as $index => $val) {
                        if($val['created_at']){
                            $val['created_at'] = date('m/d/Y h:i:s', strtotime($val['created_at']));
                        }
                        if($val['updated_at']){
                            $val['updated_at'] = date('m/d/Y h:i:s', strtotime($val['updated_at']));
                        }
                        if($val['v_ftp_password']){
                            $val['v_ftp_password'] = base64_encode($val['v_ftp_password']);
                        }
                        $sheet->row($intCount, $val);
                        $intCount++;
                        $srNo++;
                    }
                });
            })->export('xls');
        }else{
             Session::flash('alert-message', 'No data found.');
            return Redirect(ADMIN_URL . 'client');
        }
    } 
    public function anyEditCms($id){
         if (Input::all()) {
         
            $inputs = Input::all();      
            $client=Client::find($id);
            $client->cms_content_1=$inputs['cms_content_1'];
            $client->cms_content_2=$inputs['cms_content_2'];
            if($client->save()){
               Session::flash('success-message', 'Record has been updated successfully.');
                return 'update_cms';
            }
        } else {
            $records = Client::with('admin')->select('id','cms_content_1','cms_content_2')->find($id);
            return View('admin.client.edit_cms', array('records' => $records, 'title' => 'Edit CMS'));
        }
        return Redirect(ADMIN_URL . 'client');
    }
}
