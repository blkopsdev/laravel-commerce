<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use Mail, Session, Redirect, Auth, Validator,Excel,Cookie,Request;
use App\Models\User, App\Models\Client,App\Models\Admin,App\Models\Employee,App\Models\Vendor,App\Models\MailTemplate,App\Models\Order,App\Models\OrderDetail, App\Models\DataFeed;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use File,PHPExcel_IOFactory;
use Illuminate\Support\Facades\Hash;
class OrderController extends BaseController
{
    public function getIndex()
    {
        $auth_user = Auth::guard('admin')->user();
        $vendor_data = Vendor::get();
        $client_data = Client::get();
        return View('admin.order.index', array('title' => 'Order List','vendor_list' => $vendor_data,'client_data'=>$client_data));
    }

    public function anyListAjax(Request $request) //Employee Listing
    {
        $data = Request::all();
        $user = Auth::guard('admin')->user();
        $module_name = "1";
        if ($data) {

            $query = new Order();
            $sortColumn = array('', 'v_vendor_name','v_shipment_number','d_order_date', 'v_bill_to_lastname','v_bill_to_firstname','v_bill_to_zip','v_item_code','v_description','e_item_status');
            $query=$query->with("OrderDetails","Vendor");
            if($user->role==2){
                $query=$query->where("i_client_id",$user->i_client_id);
            }else if($user->role==3){
                 $query=$query->where("i_vendor_id",$user->i_vendor_id);
            }else if($user->role==4){
                $employee_data=Employee::where('id',$user->i_emp_id)->first();
                $query=$query->where("i_vendor_id",$employee_data->i_vendor_id);
            }
            if(isset($data['vendor_id']) && $data['vendor_id'] != '') {
                 $query = $query->whereHas('Vendor', function($q) use($data){
                    $q->where('tbl_vendor.id', '=',  trim($data['vendor_id']));
                });
            }
           /* if(isset($data['v_vendor_name']) && $data['v_vendor_name'] != '') {
                 $query = $query->whereHas('Vendor', function($q) use($data){
                    $q->where('tbl_vendor.v_vendor_name', 'LIKE', '%' . trim($data['v_vendor_name']) . '%');
                });
            }*/
            if (isset($data['v_shipment_number']) && $data['v_shipment_number'] != '') {
                $query = $query->where('v_shipment_number', 'LIKE', '%' . trim($data['v_shipment_number']) . '%');
            }            
            if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "" && $data['from_date'] == $data['to_date'] ) {
               $query = $query->where('tbl_order.d_order_date','LIKE','%'. date('Y-m-d',strtotime($data['from_date'])).'%');
            }else if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "") {
                $data['to_date'] = $data['to_date']. "23:59:59";
                $query = $query->whereBetween('tbl_order.d_order_date',  array(date('Y-m-d H:i:s',strtotime($data['from_date'])),date('Y-m-d H:i:s',strtotime($data['to_date']))));
            }else  if (isset($data['from_date']) && $data['from_date'] != ""){
                  $query = $query->where('tbl_order.d_order_date','>=',date('Y-m-d',strtotime($data['from_date'])));
            }else if (isset($data['to_date']) && $data['to_date'] != ""){
                 $data['to_date'] = $data['to_date']. "23:59:59";
                  $query = $query->where('tbl_order.d_order_date','<=',date('Y-m-d H:i:s',strtotime($data['to_date'])));
            } 
            if(isset($data['v_bill_to_lastname']) && $data['v_bill_to_lastname'] != '') {
                $query = $query->where('v_bill_to_lastname', 'LIKE', '%' . trim($data['v_bill_to_lastname']) . '%');
            }           
            if(isset($data['v_bill_to_firstname']) && $data['v_bill_to_firstname'] != '') {
                 $query = $query->where('v_bill_to_firstname', 'LIKE', '%' . trim($data['v_bill_to_firstname']) . '%');
            }           
            if(isset($data['v_bill_to_zip']) && $data['v_bill_to_zip'] != '') {
                $query = $query->where('v_bill_to_zip', 'LIKE', '%' . trim($data['v_bill_to_zip']) . '%');
            }
            if(isset($data['v_item_code']) && $data['v_item_code'] != '') {
                 $query = $query->whereHas('OrderDetails', function($q) use($data){
                    $q->where('tbl_order_details.v_item_code', 'LIKE', '%' . trim($data['v_item_code']) . '%');
                });
            }
            if(isset($data['v_description']) && $data['v_description'] != '') {
                  $query = $query->whereHas('OrderDetails', function($q) use($data){
                    $q->where('tbl_order_details.v_description', 'LIKE', '%' . trim($data['v_description']) . '%');
                });
            }
            if(isset($data['e_item_status']) && $data['e_item_status'] != '') {
                 $query = $query->whereHas('OrderDetails', function($q) use($data){
                    $q->where('tbl_order_details.e_item_status', trim($data['e_item_status']));
                });
            }         
            $rec_per_page = REC_PER_PAGE;

            if (isset($data['length'])) {
                if ($data['length'] == '-1') {
                    $rec_per_page = '';
                } else {
                    $rec_per_page = $data['length'];
                }
            }

            $sort_order = $data['order']['0']['dir'];
            $order_field = $sortColumn[$data['order']['0']['column']];
            
            if ($sort_order != '' && $order_field != '') {
                if($order_field == 'v_vendor_name'){
                    $query = $query->whereHas('Vendor', function($q) use($data,$sort_order){
                        $q->orderBy('tbl_vendor.v_vendor_name',$sort_order);
                    });
                }else if($order_field == 'v_item_code'){
                    $query = $query->whereHas('OrderDetails', function($q) use($data,$sort_order){
                        $q->orderBy('tbl_order_details.v_item_code',$sort_order);
                    });
                }else if($order_field == 'v_description'){
                    $query = $query->whereHas('OrderDetails', function($q) use($data,$sort_order){
                        $q->orderBy('tbl_order_details.v_description',$sort_order);
                    });
                }else if($order_field == 'e_item_status'){
                    $query = $query->whereHas('OrderDetails', function($q) use($data,$sort_order){
                        $q->orderBy('tbl_order_details.e_item_status',$sort_order);
                    });
                }else
                {
                    $query = $query->orderBy($order_field, $sort_order);
                }
                
            } else {
                $query = $query->orderBy('id', 'desc');
            }
            
            if($rec_per_page == ''){
                $temp_records = $query->get();
                $rec_per_page = count($temp_records);
                $users = $query->paginate($rec_per_page);
            } else {
                $users = $query->paginate($rec_per_page);
            }
            
            $arrUsers = $users->toArray();                        
            $data = array();
            foreach ($arrUsers['data'] as $key => $val) {
                $index = 0;
                if($user->role != 1)
                 {
                    if(isset($this->permissions[$module_name]['e_delete']) && $this->permissions[$module_name]['e_delete'] == '1')
                    {   
                    $data[$key][$index++] = '<input type="checkbox" name="id[]" value="'.$val['id'].'" class="delete_'.$val['id'].'">';
                    }else{
                        // $data[$key][$index++]='';
                    }
                 }
                else
                {
                     $data[$key][$index++] = '<input type="checkbox" name="id[]" value="'.$val['id'].'" class="delete_'.$val['id'].'">'; 
                }
                if($user->role!=3 && $user->role!=4 && $user->role!=5){
                    $data[$key][$index++] = $val['vendor']['v_vendor_name'];
                }
                $data[$key][$index++] = '<a rel="' . $val['id'] . '" href="' . ADMIN_URL . 'order/edit/' . $val['v_shipment_number'] . '">'. $val["v_shipment_number"] .'</a>';
                $data[$key][$index++] = date('m/d/Y', strtotime($val['d_order_date']));
                $data[$key][$index++] = $val['v_bill_to_lastname'];
                $data[$key][$index++] = $val['v_bill_to_firstname'];
                $data[$key][$index++] = $val['v_bill_to_zip'];
                $data[$key][$index++] = (!empty($val['order_details']))?$val['order_details'][0]['v_item_code']:'';
                $data[$key][$index++] = (!empty($val['order_details']))?$val['order_details'][0]['v_description']:'';
                $data[$key][$index++] = (!empty($val['order_details']))?$val['order_details'][0]['e_item_status']:'';
                $action='';
                if($user->role != 1)
                 {
                    if(isset($this->permissions[$module_name]['e_edit']) && $this->permissions[$module_name]['e_edit'] == '1' && isset($this->permissions[$module_name]['e_delete']) && $this->permissions[$module_name]['e_delete'] == '0')
                    {   
                        $action = '<div class="actions"><a class="edit btn default btn-xs black" rel="' . $val['id'] . '" href="' . ADMIN_URL . 'order/edit/' . $val['v_shipment_number'] . '"  title="Edit"><i class="fa fa-edit"></i></a>';
                    }else if(isset($this->permissions[$module_name]['e_delete']) && $this->permissions[$module_name]['e_delete'] == '1' || isset($this->permissions[$module_name]['e_edit']) && $this->permissions[$module_name]['e_edit'] == '1'){
                        $action = '<div class="actions"><a class="edit btn default btn-xs black" rel="' . $val['id'] . '" href="' . ADMIN_URL . 'order/edit/' . $val['v_shipment_number'] . '"  title="Edit"><i class="fa fa-edit"></i></a><a href="javascript:;" id="delete_record" rel="' . $val['id'] . '" delete-url="' . ADMIN_URL . 'order/delete/' . $val['id'] . '" class="btn default btn-xs black delete" title="Delete"><i class="icon-trash"></i></a>';
                    }
                 }
                else
                {
                     $action = '<div class="actions"><a class="edit btn default btn-xs black" rel="' . $val['id'] . '" href="' . ADMIN_URL . 'order/edit/' . $val['v_shipment_number'] . '"  title="Edit"><i class="fa fa-edit"></i></a><a href="javascript:;" id="delete_record" rel="' . $val['id'] . '" delete-url="' . ADMIN_URL . 'order/delete/' . $val['id'] . '" class="btn default btn-xs black delete" title="Delete"><i class="icon-trash"></i></a>';
                }
                
                $data[$key][$index++] = $action;
            }
            $return_data['data'] = $data;
            $return_data['recordsTotal'] = $arrUsers['total'];
            $return_data['recordsFiltered'] = $arrUsers['total'];
            $return_data['data_array'] = $arrUsers['data'];
            return $return_data;
        }
    }

    public function getDelete($id)
    {
        $order = Order::find($id);
        if (!empty($order)) {
            $order_details = OrderDetail::where('i_order_id',$id)->delete();
            $order->delete();
            return 'TRUE';
        } else {
            return 'FALSE';
        }  
    }
    public function anyEdit($id)
    {
        if (Input::all()) {
            $inputs = Input::all();  
            $order = Order::find($id); 
            $order_details = OrderDetail::where('i_order_id',$id)->first();   
            $order->v_bill_to_firstname = trim($inputs['v_bill_to_firstname']);
            $order->v_bill_to_lastname = trim($inputs['v_bill_to_lastname']);
            $order->v_bill_to_address1 = trim($inputs['v_bill_to_address1']);
            $order->v_bill_to_address2 = trim($inputs['v_bill_to_address2']);
            $order->v_bill_to_city = trim($inputs['v_bill_to_city']);
            $order->v_bill_to_state = trim($inputs['v_bill_to_state']);
            $order->v_bill_to_zip = trim($inputs['v_bill_to_zip']);
            $order->v_bill_to_phonenumber = trim($inputs['v_bill_to_phonenumber']);
            $order->v_gift_message = trim($inputs['v_gift_message']);
            $order_details->v_special_instruction = trim($inputs['v_special_instruction']);
            $order->updated_at=date('Y-m-d h:i:s');     
            if ($order->save()) {                
                if($order_details->save())
                {
                    Session::flash('success-message', 'Order has been updated successfully.');
                    return '';    
                }
            }

        } else {
            $records = Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->first()->toArray();
            $previous = Order::where('v_shipment_number', '<', $records['v_shipment_number'])->max('v_shipment_number');
            $next = Order::where('v_shipment_number', '>', $records['v_shipment_number'])->min('v_shipment_number');
            $total_records = OrderDetail::with("Order")->whereHas('Order', function($q) use($records){
                    $q->where('tbl_order.v_shipment_number',$records['v_shipment_number']);
            })->count();
            return View('admin.order.edit_order_details', array('records' => $records, 'title' => 'Edit Order','previous'=>$previous,'next'=>$next,'total_records'=>$total_records));
        }
        return Redirect(ADMIN_URL . 'order');
    }
    public function anyEditOrder($id)
    {
        if (Input::all()) {
            $inputs = Input::all(); 
            $order = Order::find($id); 
            $order_details = OrderDetail::where('i_order_id',$id)->get();   
            $order->v_bill_to_firstname = trim($inputs['v_bill_to_firstname']);
            $order->v_bill_to_lastname = trim($inputs['v_bill_to_lastname']);
            $order->v_bill_to_address1 = trim($inputs['v_bill_to_address1']);
            $order->v_bill_to_address2 = trim($inputs['v_bill_to_address2']);
            $order->v_bill_to_city = trim($inputs['v_bill_to_city']);
            $order->v_bill_to_state = trim($inputs['v_bill_to_state']);
            $order->v_bill_to_zip = trim($inputs['v_bill_to_zip']);
            $order->v_bill_to_phonenumber = trim($inputs['v_bill_to_phonenumber']);
            $order->v_gift_message = trim($inputs['v_gift_message']);
            foreach ($order_details as $key => $value) {
                $order_details_obj=OrderDetail::find($value->id);
                $order_details_obj->v_special_instruction = trim($inputs['v_special_instruction-'.$value->id]);
                $order_details_obj->save();
            }
            $order->updated_at=date('Y-m-d h:i:s');     
            if ($order->save()) {                
                    Session::flash('success-message', 'Order has been updated successfully.');
                    return '';    
            }

        } else {
            $records = Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->first();
            return View('admin.order.edit_order', array('records' => $records, 'title' => 'Edit Order'));
        }
        return Redirect(ADMIN_URL . 'order');
    }

    public function postBulkAction()
    {
        $data = Request::all();
        if(count($data) > 0) {
            if ($data['action'] == 'Delete') {
                $orderdetials = OrderDetail::whereIn('i_order_id',$data['ids'])->delete();
                $order = Order::whereIn('id',$data['ids'])->delete();
                if($order){
                    return 'TRUE';
                } else { return 'FALSE'; }
            } else if ($data['action'] == 'edit_order') {   
               // $id='639782-3';
                $records = Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->first();
                return View('admin.order.edit_order', array('records' => $records, 'title' => 'Edit Order')); 
            }
        } 
    }
    public function anyExportToExcel()
    {        
        $data = Input::all();
        $query = Order::with("OrderDetails","Vendor");
        $user = Auth::guard('admin')->user();
        $sortColumn = array('', 'v_vendor_name','v_shipment_number','d_order_date', 'v_bill_to_lastname','v_bill_to_firstname','v_bill_to_zip','v_item_code','v_description','e_item_status');
        if($user->role==2){
                $query=$query->where("i_client_id",$user->i_client_id);
            }else if($user->role==3){
                 $query=$query->where("i_vendor_id",$user->i_vendor_id);
            }else if($user->role==4){
                $employee_data=Employee::where('id',$user->i_emp_id)->first();
                $query=$query->where("i_vendor_id",$employee_data->i_vendor_id);
            }
            if(isset($data['vendor_id']) && $data['vendor_id'] != '') {
                 $query = $query->whereHas('Vendor', function($q) use($data){
                    $q->where('tbl_vendor.id', 'LIKE', '%' . trim($data['vendor_id']) . '%');
                });
        }
        if(isset($data['v_vendor_name']) && $data['v_vendor_name'] != '') {
                 $query = $query->whereHas('Vendor', function($q) use($data){
                    $q->where('tbl_vendor.v_vendor_name', 'LIKE', '%' . trim($data['v_vendor_name']) . '%');
                });
        }
        if (isset($data['v_shipment_number']) && $data['v_shipment_number'] != '') {
            $query = $query->where('v_shipment_number', 'LIKE', '%' . trim($data['v_shipment_number']) . '%');
        }            
        if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "" && $data['from_date'] == $data['to_date'] ) {
           $query = $query->where('tbl_order.d_order_date','LIKE','%'. date('Y-m-d',strtotime($data['from_date'])).'%');
        }else if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "") {
            $data['to_date'] = $data['to_date']. "23:59:59";
            $query = $query->whereBetween('tbl_order.d_order_date',  array(date('Y-m-d H:i:s',strtotime($data['from_date'])),date('Y-m-d H:i:s',strtotime($data['to_date']))));
        }else  if (isset($data['from_date']) && $data['from_date'] != ""){
              $query = $query->where('tbl_order.d_order_date','>=',date('Y-m-d',strtotime($data['from_date'])));
        }else if (isset($data['to_date']) && $data['to_date'] != ""){
             $data['to_date'] = $data['to_date']. "23:59:59";
              $query = $query->where('tbl_order.d_order_date','<=',date('Y-m-d H:i:s',strtotime($data['to_date'])));
        } 
        if(isset($data['v_bill_to_lastname']) && $data['v_bill_to_lastname'] != '') {
            $query = $query->where('v_bill_to_lastname', 'LIKE', '%' . trim($data['v_bill_to_lastname']) . '%');
        }           
        if(isset($data['v_bill_to_firstname']) && $data['v_bill_to_firstname'] != '') {
             $query = $query->where('v_bill_to_firstname', 'LIKE', '%' . trim($data['v_bill_to_firstname']) . '%');
        }           
        if(isset($data['v_bill_to_zip']) && $data['v_bill_to_zip'] != '') {
            $query = $query->where('v_bill_to_zip', 'LIKE', '%' . trim($data['v_bill_to_zip']) . '%');
        }
        if(isset($data['v_item_code']) && $data['v_item_code'] != '') {
             $query = $query->whereHas('OrderDetails', function($q) use($data){
                $q->where('tbl_order_details.v_item_code', 'LIKE', '%' . trim($data['v_item_code']) . '%');
            });
        }
        if(isset($data['v_description']) && $data['v_description'] != '') {
              $query = $query->whereHas('OrderDetails', function($q) use($data){
                $q->where('tbl_order_details.v_description', 'LIKE', '%' . trim($data['v_description']) . '%');
            });
        }
        if(isset($data['e_item_status']) && $data['e_item_status'] != '') {
             $query = $query->whereHas('OrderDetails', function($q) use($data){
                $q->where('tbl_order_details.e_item_status', '=', trim($data['e_item_status']));
            });
        }

        $rec_per_page = REC_PER_PAGE;

        if (isset($data['length'])) {
            if ($data['length'] == '-1') {
                $rec_per_page = '';
            } else {
                $rec_per_page = $data['length'];
            }
        }

        $sort_order = $data['order']['0']['dir'];
        $order_field = $sortColumn[$data['order']['0']['column']];

        if ($sort_order != '' && $order_field != '') {
            if($order_field == 'v_vendor_name'){
                     $query = $query->join('tbl_vendor','tbl_vendor.id','=','tbl_order.i_vendor_id')->orderBy('tbl_vendor.v_vendor_name',$sort_order);
            }else if($order_field == 'v_item_code'){
                 $query = $query->join('tbl_order_details','tbl_order.id','=','tbl_order_details.i_order_id')->orderBy('tbl_order_details.v_item_code',$sort_order);
            }else if($order_field == 'v_description'){
                 $query = $query->join('tbl_order_details','tbl_order.id','=','tbl_order_details.i_order_id')->orderBy('tbl_order_details.v_description',$sort_order);
            }else if($order_field == 'e_item_status'){
                 $query = $query->join('tbl_order_details','tbl_order.id','=','tbl_order_details.i_order_id')->orderBy('tbl_order_details.e_item_status',$sort_order);
            }else
            {
                $query = $query->orderBy($order_field, $sort_order);
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }
        // $query->select('id','v_shipment_number','d_order_date','v_bill_to_lastname','v_bill_to_firstname','v_bill_to_zip','v_ship_to_company','v_ship_to_firstname','v_ship_to_address1','v_ship_to_city','v_ship_to_state','v_ship_to_zip','v_ship_to_phonenumber','v_ship_to_email','v_website');

        $users = $query->get();

        $records = $users->toArray();

        // $records = $records['data'];
        
        if (count($records) > 0) {
            $field = array();
            $field['v_vendor_name'] = 'Supplier Name';
            $field['id'] = ' Order ID';
            $field['d_order_date'] = ' Order Date';
            $field['v_bill_to_lastname'] = 'Last Name';
            $field['v_bill_to_firstname'] = 'First Name';
            $field['v_bill_to_zip'] = ' Zip Code ';
            $field['v_item_code'] = ' Product Id ';
            $field['v_description'] = ' Item Name ';
            $field['e_item_status'] = '  Item Status  ';
            $field['i_quanity'] = 'Quantity';
            $field['v_ship_to_company'] = 'Ship To - Company';
            $field['v_ship_to_firstname'] = 'Ship To - Name';
            $field['v_ship_to_address1'] = 'Ship To - Address';
            $field['v_ship_to_city'] = 'Ship To - City';
            $field['v_ship_to_state'] = 'Ship To - State';
            $field['v_ship_to_zip'] = 'Ship To - Zip';
            $field['v_ship_to_phonenumber'] = 'Phone Number';
            $field['v_ship_to_email'] = 'Email';
            $field['v_website'] = 'Website Name';
            $field['f_cogs'] = 'Wholesale Price';
            $field['d_back_ordered_date'] = 'Back Order Date';
            $field['v_canceled_reason'] = 'Cancel Reason';
            

            Excel::create('Order_List', function ($excel) use ($records, $field) {
                $excel->sheet('Order_List', function ($sheet) use ($records, $field) {
                    $sheet->setOrientation('landscape');
                    $sheet->setHeight(1, 30);
                    $sheet->mergeCells('A1:V1');
                    $sheet->setWidth(array('A' => 30, 'B' => 10, 'C' => 20, 'D' => 20, 'E' => 20, 'F' => 20, 'G' => 15, 'H' => 40,'I' => 30,'J' => 10,'K' => 30,'L' => 30,'M' => 30,'N' => 20,'O' => 15,'P' => 15,'Q' => 20,'R' => 30,'S' => 30,'T' => 10,'U' => 20,'V' => 50));

                    $sheet->cells('A1:V1', function ($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('middle');
                        $cell->setFontSize('20');
                        $cell->setFontWeight('bold');
                    });
                    $sheet->row(1, array('Order List'));
                    $sheet->cells('A2:V2', function ($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('middle');
                        $cell->setFontSize('12');
                        $cell->setFontWeight('bold');
                    });
                    $sheet->row(2, $field);
                    $intCount = 3;
                    $srNo = 1;
                    // dd($records);
                    foreach ($records as $index => $val) {
                        $vals=array();
                        $vals['v_vendor_name'] = $val['vendor']['v_vendor_name'];
                        $vals['id'] = $val['v_shipment_number'];
                        $vals['d_order_date'] = $val['d_order_date'];
                        $vals['v_bill_to_lastname'] = $val['v_bill_to_lastname'];
                        $vals['v_bill_to_firstname'] = $val['v_bill_to_firstname'];
                        $vals['v_bill_to_zip'] = $val['v_bill_to_zip'];
                        if(!empty($val['order_details'])){
                            $vals['v_item_code'] = $val['order_details'][0]['v_item_code'];
                            $vals['v_description'] = $val['order_details'][0]['v_description'];
                            $vals['e_item_status'] = $val['order_details'][0]['e_item_status'];
                            $vals['i_quanity'] = $val['order_details'][0]['i_quanity'];
                        }else{
                            $vals['v_item_code'] = '';
                            $vals['v_description'] = '';
                            $vals['e_item_status'] = '';
                            $vals['i_quanity'] = '';
                        }
                        
                        $vals['v_ship_to_company'] = $val['v_ship_to_company'];
                        $vals['v_ship_to_firstname'] = $val['v_ship_to_firstname'];
                        $vals['v_ship_to_address1'] = $val['v_ship_to_address1'];
                        $vals['v_ship_to_city'] = $val['v_ship_to_city'];
                        $vals['v_ship_to_state'] = $val['v_ship_to_state'];
                        $vals['v_ship_to_zip'] = $val['v_ship_to_zip'];
                        $vals['v_ship_to_phonenumber'] = $val['v_ship_to_phonenumber'];
                        $vals['v_ship_to_email'] = $val['v_ship_to_email'];
                        $vals['v_website'] = $val['v_website'];
                        if(!empty($val['order_details'])){
                            $vals['f_cogs'] = $val['order_details'][0]['f_cogs'];
                        $vals['d_back_ordered_date'] = $val['order_details'][0]['d_back_ordered_date'];
                        $vals['v_canceled_reason'] = $val['order_details'][0]['v_canceled_reason'];
                        }else{
                            $vals['f_cogs'] = '';
                        $vals['d_back_ordered_date'] = '';
                        $vals['v_canceled_reason'] = '';

                        }
                        
                        $sheet->row($intCount, $vals);
                        $intCount++;
                        $srNo++;
                    }
                });
            })->export('xls');
        }else{
             Session::flash('alert-message', 'No data found.');
            return Redirect(ADMIN_URL . 'order');
        }
    }
    public function anyOrderDetail(Request $request,$id) //Employee Listing
    {
        $data = Request::all();
        if ($data) {

            $query = new OrderDetail();
            $sortColumn = array('', 'v_vendor_name','v_shipment_number','d_order_date', 'v_bill_to_lastname','v_bill_to_firstname','v_bill_to_zip','v_item_code','v_description','e_item_status');
                $query=$query->with("Order")->whereHas('Order', function($q) use($id){
                    $q->where('tbl_order.v_shipment_number',$id);
                });
            if(isset($data['v_vendor_name']) && $data['v_vendor_name'] != '') {
                 $query = $query->whereHas('Vendor', function($q) use($data){
                    $q->where('tbl_vendor.v_vendor_name', 'LIKE', '%' . trim($data['v_vendor_name']) . '%');
                });
            }
            if (isset($data['v_shipment_number']) && $data['v_shipment_number'] != '') {
                $query = $query->where('v_shipment_number', 'LIKE', '%' . trim($data['v_shipment_number']) . '%');
            }            
            if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "" && $data['from_date'] == $data['to_date'] ) {
               $query = $query->where('tbl_order.d_order_date','LIKE','%'. date('Y-m-d',strtotime($data['from_date'])).'%');
            }else if (isset($data['from_date']) && $data['from_date'] != "" && isset($data['to_date']) && $data['to_date'] != "") {
                $data['to_date'] = $data['to_date']. "23:59:59";
                $query = $query->whereBetween('tbl_order.d_order_date',  array(date('Y-m-d H:i:s',strtotime($data['from_date'])),date('Y-m-d H:i:s',strtotime($data['to_date']))));
            }else  if (isset($data['from_date']) && $data['from_date'] != ""){
                  $query = $query->where('tbl_order.d_order_date','>=',date('Y-m-d',strtotime($data['from_date'])));
            }else if (isset($data['to_date']) && $data['to_date'] != ""){
                 $data['to_date'] = $data['to_date']. "23:59:59";
                  $query = $query->where('tbl_order.d_order_date','<=',date('Y-m-d H:i:s',strtotime($data['to_date'])));
            } 
            if(isset($data['v_bill_to_lastname']) && $data['v_bill_to_lastname'] != '') {
                $query = $query->where('v_bill_to_lastname', 'LIKE', '%' . trim($data['v_bill_to_lastname']) . '%');
            }           
            if(isset($data['v_bill_to_firstname']) && $data['v_bill_to_firstname'] != '') {
                 $query = $query->where('v_bill_to_firstname', 'LIKE', '%' . trim($data['v_bill_to_firstname']) . '%');
            }           
            if(isset($data['v_bill_to_zip']) && $data['v_bill_to_zip'] != '') {
                $query = $query->where('v_bill_to_zip', 'LIKE', '%' . trim($data['v_bill_to_zip']) . '%');
            }
            if(isset($data['v_item_code']) && $data['v_item_code'] != '') {
                 $query = $query->whereHas('OrderDetails', function($q) use($data){
                    $q->where('tbl_order_details.v_item_code', 'LIKE', '%' . trim($data['v_item_code']) . '%');
                });
            }
            if(isset($data['v_description']) && $data['v_description'] != '') {
                  $query = $query->whereHas('OrderDetails', function($q) use($data){
                    $q->where('tbl_order_details.v_description', 'LIKE', '%' . trim($data['v_description']) . '%');
                });
            }
            if(isset($data['e_item_status']) && $data['e_item_status'] != '') {
                 $query = $query->whereHas('OrderDetails', function($q) use($data){
                    $q->where('tbl_order_details.e_item_status', 'LIKE', '%' . trim($data['e_item_status']) . '%');
                });
            }         
            $rec_per_page = REC_PER_PAGE;

            if (isset($data['length'])) {
                if ($data['length'] == '-1') {
                    $rec_per_page = '';
                } else {
                    $rec_per_page = $data['length'];
                }
            }

            $sort_order = $data['order']['0']['dir'];
            $order_field = $sortColumn[$data['order']['0']['column']];

            if ($sort_order != '' && $order_field != '') {
                if($order_field == 'v_vendor_name'){
                     $query = $query->join('tbl_vendor','tbl_vendor.id','=','tbl_order.i_vendor_id')->orderBy('tbl_vendor.v_vendor_name',$sort_order);
                }else if($order_field == 'v_item_code'){
                     $query = $query->join('tbl_order_details','tbl_order.id','=','tbl_order_details.i_order_id')->orderBy('tbl_order_details.v_item_code',$sort_order);
                }else if($order_field == 'v_description'){
                     $query = $query->join('tbl_order_details','tbl_order.id','=','tbl_order_details.i_order_id')->orderBy('tbl_order_details.v_description',$sort_order);
                }else if($order_field == 'e_item_status'){
                     $query = $query->join('tbl_order_details','tbl_order.id','=','tbl_order_details.i_order_id')->orderBy('tbl_order_details.e_item_status',$sort_order);
                }else
                {
                    $query = $query->orderBy($order_field, $sort_order);
                }
                
            } else {
                // $query = $query->join('tbl_order_details','tbl_order.id','=','tbl_order_details.i_order_id')->orderBy('tbl_order_details.id','desc');
                $query = $query->orderBy('id', 'desc');
            }
            if($rec_per_page == ''){
                $temp_records = $query->get();
                $rec_per_page = count($temp_records);
                $users = $query->paginate($rec_per_page);
            } else {
                $users = $query->paginate($rec_per_page);
            }
            //$users = $query->paginate($rec_per_page);
            $arrUsers = $users->toArray();  
            // dd($arrUsers);
            $data = array();
            foreach ($arrUsers['data'] as $key => $val) {
                $index = 0;
                $subTotal=0;
                $subTotal=$val['i_quanity'] * $val['f_cogs'];
                // $data[$key][$index++] = '<input type="checkbox" name="id[]" value="' . $val['id'] . '" class="delete_' . $val['id'] . '">';
                $data[$key][$index++] = $val['v_item_code'];
                $data[$key][$index++] = $val["v_description"];
                $data[$key][$index++] = $val['i_quanity'];
                $data[$key][$index++] = $val['f_cogs'];
                $data[$key][$index++] = $subTotal;
                $status='<select class="form-control form-filter input-sm item_status" name="e_item_status-'.$val['id'].'" id="e_item_status-'.$val['id'].'">
                            <option value="">Select...</option>
                            <option value="Pending"'.(($val['e_item_status']=="Pending")?"selected='selected'":"").'>Pending</option>
                            <option value="Confirm and Approved" '.(($val['e_item_status']=="Confirm and Approved")?"selected='selected'":"").'>Confirm and Approved</option>
                            <option value="Back Ordered" '.(($val['e_item_status']=="Back Ordered")?"selected='selected'":"").'>Back Ordered</option>
                            <option value="Canceled" '.(($val['e_item_status']=="Canceled")?"selected='selected'":"").'>Canceled</option>
                            <option value="Hold - Do not split ship" '.(($val['e_item_status']=="Hold - Do not split ship")?"selected='selected'":"").'>Hold - Do not split ship</option>
                        </select>';
                $data[$key][$index++] = $status;
                $back_order_date='';
                if($val['d_back_ordered_date']!='0000-00-00')
                {
                    $back_order_date=date('m/d/Y', strtotime($val['d_back_ordered_date']));
                }
                $d_back_ordered_date='';
                if($val['e_item_status']=='Back Ordered'){
                    $back_order_date='<input type="text" name="d_back_ordered_date-'.$val['id'].'" id="d_back_ordered_date-'.$val['id'].'" class="d_back_ordered_date datetimepicker form-control form-filter input-sm" placeholder="Back order date" value="'.$back_order_date.'" />';
                }else
                {
                    $back_order_date='<input type="text" name="d_back_ordered_date-'.$val['id'].'" id="d_back_ordered_date-'.$val['id'].'" class="form-control form-filter input-sm" placeholder="Back order date" value="'.$back_order_date.'" readonly/>';
                }
                $data[$key][$index++] = $back_order_date;
                $cancel_rsn='';
                if($val['e_item_status']=='Canceled'){
                    $cancel_rsn='<textarea id="v_canceled_reason-'.$val['id'].'" name="v_canceled_reason-'.$val['id'].'" class="form-control v_canceled_reason" placeholder="Canceled reason">'.$val['v_canceled_reason'].'</textarea>';
                }else
                {
                    $cancel_rsn='<textarea id="v_canceled_reason-'.$val['id'].'" name="v_canceled_reason-'.$val['id'].'" class="form-control v_canceled_reason" placeholder="Canceled reason" readonly>'.$val['v_canceled_reason'].'</textarea>';
                }
                
                $data[$key][$index++] = $cancel_rsn;
            }
            $return_data['data'] = $data;
            $return_data['recordsTotal'] = $arrUsers['total'];
            $return_data['recordsFiltered'] = $arrUsers['total'];
            $return_data['data_array'] = $arrUsers['data'];
            return $return_data;
        }
    }
    public function anyEditStatus($id)
    {
        $inputs=Input::all();
        $flag=0;
        $order_details = OrderDetail::where('i_order_id',$id)->get();
        foreach ($order_details as $key => $value) {
            $order_details_obj=OrderDetail::find($value->id);
            $order_details_obj->e_item_status = trim($inputs['e_item_status-'.$value->id]);
            if($inputs['e_item_status-'.$value->id]=='Back Ordered' && $inputs['d_back_ordered_date-'.$value->id]!=''){
                $order_details_obj->d_back_ordered_date = date('Y-m-d',strtotime(trim($inputs['d_back_ordered_date-'.$value->id]))); 
                $order_details_obj->v_canceled_reason='';
            }else if($inputs['e_item_status-'.$value->id]=='Canceled' && $inputs['v_canceled_reason-'.$value->id]!=''){
                $order_details_obj->v_canceled_reason = trim($inputs['v_canceled_reason-'.$value->id]);
                $order_details_obj->d_back_ordered_date='';
            }else{
                $order_details_obj->d_back_ordered_date='';
                $order_details_obj->v_canceled_reason='';
            }

            if($order_details_obj->save())
            {
                $flag=1;
            }else
            {
                $flag=0;
                exit;
            }

        }
        if($flag==1)
        {
            Session::flash('success-message', 'Order has been updated successfully.');
            return "TRUE";
        }else
        {
            return "FALSE";
        }
    }
    public function anyDropshipOrder($id)
    {
        $order=Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->first()->toArray();
        $client=Client::with("Admin")->where('id',$order['i_client_id'])->first()->toArray();
        return View('admin.order.dropship_order', array('order' => $order,'client'=>$client, 'title' => 'Dropship Order'));         
    }
    public function anyBillSlip($id)
    {
        $order=Order::with("OrderDetails","Vendor")->where('v_shipment_number',$id)->first()->toArray();
        $client=Client::with("Admin")->where('id',$order['i_client_id'])->first()->toArray();
        return View('admin.order.billslip', array('order' => $order,'client'=>$client, 'title' => 'BillSlip Order'));            
    }
    public function anyImportToExcelOrder()
    {
        $auth_user = Auth::guard('admin')->user();
        $data = Input::all();
        if(!empty($data)){
            if(isset($data['id']) && $data['id'] != ''){
                $client_id = $data['id'];
            } else {
                $client_id = $auth_user->i_client_id; 
            }
        } else {
            $client_id = $auth_user->i_client_id; 
        }
        
        $file_flag = 0;
        $folder_name = Client::find($client_id);
        $folder_name = $folder_name->v_folder_name;
        $dir = CLIENT_FOLDER_PATH.$folder_name;
        if(is_dir($dir)){
            $files =  glob($dir . "/*.txt");
            foreach($files as $file) {
                rename($file, str_replace(".txt","_".time().".csv",$file));	 //convert text file to csv
            }
            $files = glob($dir . "/*.csv");
            $file_num = 1;
            if (!empty($files)) {
                $total_files = 0;
                $total_data_feed = 0;
                $total_order = 0;
                $total_order_details = 0;
                for($f = 0; $f < count($files); $f++) {
                    $pathtofile = $files[$f];
                    $extension = pathinfo($pathtofile,PATHINFO_EXTENSION);
                    if($extension == 'csv') {
                        $objPHPExcel = PHPExcel_IOFactory::load($files[$f]);
                        foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
                            if($key == 0) {
                                $datafeed_data = $worksheet->toArray();
                                $total_count = count($datafeed_data) - 1;
                            }
                        }
                        for($i=1; $i < count($datafeed_data); $i++){
                            if($datafeed_data[$i][0] == ''){
                                break;
                            }
                            $data_feed = new DataFeed;
                            $insert_array = array('i_client_id', 'v_bill_to_firstname', 'v_bill_to_lastname', 'v_bill_to_address1', 'v_bill_to_address2', 'v_bill_to_city', 'v_bill_to_state', 'v_bill_to_zip', 'v_bill_to_phonenumber', 'v_bill_to_email', 'v_ship_to_firstname', 'v_ship_to_lastname', 'v_ship_to_company', 'v_ship_to_address1', 'v_ship_to_address2', 'v_ship_to_city', 'v_ship_to_state', 'v_ship_to_zip', 'v_ship_to_phonenumber', 'v_ship_to_email', 'v_shipment_number', 'd_order_date', 'v_website', 'v_item_code', 'v_supplier_name', 'v_supplier_code', 'v_description', 'i_quanity', 'f_cogs', 'v_special_instruction', 'v_gift_message', 'i_file_number','created_at','updated_at');
                            $data_feed->i_client_id = $client_id;
                            $data_feed->v_bill_to_firstname = $datafeed_data[$i][0];
                            $data_feed->v_bill_to_lastname = $datafeed_data[$i][1];
                            $data_feed->v_bill_to_address1 = $datafeed_data[$i][2];
                            $data_feed->v_bill_to_address2 = $datafeed_data[$i][3];
                            $data_feed->v_bill_to_city = $datafeed_data[$i][4];
                            $data_feed->v_bill_to_state = $datafeed_data[$i][5];
                            $data_feed->v_bill_to_zip = $datafeed_data[$i][6];
                            $data_feed->v_bill_to_phonenumber = $datafeed_data[$i][7];
                            $data_feed->v_bill_to_email = $datafeed_data[$i][8];
                            $data_feed->v_ship_to_firstname = $datafeed_data[$i][9];
                            $data_feed->v_ship_to_lastname = $datafeed_data[$i][10];
                            $data_feed->v_ship_to_company = $datafeed_data[$i][11];
                            $data_feed->v_ship_to_address1 = $datafeed_data[$i][12];
                            $data_feed->v_ship_to_address2 = $datafeed_data[$i][13];
                            $data_feed->v_ship_to_city = $datafeed_data[$i][14];
                            $data_feed->v_ship_to_state = $datafeed_data[$i][15];
                            $data_feed->v_ship_to_zip = $datafeed_data[$i][16];
                            $data_feed->v_ship_to_phonenumber = $datafeed_data[$i][17];
                            $data_feed->v_ship_to_email = $datafeed_data[$i][18];
                            $data_feed->v_shipment_number = $datafeed_data[$i][19];
                            $data_feed->d_order_date = $datafeed_data[$i][20];
                            $data_feed->v_website = $datafeed_data[$i][21];
                            $data_feed->v_item_code = $datafeed_data[$i][22];
                            $data_feed->v_supplier_name = $datafeed_data[$i][23];
                            $data_feed->v_supplier_code = $datafeed_data[$i][24];
                            $data_feed->v_description = $datafeed_data[$i][25];
                            $data_feed->i_quanity = $datafeed_data[$i][26];
                            $data_feed->f_cogs = $datafeed_data[$i][27];
                            $data_feed->v_special_instruction = $datafeed_data[$i][28];
                            $data_feed->v_gift_message = $datafeed_data[$i][29];
                            $data_feed->i_file_number = $f;
                            $data_feed->created_at = date("Y-m-d h:i:s", time() + $i);
                            $data_feed->updated_at = date("Y-m-d h:i:s", time() + $i);
                            $data_feed->save();
                        }
                    }
                    $filename 	= str_replace(CLIENT_FOLDER_PATH,"",$files[$f]);
                    $cFileArr[$f]['filename']	= $filename;
                    $cFileArr[$f]['line_count'] = $total_count-1;
                    $cFileArr[$f]['line_get'] = 0;
                    //@unlink($files[$f]);
                    $file_flag = 1;
                    $file_num++;
                    $total_files++;
                }
                echo $client_id;
                exit;
                $selQry = DataFeed::where('i_client_id',$client_id)->groupBy('v_shipment_number')->get();
                $rec = $selQry->toArray();
                for($i = 0; $i < count($rec); $i++) {
                    $flag = 1;
                    $subSelQry = DataFeed::where('v_shipment_number',$rec[$i]['v_shipment_number'])->where('i_client_id',$client_id)->get();
                    $subrec = $subSelQry->toArray();
                    for($j= 0; $j < count($subrec); $j++) {
                        if(trim($subrec[$j]['v_item_code']) != '' and trim($subrec[$j]['v_supplier_code'])!='') {
                            if($flag == 1) {
                                $iClientId = $client_id;
                                $iVendorId  = $this->getVendorRelationId($subrec[$j]['v_supplier_name'],$client_id);
                                if(trim($iClientId)!='' and trim($iVendorId)!='')
                                {
                                    $order =  new Order;
                                    $order->i_client_id = $iClientId;
                                    $order->i_vendor_id = $iVendorId; 
                                    $order->v_bill_to_firstname = $subrec[$j]['v_bill_to_firstname']; 
                                    $order->v_bill_to_lastname = $subrec[$j]['v_bill_to_lastname']; 
                                    $order->v_bill_to_address1 = $subrec[$j]['v_bill_to_address1']; 
                                    $order->v_bill_to_address2 = $subrec[$j]['v_bill_to_address2']; 
                                    $order->v_bill_to_city = $subrec[$j]['v_bill_to_city']; 
                                    $order->v_bill_to_state = $subrec[$j]['v_bill_to_state']; 
                                    $order->v_bill_to_zip = $subrec[$j]['v_bill_to_zip']; 
                                    $order->v_bill_to_phonenumber = $subrec[$j]['v_bill_to_phonenumber']; 
                                    $order->v_bill_to_email = $subrec[$j]['v_bill_to_email']; 
                                    $order->v_ship_to_firstname = $subrec[$j]['v_ship_to_firstname']; 
                                    $order->v_ship_to_lastname = $subrec[$j]['v_ship_to_lastname']; 
                                    $order->v_ship_to_company = $subrec[$j]['v_ship_to_company'];
                                    $order->v_ship_to_address1 = $subrec[$j]['v_ship_to_address1']; 
                                    $order->v_ship_to_address2 = $subrec[$j]['v_ship_to_address2']; 
                                    $order->v_ship_to_city = $subrec[$j]['v_ship_to_city']; 
                                    $order->v_ship_to_state = $subrec[$j]['v_ship_to_state']; 
                                    $order->v_ship_to_zip = $subrec[$j]['v_ship_to_zip']; 
                                    $order->v_ship_to_phonenumber = $subrec[$j]['v_ship_to_phonenumber']; 
                                    $order->v_ship_to_email = $subrec[$j]['v_ship_to_email']; 
                                    $order->v_shipment_number = $subrec[$j]['v_shipment_number']; 
                                    $order->d_order_date = $subrec[$j]['d_order_date']; 
                                    $order->v_website = $subrec[$j]['v_website']; 
                                    $order->v_gift_message = $subrec[$j]['v_gift_message'];
                                    $order->save();
                                    $orderid = $order->id;
                                    $flag = 0;
                                }
                            }
                            if(trim($iClientId)!='' and trim($iVendorId)!='')
                            {
                                $FileNum = $subrec[$j]['i_file_number'];
                                $cFileArr[$FileNum]['line_get'] = $cFileArr[$FileNum]['line_get'] + 1;
                                $order_detail =  New OrderDetail;
                                $order_detail->i_order_id = $orderid;
                                $order_detail->v_item_code = $subrec[$j]['v_item_code'];
                                $order_detail->v_supplier_code = $subrec[$j]['v_supplier_code'];
                                $order_detail->i_quanity = $subrec[$j]['i_quanity'];
                                $order_detail->v_description = $subrec[$j]['v_description'];
                                $order_detail->f_cogs = $subrec[$j]['f_cogs'];
                                $order_detail->v_special_instruction = $subrec[$j]['v_special_instruction'];
                                $order_detail->save();
                            }
                    }
                }
                    //DataFeed::where('i_client_id',$client_id)->delete();
                    if($file_flag == 1) {
                        foreach($cFileArr as $file) {
                            // echo "Total <strong>".$file['line_get']."</strong> records are imported <strong>out of ".$file['line_count']."</strong> from file name '".$folder_name."'.<br />";
                        }
                        
                    } else {
                        // echo "New Datafeed file is not available. Process completed.";
                    }
                } 
                Session::flash('success-message', 'Datafeed have been updated successfully.');
                return 'TRUE';
        } else {
                Session::flash('alert-message', 'Folder '.$folder_name.' is empty.');
                return 'TRUE';
            }
        } else {
            Session::flash('alert-message', ' Folder '.$folder_name.' does not exists.');
            return 'TRUE';
        }
}
public function anyCronForImportFeed() {
    $path =  CLIENT_FOLDER_PATH;
    $dirs = array();
    $dir = dir($path);
    while (false !== ($entry = $dir->read())) {
        if ($entry != '.' && $entry != '..') {
            if (is_dir($path . '/' .$entry)) {
                $dirs[] = $entry; 
            }
        }
    }
    $alert_msg = '';
    $success_msg = '';
    $client_data = Client::pluck('id','v_folder_name')->toArray();
    foreach($dirs as $key => $folder_name){
        if(array_key_exists($folder_name, $client_data)){
            $client_id = $client_data[$folder_name];
        }
        $dir = CLIENT_FOLDER_PATH.$folder_name;
        if(is_dir($dir)){
            $files =  glob($dir . "/*.txt");
            foreach($files as $file) {
                rename($file, str_replace(".txt","_".time().".csv",$file));	 //convert text file to csv
            }
            $files = glob($dir . "/*.csv");
            $file_num = 1;
            if (!empty($files)) {
                $total_files = 0;
                $total_data_feed = 0;
                $total_order = 0;
                $total_order_details = 0;
                for($f = 0; $f < count($files); $f++) {
                    $pathtofile = $files[$f];
                    $extension = pathinfo($pathtofile,PATHINFO_EXTENSION);
                    if($extension == 'csv') {
                        $objPHPExcel = PHPExcel_IOFactory::load($files[$f]);
                        foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
                            if($key == 0) {
                                $datafeed_data = $worksheet->toArray();
                                $total_count = count($datafeed_data) - 1;
                            }
                        }
                        for($i=0; $i < count($datafeed_data); $i++){
                            if($datafeed_data[$i][0] == ''){
                                break;
                            }
                            $data_feed = new DataFeed;
                            $insert_array = array('i_client_id', 'v_bill_to_firstname', 'v_bill_to_lastname', 'v_bill_to_address1', 'v_bill_to_address2', 'v_bill_to_city', 'v_bill_to_state', 'v_bill_to_zip', 'v_bill_to_phonenumber', 'v_bill_to_email', 'v_ship_to_firstname', 'v_ship_to_lastname', 'v_ship_to_company', 'v_ship_to_address1', 'v_ship_to_address2', 'v_ship_to_city', 'v_ship_to_state', 'v_ship_to_zip', 'v_ship_to_phonenumber', 'v_ship_to_email', 'v_shipment_number', 'd_order_date', 'v_website', 'v_item_code', 'v_supplier_name', 'v_supplier_code', 'v_description', 'i_quanity', 'f_cogs', 'v_special_instruction', 'v_gift_message', 'i_file_number','created_at','updated_at');
                            $data_feed->i_client_id = $client_id;
                            $data_feed->v_bill_to_firstname = $datafeed_data[$i][0];
                            $data_feed->v_bill_to_lastname = $datafeed_data[$i][1];
                            $data_feed->v_bill_to_address1 = $datafeed_data[$i][2];
                            $data_feed->v_bill_to_address2 = $datafeed_data[$i][3];
                            $data_feed->v_bill_to_city = $datafeed_data[$i][4];
                            $data_feed->v_bill_to_state = $datafeed_data[$i][5];
                            $data_feed->v_bill_to_zip = $datafeed_data[$i][6];
                            $data_feed->v_bill_to_phonenumber = $datafeed_data[$i][7];
                            $data_feed->v_bill_to_email = $datafeed_data[$i][8];
                            $data_feed->v_ship_to_firstname = $datafeed_data[$i][9];
                            $data_feed->v_ship_to_lastname = $datafeed_data[$i][10];
                            $data_feed->v_ship_to_company = $datafeed_data[$i][11];
                            $data_feed->v_ship_to_address1 = $datafeed_data[$i][12];
                            $data_feed->v_ship_to_address2 = $datafeed_data[$i][13];
                            $data_feed->v_ship_to_city = $datafeed_data[$i][14];
                            $data_feed->v_ship_to_state = $datafeed_data[$i][15];
                            $data_feed->v_ship_to_zip = $datafeed_data[$i][16];
                            $data_feed->v_ship_to_phonenumber = $datafeed_data[$i][17];
                            $data_feed->v_ship_to_email = $datafeed_data[$i][18];
                            $data_feed->v_shipment_number = $datafeed_data[$i][19];
                            $data_feed->d_order_date = $datafeed_data[$i][20];
                            $data_feed->v_website = $datafeed_data[$i][21];
                            $data_feed->v_item_code = $datafeed_data[$i][22];
                            $data_feed->v_supplier_name = $datafeed_data[$i][23];
                            $data_feed->v_supplier_code = $datafeed_data[$i][24];
                            $data_feed->v_description = $datafeed_data[$i][25];
                            $data_feed->i_quanity = $datafeed_data[$i][26];
                            $data_feed->f_cogs = $datafeed_data[$i][27];
                            $data_feed->v_special_instruction = $datafeed_data[$i][28];
                            $data_feed->v_gift_message = $datafeed_data[$i][29];
                            $data_feed->i_file_number = $f;
                            $data_feed->created_at = date("Y-m-d h:i:s", time() + $i);
                            $data_feed->updated_at = date("Y-m-d h:i:s", time() + $i);
                            $data_feed->save();
                        }
                    }
                    $filename 	= str_replace(CLIENT_FOLDER_PATH,"",$files[$f]);
                    $cFileArr[$f]['filename']	= $filename;
                    $cFileArr[$f]['line_count'] = $total_count-1;
                    $cFileArr[$f]['line_get'] = 0;
                    //@unlink($files[$f]);
                    $file_flag = 1;
                    $file_num++;
                    $total_files++;
                }
                $selQry = DataFeed::where('i_client_id',$client_id)->groupBy('v_shipment_number')->get();
                $rec = $selQry->toArray();
                for($i = 0; $i < count($rec); $i++) {
                    $flag = 1;
                    $subSelQry = DataFeed::where('v_shipment_number',$rec[$i]['v_shipment_number'])->where('i_client_id',$client_id)->get();
                    $subrec = $subSelQry->toArray();
                    
                    for($j= 0; $j < count($subrec); $j++) {
                        if(trim($subrec[$j]['v_item_code']) != '' and trim($subrec[$j]['v_supplier_code'])!='') {
                            if($flag == 1) {
                                $subrec[$j]['v_supplier_name'];
                                $iClientId = $client_id;
                                $iVendorId  = $this->getVendorRelationId($subrec[$j]['v_supplier_name'],$client_id);
                                if(trim($iClientId)!='' and trim($iVendorId)!='')
                                {
                                    $order =  new Order;
                                    $order->i_client_id = $iClientId;
                                    $order->i_vendor_id = $iVendorId; 
                                    $order->v_bill_to_firstname = $subrec[$j]['v_bill_to_firstname']; 
                                    $order->v_bill_to_lastname = $subrec[$j]['v_bill_to_lastname']; 
                                    $order->v_bill_to_address1 = $subrec[$j]['v_bill_to_address1']; 
                                    $order->v_bill_to_address2 = $subrec[$j]['v_bill_to_address2']; 
                                    $order->v_bill_to_city = $subrec[$j]['v_bill_to_city']; 
                                    $order->v_bill_to_state = $subrec[$j]['v_bill_to_state']; 
                                    $order->v_bill_to_zip = $subrec[$j]['v_bill_to_zip']; 
                                    $order->v_bill_to_phonenumber = $subrec[$j]['v_bill_to_phonenumber']; 
                                    $order->v_bill_to_email = $subrec[$j]['v_bill_to_email']; 
                                    $order->v_ship_to_firstname = $subrec[$j]['v_ship_to_firstname']; 
                                    $order->v_ship_to_lastname = $subrec[$j]['v_ship_to_lastname']; 
                                    $order->v_ship_to_company = $subrec[$j]['v_ship_to_company'];
                                    $order->v_ship_to_address1 = $subrec[$j]['v_ship_to_address1']; 
                                    $order->v_ship_to_address2 = $subrec[$j]['v_ship_to_address2']; 
                                    $order->v_ship_to_city = $subrec[$j]['v_ship_to_city']; 
                                    $order->v_ship_to_state = $subrec[$j]['v_ship_to_state']; 
                                    $order->v_ship_to_zip = $subrec[$j]['v_ship_to_zip']; 
                                    $order->v_ship_to_phonenumber = $subrec[$j]['v_ship_to_phonenumber']; 
                                    $order->v_ship_to_email = $subrec[$j]['v_ship_to_email']; 
                                    $order->v_shipment_number = $subrec[$j]['v_shipment_number']; 
                                    $order->d_order_date = $subrec[$j]['d_order_date']; 
                                    $order->v_website = $subrec[$j]['v_website']; 
                                    $order->v_gift_message = $subrec[$j]['v_gift_message'];
                                    $order->save();
                                    $orderid = $order->id;
                                    $flag = 0;
                                }
                            }
                            if(trim($iClientId)!='' and trim($iVendorId)!='')
                            {
                                $FileNum = $subrec[$j]['i_file_number'];
                                $cFileArr[$FileNum]['line_get'] = $cFileArr[$FileNum]['line_get'] + 1;
                                $order_detail =  New OrderDetail;
                                $order_detail->i_order_id = $orderid;
                                $order_detail->v_item_code = $subrec[$j]['v_item_code'];
                                $order_detail->v_supplier_code = $subrec[$j]['v_supplier_code'];
                                $order_detail->i_quanity = $subrec[$j]['i_quanity'];
                                $order_detail->v_description = $subrec[$j]['v_description'];
                                $order_detail->f_cogs = $subrec[$j]['f_cogs'];
                                $order_detail->v_special_instruction = $subrec[$j]['v_special_instruction'];
                                $order_detail->save();
                            }
                    }
                }
                    //DataFeed::where('i_client_id',$client_id)->delete();
                    if($file_flag == 1) {
                        foreach($cFileArr as $file) {
                            echo "Total <strong>".$file['line_get']."</strong> records are imported <strong>out of ".$file['line_count']."</strong> from file name '".$folder_name."'.<br />";
                        }
                        
                    } else {
                        echo "New Datafeed file is not available. Process completed.";
                    }
                } 
                echo  'Datafeed have been updated successfully.<br/>';
        } else {
                echo  'Folder '.$folder_name.' is empty.<br/>';
            }
        } else {
            echo 'Folder '.$folder_name.' does not exists.<br/>';
            
        }
    }
    // if($alert_msg != ''){
    //     Session::flash('alert-message', $alert_msg);
    // }
    // if($success_msg != ''){
    //     Session::flash('success-message', $success_msg);
    // }
    
   // return Redirect(ADMIN_URL . 'order');
}
public function getVendorRelationId($SupplierName,$client_id)
{
	$qry = Vendor::select('id')->where('v_company_name',$SupplierName)->where('i_client_id', $client_id)->get();
	return $qry[0]['id'];
}
   
}

