<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use Mail, Session, Redirect, Auth, Validator,Excel,Cookie,Request;
use App\Models\User, App\Models\Client,App\Models\Admin,App\Models\Employee,App\Models\Vendor,App\Models\MailTemplate;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use File;
use Illuminate\Support\Facades\Hash;

class MailTemplateController extends Controller
{
    public function getIndex()
    {
        return View('admin.mail_template.index', array('title' => 'Mail Template List'));
    }

    public function anyListAjax(Request $request) //Employee Listing
    {
        $data = Request::all();
        if ($data) {

            $query = new MailTemplate();
            $sortColumn = array('', 'v_template_name');            
            if (isset($data['v_template_name']) && $data['v_template_name'] != '') {
                $query = $query->where('v_template_name', 'LIKE', '%' . trim($data['v_template_name']) . '%');
            }                      
            $rec_per_page = REC_PER_PAGE;

            if (isset($data['length'])) {
                if ($data['length'] == '-1') {
                    $rec_per_page = '';
                } else {
                    $rec_per_page = $data['length'];
                }
            }

            $sort_order = $data['order']['0']['dir'];
            $order_field = $sortColumn[$data['order']['0']['column']];

            if ($sort_order != '' && $order_field != '') {                
                $query = $query->orderBy($order_field, $sort_order);
            } else {
                $query = $query->orderBy('id', 'desc');
            }
            if($rec_per_page == ''){
                $temp_records = $query->get();
                $rec_per_page = count($temp_records);
                $users = $query->paginate($rec_per_page);
            } else {
                $users = $query->paginate($rec_per_page);
            }
            //$users = $query->paginate($rec_per_page);
            $arrUsers = $users->toArray();                        
            $data = array();
            foreach ($arrUsers['data'] as $key => $val) {
                $index = 0;
                $data[$key][$index++] = '<input type="checkbox" name="id[]" value="' . $val['id'] . '" class="delete_' . $val['id'] . '">';
                $data[$key][$index++] = $val['v_template_name'];                
                $action = '<div class="actions"><a class="edit btn default btn-xs black" rel="' . $val['id'] . '" href="' . ADMIN_URL . 'mail-template/edit/' . $val['id'] . '"  title="Edit"><i class="fa fa-edit"></i></a>';
                $data[$key][$index++] = $action;
            }
            $return_data['data'] = $data;
            $return_data['recordsTotal'] = $arrUsers['total'];
            $return_data['recordsFiltered'] = $arrUsers['total'];
            $return_data['data_array'] = $arrUsers['data'];
            return $return_data;
        }
    }

    public function getDelete($id)
    {
        $employee = Employee::find($id);
        if (!empty($employee)) {
            $admin = Admin::where('i_emp_id',$id)->delete();
            $employee->delete();
            return 'TRUE';
        } else {
            return 'FALSE';
        }  
    }
    public function anyEdit($id)
    {
        if (Input::all()) {
            $inputs = Input::all();             
            $mail_template = MailTemplate::find($id);             
            $mail_template->v_template_name = trim($inputs['v_template_name']);           
            $mail_template->v_subject = trim($inputs['v_subject']);           
            $mail_template->v_from_email_id = trim($inputs['v_from_email_id']);           
            $mail_template->t_email_content = trim($inputs['t_email_content']);           
            $mail_template->updated_at=date('Y-m-d h:i:s');           
            if ($mail_template->save()) { 
                    Session::flash('success-message', 'Mail Template has been updated successfully.');
                    return '';    
               
            }
                        
            

        } else {

            $records = MailTemplate::find($id);              
            return View('admin.mail_template.edit_mail_template', array('records' => $records, 'title' => 'Edit Mail Template'));
        }
        return Redirect(ADMIN_URL . 'mail-template');
    }

    public function postBulkAction()
    {
        $data = Request::all();        
        if(count($data) > 0) {
            if ($data['action'] == 'Delete') {                                
                $admin = Admin::whereIn('i_client_id',$data['ids'])->delete();
                $employee = Employee::whereIn('id',$data['ids'])->delete();
                if($employee){
                    return 'TRUE';
                } else { return 'FALSE'; }
            }
        } 
    }
}
