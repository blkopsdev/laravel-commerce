<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use Mail, Session, Redirect, Auth, Validator, Excel, Cookie, Request, Zipper, DateTime;
use App\Models\User, App\Models\Client,App\Models\Admin,App\Models\Employee,App\Models\Vendor,App\Models\MailTemplate,App\Models\PurchaseOrder,App\Models\PurchaseOrderDetail, App\Models\PurchaseDataFeed;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use File, PHPExcel_IOFactory, PHPExcel_Shared_Date, PHPExcel_Style_NumberFormat;
use Illuminate\Support\Facades\Hash;
class PurchaseOrderController extends BaseController
{
    public function getIndex()
    {
        $auth_user = Auth::guard('admin')->user();
        if($auth_user->role==2){
            $vendor_data = Vendor::where('i_client_id',$auth_user->i_client_id)->orderBy('v_vendor_name')->get();
        }else{
            $vendor_data = Vendor::orderBy('v_vendor_name')->get();
        }
        $client_data = Client::where('e_status','Active')->get();
        return View('admin.purchase_order.index', array('title' => 'Purchase Order List','vendor_list' => $vendor_data,'client_data'=>$client_data));
    }

    //Purchase Order Listing
    public function anyListAjax(Request $request)
    {
        $data = Request::all();
        $user = Auth::guard('admin')->user();
        $module_name = "6";
        if ($data) {
            DB::enableQueryLog();
            $query = new PurchaseOrderDetail();            
            if($user->e_type== 'Employee' || $user->e_type== 'Vendor'){
                $sortColumn = array('v_po_number','d_order_issue_date', 'd_expected_delivery_date','d_vendor_expected_delivery_date','v_vendor_product_name','i_quantity','e_item_status','f_order_total');
            } else {
                $sortColumn = array('', 'v_vendor_name','v_po_number','d_order_issue_date', 'd_expected_delivery_date','d_vendor_expected_delivery_date','v_vendor_product_name','i_quantity','e_item_status','f_order_total');
            }

            // $query = $query->with("Order","Order.Vendor");


            $query = $query->join('tbl_purchase_order', function ($join){
                            $join->on('tbl_purchase_order_details.i_purchase_order_id', '=', 'tbl_purchase_order.id')->whereNull('tbl_purchase_order.deleted_at');
                        });
            $query = $query->join('tbl_vendor', function ($join){
                            $join->on('tbl_purchase_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                        });

            if($user->role==2){
                $query=$query->where("tbl_purchase_order.i_client_id",$user->i_client_id);
            }else if($user->role==3){                
                $vendor_ids = Vendor::where('v_email',$user->v_email)->where('id',$user->i_vendor_id)->pluck('id');
                $query=$query->whereIn('tbl_purchase_order.i_vendor_id',$vendor_ids);
            }else if($user->role==4){
                $employee_data=Employee::where('id',$user->i_emp_id)->first();
                $query=$query->where("tbl_purchase_order.i_vendor_id",$employee_data->i_vendor_id);
            }
            if(isset($data['i_vendor_id']) && $data['i_vendor_id'] != '') {
                 $query = $query->whereHas('PurchaseOrder.Vendor', function($q) use($data){
                    $q->where('tbl_vendor.id', '=',  trim($data['i_vendor_id']));
                });
            }
            if (isset($data['v_po_number']) && $data['v_po_number'] != '') {
                $query = $query->whereHas('PurchaseOrder', function($q) use($data){
                    $q->where('tbl_purchase_order.v_po_number', 'LIKE', '%' . trim($data['v_po_number']) . '%');
                });
            }
            if (isset($data['from_order_issue_date']) && $data['from_order_issue_date'] != ""){
                 $query = $query->whereHas('PurchaseOrder', function($q) use($data){
                    $q->where('tbl_purchase_order.d_order_issue_date','>=',date('Y-m-d',strtotime($data['from_order_issue_date'])));
                });
            }                     
            if (isset($data['b_hidden']) && $data['b_hidden'] != ""){                                 
            } else {
                $query = $query->where('b_hidden','=',0);
            }
            if (isset($data['to_order_issue_date']) && $data['to_order_issue_date'] != ""){
                 //$data['to_order_issue_date'] = $data['to_order_issue_date']. "23:59:59";
                 $query = $query->whereHas('PurchaseOrder', function($q) use($data){
                    $q->where('tbl_purchase_order.d_order_issue_date','<=',date('Y-m-d',strtotime($data['to_order_issue_date'])));
                });
            }

            if (isset($data['from_expected_delivery_date']) && $data['from_expected_delivery_date'] != ""){
                 $query = $query->whereHas('PurchaseOrder', function($q) use($data){
                    $q->where('tbl_purchase_order.d_expected_delivery_date','>=',date('Y-m-d',strtotime($data['from_expected_delivery_date'])));
                });
            }
            if (isset($data['to_expected_delivery_date']) && $data['to_expected_delivery_date'] != ""){
                 //$data['to_expected_delivery_date'] = $data['to_expected_delivery_date']. "23:59:59";
                 $query = $query->whereHas('PurchaseOrder', function($q) use($data){
                    $q->where('tbl_purchase_order.d_expected_delivery_date','<=',date('Y-m-d',strtotime($data['to_expected_delivery_date'])));
                });
            }
            if (isset($data['from_vendor_expected_delivery_date']) && $data['from_vendor_expected_delivery_date'] != ""){
                $query = $query->where('d_vendor_expected_delivery_date','>=',date('Y-m-d',strtotime($data['from_vendor_expected_delivery_date'])));
            }
            if (isset($data['to_vendor_expected_delivery_date']) && $data['to_vendor_expected_delivery_date'] != ""){
                //$data['to_vendor_expected_delivery_date'] = $data['to_vendor_expected_delivery_date']. "23:59:59";
                $query = $query->where('d_vendor_expected_delivery_date','<=',date('Y-m-d',strtotime($data['to_vendor_expected_delivery_date'])));
            }

            if(isset($data['v_vendor_product_name']) && $data['v_vendor_product_name'] != '') {
                 $query = $query->where('v_vendor_product_name',  'LIKE', '%' .trim($data['v_vendor_product_name']). '%');
            }
            if (isset($data['i_quantity_from']) && $data['i_quantity_from'] != ""){
                $query = $query->where('i_quantity','>=',trim($data['i_quantity_from']));
            }
            if (isset($data['i_quantity_to']) && $data['i_quantity_to'] != ""){
                $query = $query->where('i_quantity','<=',trim($data['i_quantity_to']));
            }
            if(isset($data['e_item_status']) && $data['e_item_status'] != '') {
                $query = $query->where('e_item_status',  trim($data['e_item_status']));
            }
            if(isset($data['e_po_status']) && $data['e_po_status'] != '') {
                $query = $query->where('e_po_status',  trim($data['e_po_status']));
            }
            if (isset($data['from_order_total']) && $data['from_order_total'] != ""){
                $query = $query->where('f_order_total','>=',trim($data['from_order_total']));
            }
            if (isset($data['to_order_total']) && $data['to_order_total'] != ""){
                $query = $query->where('f_order_total','<=',trim($data['to_order_total']));
            }
            $rec_per_page = REC_PER_PAGE;

            if (isset($data['length'])) {
                if ($data['length'] == '-1') {
                    $rec_per_page = '';
                } else {
                    $rec_per_page = $data['length'];
                }
            }

            $sort_order = $data['order']['0']['dir'];
            $order_field = $sortColumn[$data['order']['0']['column']];
            if ($sort_order != '' && $order_field != '') {
                if ($order_field == 'v_vendor_name'){
                    $query = $query->orderBy('tbl_vendor.v_vendor_name', $sort_order);
                } else if ($order_field == 'v_po_number') {
                    $query = $query->orderBy('tbl_purchase_order.v_po_number', $sort_order);
                } else if ($order_field == 'd_order_issue_date') {
                    $query = $query->orderBy('tbl_purchase_order.d_order_issue_date', $sort_order);
                } else if ($order_field == 'd_expected_delivery_date') {
                    $query = $query->orderBy('tbl_purchase_order.d_expected_delivery_date', $sort_order);
                } else if ($order_field == 'f_order_total') {
                    $query = $query->orderBy('tbl_purchase_order.f_order_total', $sort_order);
                } else if ($order_field == 'updated_at') {
                    $query = $query->orderBy('tbl_purchase_order.updated_at', $sort_order);
                } else {
                    $query = $query->orderBy($order_field, $sort_order);
                }

            } else {
                $query = $query->orderBy('tbl_purchase_order.updated_at', 'desc');
            }

            $query = $query->select('tbl_purchase_order_details.*','tbl_purchase_order_details.id as orderDetailId','tbl_vendor.*','tbl_purchase_order.*');
            if($rec_per_page == ''){
                $temp_records = $query->get();
                $rec_per_page = count($temp_records);
                $purchaseOrder = $query->paginate($rec_per_page);
            } else {
                $purchaseOrder = $query->paginate($rec_per_page);
            }

            /* $queries = DB::getQueryLog();
            $lastQuery = last($queries);
            pr($lastQuery);
            exit; */

            $arrRecords = $purchaseOrder->toArray();

            $data = array();
            foreach ($arrRecords['data'] as $key => $val) {

                $index = 0;

                if($user->role != 1) {
                    if(isset($this->permissions[$module_name]['e_delete']) && $this->permissions[$module_name]['e_delete'] == '1') {
                            $data[$key][$index++] = '<input type="checkbox" name="id[]" value="'.$val['orderDetailId'].'" class="delete_'.$val['orderDetailId'].'">';
                    }
                } else {
                    $data[$key][$index++] = '<input type="checkbox" name="id[]" value="'.$val['orderDetailId'].'" class="delete_'.$val['orderDetailId'].'">';
                }
                if($user->role!=3 && $user->role!=4){
                    $data[$key][$index++] = $val['v_vendor_name'];
                }
                $data[$key][$index++] = '<a rel="' . $val['i_purchase_order_id'] . '" href="' . ADMIN_URL . 'purchase_order/edit/' . $val['v_po_number'] . '">'. $val["v_po_number"] .'</a>';
                $data[$key][$index++] = ($val['d_order_issue_date'] != '0000-00-00' && $val['d_order_issue_date'] != '') ? date('m/d/Y', strtotime($val['d_order_issue_date'])) : '';
                // $data[$key][$index++] = (!empty($val['e_po_status'])) ? $val['e_po_status'] : 'Incomplete';
                $data[$key][$index++] = ($val['d_expected_delivery_date'] != '0000-00-00' && $val['d_expected_delivery_date'] != '') ? date('m/d/Y', strtotime($val['d_expected_delivery_date'])) : '';
                $data[$key][$index++] = ($val['d_vendor_expected_delivery_date'] != '0000-00-00' && $val['d_vendor_expected_delivery_date'] != '') ?  date('m/d/Y', strtotime($val['d_vendor_expected_delivery_date'])) : '';
                $data[$key][$index++] = $val['v_vendor_product_name'];
                $data[$key][$index++] = (!empty($val['i_quantity'])) ? $val['i_quantity'] : '';
                $data[$key][$index++] = (!empty($val['e_item_status'])) ? $val['e_item_status'] : '';
                //$data[$key][$index++] = (!empty($val['f_order_total'])) ? '$'.$val['f_order_total'] : '';                
                $action='<span class="data-hidden-val" data-hidden-val = "'.$val['b_hidden'].'"></span>';                
                if($user->role != 1) {
                    if (isset($this->permissions[$module_name]['e_edit']) && $this->permissions[$module_name]['e_edit'] == '1' && isset($this->permissions[$module_name]['e_delete']) && $this->permissions[$module_name]['e_delete'] == '0') {

                        $action .= '<div class="actions"><a class="edit btn default btn-xs black" rel="' . $val['i_purchase_order_id'] . '" href="' . ADMIN_URL . 'purchase_order/edit/' . $val['v_po_number'] . '"  title="Edit"><i class="fa fa-edit"></i></a>';

                    } else if (isset($this->permissions[$module_name]['e_delete']) && $this->permissions[$module_name]['e_delete'] == '1' || isset($this->permissions[$module_name]['e_edit']) && $this->permissions[$module_name]['e_edit'] == '1') {

                        $action .= '<div class="actions"><a class="edit btn default btn-xs black" rel="' . $val['orderDetailId'] . '" href="' . ADMIN_URL . 'purchase_order/edit/' . $val['v_po_number'] . '"  title="Edit"><i class="fa fa-edit"></i></a><a href="javascript:;" id="delete_record" rel="' . $val['orderDetailId'] . '" delete-url="' . ADMIN_URL . 'purchase_order/delete/' . $val['orderDetailId'] . '" class="btn default btn-xs black delete" title="Delete"><i class="icon-trash"></i></a>';
                    }
                } else {
                    $action .= '<div class="actions"><a class="edit btn default btn-xs black" rel="' . $val['orderDetailId'] . '" href="' . ADMIN_URL . 'purchase_order/edit/' . $val['v_po_number'] . '"  title="Edit"><i class="fa fa-edit"></i></a><a href="javascript:;" id="delete_record" rel="' . $val['orderDetailId'] . '" delete-url="' . ADMIN_URL . 'purchase_order/delete/' . $val['orderDetailId'] . '" class="btn default btn-xs black delete" title="Delete"><i class="icon-trash"></i></a>';
                }

                $data[$key][$index++] = $action;
            }
            $return_data['data'] = $data;
            $return_data['recordsTotal'] = $arrRecords['total'];
            $return_data['recordsFiltered'] = $arrRecords['total'];
            $return_data['data_array'] = $arrRecords['data'];
            return $return_data;
        }
    }

    // Purchase Order Delete
    public function getDelete($id)
    {
        $order = PurchaseOrderDetail::find($id);
        if (!empty($order)) {
            $order_id = $order['i_purchase_order_id'];
            $order_details = PurchaseOrderDetail::where('id',$id)->delete();
            $order_count = PurchaseOrderDetail::where('i_purchase_order_id',$order_id)->count();
            if ($order_count <= 0) {
                $order = PurchaseOrder::find($order_id);
                $order = $order->delete();
            } else {
                $sub_total = PurchaseOrderDetail::where('i_purchase_order_id',$order_id)->sum('f_sub_total');
                DB::table('tbl_purchase_order')->where('id',$order_id)->update(['f_order_total' => $sub_total]);
            }
            return 'TRUE';
        } else {
            return 'FALSE';
        }
    }

    // Purchase Order Detail Delete
    public function getOrderDetailsDelete($id)
    {
        $OrderDetail = PurchaseOrderDetail::find($id);
        if (!empty($OrderDetail)) {
            $order_id = $OrderDetail['i_purchase_order_id'];
            $OrderDetail->delete();
            $order_count = PurchaseOrderDetail::where('i_purchase_order_id',$order_id)->count();
            if ($order_count <= 0) {
                $order = PurchaseOrder::find($order_id);
                $order = $order->delete();
            } else {
                $sub_total = PurchaseOrderDetail::where('i_purchase_order_id',$order_id)->sum('f_sub_total');
                DB::table('tbl_purchase_order')->where('id',$order_id)->update(['f_order_total' => $sub_total]);
           
            }
            return 'TRUE';
        } else {
            return 'FALSE';
        }
    }

    // Purchase Order Detail
    public function anyEdit($id)
    {

        $value= '';
        foreach($_COOKIE as $key => $val){
            if (strpos($key, 'list-ajax') !== false) {
                $value = $val;
            }
        }
        if(isset($value) && $value != ''){
            $data = json_decode($value);
        }

        if (isset($data) && !empty($data)) {
            $user = Auth::guard('admin')->user();
            $module_name = "1";
            $query = new PurchaseOrderDetail();

            $data= (array) $data;
            $query = $query->join('tbl_purchase_order', function ($join){
                        $join->on('tbl_purchase_order_details.i_purchase_order_id', '=', 'tbl_purchase_order.id')->whereNull('tbl_purchase_order.deleted_at');
                    });
            $query = $query->join('tbl_vendor', function ($join){
                        $join->on('tbl_purchase_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                    });

            if($user->role==2){
                $vendor_ids = Vendor::where('i_client_id',$user->i_client_id)->pluck('id');
                $query=$query->whereIn('tbl_purchase_order.i_vendor_id',$vendor_ids);
            } else if($user->role==3){
                $vendor_list = Vendor::select('id')->where('v_email',$user->v_email)->get();
                $vendor_ids = array();
                if(count($vendor_list)>0){
                    $vendor_list = $vendor_list->toArray();
                    foreach ($vendor_list as $key => $value) {
                        $vendor_ids[] = $value['id'];
                    }
                }else{
                    $vendor_list = array();
                }
                /*$query=$query->where("tbl_order.i_vendor_id",$user->i_vendor_id);*/
                if(!empty($vendor_list)){
                    $query=$query->orwhereIn('tbl_purchase_order.i_vendor_id',$vendor_ids);
                }
            } else if($user->role==4){
                $employee_data=Employee::where('id',$user->i_emp_id)->first();
                $query=$query->where("tbl_purchase_order.i_vendor_id",$employee_data->i_vendor_id);
            }


            $query1 = $query;
            $records_1 = clone $query1;
            $query2 = clone $query1;
            $query3 = clone $query2;
            //$query4 = clone $query3;
            $records = $records_1->where('tbl_purchase_order.v_po_number',$id)->first();
            $previous = $query2->where('tbl_purchase_order.v_po_number', '<', $id)->max('tbl_purchase_order.v_po_number');
            $next = $query3->where('tbl_purchase_order.v_po_number', '>', $id)->min('tbl_purchase_order.v_po_number');
            if($previous == ''){
                $previous = 0;
            }
            if($next == ''){
                $next = 0;
            }
            $total_records = $records_1->where('tbl_purchase_order.v_po_number',$id)->count();
        }
        if(isset($records) && !empty($records)) {
                $item_status = PurchaseOrderDetail::where('i_purchase_order_id',$records->i_purchase_order_id)->pluck('e_item_status');                
                $item_status = $item_status->toArray();                
                return View('admin.purchase_order.edit_order_details', array('records' => $records, 'title' => 'Edit Purchase Order','previous'=>$previous,'next'=>$next,'total_records'=>$total_records,'item_status'=>$item_status));
        } else{
            return Redirect(ADMIN_URL . 'purchase_order');
        }

        return Redirect(ADMIN_URL . 'purchase_order');
    }

    // Purchase Order Bulk Delete
    public function postBulkAction()
    {
        $data = Request::all();
        if(count($data) > 0) {
            if ($data['action'] == 'Delete') {
                $orderDetails = PurchaseOrderDetail::whereIn('id',$data['ids'])->get();
                foreach($orderDetails as $key => $value) {
                    $order_id = $value['i_purchase_order_id'];
                    PurchaseOrderDetail::where('id',$value['id'])->delete();
                    $order_count = PurchaseOrderDetail::where('i_purchase_order_id',$order_id)->count();
                    if ($order_count <= 0) {
                        $order = PurchaseOrder::find($order_id);
                        $order = $order->delete();
                    }else {
                        $sub_total = PurchaseOrderDetail::where('i_purchase_order_id',$order_id)->sum('f_sub_total');
                        DB::table('tbl_purchase_order')->where('id',$order_id)->update(['f_order_total' => $sub_total]);
           
                    }
                }
                $details = PurchaseOrderDetail::whereIn('id',$data['ids'])->get();
                if (count($details) == 0 ) {
                    return 'TRUE';
                } else { return 'FALSE'; }
            } else if($data['action'] == 'Hide') {
                $orderDetails = PurchaseOrderDetail::whereIn('id',$data['ids'])->update(['b_hidden' => '1','d_hidden_date' => date('Y-m-d')]);                
                if ($orderDetails) {
                    return 'TRUE';
                } else {
                     return 'FALSE';
                }
            }
            else if($data['action'] == 'Unhide') {
                $orderDetails = PurchaseOrderDetail::whereIn('id',$data['ids'])->update(['b_hidden' => '0','d_hidden_date'=> null]);                
                if ($orderDetails) {
                    return 'TRUE';
                } else {
                     return 'FALSE';
                }
            }
        }
    }
    // Purchase Order Export
    public function anyExportDataFeed(){

        $data = Input::all();
        $user = Auth::guard('admin')->user();
        $module_name = "1";
        $query = new PurchaseOrderDetail();
        if($user->e_type== 'Employee' || $user->e_type== 'Vendor'){
            $sortColumn = array('v_po_number','d_order_issue_date', 'd_expected_delivery_date','d_vendor_expected_delivery_date','v_vendor_product_name','i_quantity','e_item_status','f_order_total');
        } else {
            $sortColumn = array('', 'v_vendor_name','v_po_number','d_order_issue_date', 'd_expected_delivery_date','d_vendor_expected_delivery_date','v_vendor_product_name','i_quantity','e_item_status','f_order_total');
        }

        $query = $query->join('tbl_purchase_order', function ($join){
                        $join->on('tbl_purchase_order_details.i_purchase_order_id', '=', 'tbl_purchase_order.id')->whereNull('tbl_purchase_order.deleted_at');
                    });
        $query = $query->join('tbl_vendor', function ($join){
                        $join->on('tbl_purchase_order.i_vendor_id', '=', 'tbl_vendor.id')->whereNull('tbl_vendor.deleted_at');
                    });

        if($user->role==2){
            //$query=$query->where("tbl_purchase_order.i_client_id",$user->i_client_id);
            // $vendor_ids = Vendor::where('i_client_id',$user->i_client_id)->pluck('id');
            // $query=$query->whereIn('tbl_purchase_order.i_vendor_id',$vendor_ids);
            $query=$query->where("tbl_purchase_order.i_client_id",$user->i_client_id);
        }else if($user->role==3){
            $vendor_ids = Vendor::where('v_email',$user->v_email)->pluck('id');
            $query=$query->whereIn('tbl_purchase_order.i_vendor_id',$vendor_ids);
        }else if($user->role==4){
            $employee_data=Employee::where('id',$user->i_emp_id)->first();
            $query=$query->where("tbl_purchase_order.i_vendor_id",$employee_data->i_vendor_id);
        }
        if(isset($data['i_vendor_id']) && $data['i_vendor_id'] != '') {
                $query = $query->whereHas('PurchaseOrder.Vendor', function($q) use($data){
                $q->where('tbl_vendor.id', '=',  trim($data['i_vendor_id']));
            });
        }
        if (isset($data['v_po_number']) && $data['v_po_number'] != '') {
            $query = $query->whereHas('PurchaseOrder', function($q) use($data){
                $q->where('tbl_purchase_order.v_po_number', 'LIKE', '%' . trim($data['v_po_number']) . '%');
            });
        }
        if (isset($data['from_order_issue_date']) && $data['from_order_issue_date'] != ""){
                $query = $query->whereHas('PurchaseOrder', function($q) use($data){
                $q->where('tbl_purchase_order.d_order_issue_date','>=',date('Y-m-d',strtotime($data['from_order_issue_date'])));
            });
        }
        if (isset($data['to_order_issue_date']) && $data['to_order_issue_date'] != ""){
               // $data['to_order_issue_date'] = $data['to_order_issue_date']. "23:59:59";
                $query = $query->whereHas('PurchaseOrder', function($q) use($data){
                $q->where('tbl_purchase_order.d_order_issue_date','<=',date('Y-m-d',strtotime($data['to_order_issue_date'])));
            });
        }

        if (isset($data['from_expected_delivery_date']) && $data['from_expected_delivery_date'] != ""){
                $query = $query->whereHas('PurchaseOrder', function($q) use($data){
                $q->where('tbl_purchase_order.d_expected_delivery_date','>=',date('Y-m-d',strtotime($data['from_expected_delivery_date'])));
            });
        }
        if (isset($data['to_expected_delivery_date']) && $data['to_expected_delivery_date'] != ""){
                //$data['to_expected_delivery_date'] = $data['to_expected_delivery_date']. "23:59:59";
                $query = $query->whereHas('PurchaseOrder', function($q) use($data){
                $q->where('tbl_purchase_order.d_expected_delivery_date','<=',date('Y-m-d',strtotime($data['to_expected_delivery_date'])));
            });
        }
        if (isset($data['from_vendor_expected_delivery_date']) && $data['from_vendor_expected_delivery_date'] != ""){
            $query = $query->where('d_vendor_expected_delivery_date','>=',date('Y-m-d',strtotime($data['from_vendor_expected_delivery_date'])));
        }
        if (isset($data['to_vendor_expected_delivery_date']) && $data['to_vendor_expected_delivery_date'] != ""){
            //$data['to_vendor_expected_delivery_date'] = $data['to_vendor_expected_delivery_date']. "23:59:59";
            $query = $query->where('d_vendor_expected_delivery_date','<=',date('Y-m-d',strtotime($data['to_vendor_expected_delivery_date'])));
        }

        if(isset($data['v_vendor_product_name']) && $data['v_vendor_product_name'] != '') {
                $query = $query->where('v_vendor_product_name',  'LIKE', '%' .trim($data['v_vendor_product_name']). '%');
        }
        if (isset($data['i_quantity_from']) && $data['i_quantity_from'] != ""){
            $query = $query->where('i_quantity','>=',trim($data['i_quantity_from']));
        }
        if (isset($data['i_quantity_to']) && $data['i_quantity_to'] != ""){
            $query = $query->where('i_quantity','<=',trim($data['i_quantity_to']));
        }
        if(isset($data['e_item_status']) && $data['e_item_status'] != '') {
            $query = $query->where('e_item_status',  trim($data['e_item_status']));
        }
        if (isset($data['from_order_total']) && $data['from_order_total'] != ""){
            $query = $query->where('f_order_total','>=',trim($data['from_order_total']));
        }
        if (isset($data['to_order_total']) && $data['to_order_total'] != ""){
            $query = $query->where('f_order_total','<=',trim($data['to_order_total']));
        }

        $rec_per_page = REC_PER_PAGE;

        if (isset($data['length'])) {
            if ($data['length'] == '-1') {
                $rec_per_page = '';
            } else {
                $rec_per_page = $data['length'];
            }
        }

        $sort_order = $data['order']['0']['dir'];
        $order_field = $sortColumn[$data['order']['0']['column']];
        if ($sort_order != '' && $order_field != '') {
            if ($order_field == 'v_vendor_name'){
                $query = $query->orderBy('tbl_vendor.v_vendor_name', $sort_order);
            } else if ($order_field == 'v_po_number') {
                $query = $query->orderBy('tbl_purchase_order.v_po_number', $sort_order);
            } else if ($order_field == 'd_order_issue_date') {
                $query = $query->orderBy('tbl_purchase_order.d_order_issue_date', $sort_order);
            } else if ($order_field == 'd_expected_delivery_date') {
                $query = $query->orderBy('tbl_purchase_order.d_expected_delivery_date', $sort_order);
            } else if ($order_field == 'f_order_total') {
                $query = $query->orderBy('tbl_purchase_order.f_order_total', $sort_order);
            } else if ($order_field == 'updated_at') {
                $query = $query->orderBy('tbl_purchase_order.updated_at', $sort_order);
            } else {
                $query = $query->orderBy($order_field, $sort_order);
            }

        } else {
            $query = $query->orderBy('tbl_purchase_order.updated_at', 'desc');
        }
        $data = '';

        $arrRecords = $query->get()->toArray();

        if($user->e_type == 'Vendor' or $user->e_type == 'Employee') {
            $header = "PO Number \t PO Order Date \t Expected Delivery Date \t Item Name  \t Total Cost \t BFD Product Code \t Vendor Product Code  \t Quantity \t Price  \t Sub Cost \t  PO Status \t Vendor's Expected Delivery Date \t Qty On Hand \t Backordered Delivery Date \t Notes";
        } else {
            $header = "Supplier Name \t PO Number \t PO Order Date \t Expected Delivery Date \t Item Name  \t Total Cost \t BFD Product Code \t Vendor Product Code  \t Quantity \t Price \t Sub Cost \t  PO Status \t Vendor's Expected Delivery Date \t Qty On Hand \t Backordered Delivery Date \t Notes";
        }
        foreach ($arrRecords as $key => $val) {

            $SupplierName = $val['v_vendor_name'];
            $PONumber = $val['v_po_number'];
            $POrderDate = ($val['d_order_issue_date'] != '0000-00-00' && $val['d_order_issue_date'] != '') ? date('m/d/Y', strtotime($val['d_order_issue_date'])) : '';
            $ExpectedDeliveryDate = ($val['d_expected_delivery_date'] != '0000-00-00' && $val['d_expected_delivery_date'] != '') ? date('m/d/Y', strtotime($val['d_expected_delivery_date'])) : '';
            $ItemName = $val['v_vendor_product_name'];
            $TotalCost = ($val['f_order_total'] != '') ? '$'.$val['f_order_total'] : '';
            $VendorProductCode = $val['v_client_item_code'];
            $ClientProductCode = $val['v_vendor_item_code'];
            $Quantity = $val['i_quantity'];
            $Price = ($val['f_price'] != '') ? '$'.$val['f_price'] : '';
            $SubTotal = ($val['f_sub_total'] != '') ? '$'.$val['f_sub_total'] : '';
            $ItemStatus = $val['e_item_status'];
            $VendorExpectedDeliveryDate = ($val['d_vendor_expected_delivery_date'] != '0000-00-00' && $val['d_vendor_expected_delivery_date'] != '') ? date('m/d/Y', strtotime($val['d_vendor_expected_delivery_date'])) : '';
            $QtyOnHand = $val['i_qty_on_hand'];
            $BackOrderedDate = ($val['d_back_ordered_date'] != '0000-00-00' && $val['d_back_ordered_date'] != '') ? date('m/d/Y', strtotime($val['d_back_ordered_date'])) : '' ;
            $Notes = trim(preg_replace('/\s\s+/', ' ', $val['v_note']));          

            if($user->e_type == 'Vendor' or $user->e_type == 'Employee') {
                $line = "$PONumber \t $POrderDate \t $ExpectedDeliveryDate \t $ItemName  \t $TotalCost \t $ClientProductCode \t $VendorProductCode \t $Quantity \t $Price \t $SubTotal \t $ItemStatus \t $VendorExpectedDeliveryDate \t $QtyOnHand \t $BackOrderedDate \t $Notes";
            } else {
                $line = "$SupplierName \t $PONumber \t $POrderDate \t $ExpectedDeliveryDate \t $ItemName  \t $TotalCost \t $ClientProductCode \t $VendorProductCode \t $Quantity \t $Price \t $SubTotal \t $ItemStatus \t $VendorExpectedDeliveryDate \t $QtyOnHand \t $BackOrderedDate \t $Notes";
            }
            $line = str_replace("\t \t","\t",$line);
            $line = str_replace('"', '""', stripslashes($line));
            $data .= trim($line)."\n";
        }
        $data = str_replace("\r", "", $data);
        if ($data == "") {
            $data = "\nNo records found.\n";
        }

        $output = $header."\n".$data;
        // Convert to UTF-16LE
        $output = mb_convert_encoding($output, 'UTF-16LE', 'UTF-8');
        // Prepend BOM
        $output = "\xFF\xFE" . $output;

        header('Pragma: public');
        header("Content-type: application/x-msexcel");
        $filename = "Purchase_Orders_list_".$user->e_type."_export_".date("d_M_Y")."_".time().rand(0,999999);
        header('Content-Disposition: attachment;  filename="'.$filename.'.xls"');
        echo $output;
    }

    // Purchase Order Details Export
    public function anyOrderExportToExcel(Request $request,$id){        
        $user = Auth::guard('admin')->user();
        $module_name = "1";
        $data = Request::all();
        $query = new PurchaseOrderDetail();
        $sortColumn = array('v_vendor_item_code','v_client_item_code', 'v_vendor_product_name','i_quantity','f_price','e_item_status','d_vendor_expected_delivery_date','i_qty_on_hand','d_back_ordered_date','v_note','f_sub_total');
        $query = $query->with("PurchaseOrder",'PurchaseOrder.Vendor')->whereHas('PurchaseOrder', function($q) use($id){
            $q->where('tbl_purchase_order.v_po_number',$id);
        });

        $sort_order = $data['order']['0']['dir'];
        $order_field = $sortColumn[$data['order']['0']['column']];            
        if ($sort_order != '' && $order_field != '') {  
            $query = $query->orderBy($order_field, $sort_order);
        }else {
            $query = $query->orderBy('id', 'desc');
        }  

        $arrRecords = $query->get()->toArray();

        if($user->e_type == 'Vendor' or $user->e_type == 'Employee') {
            $header = "PO Number \t PO Order Date \t Expected Delivery Date \t Item Name  \t Total Cost \t BFD Product Code \t Vendor Product Code  \t Quantity \t Price  \t Sub Cost \t  PO Status \t Vendor's Expected Delivery Date \t Qty On Hand \t Backordered Delivery Date \t Notes";
        } else {
            $header = "Supplier Name \t PO Number \t PO Order Date \t Expected Delivery Date \t Item Name  \t Total Cost \t BFD Product Code \t Vendor Product Code  \t Quantity \t Price \t Sub Cost \t  PO Status \t Vendor's Expected Delivery Date \t Qty On Hand \t Backordered Delivery Date \t Notes";
        }

        $data = '';
        foreach ($arrRecords as $key => $val) {

            $SupplierName = $val['purchase_order']['vendor']['v_vendor_name'];
            $PONumber = $val['purchase_order']['v_po_number'];
            $POrderDate = ($val['purchase_order']['d_order_issue_date'] != '0000-00-00' && $val['purchase_order']['d_order_issue_date'] != '') ? date('m/d/Y', strtotime($val['purchase_order']['d_order_issue_date'])) : '';
            $ExpectedDeliveryDate = ($val['purchase_order']['d_expected_delivery_date'] != '0000-00-00' && $val['purchase_order']['d_expected_delivery_date'] != '') ? date('m/d/Y', strtotime($val['purchase_order']['d_expected_delivery_date'])) : '';
            $ItemName = $val['v_vendor_product_name'];
            $TotalCost = ($val['purchase_order']['f_order_total'] != '') ? '$'.$val['purchase_order']['f_order_total'] : '';
            $VendorProductCode = $val['v_client_item_code'];
            $ClientProductCode = $val['v_vendor_item_code'];
            $Quantity = $val['i_quantity'];
            $Price = ($val['f_price'] != '') ? '$'.$val['f_price'] : '';
            $SubTotal = ($val['f_sub_total'] != '') ? '$'.$val['f_sub_total'] : '';
            $ItemStatus = $val['e_item_status'];
            $VendorExpectedDeliveryDate = ($val['d_vendor_expected_delivery_date'] != '0000-00-00' && $val['d_vendor_expected_delivery_date'] != '') ? date('m/d/Y', strtotime($val['d_vendor_expected_delivery_date'])) : '';
            $QtyOnHand = $val['i_qty_on_hand'];
            $BackOrderedDate = ($val['d_back_ordered_date'] != '0000-00-00' && $val['d_back_ordered_date'] != '') ? date('m/d/Y', strtotime($val['d_back_ordered_date'])) : '' ;
            $Notes = trim(preg_replace('/\s\s+/', ' ', $val['v_note']));          

            if($user->e_type == 'Vendor' or $user->e_type == 'Employee') {
                $line = "$PONumber \t $POrderDate \t $ExpectedDeliveryDate \t $ItemName  \t $TotalCost \t $ClientProductCode \t $VendorProductCode \t $Quantity \t $Price \t $SubTotal \t $ItemStatus \t $VendorExpectedDeliveryDate \t $QtyOnHand \t $BackOrderedDate \t $Notes";
            } else {
                $line = "$SupplierName \t $PONumber \t $POrderDate \t $ExpectedDeliveryDate \t $ItemName  \t $TotalCost \t $ClientProductCode \t $VendorProductCode \t $Quantity \t $Price \t $SubTotal \t $ItemStatus \t $VendorExpectedDeliveryDate \t $QtyOnHand \t $BackOrderedDate \t $Notes";
            }
            $line = str_replace("\t \t","\t",$line);
            $line = str_replace('"', '""', stripslashes($line));
            $data .= trim($line)."\n";
        }
        $data = str_replace("\r", "", $data);
        if ($data == "") {
            $data = "\nNo records found.\n";
        }

        $output = $header."\n".$data;
        // Convert to UTF-16LE
        $output = mb_convert_encoding($output, 'UTF-16LE', 'UTF-8');
        // Prepend BOM
        $output = "\xFF\xFE" . $output;

        header('Pragma: public');
        header("Content-type: application/x-msexcel");
        $filename = "purchase_order_".$id."_export_".date("d_M_Y")."_".time().rand(0,999999);
        header('Content-Disposition: attachment;  filename="'.$filename.'.xls"');
        echo $output;
    }

    //Purchase Order Details Listing
    public function anyOrderDetail(Request $request,$id)
    {
        $data = Request::all();
        $user = Auth::guard('admin')->user();
        if ($data) {
            DB::enableQueryLog();
            $query = new PurchaseOrderDetail();
            $sortColumn = array('v_vendor_item_code','v_client_item_code', 'v_vendor_product_name','i_quantity','f_price','e_item_status','d_vendor_expected_delivery_date','i_qty_on_hand','d_back_ordered_date','v_note','f_sub_total');

            $query = $query->with("PurchaseOrder",'PurchaseOrder.Vendor')->whereHas('PurchaseOrder', function($q) use($id){
                $q->where('tbl_purchase_order.v_po_number',$id);
            });
            $sort_order = $data['order']['0']['dir'];
            $order_field = $sortColumn[$data['order']['0']['column']];            
            if ($sort_order != '' && $order_field != '') {  
                $query = $query->orderBy($order_field, $sort_order);
            }else {
                $query = $query->orderBy('id', 'desc');
            }                       
            $arrRecords = $query->get()->toArray();


            $data = array();
            $total_cost = 0;
            $i_cnt = 0;
            foreach ($arrRecords as $key => $val) {
                $index = 0;
                $i_cnt++;
                $data[$key][$index++] = $val['v_vendor_item_code'];
                $data[$key][$index++] = $val['v_client_item_code'];                
                $data[$key][$index++] = $val["v_vendor_product_name"];
                $data[$key][$index++] = $val['i_quantity'];
                $data[$key][$index++] = ($val['f_price'] != '') ? '$'.$val['f_price'] : '';
                $status='<select class="form-control form-filter input-sm purchase_item_status" name="e_item_status-'.$val['id'].'" id="e_item_status-'.$val['id'].'">                        
                        <option value="Pending"'.(($val['e_item_status']=="Pending")?"selected='selected'":"").'>Pending</option>
                        <option value="Confirm and Approved" '.(($val['e_item_status']=="Confirm and Approved")?"selected='selected'":"").'>Confirm and Approved</option>
                        <option value="Back Ordered" '.(($val['e_item_status']=="Back Ordered")?"selected='selected'":"").'>Back Ordered</option>
                        <option value="Discontinued" '.(($val['e_item_status']=="Discontinued")?"selected='selected'":"").'>Discontinued</option>
                        <option value="Partial Inventory" '.(($val['e_item_status']=="Partial Inventory")?"selected='selected'":"").'>Partial Inventory</option>
                        
                    </select>';
                $data[$key][$index++] = $status;
                $vendor_expected_delivery_date='';
                if ($val['d_vendor_expected_delivery_date'] != '0000-00-00' && $val['d_vendor_expected_delivery_date'] != '') {
                    $vendor_expected_delivery_date=date('m/d/Y', strtotime($val['d_vendor_expected_delivery_date']));
                } else {
                    $vendor_expected_delivery_date='';
                }
                if($val['e_item_status'] == 'Confirm and Approved' || $val['e_item_status'] == 'Partial Inventory') {
                    $vendor_expected_delivery_date='<input type="text" name="d_vendor_expected_delivery_date-'.$val['id'].'" id="d_vendor_expected_delivery_date-'.$val['id'].'" class="d_vendor_expected_delivery_date datetimepicker form-control form-filter input-sm" placeholder="Delivery Date" value="'.$vendor_expected_delivery_date.'" />';
                } else {
                    $vendor_expected_delivery_date='<input type="text" name="d_vendor_expected_delivery_date-'.$val['id'].'" id="d_vendor_expected_delivery_date-'.$val['id'].'" class="form-control form-filter input-sm" placeholder="Delivery Date" value="'.$vendor_expected_delivery_date.'" readonly/>';
                }
                $data[$key][$index++] = $vendor_expected_delivery_date;
                if($val['e_item_status'] == 'Partial Inventory') {
                    $data[$key][$index++] = '<input type="text" name="i_qty_on_hand-'.$val['id'].'" id="i_qty_on_hand-'.$val['id'].'" class="form-control form-filter input-sm" placeholder="Qty On Hand" value="'.$val['i_qty_on_hand'].'"/><div class="error-number-inner" id="i_qty_on_hand-'.$val['id'].'number_error" style="display:none">Please enter qty on hand > 0.</div>';
                }else{
                    $data[$key][$index++] = '<input type="text" name="i_qty_on_hand-'.$val['id'].'" id="i_qty_on_hand-'.$val['id'].'" class="form-control form-filter input-sm" placeholder="Qty On Hand" value="" readonly/><div class="error-number-inner" id="i_qty_on_hand-'.$val['id'].'number_error" style="display:none">Please enter qty on hand > 0.</div>';
                }
                

                //$val['i_qty_on_hand'];
                $back_order_date='';
                if ($val['d_back_ordered_date'] != '0000-00-00' && $val['d_back_ordered_date'] != '') {
                    $back_order_date=date('m/d/Y', strtotime($val['d_back_ordered_date']));
                } else {
                    $back_order_date='';
                }
                if($val['e_item_status'] == 'Back Ordered' || $val['e_item_status'] == 'Partial Inventory') {
                    $back_order_date='<input type="text" name="d_back_ordered_date-'.$val['id'].'" id="d_back_ordered_date-'.$val['id'].'" class="d_back_ordered_date datetimepicker form-control form-filter input-sm" placeholder="Back order date" value="'.$back_order_date.'" />';
                } else {
                    $back_order_date='<input type="text" name="d_back_ordered_date-'.$val['id'].'" id="d_back_ordered_date-'.$val['id'].'" class="form-control form-filter input-sm" placeholder="Back order date" value="'.$back_order_date.'" readonly/>';
                }
                $data[$key][$index++] = $back_order_date;
                $cancel_rsn='';

                $cancel_rsn='<textarea id="v_note'.$val['id'].'" name="v_note-'.$val['id'].'" class="form-control v_note" placeholder="Notes">'.$val['v_note'].'</textarea>';

                $data[$key][$index++] = $cancel_rsn;                
                $data[$key][$index++] = ($val['f_sub_total'] != '') ? '$'.$val['f_sub_total'] : '';
                $total_cost +=  $val['f_sub_total'];
                if($user->role == 1){
                    $action = '<div class="actions"><a href="javascript:;" id="delete_record" rel="' . $val['id'] . '" delete-url="' . ADMIN_URL . 'purchase_order/order-details-delete/' . $val['id'] . '" class="btn default btn-xs black delete" title="Delete"><i class="icon-trash"></i></a>';
                    $data[$key][$index++] = $action;
                }
            }
            if($total_cost!='' && $total_cost!=0){
                $newindex = $i_cnt++;
                $data[$newindex][0] = "<span class='total_cost' data-rel-total = 'total_cost'>Total Cost: $".number_format((float)$total_cost,'2')."</span>";
                $data[$newindex][1] = '';
                $data[$newindex][2] = '';
                $data[$newindex][3] = '';
                $data[$newindex][4] = '';
                $data[$newindex][5] = '';
                $data[$newindex][6] = '';
                $data[$newindex][7] = '';
                $data[$newindex][8] = '';
                $data[$newindex][9] = '';
                $data[$newindex][10] = '';                
                $data[$newindex][11] = '';                
            }
            $return_data['data'] = $data;
            return $return_data;
        }
    }

    //Purchase Order Detail Update Status
    public function anyEditStatus($id)
    {
        $inputs=Input::all();                
        $flag=0;
        $order=PurchaseOrder::select('id')->where('v_po_number',$inputs['order_po_number'])->get()->toArray();
        $order_ids=array();
        foreach ($order as $key => $value) {
            $order_ids[]=$value['id'];
        }
        $order_details = PurchaseOrderDetail::whereIn('i_purchase_order_id',$order_ids)->get();
        foreach ($order_details as $key => $value) {
            $order_details_obj = PurchaseOrderDetail::find($value->id);
            $order_details_obj->e_item_status = trim($inputs['e_item_status-'.$value->id]);
            if($inputs['e_item_status-'.$value->id] == 'Back Ordered' && $inputs['d_back_ordered_date-'.$value->id] != '') {
                $order_details_obj->d_back_ordered_date = date('Y-m-d',strtotime(trim($inputs['d_back_ordered_date-'.$value->id])));
                $order_details_obj->v_note = trim($inputs['v_note-'.$value->id]);
                $order_details_obj->i_qty_on_hand = trim($inputs['i_qty_on_hand-'.$value->id]);
                $order_details_obj->d_vendor_expected_delivery_date='';
                $order_details_obj->e_po_status = 'Complete';
            } else if($inputs['e_item_status-'.$value->id] == 'Confirm and Approved' && $inputs['d_vendor_expected_delivery_date-'.$value->id] != '') {
                $order_details_obj->d_vendor_expected_delivery_date = date('Y-m-d',strtotime(trim($inputs['d_vendor_expected_delivery_date-'.$value->id])));
                $order_details_obj->v_note = trim($inputs['v_note-'.$value->id]);
                $order_details_obj->i_qty_on_hand = trim($inputs['i_qty_on_hand-'.$value->id]);
                $order_details_obj->d_back_ordered_date='';
                $order_details_obj->e_po_status = 'Complete';

            }  else if ($inputs['e_item_status-'.$value->id] == 'Discontinued' && $inputs['v_note-'.$value->id] != '') {
                $order_details_obj->v_note = trim($inputs['v_note-'.$value->id]);
                $order_details_obj->i_qty_on_hand = trim($inputs['i_qty_on_hand-'.$value->id]);
                $order_details_obj->d_back_ordered_date='';
                $order_details_obj->d_vendor_expected_delivery_date='';
                $order_details_obj->e_po_status = 'Complete';

            } else if ($inputs['e_item_status-'.$value->id] == 'Partial Inventory' && $inputs['v_note-'.$value->id] != ''){
                $order_details_obj->v_note = trim($inputs['v_note-'.$value->id]);
                $order_details_obj->i_qty_on_hand = trim($inputs['i_qty_on_hand-'.$value->id]);
                $order_details_obj->d_back_ordered_date = date('Y-m-d',strtotime(trim($inputs['d_back_ordered_date-'.$value->id])));
                $order_details_obj->d_vendor_expected_delivery_date = date('Y-m-d',strtotime(trim($inputs['d_vendor_expected_delivery_date-'.$value->id])));
                $order_details_obj->e_po_status = 'Complete';

            } else if ($inputs['e_item_status-'.$value->id] == 'Partial Inventory' && $inputs['v_note-'.$value->id] == ''){
                $order_details_obj->v_note = trim($inputs['v_note-'.$value->id]);
                $order_details_obj->i_qty_on_hand = trim($inputs['i_qty_on_hand-'.$value->id]);
                $order_details_obj->d_back_ordered_date = date('Y-m-d',strtotime(trim($inputs['d_back_ordered_date-'.$value->id])));
                $order_details_obj->d_vendor_expected_delivery_date = date('Y-m-d',strtotime(trim($inputs['d_vendor_expected_delivery_date-'.$value->id])));
                $order_details_obj->e_po_status = 'Incomplete';
            }else {                
                $order_details_obj->d_back_ordered_date='';
                $order_details_obj->i_qty_on_hand = trim($inputs['i_qty_on_hand-'.$value->id]);
                $order_details_obj->v_note = trim($inputs['v_note-'.$value->id]);
                $order_details_obj->d_vendor_expected_delivery_date='';
                $order_details_obj->e_po_status = 'Incomplete';
            }            
            if($order_details_obj->save()) {
                $flag=1;
            } else {
                $flag=0;
                exit;
            }

        }
        if($flag==1)
        {
            Session::flash('success-message', 'Purchase Order has been updated successfully.');
            return "TRUE";
        }else
        {
            return "FALSE";
        }
    }


    public function anyImportToExcelPurchaseOrder()
    {
        $auth_user = Auth::guard('admin')->user();
        $data = Input::all();          
        if(!empty($data)){
            if(isset($data['id']) && $data['id'] != ''){
                $client_id = $data['id'];
            } else {
                $client_id = $auth_user->i_client_id;
            }
        } else {
            $client_id = $auth_user->i_client_id;
        }        
        $folder_name = Client::find($client_id);       
        $folder_name = $folder_name->v_folder_name;                 
        $dir = PURCHASE_FEED_PATH.$folder_name;
        $destination =  PURCHASE_FEED_BACKUP_PATH;
        $source =  PURCHASE_FEED_PATH;
        $cFileArr_Err = array();
        $cFileArr = array();
        if(is_dir($dir)){
            $files =  glob($dir . "/*.txt");
            foreach($files as $file) {
                rename($file, str_replace(".txt","_".time().".csv",$file));	 //convert text file to csv
            }
            $files = glob($dir . "/*.csv");
            $file_num = 1;                                       
            if (!empty($files)) {
                foreach($files as $kfile => $file) {                  
                    $pathtofile = $file;
                    $file_name_copy = str_replace($source.$folder_name,'',$file);                                        
                    $extension = pathinfo($pathtofile,PATHINFO_EXTENSION);
                    if($extension == 'csv') {
                        $total_count = 0;                        
                        try{
                            // $objPHPExcel = PHPExcel_IOFactory::load($file);
                            $inputFileType=PHPExcel_IOFactory::identify($file);
                            $objReader=PHPExcel_IOFactory::createReader('CSV'); 
                            $objPHPExcel=$objReader->load($file);
                        }catch(\Exception $e) {
                            $filename_error	= str_replace(PURCHASE_FEED_PATH,"",$file);
                            $cFileArr_Err[$kfile]['filename']	= $filename_error;
                            continue;
                        }
                        $no_of_colums = "13";
                        $no_of_colums_t = "14";
                        $columns_empty = $objPHPExcel->getActiveSheet(0)->toArray()[0];
                        $columns = array_filter($columns_empty);                        
                        if($no_of_colums == count($columns) || $no_of_colums_t == count($columns)) {
                            foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
                                if($key == 0) {
                                    $datafeed_data = $worksheet->toArray();
                                    $total_count = count($datafeed_data);
                                }
                            }
                            $i = 1;
                            //pr($datafeed_data);exit;
                            foreach($datafeed_data as $k => $val){
                                $data_val = array_filter($val);
                                if ($k != 0 && count($data_val) > 0 && ((isset($val[12]) && $val[12] != 'DNI') || !isset($val[12])) ) {
                                    $data_feed = new PurchaseDataFeed;      
                                    $data_feed->i_client_id = $client_id;
                                    $data_feed->v_po_number = (isset($val[0])) ? $val[0] : '';
                                    $data_feed->i_vendor_id = (isset($val[1])) ? $val[1] : '';
                                    $data_feed->v_supplier_name = (isset($val[2])) ? $val[2] : '';
                                    if(isset($val[3])){
                                        $val[3] = str_replace('-', '/', $val[3]);    
                                    }else if (isset($val[4])) {
                                        $val[4] = str_replace('-', '/', $val[4]);    
                                    }

                                    $val[3] = str_replace('-', '/', $val[3]);
                                    $val[4] = str_replace('-', '/', $val[4]);
                                    $data_feed->d_order_issue_date = (isset($val[3])) ? ((strpos($val[3], '/') !== false)
                                    ? date('Y-m-d', strtotime($val[3])) : PHPExcel_Style_NumberFormat::toFormattedString($val[3],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2)) : NULL;
                                    $data_feed->d_expected_delivery_date = (isset($val[4])) ? ((strpos($val[4], '/') !== false)
                                    ? date('Y-m-d', strtotime($val[4])) : PHPExcel_Style_NumberFormat::toFormattedString($val[4],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2)) : NULL;
                                    // $data_feed->d_order_issue_date = (isset($val[2])) ? $val[2] : '';
                                    // $data_feed->d_expected_delivery_date = (isset($val[3])) ? $val[3] : '';
                                    $data_feed->v_client_item_code = (isset($val[5])) ? $val[5] : '';
                                    $data_feed->v_vendor_item_code = (isset($val[6])) ? $val[6] : '';
                                    $data_feed->v_vendor_product_name = (isset($val[7])) ? $val[7] : '';
                                    $data_feed->i_quantity = (isset($val[8])) ? $val[8] : '';
                                    $data_feed->f_price = (isset($val[9])) ? $val[9] : '';
                                    $data_feed->f_sub_total = (isset($val[10])) ? $val[10] : '';
                                    $data_feed->f_order_total = (isset($val[11])) ? $val[11] : '';
                                    $data_feed->v_note = (isset($val[12])) ? $val[12] : '';
                                    $data_feed->i_file_number = $k + 1;
                                    $data_feed->created_at = date("Y-m-d h:i:s", time() + $i);
                                    $data_feed->updated_at = date("Y-m-d h:i:s", time() + $i);
                                    $data_feed->save();
                                    $i++;
                                }
                                $filename = str_replace(PURCHASE_FEED_PATH,"",$file);
                                $cFileArr[$kfile]['filename']	= $filename;
                                $cFileArr[$kfile]['line_count'] = $total_count-1;
                                $cFileArr[$kfile]['line_get'] = $i - 1;
                                //$cFileArr[$f]['line_get'] = 0;
                            }
                        } else {
                            $filename_error	= str_replace(PURCHASE_FEED_PATH,"",$file);
                            $cFileArr_Err[$kfile]['filename']	= $filename_error;
                        }
                    }  
                    copy($file, $destination.$file_name_copy);                                   
                    @unlink(WWW_ROOT.$file);
                    $file_num ++;

                }                
                $poRecords = PurchaseDataFeed::where('i_client_id',$client_id)->select(DB::raw('DISTINCT(v_po_number)'))->get();
                $arrPoRecords = $poRecords->toArray();                              
                foreach($arrPoRecords as $poRecord) {
                    $flag = 1;
                    $subRecords = PurchaseDataFeed::where('v_po_number', $poRecord['v_po_number'])->get();
                    $arrSubRecords = $subRecords->toArray();

                    foreach($arrSubRecords as $index => $subRecord) {                        
                        if(trim($subRecord['v_supplier_name']) != '' && trim($subRecord['v_po_number']) != '') {
                            $clientId = $client_id;
                            if(isset($subRecord['i_vendor_id']) && $subRecord['i_vendor_id'] != '' && $subRecord['i_vendor_id'] > 0){
                                //$vendorId = $subRecord['i_vendor_id'];
                                $vendorId = $this->getVendorIdBySupplierCode($subRecord['i_vendor_id']);    
                            } 
                            if($vendorId == '' || empty($vendorId)) {
                                $vendorId = $this->getVendorRelationId($subRecord['v_supplier_name'],$subRecord['i_vendor_id']);
                            }
                            if(trim($clientId) != '' && trim($vendorId) != '') {
                                /* $duplicateRecord = PurchaseOrder::where(['v_po_number' => $subRecord['v_po_number'], 'i_vendor_id' => $vendorId])->first();
                                if(empty($duplicateRecord)) { */
                                
                                    if($flag == 1) {
                                        $sub_total = PurchaseDataFeed::where('v_po_number', $poRecord['v_po_number'])->sum('f_sub_total');
                        
                                        $order =  new PurchaseOrder;
                                        $order->i_client_id = (isset($clientId)) ? $clientId : '';
                                        $order->i_vendor_id = (isset($vendorId)) ? $vendorId : '';
                                        $order->v_po_number = $subRecord['v_po_number'];
                                        $order->d_order_issue_date = ($subRecord['d_order_issue_date'] != '') ? $subRecord['d_order_issue_date'] : NULL;
                                        $order->d_expected_delivery_date = ($subRecord['d_expected_delivery_date'] != '') ? $subRecord['d_expected_delivery_date'] : NULL;
                                        $order->f_order_total = $sub_total;//$subRecord['f_order_total'];
                                        $order->save();
                                        $purchaseOrderId = $order->id;
                                        $flag = 0;
                                    }
                                    if(trim($purchaseOrderId) != '') {
                                        $order_detail =  new PurchaseOrderDetail;
                                        $order_detail->i_purchase_order_id = (isset($purchaseOrderId)) ? $purchaseOrderId : '';
                                        $order_detail->v_client_item_code = $subRecord['v_client_item_code'];
                                        $order_detail->v_vendor_item_code = $subRecord['v_vendor_item_code'];
                                        $order_detail->v_vendor_product_name = $subRecord['v_vendor_product_name'];
                                        $order_detail->i_quantity = ($subRecord['i_quantity'] != '') ? $subRecord['i_quantity'] : '';
                                        $order_detail->f_price = $subRecord['f_price'];
                                        $order_detail->f_sub_total = $subRecord['f_sub_total'];
                                        $order_detail->v_note = $subRecord['v_note'];
                                        $order_detail->save();

                                    }
                               // }
                            }
                        }
                    }
                    PurchaseDataFeed::where('i_client_id',$client_id)->where('v_po_number', $poRecord['v_po_number'])->delete();
                }                                
                $message = '';

                if(isset($cFileArr) && !empty($cFileArr)){
                    foreach($cFileArr as $file){
                        $message.= "Total <strong>".$file['line_get']."</strong> records are imported <strong>out of ".$file['line_count']."</strong> from file name '".$file['filename']."'.<br />";
                    }
                }
                $message_err = '';
                if(isset($cFileArr_Err) && !empty($cFileArr_Err)){
                    foreach($cFileArr_Err as $file_err){
                        $message_err.= "File name '".$file_err['filename']."' is not imported. Please check the format of file.<br />";
                    }
                }
                if($message_err != '') {
                    Session::flash('alert-message', $message_err);
                }
                if($message != '') {
                    Session::flash('success-message', $message);
                }

                return 'TRUE';
            } else {
                Session::flash('alert-message', 'There is no file to import. Please try again later.');
                return 'TRUE';
            }
        } else {
            Session::flash('alert-message', ' Folder '.$folder_name.' does not exists.');
            return 'TRUE';
        }
    }
    public function getVendorRelationId($SupplierName, $vendorId)
    {
        $query = Vendor::select('id')->where('v_company_name', $SupplierName)->orWhere('v_vendor_id', $vendorId)->first();
        if(isset($query) && !empty($query)){
            return $query['id'];
        } else {
            return '';
        }

    }
    public function getVendorIdBySupplierCode($supplier_code)
    {
        $qry = Vendor::select('id')->where('v_vendor_id',$supplier_code)->get()->toArray();    
        if(isset($qry) && !empty($qry)){
            return $qry[0]['id'];
        } else {
            return '';
        }
        
    }
    public function anyCronForImportFeed() {
        $day = date('D');
        $time = date('h:i A');
        if((($time ==  '06:30 AM' || $time == '03:40 PM')) || ($day == 'Fri' && $time == '12:30 PM' )) {
            $path =  PURCHASE_FEED_PATH;
            $dirs = array();
            $dir = dir($path);
            while (false !== ($entry = $dir->read())) {
                if ($entry != '.' && $entry != '..') {
                    if (is_dir($path . '/' .$entry)) {
                        $dirs[] = $entry;
                    }
                }
            }

            $client_data = Client::where('e_status','Active')->pluck('id','v_folder_name')->toArray();
            $source =  PURCHASE_FEED_PATH;
            $destination =  PURCHASE_FEED_BACKUP_PATH;
            $message_err = '';
            $message = '';
            $no_data_folder = '';
            $no_folder = '';
            $folder_arr = array();

            foreach($dirs as $key => $folder_name) {
                if(array_key_exists($folder_name, $client_data)) {
                    array_push($folder_arr,$folder_name);
                }
            }
            foreach($folder_arr as $key => $folder_name) {
                $cFileArr_Err = array();
                $cFileArr = array();
                if(array_key_exists($folder_name, $client_data)){
                    $client_id = $client_data[$folder_name];
                }
                $dir = PURCHASE_FEED_PATH.$folder_name;

                if(is_dir($dir)){
                    $files =  glob($dir . "/*.txt");
                    foreach($files as $file) {
                        rename($file, str_replace(".txt","_".time().".csv",$file));	 //convert text file to csv
                    }
                    $files = glob($dir . "/*.csv");
                    $file_num = 1;                    
                    if (!empty($files)) {
                        $total_files = 0;
                        foreach($files as $kfile => $file) {
                            $pathtofile = $file;
                            $file_name_copy = str_replace($source.$folder_name,'',$file);                                        
                            $extension = pathinfo($pathtofile,PATHINFO_EXTENSION);
                            if($extension == 'csv') {
                                $total_count = 0;                                
                                try{
                                    // $objPHPExcel = PHPExcel_IOFactory::load($file);
                                    $inputFileType=PHPExcel_IOFactory::identify($file);
                                    $objReader=PHPExcel_IOFactory::createReader('CSV'); 
                                    $objPHPExcel=$objReader->load($file);
                                }catch(\Exception $e) {
                                    $filename_error	= str_replace(PURCHASE_FEED_PATH,"",$file);
                                    $cFileArr_Err[$kfile]['filename']	= $filename_error;
                                    continue;
                                }
                                $no_of_colums = "13";                                
                        		$no_of_colums_t = "14";
                                $columns_empty = $objPHPExcel->getActiveSheet(0)->toArray()[0];
                                $columns = array_filter($columns_empty);
                                if($no_of_colums == count($columns) || $no_of_colums_t == count($columns)) {
                                    foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
                                        if($key == 0) {
                                            $datafeed_data = $worksheet->toArray();
                                            $total_count = count($datafeed_data);
                                        }
                                    }
                                    $i = 1;

                                    foreach($datafeed_data as $k => $val){
                                        $data_val = array_filter($val);
                                        if ($k != 0 && count($data_val) > 0 && ((isset($val[12]) && $val[12] != 'DNI') || !isset($val[12])) ) {
                                            $data_feed = new PurchaseDataFeed;

                                            $data_feed->i_client_id = $client_id;
                                            $data_feed->v_po_number = (isset($val[0])) ? $val[0] : '';
                                            $data_feed->i_vendor_id = (isset($val[1])) ? $val[1] : '';
                                            $data_feed->v_supplier_name = (isset($val[2])) ? $val[2] : '';
                                            $data_feed->d_order_issue_date = (isset($val[3])) ? ((strpos($val[3], '/') !== false)
                                    ? date('Y-m-d', strtotime($val[3])) : PHPExcel_Style_NumberFormat::toFormattedString($val[3],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2)) : NULL;
                                    $data_feed->d_expected_delivery_date = (isset($val[4])) ? ((strpos($val[4], '/') !== false)
                                    ? date('Y-m-d', strtotime($val[4])) : PHPExcel_Style_NumberFormat::toFormattedString($val[4],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2)) : NULL;
                                            $data_feed->v_client_item_code = (isset($val[5])) ? $val[5] : '';
                                            $data_feed->v_vendor_item_code = (isset($val[6])) ? $val[6] : '';
                                            $data_feed->v_vendor_product_name = (isset($val[7])) ? $val[7] : '';
                                            $data_feed->i_quantity = (isset($val[8])) ? $val[8] : '';
                                            $data_feed->f_price = (isset($val[9])) ? $val[9] : '';
                                            $data_feed->f_sub_total = (isset($val[10])) ? $val[10] : '';
                                            $data_feed->f_order_total = (isset($val[11])) ? $val[11] : '';
                                            $data_feed->v_note = (isset($val[12])) ? $val[12] : '';
                                            $data_feed->i_file_number = $k + 1;
                                            $data_feed->created_at = date("Y-m-d h:i:s", time() + $i);
                                            $data_feed->updated_at = date("Y-m-d h:i:s", time() + $i);
                                            $data_feed->save();
                                            $i++;
                                        }
                                        $filename = str_replace(PURCHASE_FEED_PATH,"",$file);
                                        $cFileArr[$kfile]['filename']	= $filename;
                                        $cFileArr[$kfile]['line_count'] = $total_count-1;
                                        $cFileArr[$kfile]['line_get'] = $i - 1;
                                        //$cFileArr[$f]['line_get'] = 0 ;
                                    }
                                } else {
                                    $filename_error	= str_replace(PURCHASE_FEED_PATH,"",$file);
                                    $cFileArr_Err[$kfile]['filename']	= $filename_error;
                                }
                            }
                            copy($file, $destination.$file_name_copy);                 
                           @unlink(WWW_ROOT.$file);
                            $file_num ++;
                            $total_files++;

                        }
                        $poRecords = PurchaseDataFeed::select(DB::raw('DISTINCT(v_po_number)'))->get();
                        $arrPoRecords = $poRecords->toArray();
                        foreach($arrPoRecords as $poRecord) {
                            $flag = 1;
                            $subRecords = PurchaseDataFeed::where('v_po_number', $poRecord['v_po_number'])->get();
                            $arrSubRecords = $subRecords->toArray();

                            foreach($arrSubRecords as $index => $subRecord) {
                                if(trim($subRecord['v_supplier_name']) != '' && trim($subRecord['v_po_number']) != '') {
                                    $clientId = $client_id;
                                    if(isset($subRecord['i_vendor_id']) && $subRecord['i_vendor_id'] != '' && $subRecord['i_vendor_id'] > 0) {
                                        //$vendorId = $subRecord['i_vendor_id'];
                                        $vendorId = $this->getVendorIdBySupplierCode($subRecord['i_vendor_id']);            
                                    } 
                                    if($vendorId == '' || empty($vendorId)) {
                                        $vendorId = $this->getVendorRelationId($subRecord['v_supplier_name'], $subRecord['i_vendor_id']);
                                    }
                                    if(trim($vendorId) != '') {
                                        /* $duplicateRecord = PurchaseOrder::where(['v_po_number' => $subRecord['v_po_number'], 'i_vendor_id' => $vendorId])->first();
                                        if(empty($duplicateRecord)) { */
                                            if($flag == 1) {
                                                $sub_total = PurchaseDataFeed::where('v_po_number', $poRecord['v_po_number'])->sum('f_sub_total');
                        
                                                $order =  new PurchaseOrder;
                                                $order->i_client_id = (isset($clientId)) ? $clientId : '';
                                                $order->i_vendor_id = (isset($vendorId)) ? $vendorId : '';
                                                $order->v_po_number = $subRecord['v_po_number'];
                                                $order->d_order_issue_date = ($subRecord['d_order_issue_date'] != '') ? $subRecord['d_order_issue_date'] : NULL;
                                                $order->d_expected_delivery_date = ($subRecord['d_expected_delivery_date'] != '') ? $subRecord['d_expected_delivery_date'] : NULL;
                                                $order->f_order_total = $sub_total;//$subRecord['f_order_total'];
                                                $order->save();
                                                $purchaseOrderId = $order->id;
                                                $flag = 0;
                                            }
                                            if(trim($purchaseOrderId) != '') {
                                                $order_detail =  new PurchaseOrderDetail;
                                                $order_detail->i_purchase_order_id = (isset($purchaseOrderId)) ? $purchaseOrderId : '';
                                                $order_detail->v_client_item_code = $subRecord['v_client_item_code'];
                                                $order_detail->v_vendor_item_code = $subRecord['v_vendor_item_code'];
                                                $order_detail->v_vendor_product_name = $subRecord['v_vendor_product_name'];
                                                $order_detail->i_quantity = ($subRecord['i_quantity'] != '') ? $subRecord['i_quantity'] : '';
                                                $order_detail->f_price = $subRecord['f_price'];
                                                $order_detail->f_sub_total = $subRecord['f_sub_total'];
                                                $order_detail->v_note = $subRecord['v_note'];
                                                $order_detail->save();

                                            }
                                    // }
                                    }
                                }
                            }
                        }

                        PurchaseDataFeed::where('i_client_id',$client_id)->delete();
                        // PurchaseDataFeed::truncate();

                        if(isset($cFileArr) && !empty($cFileArr)){
                            foreach($cFileArr as $file){
                                $message.= "Total <strong>".$file['line_get']."</strong> records are imported <strong>out of ".$file['line_count']."</strong> from file name '".$file['filename']."'.<br />";
                            }
                        }
                        if(isset($cFileArr_Err) && !empty($cFileArr_Err)){
                            foreach($cFileArr_Err as $file_err){
                                $message_err.= "File name '".$file_err['filename']."' is not imported. Please check the format of file.<br />";
                            }
                        }
                    } else {
                        $no_data_folder.= 'There is no file to import from purchase order feed. Please try again later. <br/>';
                    }
                } else {
                    $no_folder.= ' Folder purchase order feed does not exists. <br/>';
                }

            }
            $mail_msg = '';
            if(isset($message) && !empty($message)){
                $mail_msg.= '<div class="success-msg"><span class="success-span">'.$message.' </span></div>';
            }
            if(isset($message_err) && !empty($message_err)){
                $mail_msg.= '<div class="alert-msg"><span class="alert-span">'.$message_err.' </span></div>';
            }
            if(isset($no_data_folder) && !empty($no_data_folder)){
                $mail_msg.= '<div class="alert-msg"><span class="alert-span">'.$no_data_folder.' </span></div>';
            }
            if(isset($no_folder) && !empty($no_folder)){
                $mail_msg.= '<div class="alert-msg"><span class="alert-span">'.$no_folder.' </span></div>';
            }
            //$to = 'edward.shane@icloud.com';
             $to ='webportal@blackforestdecor.com';
            $subject = 'Dropship - Cron job import purchase feed';
            Mail::send('admin.emails.auth.dropship-import-datafeed', array('strTemplate' => $mail_msg), function($msg) use ($subject, $to){
                            $msg->to($to);
                            $msg->subject($subject);
            }); 
            $to = 'testing.demo@gmail.com';
            $subject = 'Dropship - Cron job import purchase feed';
            //$admin_email = ADMIN_EMAIL;
            $admin_email = 'testing.kancy@gmail.com';
            Mail::send('admin.emails.auth.dropship-import-datafeed', array('strTemplate' => $mail_msg), function($msg) use ($subject, $to){
                            $msg->to($to);
                            $msg->subject($subject);
            });
        }
    }
     public function anyEditOrder($id)
    {
        if (Input::all()) {
            $inputs = Input::all();                          
            $cnt = count($inputs['purchase_order_detail_id']);            
            $purchase_order = PurchaseOrder::find($id);             
            $purchase_order->i_vendor_id = trim($inputs['i_vendor_id']);
            if(trim($inputs['d_order_issue_date']) != '' && trim($inputs['d_order_issue_date']) != '0000-00-00'){ 
                $d_order_issue_date = date('Y-m-d', strtotime(trim($inputs['d_order_issue_date']))); 
            } else { 
                $back_order_Date =''; 
            }
            if(trim($inputs['d_expected_delivery_date']) != '' && trim($inputs['d_expected_delivery_date']) != '0000-00-00'){ 
                $d_expected_delivery_date = date('Y-m-d', strtotime(trim($inputs['d_expected_delivery_date']))); 
            } else { 
                $d_expected_delivery_date =''; 
            }            
            $purchase_order->d_order_issue_date = $d_order_issue_date;
            $purchase_order->d_expected_delivery_date = $d_expected_delivery_date;
            $purchase_order->updated_at=date('Y-m-d h:i:s');     
            $purchase_total_amt = $purchase_order->f_order_total;
            $purchase_sub_total = 0;
            for($i=0;$i<$cnt;$i++){
                $sub_total = trim($inputs['i_quantity'][$i]) * trim($inputs['f_price'][$i]);
                if($inputs['purchase_order_detail_id'][$i]!=''){
                    $purchase_order_details_obj = PurchaseOrderDetail::find($inputs['purchase_order_detail_id'][$i]);
                }else{
                    $purchase_order_details_obj = new PurchaseOrderDetail();
                    $purchase_order_details_obj->i_purchase_order_id = $id;
                    $purchase_sub_total = $purchase_sub_total + $sub_total;
                }
                $purchase_order_details_obj->v_vendor_item_code = trim($inputs['v_client_item_code'][$i]);
                $purchase_order_details_obj->v_client_item_code = trim($inputs['v_vendor_item_code'][$i]);
                $purchase_order_details_obj->v_vendor_product_name = trim($inputs['v_vendor_product_name'][$i]);
                $purchase_order_details_obj->i_quantity = trim($inputs['i_quantity'][$i]);
                $purchase_order_details_obj->f_price = trim($inputs['f_price'][$i]);
                $purchase_order_details_obj->f_sub_total = trim($inputs['f_sub_total'][$i]);
                $purchase_order_details_obj->i_qty_on_hand = trim($inputs['i_qty_on_hand'][$i]);
                $purchase_order_details_obj->v_note = trim($inputs['v_note'][$i]);
                $purchase_order_details_obj->e_item_status = trim($inputs['e_item_status'][$i]);                
                $purchase_order_details_obj->f_sub_total = $sub_total;                
                if(trim($inputs['e_item_status'][$i])=='Confirm and Approved'){
                    if(trim($inputs['d_vendor_expected_delivery_date'][$i]) != '' && trim($inputs['d_vendor_expected_delivery_date'][$i]) != '0000-00-00'){ 
                        $d_vendor_expected_delivery_date = date('Y-m-d', strtotime(trim($inputs['d_vendor_expected_delivery_date'][$i]))); 
                    } else { 
                        $d_vendor_expected_delivery_date =''; 
                    }
                    $purchase_order_details_obj->d_vendor_expected_delivery_date = $d_vendor_expected_delivery_date;
                    $purchase_order_details_obj->d_back_ordered_date = '';
                }else if(trim($inputs['e_item_status'][$i])=='Back Ordered'){
                    if(trim($inputs['d_back_ordered_date'][$i]) != '' && trim($inputs['d_back_ordered_date'][$i]) != '0000-00-00'){ 
                        $d_back_ordered_date = date('Y-m-d', strtotime(trim($inputs['d_back_ordered_date'][$i]))); 
                    } else { 
                        $d_back_ordered_date =''; 
                    }
                    $purchase_order_details_obj->d_vendor_expected_delivery_date = '';
                    $purchase_order_details_obj->d_back_ordered_date = $d_back_ordered_date;
                }else if(trim($inputs['e_item_status'][$i])=='Partial Inventory'){
                    if(trim($inputs['d_back_ordered_date'][$i]) != '' && trim($inputs['d_back_ordered_date'][$i]) != '0000-00-00'){ 
                        $d_back_ordered_date = date('Y-m-d', strtotime(trim($inputs['d_back_ordered_date'][$i]))); 
                    } else { 
                        $d_back_ordered_date =''; 
                    }
                    if(trim($inputs['d_vendor_expected_delivery_date'][$i]) != '' && trim($inputs['d_vendor_expected_delivery_date'][$i]) != '0000-00-00'){ 
                        $d_vendor_expected_delivery_date = date('Y-m-d', strtotime(trim($inputs['d_vendor_expected_delivery_date'][$i]))); 
                    } else { 
                        $d_vendor_expected_delivery_date =''; 
                    }
                    $purchase_order_details_obj->d_vendor_expected_delivery_date = $d_vendor_expected_delivery_date;
                    $purchase_order_details_obj->d_back_ordered_date = $d_back_ordered_date;                    
                }else{
                    $purchase_order_details_obj->d_vendor_expected_delivery_date = '';
                    $purchase_order_details_obj->d_back_ordered_date = '';
                }
                $purchase_order_details_obj->save();
            }   
            $purchase_total_amt = $purchase_total_amt + $purchase_sub_total;
            $purchase_order->f_order_total = $purchase_total_amt;
            if ($purchase_order->save()) {                
                    Session::flash('success-message', 'Purchase order has been updated successfully.');
                    return '';    
            }

        } else {
            $user = Auth::guard('admin')->user();                        
            $records[] = array();
            if($user->role==2){
                //$records=$records->where("tbl_purchase_order.i_client_id",$user->i_client_id);
                $records =  PurchaseOrderDetail::with("PurchaseOrder","PurchaseOrder.Vendor")->whereHas('PurchaseOrder', function($q) use($id,$user){
                    $q->where('tbl_purchase_order.v_po_number',$id)->where("tbl_purchase_order.i_client_id",$user->i_client_id);
                })->orderBy('id', 'desc');  
                $records = $records->get();              
            }else if($user->role==3){                                
                $vendor_list = Vendor::select('id')->where('v_email',$user->v_email)->get();
                $vendor_ids = array();
                if(count($vendor_list)>0){
                    $vendor_list = $vendor_list->toArray();
                    foreach ($vendor_list as $key => $value) {
                        $vendor_ids[] = $value['id'];
                    }                    
                }else{
                    $vendor_list = array();
                }  
                 $records =  PurchaseOrderDetail::with("PurchaseOrder","PurchaseOrder.Vendor")->whereHas('PurchaseOrder', function($q) use($id,$vendor_ids){
                    $q->where('tbl_purchase_order.v_po_number',$id)->whereIn('tbl_purchase_order.i_vendor_id',$vendor_ids);
                })->orderBy('id', 'desc'); 
                $records = $records->get();                                               
            } else if($user->role==4){
                $employee_data=Employee::where('id',$user->i_emp_id)->first();

                // $records =  PurchaseOrderDetail::with("PurchaseOrder","PurchaseOrder.Vendor")->whereHas('PurchaseOrder', function($q) use($id,$employee_data){
                //     $q->where('tbl_purchase_order.v_po_number',$id)->where("tbl_purchase_order.i_vendor_id",$employee_data->i_vendor_id);
                // })->orderBy('id', 'desc');                                
                // $records = $records->get();
            }else{
                 $records =  PurchaseOrderDetail::with("PurchaseOrder","PurchaseOrder.Vendor")->whereHas('PurchaseOrder', function($q) use($id,$user){
                    $q->where('tbl_purchase_order.v_po_number',$id);
                })->orderBy('id', 'desc');
                $records = $records->get();
            }                       
            
            $vendor_list = Vendor::select('id','v_company_name')->where('e_status','Active')->get();                   if(count($records)>0 && !empty($records)) {
                return View('admin.purchase_order.edit_order', array('records' => $records,'vendor_list'=>$vendor_list, 'title' => 'Edit Purchase Order'));
            }else{
                return Redirect(ADMIN_URL . 'purchase_order');        
            }
            
        }
        return Redirect(ADMIN_URL . 'purchase_order');
    }
    public function anyCronMailing(){
        $vendor = Vendor::where('e_status','Active')->select('id','v_email','v_vendor_name','po_email')->get();
        if(count($vendor)>0){
            $ordArrListNew = '';  
            $emailCounter = 0;
            $emailIds = 'EMAIL ID List <br>';
            foreach ($vendor as $key => $value) {
                $vendor_id = $value->id;
                $vendor_name = $value->v_vendor_name;
                if(!empty($value->po_email)) {                    
                    $vendor_mail = explode(',',$value->po_email); 
                    $OrderRec = PurchaseOrder::where('i_vendor_id',$vendor_id)->where('t_is_mail_sent','0')->select('id','v_po_number','i_client_id')->get();
                    if(count($OrderRec)>0){                   
                        $OrderIdList = '';          
                        $ordArr = array();
                        $ordArrFolder = array();
                        foreach ($OrderRec as $order_key => $order_value) {
                            $client_folder_name = Client::where('id',$order_value->i_client_id)->select('v_company')->first();
                            if(count($client_folder_name) > 0) {
                                $ordArr[$order_value->v_po_number] = $order_value->v_po_number;
                                $ordArrFolder = $client_folder_name->v_company;
                                $OrderUpdate = PurchaseOrder::find($order_value->id);
                                $OrderUpdate->t_is_mail_sent = '1';
                                $OrderUpdate->save();
                            }
                        }                    
                        if(count($ordArr)>0){
                            foreach($ordArr as $ordId) {                        
                                $OrderIdList.=  "<br />&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;".$ordId."\r\n";  
                            }                        
                            $OrderIdList = trim($OrderIdList);
                        }                    
                        $ordArrListNew.= $OrderIdList;
                        if($OrderIdList)
                        {
                            $objEmailTemplate = MailTemplate::find(11)->toArray();
                            $strTemplate = $objEmailTemplate['t_email_content'];
                            $strTemplate = str_replace('[PROJECT_NAME]',PROJECT_NAME,$strTemplate);
                            $strTemplate = str_replace('[USER_NAME]',trim($vendor_name),$strTemplate);
                            $strTemplate = str_replace('[ORDER_IDS]',trim($OrderIdList),$strTemplate);
                            $strTemplate = str_replace('[FOLDER_NAME]',trim($ordArrFolder),$strTemplate);
                            $strTemplate = str_replace('[SITE_NAME]',SITE_NAME,$strTemplate);
                            $strTemplate = str_replace('[SITE_URL]',SITE_URL,$strTemplate);
                            $subject='New Purchase orders at '.PROJECT_NAME;
                            // mail sent to user with new link
                            $send_mailid='';
                            if(TESTING_MAILID!=''){
                                $send_mailid = TESTING_MAILID;
                            }else{
                                $send_mailid = $vendor_mail;
                            }                         
                            // $send_mailid = ['testing.demo@gmail.com','testing.kancy@gmail.com'];                      
                            /* Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$strTemplate), function($message) use ($subject,$send_mailid){ 
                                $message->to($send_mailid);   
                                $message->subject($subject);                                
                            }); */
                            foreach($vendor_mail as $mail_val) {
                                $emailIds.= $mail_val."<br>";
                            }
                            $emailCounter++;
                        }
                        
                    }
                }
            }
            $to = 'testing.demo@gmail.com';
            $subject = 'Dropship - Purchase Cron job mailing';
            //$admin_email = ADMIN_EMAIL;
            $admin_email = 'testing.kancy@gmail.com';
            $message = "cron Hello! This is a simple email message from drop ship.".date('Y-m-d H:i:s')."<br />".$ordArrListNew."<br>--------".$emailCounter."<br>--------".$emailIds;
            Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$message), function($msg) use ($subject,$to){ 
                            $msg->to($to);   
                            $msg->subject($subject);                                
            });
        }
        echo 'Done';
    }
    public function anyPurchaseOrder($id)
    {
        $user = Auth::guard('admin')->user();   
        require_once(base_path().'/vendor/mpdf/vendor/autoload.php');        
        if($user->role==2){            
            $order = PurchaseOrder::with("PurchaseOrderDetails","Vendor")->where('v_po_number',$id)->where("i_client_id",$user->i_client_id)->first();
                if((!empty($order)) && $order!=''){
                $order = $order->toArray();
            }
        }else if($user->role==3){             
             $order = PurchaseOrder::with("PurchaseOrderDetails","Vendor")->where('v_po_number',$id)->where("i_vendor_id",$user->i_vendor_id)->first();
             if((!empty($order)) && $order!=''){
                $order = $order->toArray();
            }
        }else if($user->role==4){
            $employee_data = Employee::where('id',$user->i_emp_id)->first();            
            $order = PurchaseOrder::with("PurchaseOrderDetails","Vendor")->where('v_po_number',$id)->where("i_vendor_id",$employee_data->i_vendor_id)->first();
            if((!empty($order)) && $order!=''){
                $order = $order->toArray();
            }
        }else{
            $order = PurchaseOrder::with("PurchaseOrderDetails","Vendor")->where('v_po_number',$id)->first();
            if((!empty($order)) && $order!=''){
                $order=$order->toArray();
            }
        }     
        if((!empty($order)) && $order!=''){
                $client = Client::with("Admin")->where('id',$order['i_client_id'])->first()->toArray();
            }        
        if((!empty($order)) && $order!=''){                        
            // return View('admin.purchase_order.purchase_order', array('order' => $order,'client'=>$client, 'title' => 'Purchase Order')); 
            $fedex_val = DB::table('tbl_sitesetting')->select('fedex_flg')->first();
            $fedex_flg = $fedex_val->fedex_flg;
            $return['order'] = $order;
            $return['client'] = $client;
            $return['fedex_flg'] = $fedex_flg;
            $filename = "Purchase_order_".$id."_".date("d_M_Y")."_".time().rand(0,999999);
            require_once(base_path().'/vendor/mpdf/src/Mpdf.php');        
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->SetDisplayMode('default', 'continuous');
            // $mpdf->shrink_tables_to_fit = 0;
            $stylesheet = file_get_contents(ASSET_URL .'css/general_bill.css'); // external css
            $mpdf->WriteHTML($stylesheet,1);
            $stylesheet = file_get_contents(ASSET_URL .'css/layout_bill.css'); // external css
            $mpdf->WriteHTML($stylesheet,1);
            $stylesheet = file_get_contents(ASSET_URL .'css/style_bill.css'); // external css
            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->WriteHTML(\View('admin.purchase_order.purchase_order',$return),2);
            $mpdf->Output($filename.'.pdf','D');            
            

        }else{            
            return Redirect(ADMIN_URL . 'purchase_order');
        }       
    }
    public function anyCronDeletePurchaseOrders(){
        $date = date('Y-m-d', strtotime('-90 days'));        
        $orders = PurchaseOrderDetail::select('id','d_hidden_date','b_hidden','i_purchase_order_id')->where('d_hidden_date','<=',$date)->where('b_hidden',1)->get();        
        $deleted_record_id = '';
        if(count($orders) > 0) {                     
            foreach ($orders as $key => $value) {                
                $OrderDetailId   = $value->id;
                $OrderId   = $value->i_purchase_order_id;        
                $purchase_order_detail_delete_record = PurchaseOrderDetail::where('id',$OrderDetailId)->delete();
                $deleted_record_id .= $OrderDetailId.',';
                $purchase_order_cnt = PurchaseOrderDetail::where('i_purchase_order_id',$OrderId)->count();
                if($purchase_order_cnt <= 0) {
                    $purchase_order_detail_delete_record = PurchaseOrder::where('id',$OrderId)->delete();
                }
            }
        }
        $to = 'testing.demo@gmail.com';
        $subject = 'Run delete  purchase order cron';        
        $admin_email = 'testing.kancy@gmail.com';
        $message = "Successfully run deleted purcashe order cron : ".$deleted_record_id;        
        Mail::send('admin.emails.auth.generate-email-template', array('strTemplate'=>$message), function($msg) use ($subject,$to){ 
                        $msg->to($to);   
                        $msg->subject($subject);                                
        }); 
        echo "Done";
    }
    public function changeFedex($status) {
        $fedex = $status;        
        if($fedex == 'enable') {            
            DB::table('tbl_sitesetting')->update(['fedex_flg' => '1']);            
            echo "fedex enabled.";
        } else if($fedex == 'disable') {
            DB::table('tbl_sitesetting')->update(['fedex_flg' => '0']);            
            echo "fedex disabled.";
        }        
    }
}
