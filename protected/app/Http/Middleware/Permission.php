<?php

namespace App\Http\Middleware;

use Closure,Session,Route,redirect;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use App\Models\Languages;

class Permission
{
    protected $auth;

    public function __construct()
    {
        $this->auth = Auth::guard('admin');
    }

    public function handle($request, Closure $next, $module, $action)
    {          
		if($this->auth->user()->role != '1'){
	
			if(!$this->auth->user()->hasPermission($module, $action)){
				return redirect(ADMIN_URL.'dashboard');	
			}
		}        
		return $next($request);
    }

    

}
