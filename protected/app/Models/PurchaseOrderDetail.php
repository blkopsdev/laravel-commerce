<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class PurchaseOrderDetail extends Model
{
	use SoftDeletes;
    protected $table = 'tbl_purchase_order_details';  	
    protected $dates = ['deleted_at'];
  	public $primaryKey = 'id';  
  	public function PurchaseOrder(){
		  return $this->hasOne('App\Models\PurchaseOrder','id','i_purchase_order_id');
	}
}
