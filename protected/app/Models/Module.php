<?php 
    
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model { 
    protected $table = 'tbl_module';
    protected $primaryKey = 'id';
    
    public function access_level()
    {
         return $this->belongsTo('App\Models\AccessLevel','i_module_id');
    }
}