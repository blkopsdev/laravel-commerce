<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Vendor extends Authenticatable
{
    use SoftDeletes;
	protected $table = 'tbl_vendor';
    protected $dates = ['deleted_at'];
    
    public function Admin(){
		  return $this->hasOne('App\Models\Admin','i_vendor_id','id');
	}
    public function Client(){
        return $this->hasOne('App\Models\Client','id','i_client_id')->select('id','v_company','v_firstname','v_lastname');
    }
    public function Employee(){
		  return $this->hasMany('App\Models\Employee','id','i_vendor_id');
	}
}