<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class TblAdmin extends Authenticatable
{
	protected $table = 'tbladmin';
	protected $primaryKey='iAdminId';
}