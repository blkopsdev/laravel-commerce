<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employee extends Model
{
	use SoftDeletes;
    protected $table = 'tbl_employee';  	
    protected $dates = ['deleted_at'];
  	public $primaryKey = 'id';  
  	public function Vendor(){
		  return $this->hasOne('App\Models\Vendor','id','i_vendor_id')->select('id','v_company_name');
	}	
	public function Admin(){
		  return $this->hasOne('App\Models\Admin','i_emp_id','id');
	   }
	protected static function boot() {
        parent::boot();
        static::deleting(function($user) { // before delete() method call this            
                $user->Admin()->delete();            
        });
    }	
}
