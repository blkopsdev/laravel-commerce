<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class PurchaseOrder extends Model
{
	use SoftDeletes;
    protected $table = 'tbl_purchase_order';  	
    protected $dates = ['deleted_at'];
  	public $primaryKey = 'id';  
  	public function PurchaseOrderDetails(){
		  return $this->hasMany('App\Models\PurchaseOrderDetail','i_purchase_order_id','id');
	}
	public function Vendor(){
		  return $this->hasOne('App\Models\Vendor','id','i_vendor_id');
	}
}
