<?php     
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class UserAccessLevel extends Model { 
    protected $table = 'tbl_user_access_level';
    protected $primaryKey = 'id';
    public $timestamps =false;
	
	public function module() {
        return $this->hasOne('App\Models\Module', 'id', 'i_module_id');
    } 
}