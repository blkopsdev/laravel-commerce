<?php
namespace App\Models;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MailTemplate extends Authenticatable
{
	protected $table = 'tbl_email_templates';    
    protected $hidden = [
        'password', 'remember_token',
    ];
}