<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class PurchaseDataFeed extends Authenticatable
{	
	protected $table = 'tbl_purchase_data_feed';
	protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];
}