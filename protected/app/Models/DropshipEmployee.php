<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class DropshipEmployee extends Model
{
	use SoftDeletes;
    protected $table = 'tbl_dropship_employee';  	
    protected $dates = ['deleted_at'];
  	public $primaryKey = 'id';  
    public function Admin(){
  		  return $this->hasone('App\Models\Admin','i_admin_id','id');
  	 }
  	public function AdminEmp(){
  		  return $this->hasone('App\Models\Admin','i_dropship_emp_id','id');
  	 }
  	protected static function boot() {
          parent::boot();
          static::deleting(function($user) { // before delete() method call this            
                  $user->Admin()->delete();            
          });
    }	
}
