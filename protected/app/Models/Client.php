<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Model
{
	use SoftDeletes;
    protected $table = 'tbl_client';  	
    protected $dates = ['deleted_at'];
  	public $primaryKey = 'id';
  	public function Admin(){
		  return $this->hasOne('App\Models\Admin','i_client_id','id');
	   }
	protected static function boot() {
        parent::boot();
        static::deleting(function($user) { // before delete() method call this            
                $user->Admin()->delete();            
        });
    }
}
