<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class DataFeed extends Authenticatable
{	
	protected $table = 'tbl_datafeed';
	protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];
}