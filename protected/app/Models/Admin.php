<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use SoftDeletes;
	protected $table = 'tbl_admin';
    protected $dates = ['deleted_at'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function Client(){
		  return $this->hasOne('App\Models\Client','id','i_client_id');
	}
	public function Vendor(){
		  return $this->hasOne('App\Models\Vendor','id','i_vendor_id');
	}
	public function Employee(){
		  return $this->hasOne('App\Models\Employee','id','i_emp_id');
	}
	public function DropshipEmployee(){
		  return $this->hasOne('App\Models\DropshipEmployee','id','i_dropship_emp_id');
	}
    public function Admin(){
		  return $this->hasOne('App\Models\DropshipEmployee','id','i_admin_id');
	}
	public function hasPermission($module, $action){
		$permissions = \App\Models\AccessLevel::where('i_role', $this->role)->whereHas('module', function($q1) use($module){
			$q1->where('id', $module);
		})->first();
		if($permissions->e_view==1){
			return true;
		} else {
			return false;
		}
	}
}