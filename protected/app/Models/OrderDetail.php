<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class OrderDetail extends Model
{
	use SoftDeletes;
    protected $table = 'tbl_order_details';  	
    protected $dates = ['deleted_at'];
  	public $primaryKey = 'id';  
  	public function Order(){
		  return $this->hasOne('App\Models\Order','id','i_order_id');
	}
}
