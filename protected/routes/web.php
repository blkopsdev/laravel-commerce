<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::group(array('middleware' => 'guest.admin'),function(){
    Route::any('/',array('uses' => 'AuthenticateController@index'));
    Route::any('login',array('uses' => 'AuthenticateController@index'));
    Route::post('/form-admin-login',array(
        'as' => 'form-admin-login',
        'uses' => 'AuthenticateController@index'
    ));
    Route::post('/form-admin-forgot',array(
        'as' => 'form-admin-forgot',
        'uses' => 'AuthenticateController@forgot_password'
    ));
    //Route::any('/reset-password/{code}',array('as' => 'reset-password','uses' => 'AuthenticateController@reset_password'));
    Route::any('/reset-password/{code}/{status?}',array('as' => 'reset-password','uses' => 'AuthenticateController@reset_password'));
    Route::any('order/cron-delete-orders','OrderController@anyCronDeleteOrders');
    Route::any('order/cron-mailing','OrderController@anyCronMailing');
    Route::any('order/cron-old-orders-mailing','OrderController@anyCronOldOrdersMail');
    Route::any('order/cron-import-feed','OrderController@anyCronForImportFeed');
    Route::any('purchase_order/cron-import-feed','PurchaseOrderController@anyCronForImportFeed');
    Route::any('purchase_order/cron-mailing','PurchaseOrderController@anyCronMailing');
    Route::any('purchase_order/cron-delete-orders','PurchaseOrderController@anyCronDeletePurchaseOrders');
    Route::any('fedex/{status}','PurchaseOrderController@changeFedex');
});
Route::any('hashMake',array('uses' => 'AuthenticateController@hashMake'));
Route::any('vendorEmail',array('uses' => 'AuthenticateController@vendorEmail'));
Route::group(array('middleware' => 'auth.admin'), function() {
    Route::get('/dashboard',array('uses' => 'AuthenticateController@dashboard'));
    Route::match(array('GET', 'POST'), '/my_profile', array('uses' => 'AuthenticateController@my_profile'));
    Route::match(array('GET', 'POST'), '/change_password', array('uses' => 'AuthenticateController@change_password'));
    Route::match(array('GET', 'POST'), '/my_settings', array('uses' => 'AuthenticateController@my_settings'));
    Route::get('logout',array('uses' => 'AuthenticateController@logout'));
    

    //Client
    Route::any('/client','ClientController@getIndex')->middleware('permission:2,e_view');
    Route::any('client/list-ajax','ClientController@anyListAjax')->middleware('permission:2,e_view');
    Route::any('client/add','ClientController@anyAdd')->middleware('permission:2,e_add');
    Route::any('client/edit/{id}','ClientController@anyEdit')->middleware('permission:2,e_edit');
    Route::any('client/delete/{id}','ClientController@getDelete')->middleware('permission:2,e_delete');
    Route::any('client/bulk-action','ClientController@postBulkAction');
    Route::post('client/change-status','ClientController@postChangeStatus');
    Route::any('client/export-to-excel','ClientController@anyExportToExcel');
    Route::any('client/cms/{id}','ClientController@anyEditCms');
    
    //Vendor
    Route::any('/vendor','VendorController@getIndex')->middleware('permission:3,e_view');
    Route::any('vendor/list-ajax','VendorController@anyListAjax')->middleware('permission:3,e_view');
    Route::any('vendor/add','VendorController@anyAdd')->middleware('permission:3,e_add');
    Route::any('vendor/edit/{id}','VendorController@anyEdit')->middleware('permission:3,e_edit');
    Route::any('vendor/delete/{id}','VendorController@getDelete')->middleware('permission:3,e_delete');
    Route::any('vendor/bulk-action','VendorController@postBulkAction');
    Route::post('vendor/change-status','VendorController@postChangeStatus');
    Route::get('vendor/copy','VendorController@postCopy');
    Route::any('vendor/export-to-excel','VendorController@anyExportToExcel');

    //Vendor Employee
    Route::any('/employee','EmployeeController@getIndex')->middleware('permission:4,e_view');
    Route::any('employee/list-ajax','EmployeeController@anyListAjax')->middleware('permission:4,e_view');
    Route::any('employee/add','EmployeeController@anyAdd')->middleware('permission:4,e_add');
    Route::any('employee/edit/{id}','EmployeeController@anyEdit')->middleware('permission:4,e_edit');
    Route::any('employee/delete/{id}','EmployeeController@getDelete')->middleware('permission:4,e_delete');
    Route::any('employee/bulk-action','EmployeeController@postBulkAction');
    Route::post('employee/change-status','EmployeeController@postChangeStatus');
    Route::any('employee/export-to-excel','EmployeeController@anyExportToExcel');

    //Mail Template
    Route::any('/mail-template','MailTemplateController@getIndex');
    Route::any('mail-template/list-ajax','MailTemplateController@anyListAjax');
    Route::any('mail-template/add','MailTemplateController@anyAdd');
    Route::any('mail-template/edit/{id}','MailTemplateController@anyEdit');
    Route::any('mail-template/delete/{id}','MailTemplateController@getDelete');
    Route::any('mail-template/bulk-action','MailTemplateController@postBulkAction');
    Route::post('mail-template/change-status','MailTemplateController@postChangeStatus');

    //Order
    Route::any('/order','OrderController@getIndex')->middleware('permission:1,e_view');
    Route::any('order/list-ajax','OrderController@anyListAjax')->middleware('permission:1,e_view');
    Route::any('order/add','OrderController@anyAdd')->middleware('permission:1,e_add');
    Route::any('order/edit/{id}','OrderController@anyEdit')->middleware('permission:1,e_edit');
    Route::any('order/delete/{id}','OrderController@getDelete')->middleware('permission:1,e_delete');
    Route::any('order/bulk-action','OrderController@postBulkAction');
    Route::post('order/change-status','OrderController@postChangeStatus');
    Route::any('order/export-to-excel','OrderController@anyExportToExcel')->middleware('permission:1,e_export');
    Route::any('order_detail/export-to-excel/{id}','OrderController@anyOrderExportToExcel')->middleware('permission:1,e_export');
    Route::any('order/order-detail/{id}','OrderController@anyOrderDetail')->middleware('permission:1,e_view');
    Route::any('order/edit-order/{id}','OrderController@anyEditOrder')->middleware('permission:1,e_edit');
    Route::any('order/edit-status/{id}','OrderController@anyEditStatus');
    Route::any('order/dropship-order/{id}','OrderController@anyDropshipOrder');
    Route::any('order/bill-slip/{id}','OrderController@anyBillSlip');
    Route::any('order/import-to-excel-order','OrderController@anyImportToExcelOrder');
    Route::any('order/export-data-feed','OrderController@anyExportDataFeed')->middleware('permission:1,e_view');
    Route::any('order/order-details-delete/{id}','OrderController@getOrderDetailsDelete')->middleware('permission:1,e_delete');

    //purchase_order
    Route::any('/purchase_order','PurchaseOrderController@getIndex')->middleware('permission:6,e_view');
    Route::any('purchase_order/list-ajax','PurchaseOrderController@anyListAjax')->middleware('permission:6,e_view');
    Route::any('purchase_order/edit/{id}','PurchaseOrderController@anyEdit')->middleware('permission:6,e_edit');
    Route::any('purchase_order/delete/{id}','PurchaseOrderController@getDelete')->middleware('permission:6,e_delete');
    Route::any('purchase_order/bulk-action','PurchaseOrderController@postBulkAction');
    Route::any('purchase_order/export-data-feed','PurchaseOrderController@anyExportDataFeed')->middleware('permission:6,e_view');
    Route::any('purchase_order_detail/export-to-excel/{id}','PurchaseOrderController@anyOrderExportToExcel')->middleware('permission:6,e_export');
    Route::any('purchase_order/order-detail/{id}','PurchaseOrderController@anyOrderDetail')->middleware('permission:6,e_view');
    Route::any('purchase_order/edit-order/{id}','PurchaseOrderController@anyEditOrder')->middleware('permission:6,e_edit');
    Route::any('purchase_order/edit-status/{id}','PurchaseOrderController@anyEditStatus');
    Route::any('purchase_order/import-to-excel-purchase-order','PurchaseOrderController@anyImportToExcelPurchaseOrder');
    Route::any('purchase_order/purchase-order-pdf/{id}','PurchaseOrderController@anyPurchaseOrder');
    
    Route::any('purchase_order/order-details-delete/{id}','PurchaseOrderController@getOrderDetailsDelete')->middleware('permission:6,e_delete');

    //Dropship Employee
    Route::any('/dropship-employee','DropshipEmployeeController@getIndex')->middleware('permission:5,e_view');
    Route::any('dropship-employee/list-ajax','DropshipEmployeeController@anyListAjax')->middleware('permission:5,e_view');
    Route::any('dropship-employee/add','DropshipEmployeeController@anyAdd')->middleware('permission:5,e_add');
    Route::any('dropship-employee/edit/{id}','DropshipEmployeeController@anyEdit')->middleware('permission:5,e_edit');
    Route::any('dropship-employee/delete/{id}','DropshipEmployeeController@getDelete')->middleware('permission:5,e_delete');
    Route::any('dropship-employee/bulk-action','DropshipEmployeeController@postBulkAction');
    Route::post('dropship-employee/change-status','DropshipEmployeeController@postChangeStatus');
    Route::any('dropship-employee/export-to-excel','DropshipEmployeeController@anyExportToExcel');

    //Role routes
    Route::any('role_access', 'RoleController@getIndex');
    Route::any('role_access/role_data/{id}', 'RoleController@anyRoleData');
    Route::any('role_access/change','RoleController@anyEdit');
    
});
